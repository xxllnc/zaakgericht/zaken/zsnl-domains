# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.exceptions import Forbidden
from typing import Dict, Tuple, TypedDict


class AllowedMimeTypeSetting(TypedDict):
    archivable: bool


OPENXML_BASE = "application/vnd.openxmlformats-officedocument."
ALLOWED_MIME_TYPES: Dict[Tuple[str, str], AllowedMimeTypeSetting] = {
    (".3gp", "audio/3gpp"): {"archivable": False},
    (".7bdat", "application/octet-stream"): {"archivable": True},
    (".7z", "application/octet-stream"): {"archivable": False},
    (".aac", "audio/x-aac"): {"archivable": True},
    (".aac", "audio/x-hx-aac-adts"): {"archivable": True},
    (".accdb", "application/msaccess"): {"archivable": False},
    (".ai", "application/postscript"): {"archivable": True},
    (".ai", "application/pdf"): {"archivable": True},
    (".aif", "audio/x-aiff"): {"archivable": True},
    (".aiff", "audio/x-aiff"): {"archivable": True},
    (".asc", "text/plain"): {"archivable": False},
    (".avi", "video/x-msvideo"): {"archivable": True},
    (".avi", "application/octet-stream"): {"archivable": False},
    (".bmp", "image/bmp"): {"archivable": False},
    (".bmp", "image/x-ms-bmp"): {"archivable": False},
    (".cer", "application/octet-stream"): {"archivable": False},
    (".cer", "application/x-pem-file"): {"archivable": False},
    (".cer", "text/plain"): {"archivable": False},
    (".crt", "application/octet-stream"): {"archivable": False},
    (".crt", "application/x-pem-file"): {"archivable": False},
    (".crt", "text/plain"): {"archivable": False},
    (".csr", "text/plain"): {"archivable": False},
    (".css", "text/plain"): {"archivable": False},
    (".csv", "application/csv"): {"archivable": True},
    (".csv", "text/csv"): {"archivable": True},
    (".csv", "text/plain"): {"archivable": True},
    (".dae", "text/xml"): {"archivable": True},
    (".dae", "model/vnd.collada+xml"): {"archivable": True},
    (".dbf", "application/x-dbf"): {"archivable": True},
    (".dbf", "application/octet-stream"): {"archivable": True},
    (".dcm", "application/dicom"): {"archivable": True},
    (".der", "application/octet-stream"): {"archivable": False},
    (".doc", "application/msword"): {"archivable": True},
    (".doc", "application/CDFV2"): {"archivable": True},
    (".docm", "application/vnd.ms-word.document.macroEnabled.12"): {
        "archivable": True
    },
    (
        ".docx",
        OPENXML_BASE + "wordprocessingml.document",
    ): {"archivable": True},
    (".dot", "application/msword"): {"archivable": False},
    (".dotm", "application/vnd.ms-word.template.macroEnabled.12"): {
        "archivable": False
    },
    (
        ".dotx",
        OPENXML_BASE + "wordprocessingml.template",
    ): {"archivable": False},
    (".dta", "application/octet-stream"): {"archivable": False},
    (".dwg", "application/octet-stream"): {"archivable": True},
    (".dwg", "image/vnd.dwg"): {"archivable": True},
    (".dxf", "text/plain"): {"archivable": True},
    (".dxf", "image/vnd.dxf"): {"archivable": True},
    (".eml", "message/rfc822"): {"archivable": True},
    (".eml", "text/plain"): {"archivable": True},
    (".eps", "application/postscript"): {"archivable": True},
    (".fbx", "application/octet-stream"): {"archivable": True},
    (".flac", "audio/flac"): {"archivable": True},
    (".gif", "image/gif"): {"archivable": False},
    (".gml", "application/gml+xml"): {"archivable": True},
    (".gml", "application/gml"): {"archivable": True},
    (".gml", "application/xml"): {"archivable": True},
    (".gml", "text/xml"): {"archivable": True},
    (".hdf", "application/x-hdf"): {"archivable": True},
    (".heic", "image/heic"): {"archivable": False},
    (".heic", "image/heif"): {"archivable": False},
    (".heif", "image/heic"): {"archivable": False},
    (".heif", "image/heif"): {"archivable": False},
    (".html", "text/html"): {"archivable": True},
    (".html", "text/xml"): {"archivable": True},
    (".htm", "text/html"): {"archivable": True},
    (".indd", "application/octet-stream"): {"archivable": False},
    (".jpeg", "image/jpeg"): {"archivable": True},
    (".jpe", "image/jpeg"): {"archivable": True},
    (".jpg", "image/jpeg"): {"archivable": True},
    (".jp2", "image/jp2"): {"archivable": True},
    (".json", "application/json"): {"archivable": False},
    (".json", "text/plain"): {"archivable": False},
    (".key", "application/x-pem-file"): {"archivable": False},
    (".key", "text/plain"): {"archivable": False},
    (".kml", "application/vnd.google-earth.kml+xml"): {"archivable": True},
    (".kml", "text/xml"): {"archivable": True},
    (".mdb", "application/octet-stream"): {"archivable": False},
    (".mid", "audio/midi"): {"archivable": True},
    (".mid", "application/x-mid"): {"archivable": True},
    (".mid", "text/plain"): {"archivable": True},
    (".mif", "text/plain"): {"archivable": False},
    (".mov", "video/quicktime"): {"archivable": True},
    (".mkv", "video/x-matroska"): {"archivable": True},
    (".mp3", "audio/mpeg"): {"archivable": True},
    (".mp4", "video/mp4"): {"archivable": True},
    (".m4a", "audio/x-m4a"): {"archivable": True},
    (".mpeg", "video/mpeg"): {"archivable": True},
    (".mpg", "video/mpeg"): {"archivable": True},
    (".msg", "application/vnd.ms-outlook"): {"archivable": True},
    (".msg", "application/vnd.ms-office"): {"archivable": True},
    (".msg", "application/CDFV2"): {"archivable": True},
    (".numbers", "application/octet-stream"): {"archivable": False},
    (".obj", "text/plain"): {"archivable": True},
    (".obj", "model/obj"): {"archivable": True},
    (".odf", "application/vnd.oasis.opendocument.text"): {"archivable": True},
    (".odp", "application/vnd.oasis.opendocument.presentation"): {
        "archivable": True
    },
    (".ods", "application/vnd.oasis.opendocument.spreadsheet"): {
        "archivable": True
    },
    (".odt", "application/vnd.oasis.opendocument.text"): {"archivable": True},
    (".ogg", "audio/ogg"): {"archivable": False},
    (".pages", "application/octet-stream"): {"archivable": False},
    (".pdf", "application/pdf"): {"archivable": True},
    (".pem", "application/x-pem-file"): {"archivable": False},
    (".pem", "text/plain"): {"archivable": False},
    (".png", "image/png"): {"archivable": True},
    (".por", "application/octet-stream"): {"archivable": False},
    (".pot", "application/vnd.ms-powerpoint"): {"archivable": False},
    (".potm", "application/vnd.ms-powerpoint.template.macroEnabled.12"): {
        "archivable": False
    },
    (
        ".potx",
        OPENXML_BASE + "presentationml.template",
    ): {"archivable": False},
    (".ppa", "application/vnd.ms-powerpoint"): {"archivable": False},
    (".pps", "application/vnd.ms-powerpoint"): {"archivable": False},
    (".ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"): {
        "archivable": False
    },
    (".ppsx", OPENXML_BASE + "presentationml.slideshow"): {
        "archivable": False
    },
    (".ppt", "application/vnd.ms-powerpoint"): {"archivable": False},
    (".pptm", OPENXML_BASE + "presentation.macroEnabled.12"): {
        "archivable": False
    },
    (".pptx", OPENXML_BASE + "presentationml.presentation"): {
        "archivable": True
    },
    (".psd", "image/vnd.adobe.photoshop"): {"archivable": False},
    (".rtf", "application/rtf"): {"archivable": True},
    (".rtf", "text/rtf"): {"archivable": True},
    (".sas7bdat", "application/octet-stream"): {"archivable": True},
    (".sav", "application/octet-stream"): {"archivable": True},
    (".shp", "application/octet-stream"): {"archivable": True},
    (".sldm", "application/vnd.ms-powerpoint.slide.macroEnabled.12"): {
        "archivable": False
    },
    (".sldx", OPENXML_BASE + "presentationml.slide"): {"archivable": False},
    (".sql", "text/plain"): {"archivable": True},
    (".sql", "application/x-sql"): {"archivable": True},
    (".svg", "image/svg+xml"): {"archivable": True},
    (".tab", "application/octet-stream"): {"archivable": False},
    (".tiff", "image/tiff"): {"archivable": True},
    (".tif", "image/tiff"): {"archivable": True},
    (".txt", "text/plain"): {"archivable": True},
    (".vsdx", "application/octet-stream"): {"archivable": False},
    (".wav", "audio/wav"): {"archivable": True},
    (".webm", "video/webm"): {"archivable": False},
    (".x3d", "model/x3d+xml"): {"archivable": True},
    (".xhtml", "application/xhtml+xml"): {"archivable": True},
    (".xls", "application/vnd.ms-excel"): {"archivable": True},
    (".xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"): {
        "archivable": False
    },
    (".xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12"): {
        "archivable": False
    },
    (".xlsm", "application/octet-stream"): {"archivable": False},
    (
        ".xlsx",
        OPENXML_BASE + "spreadsheetml.sheet",
    ): {"archivable": True},
    (".xlt", "application/vnd.ms-excel"): {"archivable": False},
    (".xltm", "application/vnd.ms-excel.template.macroEnabled.12"): {
        "archivable": False
    },
    (
        ".xltx",
        OPENXML_BASE + "spreadsheetml.template",
    ): {"archivable": False},
    (".xml", "application/xml"): {"archivable": True},
    (".xml", "text/xml"): {"archivable": True},
    (".xps", "application"): {"archivable": False},
    (".xps", "application/vnd.ms-xpsdocument"): {"archivable": False},
    (".zip", "application/octet-stream"): {"archivable": False},
    (".zip", "application/zip"): {"archivable": False},
    (".ztb", "application/octet-stream"): {"archivable": False},
    (".ztb", "application/zip"): {"archivable": False},
}


def is_archivable(extension: str, mimetype: str) -> bool:
    """
    Returns whether a given extension + MIME type combination is considered
    "archivable".

    Raises an exception if the mime type/extension combination is not in
    our whitelist.
    """

    if (extension.lower(), mimetype) not in ALLOWED_MIME_TYPES:
        raise Forbidden(
            f"Unknown file extension or file type ({extension=}, {mimetype=}).",
            "file/type_not_allowed",
        )

    return ALLOWED_MIME_TYPES[(extension.lower(), mimetype)]["archivable"]
