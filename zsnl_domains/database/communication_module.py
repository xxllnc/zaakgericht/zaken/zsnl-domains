# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .base import Base
from .types import GUID, JSONEncodedDict, UTCDateTime
from sqlalchemy.schema import Column, ForeignKey
from sqlalchemy.types import Enum, Integer, String


class Thread(Base):
    """A Thread representation"""

    __tablename__ = "thread"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)

    contact_uuid = Column(GUID, nullable=True)
    contact_displayname = Column(String, nullable=True)

    case_id = Column(Integer, ForeignKey("zaak.id"), nullable=True)

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)

    thread_type = Column(Enum("note", "contact_moment", "external", "postex"))
    last_message_cache = Column(JSONEncodedDict)
    message_count = Column(Integer, nullable=False, default=0)
    unread_pip_count = Column(Integer, nullable=False, default=0)
    unread_employee_count = Column(Integer, nullable=False, default=0)
    attachment_count = Column(Integer, nullable=False, default=0)


class ThreadMessage(Base):
    """A Thread Message representation"""

    __tablename__ = "thread_message"

    id = Column(Integer, primary_key=True)
    uuid = Column(GUID, nullable=False)

    thread_id = Column(Integer, ForeignKey("thread.id"), nullable=False)

    message_slug = Column(String, nullable=False)
    type = Column(Enum("note", "contact_moment", "external", "postex"))

    created_by_uuid = Column(GUID, nullable=True)
    created_by_displayname = Column(String, nullable=True)

    created = Column(UTCDateTime)
    last_modified = Column(UTCDateTime)

    message_date = Column(UTCDateTime, nullable=True)

    thread_message_note_id = Column(
        Integer, ForeignKey("thread_message_note.id"), nullable=True
    )
    thread_message_contact_moment_id = Column(
        Integer, ForeignKey("thread_message_contact_moment.id"), nullable=True
    )
    thread_message_external_id = Column(
        Integer, ForeignKey("thread_message_external.id"), nullable=True
    )


class ThreadMessageNote(Base):
    """A Thread Message Note representation"""

    __tablename__ = "thread_message_note"

    id = Column(Integer, primary_key=True)
    content = Column(String, nullable=False)


class ThreadMessageContactmoment(Base):
    """A Thread Message Contact Moment representation"""

    __tablename__ = "thread_message_contact_moment"

    id = Column(Integer, primary_key=True)
    content = Column(String, nullable=False)
    contact_channel = Column(
        Enum(
            "assignee",
            "frontdesk",
            "phone",
            "mail",
            "email",
            "webform",
            "social_media",
            "external_application",
        )
    )

    direction = Column(Enum("incoming", "outgoing"), nullable=True)
    recipient_uuid = Column(Integer, ForeignKey("subject.id"), nullable=False)
    recipient_displayname = Column(String, nullable=False)


class ThreadMessageExternal(Base):
    """A Thread Message "External Message" representation"""

    __tablename__ = "thread_message_external"

    id = Column(Integer, primary_key=True)
    content = Column(String, nullable=False)
    subject = Column(String, nullable=False)
    participants = Column(JSONEncodedDict, nullable=False)
    type = Column(Enum("pip", "email", "postex"), nullable=True)
    direction = Column(
        Enum("incoming", "outgoing", "unspecified"), nullable=False
    )
    source_file_id = Column(Integer, ForeignKey("filestore.id"), nullable=True)
    read_employee = Column(UTCDateTime, nullable=True)
    read_pip = Column(UTCDateTime, nullable=True)
    attachment_count = Column(Integer, nullable=False, default=0)
    failure_reason = Column(String, nullable=True)


class ThreadMessageAttachment(Base):
    """A Thread Message Attachment representation"""

    __tablename__ = "thread_message_attachment"

    uuid = Column(GUID, nullable=False)
    id = Column(Integer, primary_key=True)

    filestore_id = Column(Integer, ForeignKey("filestore.id"), nullable=False)
    filename = Column(String, nullable=False)
    thread_message_id = Column(
        Integer, ForeignKey("thread_message.id"), nullable=False
    )


class ThreadMessageAttachmentDerivative(Base):
    """A Thread Message Attachment Derivative representation"""

    __tablename__ = "thread_message_attachment_derivative"

    id = Column(Integer, primary_key=True)
    thread_message_attachment_id = Column(
        Integer, ForeignKey("thread_message_attachment.id")
    )
    filestore_id = Column(Integer, ForeignKey("filestore.id"), nullable=False)
    max_width = Column(Integer, nullable=False)
    max_height = Column(Integer, nullable=False)
    date_generated = Column(UTCDateTime, nullable=False)
    type = Column(Enum("pfd", "thumbnail", "docx", "doc"), nullable=False)
