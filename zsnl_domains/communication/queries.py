# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .entities import Case, Contact, ContactMomentOverview, Message
from .repositories import constants
from minty.cqrs import QueryBase
from minty.exceptions import Forbidden, NotFound
from minty.validation import validate_with
from pkgutil import get_data
from pydantic import validate_arguments
from typing import Iterable, List
from uuid import UUID


class Queries(QueryBase):
    """Queries class for communication management."""

    @validate_with(get_data(__name__, "./validation/search_contact.json"))
    def search_contact(
        self, keyword: str, type_filter: Iterable[str]
    ) -> List[Contact]:
        """Retrieve a List with contacts by search keyword

        :param keyword: Keyword to search for in the contacts
        :type keyword: string

        :return: A result set of Contact objects
        :rtype: List[Contact]
        """
        repo = self.get_repository("contact")
        contacts = repo.search_contact(
            keyword=keyword, type_filter=set(type_filter)
        )

        return contacts

    @validate_with(get_data(__name__, "./validation/get_thread_list.json"))
    def get_thread_list(
        self, contact_uuid: UUID, case_uuid, message_types: list
    ):
        """Retrieve a list of threads for a case or a subject by uuid.

        :param contact_uuid: uuid
        :type contact_uuid: UUID
        :param case_uuid: uuid
        :type case_uuid: UUID
        :param message_types: external, note, contact_moment
        :type message_types: str
        :return: list of threads
        :rtype: List[Thread]
        """
        repo = self.get_repository("thread")
        thread_list = repo.get_thread_list(
            contact_uuid=contact_uuid,
            case_uuid=case_uuid,
            message_types=message_types,
            user_uuid=self.user_uuid,
            permission="read",
            user_info=self.user_info,
        )

        return thread_list

    @validate_with(get_data(__name__, "./validation/get_message_list.json"))
    def get_message_list(self, thread_uuid: UUID) -> List[Message]:
        """Return a list of message for the specified thread (uuid).

        :param thread_uuid: thread uuid
        :type thread_uuid: UUID
        :return: list of messages
        :rtype: List[Message]
        """
        case_repo = self.get_repository("case")

        # includes ACL check to see if user has permission
        case = case_repo.find_case_by_thread_uuid(
            thread_uuid=thread_uuid,
            user_uuid=self.user_uuid,
            user_info=self.user_info,
        )

        case_id = case.id if case else None

        message_repo = self.get_repository("message")
        messages = message_repo.get_messages_by_thread_uuid(
            uuid=thread_uuid, user_info=self.user_info, case_id=case_id
        )
        return {"case": case, "messages": messages}

    @validate_with(get_data(__name__, "./validation/search_case.json"))
    def search_cases(
        self, search_term, permission, case_status_filter, limit
    ) -> List[Case]:
        """Retrieve a List with Cases found by searching with a search_term

        :param search_term: Search term to search for cases.
        :type search_term: string
        :param permission: The requested permission for a case.
        :type permission: string
        :param case_status_filter: Filter on case status to use when searching.
            Only cases with the specified statuses will be returned.
        :type include_statuses: Iterable[str]
        :param limit: Maximum number of cases to return.
        :type limit: int or None


        :return: A result set of Case objects
        :rtype: List[Case]
        """

        repo = self.get_repository("case")

        search_args = {
            "search_term": search_term,
            "user_uuid": self.user_uuid,
            "permission": permission,
        }

        if case_status_filter:
            search_args["case_status_filter"] = case_status_filter
        if limit:
            search_args["limit"] = limit

        cases = repo.search_cases(**search_args)

        return cases

    @validate_arguments
    def get_case_list_for_contact(self, contact_uuid: UUID) -> List[Case]:
        """Retrieve a List of Cases for the specified contact that the current
        user has access to

        :return: A result set of Case objects
        :rtype: List[Case]
        """
        is_pip_user = self.user_info.permissions.get("pip_user", False)

        if is_pip_user and self.user_uuid != contact_uuid:
            raise Forbidden(
                "You can only retrieve an allowed case list for yourself",
                "communication/case/not_allowed",
            )

        repo = self.get_repository("case")
        cases = repo.get_case_list_for_contact(
            is_pip_user=is_pip_user,
            user_uuid=self.user_uuid,
            contact_uuid=contact_uuid,
        )

        return cases

    @validate_with(get_data(__name__, "./validation/get_download_link.json"))
    def get_download_link(self, attachment_uuid: UUID):
        is_pip_user = self.user_info.permissions.get("pip_user", False)

        attachment_repo = self.get_repository("attachment")

        file = attachment_repo.get_file_by_uuid(
            attachment_uuid=attachment_uuid,
            user_uuid=self.user_uuid,
            is_pip_user=is_pip_user,
        )
        url = attachment_repo.generate_download_url(file)
        return url

    @validate_with(get_data(__name__, "./validation/get_preview_link.json"))
    def get_preview_link(self, attachment_uuid: UUID):
        is_pip_user = self.user_info.permissions.get("pip_user", False)

        attachment_repo = self.get_repository("attachment")
        file = attachment_repo.get_file_by_uuid(
            attachment_uuid=attachment_uuid,
            user_uuid=self.user_uuid,
            is_pip_user=is_pip_user,
        )

        if (
            file.mimetype == constants.PDF_MIMETYPE
            or file.mimetype in constants.IMAGE_MIMETYPES
        ):
            preview_uuid = file.uuid
            preview_mimetype = file.mimetype
            preview_storage_location = file.storage_location
            preview_filename = file.filename
        else:
            preview_uuid = file.preview_uuid
            preview_mimetype = file.preview_mimetype
            preview_storage_location = file.preview_storage_location
            preview_filename = file.preview_filename

        if preview_uuid is None:
            raise NotFound(
                f"No preview found for attachment with uuid {attachment_uuid}",
                "attachment/preview_not_found",
            )

        preview_url = attachment_repo.generate_preview_url(
            preview_uuid=preview_uuid,
            preview_mimetype=preview_mimetype,
            preview_storage_location=preview_storage_location,
            preview_filename=preview_filename,
        )
        return preview_url

    def get_contact_moment_list(self) -> List[ContactMomentOverview]:
        """Retrieve a List of all the contact moments

        :return: A result set of ContactMomentOverview objects
        :rtype: List[ContactMomentOverview]
        """
        repo = self.get_repository("contact_moment_overview")
        contact_moments = repo.get_contact_moment_list()

        return contact_moments
