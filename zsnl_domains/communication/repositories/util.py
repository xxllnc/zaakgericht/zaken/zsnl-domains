# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import string

BASE62_CHARACTERS = (
    string.digits + string.ascii_uppercase + string.ascii_lowercase
)
BASE62_REVERSE = dict((c, i) for i, c in enumerate(BASE62_CHARACTERS))


def base62_encode(integer: int) -> str:
    if integer == 0:
        return BASE62_CHARACTERS[0]

    length = len(BASE62_CHARACTERS)
    ret = ""
    while integer != 0:
        ret = BASE62_CHARACTERS[integer % length] + ret
        integer = integer // length

    return ret


def base62_decode(string: str) -> int:
    length = len(BASE62_REVERSE)
    ret = 0
    for i, c in enumerate(string[::-1]):
        ret += (length**i) * BASE62_REVERSE[c]

    return ret
