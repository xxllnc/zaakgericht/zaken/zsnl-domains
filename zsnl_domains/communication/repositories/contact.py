# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from ..entities.contact import Contact
from . import database_queries
from minty.exceptions import NotFound
from typing import AbstractSet, List
from uuid import UUID


class ContactRepository(DatabaseRepositoryBase):
    def search_contact(
        self, keyword: str, type_filter: AbstractSet[str]
    ) -> List[Contact]:
        """Retrieve a list of Contacts, given a search keyword.

        If the keyword is None return no result(empty list).

        :param keyword: The search keyword to retrieve contents of the contacts.
        :type keyword: str
        :param type_filter: Filter specifying which types of contact can be returned.
        :type type_filter: Iterable[str]
        :return: A result set of Contact objects
        :rtype: List[Contact]
        """

        keyword = keyword.lower()
        contacts = self.session.execute(
            database_queries.search_contact_query(
                keyword=keyword, type_filter=type_filter
            )
        ).fetchall()

        return [
            self._transform_to_entity(contact_row) for contact_row in contacts
        ]

    def get_contact_by_uuid(self, contact_uuid: UUID):
        """Find a contact from Subject table by a given uuid and Return a Contact entity instance."""
        contact_row = self.session.execute(
            database_queries.contact_query(contact_uuid)
        ).fetchone()
        if not contact_row:
            raise NotFound(
                f"Contact with uuid '{contact_uuid}' not found.",
                "contact/not_found",
            )
        return Contact(
            id=contact_row.id,
            uuid=contact_row.uuid,
            type=contact_row.type,
            name=contact_row.name,
        )

    def get_requestor_contact_from_case(self, case_uuid: UUID) -> Contact:
        """Find a contact of Requestor/Aanvrager from a Case and Return it as Contact entity instance.

        :param case_uuid: the case uuid to get the contact of requestor
        :type case_uuid: UUID
        :return: A contact instance
        :rtype Contact
        """
        contact_uuid = database_queries.requestor_uuid_from_case_query(
            case_uuid
        )
        contact_row = self.session.execute(
            database_queries.contact_query(contact_uuid)
        ).fetchone()

        if not contact_row:
            raise NotFound("Contact not found.", "contact/not_found")

        return Contact(
            id=contact_row.id,
            uuid=contact_row.uuid,
            type=contact_row.type,
            name=contact_row.name,
        )

    def _transform_to_entity(self, contact_row):
        """Create a Contact instance from a database row as returned by the
        contacts queries"""
        address = self._prepare_contact_address(contact_row)
        if address is None and contact_row.abroad_address is not None:
            address = contact_row.abroad_address

        contact_type_map = {
            "natuurlijk_persoon": "person",
            "employee": "employee",
            "bedrijf": "organization",
        }
        return Contact(
            uuid=contact_row.uuid,
            id=contact_row.id,
            type=contact_type_map.get(contact_row.type),
            name=contact_row.name,
            address=address,
            email_address=contact_row.email,
        )

    def _prepare_contact_address(self, contact_row):
        if not contact_row.place:
            return None

        if not contact_row.street_name:
            return f"{contact_row.place}"

        base_address = f"{contact_row.street_name}"
        if contact_row.house_number:
            base_address = f"{base_address} {contact_row.house_number}"

        if contact_row.house_letter:
            base_address = f"{base_address}{contact_row.house_letter}"

        if contact_row.addition:
            base_address = f"{base_address}-{contact_row.addition}"

        return f"{base_address}, {contact_row.place}"

    def get_subject_relation_contacts_from_case(
        self, case_uuid: UUID
    ) -> List[Contact]:
        """Get subject relations (Behandlaar, Aanvrager, Coordinator, Advocaat etc) of a case and return it as list of contact entities.

        :param case_uuid: the case uuid
        :type case_uuid: UUID
        :return: Lis of contacts
        :rtype List[Contact]
        """
        contact_uuids = (
            database_queries.subject_relation_uuids_from_case_query(case_uuid)
        )
        contact_rows = self.session.execute(
            database_queries.contacts_query(contact_uuids)
        ).fetchall()

        return [
            Contact(
                id=contact_row.id,
                uuid=contact_row.uuid,
                type=contact_row.type,
                name=contact_row.name,
            )
            for contact_row in contact_rows
        ]
