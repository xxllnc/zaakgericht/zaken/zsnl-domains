# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ..entities.contact import Contact
from datetime import datetime
from minty.cqrs import event
from minty.entity import EntityBase
from minty.exceptions import Conflict
from typing import Union
from uuid import UUID


class Message(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        thread_uuid: UUID,
        case_uuid: Union[UUID, None],
        message_slug: str,
        message_type: str,
        created_date: datetime,
        last_modified: datetime,
        created_by: Contact,
        created_by_displayname: str,
        message_date: datetime = None,
    ):
        self.uuid = uuid
        self.thread_uuid = thread_uuid
        self.case_uuid = case_uuid
        self.message_slug = message_slug
        self.message_type = message_type
        self.created_date = created_date
        self.last_modified = last_modified
        self.message_date = message_date
        self.created_by = created_by
        self.created_by_displayname = created_by_displayname

    def create(self):
        raise NotImplementedError()

    @event(
        "MessageDeleted",
        extra_fields=["thread_uuid", "case_uuid", "message_type"],
    )
    def delete(self):
        """Deletes the current message and attachments."""
        pass

    @event("MessageRead")
    def mark_read(self, context, timestamp):
        """Mark a message as read. Only mark if it is of type external"""
        if self.message_type != "external":
            message_type = self.message_type
            raise Conflict(
                f"Cannot mark read for {message_type}",
                "message/cannot_mark_read",
            )

    @event("MessageMarkedUnread")
    def mark_unread(self, context):
        """
        Mark a message unread. This method is overloaded in MessageExternal
        for any other message type raise a conflict since only external
        messages can be marked unread.

        :param context: the context, not ne
        :return:
        """
        if self.message_type != "external":
            raise Conflict(
                f"Cannot mark messages unread for type '{self.message_type}'",
                "message/cannot_mark_unread",
            )
