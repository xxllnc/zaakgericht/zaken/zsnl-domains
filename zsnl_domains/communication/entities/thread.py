# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .case import Case
from .contact import Contact
from datetime import datetime, timezone
from minty.cqrs import event
from minty.entity import EntityBase
from minty.exceptions import Conflict
from typing import Optional
from uuid import UUID


class Thread(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        id: int = None,
        thread_type: str = None,
        created=None,
        last_modified=None,
        last_message_cache=None,
        case: Case = None,
        contact: Contact = None,
        number_of_messages: int = 0,
        unread_pip_count: int = None,
        unread_employee_count: int = None,
        attachment_count: int = None,
    ):
        self.uuid = uuid
        self.id = id
        self.thread_type = thread_type
        self.created = created
        self.last_modified = last_modified
        self.last_message_cache = last_message_cache
        self.contact = contact
        self.case = case
        self.contact_displayname = None
        self.number_of_messages = number_of_messages
        self.unread_pip_count = unread_pip_count
        self.unread_employee_count = unread_employee_count
        self.attachment_count = attachment_count

    @event("ThreadCreated")
    def create(
        self,
        thread_type: str,
        contact: Contact,
        case: Optional[Case],
        last_message_cache=None,
    ):
        now = datetime.now(timezone.utc).isoformat()
        self.thread_type = thread_type

        if contact is not None:
            self.contact = contact
            self.contact_displayname = contact.name
        if case is not None:
            self.case = case
        self.last_message_cache = last_message_cache or {}
        self.created = now
        self.last_modified = now
        self.unread_pip_count = 0
        self.unread_employee_count = 0
        self.attachment_count = 0

    @event("ThreadToCaseLinked")
    def link_thread_to_case(self, case: Case, external_message_type: str):
        if self.last_message_cache["message_type"] != external_message_type:
            raise Conflict(
                f"Thread with message type '{self.last_message_cache['message_type']}' "
                f"could not be linked to another message type of '{external_message_type}'",
                "communication/thread/not_linked",
            )

        if self.case is not None:
            raise Conflict(
                f"Thread with uuid '{self.uuid}' could not be linked to case "
                + f"'{case.id}', because it's already linked to a case.",
                "communication/thread/not_linked",
            )

        now = datetime.now(timezone.utc).isoformat()
        self.case = case
        self.uuid = self.uuid
        self.last_message_cache = {"message_type": external_message_type}
        self.last_modified = now

    @event("ThreadDeleted")
    def delete(self):
        """Deletes the current thread"""
        pass

    @event("ThreadUnReadCountDecremented")
    def decrement_unread_count(self, context):
        if context == "pip":
            self.unread_pip_count = self.unread_pip_count - 1
        else:
            self.unread_employee_count = self.unread_employee_count - 1

    @event("ThreadUnReadCountIncremented")
    def increment_unread_count(self, context: Optional[str] = None):

        if context is None:
            self.unread_employee_count = self.unread_employee_count + 1
            self.unread_pip_count = self.unread_pip_count + 1
        elif context == "pip":
            self.unread_pip_count = self.unread_pip_count + 1
        elif context == "employee":
            self.unread_employee_count = self.unread_employee_count + 1

    @event("ThreadAttachmentCountIncremented")
    def increment_attachment_count(self):
        self.attachment_count = self.attachment_count + 1

    @event("ThreadAttachmentCountDecremented")
    def decrement_attachment_count(self):
        self.attachment_count = self.attachment_count - 1
