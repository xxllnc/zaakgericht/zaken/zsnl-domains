# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from uuid import UUID


class File:
    def __init__(
        self,
        uuid: UUID,
        filename: str,
        md5: str,
        size: int,
        mimetype: str,
        date_created,
        storage_location,
    ):
        self.uuid = uuid
        self.filename = filename
        self.md5 = md5
        self.mimetype = mimetype
        self.size = size
        self.date_created = date_created
        self.storage_location = storage_location
