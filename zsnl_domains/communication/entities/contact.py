# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import EntityBase
from uuid import UUID


class Contact(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self,
        uuid: UUID,
        name: str,
        id: int = None,
        type: str = None,
        address: str = None,
        email_address: str = None,
    ):
        self.id = id
        self.uuid = uuid
        self.name = name
        self.type = type
        self.address = address
        self.email_address = email_address
