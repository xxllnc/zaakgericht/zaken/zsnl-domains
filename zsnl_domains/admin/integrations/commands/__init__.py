# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from . import base

COMMANDS = minty.cqrs.build_command_lookup_table(
    commands={
        base.ExchangeCodeForRefreshToken,
        base.SaveEmailIntegrationLoginState,
    }
)
