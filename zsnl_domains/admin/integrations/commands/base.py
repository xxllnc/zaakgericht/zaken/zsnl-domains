# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.exceptions
import requests
from ..repositories import EmailIntegrationRepository
from pydantic import validate_arguments
from typing import cast
from uuid import UUID


class SaveEmailIntegrationLoginState(minty.cqrs.SplitCommandBase):
    name = "save_email_integration_login_state"

    @validate_arguments
    def __call__(
        self,
        integration_uuid: UUID,
        code_verifier: bytes,
        state: bytes,
    ):
        """
        Store the code_verifier and state for the current/most recent login
        session with the integration.
        """

        repo = cast(
            EmailIntegrationRepository,
            self.get_repository("email_integration"),
        )

        integration = repo.get(uuid=integration_uuid)

        integration.save_login_state(code_verifier=code_verifier, state=state)
        repo.save()


class ExchangeCodeForRefreshToken(minty.cqrs.SplitCommandBase):
    name = "exchange_code_for_refresh_token"

    @validate_arguments
    def __call__(self, redirect_uri: str, code: str, state: str):
        """
        Take the authorization code we got from the login process, and exchange
        it for a refresh token.
        """

        repo = cast(
            EmailIntegrationRepository,
            self.get_repository("email_integration"),
        )
        integration = repo.get_by_state(state_param=state)

        (
            retrieve_token_uri,
            retrieve_token_data,
        ) = integration.get_token_request()

        retrieve_token_data["redirect_uri"] = redirect_uri
        retrieve_token_data["code"] = code

        response = requests.post(
            retrieve_token_uri, retrieve_token_data, timeout=30
        )

        response_data = response.json()

        if response.status_code != 200 or "error" in response_data:
            self.logger.info(f"Response: {response_data}")

            error = response_data.get("error", "unknown_error")
            error_description = response_data.get(
                "error_description", "no description found"
            )
            raise minty.exceptions.CQRSException(
                f"{error=}: {error_description=}"
            )

        refresh_token = response_data["refresh_token"]

        integration.save_refresh_token(refresh_token)
        repo.save()
