# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import base64
import hashlib
import json
import minty.exceptions
import secrets
import urllib.parse
from minty.entity import Entity, EntityBase
from pydantic import Field
from typing import Any, Dict, NamedTuple, Tuple
from uuid import UUID


class LoginUrlData(NamedTuple):
    login_url: str
    code_verifier: bytes
    state: bytes


class Integration(EntityBase):
    """Data structure which hold information about an
    integration/interface used by zaaksysteem.
    """

    @property
    def entity_id(self):
        return self.uuid

    def __init__(
        self, id: int, uuid: UUID, name: str, active: bool, module: str
    ):
        self.id = id
        self.uuid = uuid
        self.name = name
        self.active = active
        self.module = module


def _generate_code_challenge():
    """
    Generate a "code challenge" and "code verifier" as described in RFC 7636,
    appendix B

    Returns a tuple of (code_challenge, code_verifier) that should be used in
    the way described in the RFC.

    https://www.rfc-editor.org/rfc/rfc7636#appendix-B
    """

    random_bytes = secrets.token_bytes(nbytes=32)
    code_verifier = base64.urlsafe_b64encode(random_bytes).rstrip(b"=")

    code_verifier_hashed = hashlib.sha256(code_verifier).digest()
    code_challenge = base64.urlsafe_b64encode(code_verifier_hashed).rstrip(
        b"="
    )

    return (code_challenge, code_verifier)


MS_OAUTH_SCOPES = {
    "emailconfiguration": "https://outlook.office.com/SMTP.Send",
    "pop3client": "https://outlook.office.com/POP.AccessAsUser.All",
}


class EmailIntegration(Entity):
    """
    An integration that contains settings for email
    """

    entity_type = "email_integration"

    name: str = Field(..., title="User-configured name of the integration")
    module: str = Field(..., title="Internal name of the integration type")

    user_config: Dict[str, Any] = Field(
        ..., title="User-configured settings of the integration"
    )
    internal_config: Dict[str, Any] = Field(
        ..., title="System-internal settings of the integration"
    )

    def generate_login_url(self, redirect_uri: str) -> LoginUrlData:
        """
        Generate an URL the user can use to log in for the email service.

        Returns a tuple with login URL, code verifier and state.

        The user should be redirected to the login URL to start an OAuth2
        authorization process.

        The code verifier and state need to be stored for use later in the
        authorization process.

        "Code verifier" is used later in the login process by the identity
        service, to make sure it's really us requesting the refresh token.

        "State" is used by us, to make sure a response that we get is related
        to a request we started.

        Currently, this is hardcoded to Microsoft's OAuth2 service, as that
        is the only "cloud" POP3/SMTP providers used by our customers at the
        time of writing.
        """

        if self.user_config["kind"] != "microsoft":
            raise minty.exceptions.Conflict(
                "Cannot generate login URL for an integration that is not configured for OAuth2",
                "email_integration/login/not_supported",
            )

        tenant_id = self.user_config["ms_tenant_id"]

        type_specific_scope = MS_OAUTH_SCOPES[self.module]

        state = base64.urlsafe_b64encode(
            secrets.token_bytes(nbytes=32)
        ).rstrip(b"=")

        full_state = json.dumps(
            {"state": state.decode("ascii"), "uuid": str(self.entity_id)}
        ).encode("ascii")
        encoded_state = base64.urlsafe_b64encode(full_state)

        (code_challenge, code_verifier) = _generate_code_challenge()

        base_url = f"https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/authorize"
        query_parameters = urllib.parse.urlencode(
            {
                "client_id": self.user_config["ms_client_id"],
                "code_challenge_method": "S256",
                "code_challenge": code_challenge,
                "prompt": "select_account",
                "redirect_uri": redirect_uri,
                "response_mode": "query",
                "response_type": "code",
                "scope": f"offline_access {type_specific_scope}",
                "state": encoded_state.decode("ascii"),
            }
        )

        return LoginUrlData(
            f"{base_url}?{query_parameters}", code_verifier, state
        )

    def get_token_request(self) -> Tuple[str, dict]:
        """
        Return the URL and a POST data that can be used to retrieve tokens.

        Caller needs to add `redirect_uri` and `code` (authorization code)
        parameters to the returned parameter list, before making the request.
        """

        tenant_id = self.user_config["ms_tenant_id"]
        code_verifier = base64.b64decode(
            self.internal_config["login_code_verifier"]
        ).decode("ascii")

        base_url = (
            f"https://login.microsoftonline.com/{tenant_id}/oauth2/v2.0/token"
        )

        parameters = {
            "client_id": self.user_config["ms_client_id"],
            "client_secret": self.user_config["ms_client_secret"],
            "code_verifier": code_verifier,
            "grant_type": "authorization_code",
            # optional: scope=
        }

        return (base_url, parameters)

    @Entity.event("LoginStateSaved", fire_always=False)
    def save_login_state(self, code_verifier: bytes, state: bytes):
        """
        Save the generated code_verifier and state for a running OAuth2 login
        process to the integration.
        """

        config = self.internal_config.copy()

        # These values are `bytes, which the JSON serializer for events
        # does not like
        config["login_code_verifier"] = base64.b64encode(code_verifier).decode(
            "ascii"
        )
        config["login_state"] = base64.b64encode(state).decode("ascii")

        self.internal_config = config

    @Entity.event("RefreshTokenSaved", fire_always=False)
    def save_refresh_token(self, refresh_token: str):
        """
        Store the refresh token received during the authorization process.
        """

        config = self.internal_config.copy()
        config["refresh_token"] = refresh_token
        config["login_state"] = None
        config["access_token_failed"] = None

        self.internal_config = config
