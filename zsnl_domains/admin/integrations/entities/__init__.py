# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .integration import EmailIntegration, Integration

__all__ = [
    "EmailIntegration",
    "Integration",
]
