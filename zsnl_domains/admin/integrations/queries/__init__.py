# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ..constants import (
    APPOINTMENT_INTEGRATION_TYPES,
    APPOINTMENT_V2_INTEGRATION_TYPES,
    DOCUMENT_INTEGRATION_TYPES,
)
from ..entities import EmailIntegration, Integration
from ..repositories import EmailIntegrationRepository, IntegrationRepository
from minty.cqrs import QueryBase
from pydantic import validate_arguments
from typing import List, cast
from uuid import UUID


class Queries(QueryBase):
    """Queries class for the integrations"""

    @property
    def integration_repository(self) -> IntegrationRepository:
        repo = cast(
            IntegrationRepository,
            self.repository_factory.get_repository(
                "integration", context=self.context, event_service=None
            ),
        )
        return repo

    @property
    def email_integration_repository(self) -> EmailIntegrationRepository:
        repo = cast(
            EmailIntegrationRepository,
            self.repository_factory.get_repository(
                "email_integration", context=self.context, event_service=None
            ),
        )
        return repo

    @validate_arguments
    def get_email_integration(
        self, integration_uuid: UUID
    ) -> EmailIntegration:
        """
        Retrieve an email integration
        """

        integration = self.email_integration_repository.get(
            uuid=integration_uuid
        )

        return integration

    def get_active_appointment_integrations(self) -> List[Integration]:
        """Get active appointment integrations.

        :return: List of active appointment integrations.
        """

        integrations = self.integration_repository.get_active_integrations(
            integration_types=APPOINTMENT_INTEGRATION_TYPES
        )
        return integrations

    def get_active_appointment_v2_integrations(self) -> List[Integration]:
        """Get active appointment integrations.

        :return: List of active "v2" appointment integrations.
        """

        integrations = self.integration_repository.get_active_integrations(
            integration_types=APPOINTMENT_V2_INTEGRATION_TYPES
        )
        return integrations

    def get_active_integrations_for_document(self) -> List[Integration]:
        """Retrieve a list of active integrations for document templating/creation.

        :return: List of active integrations for document.
        :rtype: list
        """

        document_integrations = (
            self.integration_repository.get_active_integrations(
                integration_types=DOCUMENT_INTEGRATION_TYPES
            )
        )
        return document_integrations
