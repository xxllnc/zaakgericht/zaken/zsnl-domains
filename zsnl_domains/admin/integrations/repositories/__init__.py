# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .email_integration import EmailIntegrationRepository
from .integration import IntegrationRepository

__all__ = [
    "EmailIntegrationRepository",
    "IntegrationRepository",
]
