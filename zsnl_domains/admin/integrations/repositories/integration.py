# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from .... import DatabaseRepositoryBase
from ..entities.integration import Integration
from sqlalchemy import sql
from typing import List, Set
from zsnl_domains.database import schema


class IntegrationRepository(DatabaseRepositoryBase):
    def get_active_integrations(
        self, integration_types: Set[str]
    ) -> List[Integration]:
        """Get a list of active integrations of the specified types."""
        query = sql.select(
            [
                schema.Interface.id,
                schema.Interface.uuid,
                schema.Interface.name,
                schema.Interface.active,
                schema.Interface.module,
            ]
        ).where(
            sql.and_(
                schema.Interface.active.is_(True),
                schema.Interface.module.in_(integration_types),
                schema.Interface.date_deleted.is_(None),
            )
        )
        integrations = self.session.execute(query).fetchall()

        return [
            self._transform_to_entity(integration)
            for integration in integrations
        ]

    def _transform_to_entity(self, query_result):
        """Transform an integration object to Integration entity.

        :param query_result: integration object
        :type query_result: schema.Interface
        :return: Integration entity
        :rtype: Integration
        """

        return Integration(
            id=query_result.id,
            uuid=query_result.uuid,
            name=query_result.name,
            active=query_result.active,
            module=query_result.module,
        )
