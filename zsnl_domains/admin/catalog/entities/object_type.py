# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.cqrs import event
from minty.entity import EntityBase
from uuid import UUID


class ObjectType(EntityBase):
    @property
    def entity_id(self):
        return self.uuid

    def __init__(self, uuid: UUID, name: str):
        self.uuid = uuid
        self.name = name
        self.deleted = None
        self.commit_message = None

    @event("ObjectTypeDeleted", extra_fields=["name"])
    def delete(self, reason: str):
        """Delete a folder.

        :param reason: reason for delete
        :type reason: str
        """

        self.deleted = datetime.now()
        self.commit_message = reason
