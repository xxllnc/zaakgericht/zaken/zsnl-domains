# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


def is_person_secret(secret: str) -> bool:
    """Determine if a person information is secret
    BRP standard: secret is a number 0-7, and 1-6 are no longer in use.
    If this field has a value, and the value is not "0", it indicates
    the person wants their registration to be confidential."""
    is_secret = False
    if secret not in {None, "", " ", "0"}:
        is_secret = True
    return is_secret
