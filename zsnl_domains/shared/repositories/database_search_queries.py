# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
from minty.cqrs import UserInfo
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from sqlalchemy.dialects import postgresql
from typing import Any, Mapping, Protocol
from zsnl_domains.case_management.entities.custom_object import (
    AuthorizationLevel,
)
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import (
    user_allowed_cases_subquery,
)
from zsnl_domains.shared.repositories.object_acl import (
    allowed_object_v1_subquery,
    allowed_object_v2_subquery,
)
from zsnl_domains.shared.util import escape_term_for_like, prepare_search_term

FilterParamsType = Mapping[str, Any]


class SearchTypeCallable(Protocol):
    """Callback protocol for all of the below "per type" search queries.

    This is used for type checking -- all "query" returning functions should
    have the same function signature."""

    def __call__(
        self,
        keyword: str,
        user_info: UserInfo,
        filter_params: FilterParamsType,
    ) -> sql.selectable.Select:  # pragma: no cover
        ...


def case_search_query(
    keyword: str,
    user_info: UserInfo,
    filter_params: FilterParamsType,
) -> sql.selectable.Select:
    """Database search query for cases.

    :param keyword: Keyword to search case.
    """
    base_conditions = [
        user_allowed_cases_subquery(user_info, "search"),
    ]
    search_conditions = []

    # Case id
    try:
        number_keyword = int(keyword)
        search_conditions.append(schema.Case.id == number_keyword)
    except ValueError:
        pass

    # Case Description
    search_conditions.append(schema.Case.onderwerp.ilike(f"%{keyword}%"))

    # Casetype Title
    search_conditions.append(schema.ZaaktypeNode.titel.ilike(f"%{keyword}%"))

    # Assignee display-name
    search_conditions.append(
        sql.cast(schema.Subject.properties, postgresql.JSON)[
            "displayname"
        ].astext.ilike(f"%{keyword}%")
    )

    base_conditions.append(sql.or_(*search_conditions))

    query = (
        sql.select(
            [
                schema.Case.uuid.label("uuid"),
                (
                    sql.cast(schema.Case.id, sqltypes.String)
                    + sql.literal(" (")
                    + sql.case(
                        [
                            (schema.Case.status == "new", "Nieuw"),
                            (schema.Case.status == "open", "In behandeling"),
                            (schema.Case.status == "stalled", "Opgeschort"),
                            (schema.Case.status == "resolved", "Afgehandeld"),
                        ],
                        else_=sql.literal("Onbekend"),
                    )
                    + sql.literal(") - ")
                    + schema.ZaaktypeNode.titel
                ).label("summary"),
                sql.func.concat(
                    sql.func.coalesce(
                        sql.cast(schema.Subject.properties, postgresql.JSON)[
                            "displayname"
                        ].astext.label("name"),
                        "Geen behandelaar",
                    ),
                    " | " + sql.func.coalesce(schema.Case.onderwerp),
                ).label("description"),
                sql.literal("case").label("type"),
                sql.cast(schema.Zaaktype.uuid, sqltypes.String).label(
                    "parent_uuid"
                ),
            ]
        )
        .select_from(
            sql.join(
                schema.Case,
                schema.ZaaktypeNode,
                schema.Case.zaaktype_node_id == schema.ZaaktypeNode.id,
            )
            .join(
                schema.Subject,
                schema.Case.behandelaar_gm_id == schema.Subject.id,
                isouter=True,
            )
            .join(
                schema.Zaaktype,
                schema.Case.zaaktype_id == schema.Zaaktype.id,
            )
        )
        .where(sql.and_(*base_conditions))
        .order_by(sql.desc(schema.Case.id))
    )
    if "relationships.case_type.id" in filter_params:
        case_type_uuid = filter_params["relationships.case_type.id"]
        query = query.where(schema.Zaaktype.uuid == case_type_uuid)

    return query


def custom_object_type_search_query(
    keyword: str,
    user_info: UserInfo,
    filter_params: FilterParamsType,
) -> sql.selectable.Select:
    "Database Search query for custom_object_type."
    return (
        sql.select(
            [
                schema.CustomObjectType.uuid,
                schema.CustomObjectTypeVersion.name.label("summary"),
                sql.literal("").label("description"),
                sql.literal("custom_object_type").label("type"),
                sql.literal(None).label("parent_uuid"),
            ]
        )
        .select_from(
            sql.join(
                schema.CustomObjectType,
                schema.CustomObjectTypeVersion,
                sql.and_(
                    schema.CustomObjectTypeVersion.id
                    == schema.CustomObjectType.custom_object_type_version_id,
                    sql.or_(
                        schema.CustomObjectTypeVersion.date_deleted.is_(None),
                        schema.CustomObjectTypeVersion.date_deleted
                        > datetime.datetime.now(datetime.timezone.utc),
                    ),
                ),
            )
        )
        .where(
            sql.or_(
                schema.CustomObjectTypeVersion.name.ilike(f"%{keyword}%"),
                schema.CustomObjectTypeVersion.title.ilike(f"%{keyword}%"),
            )
        )
        .order_by(sql.desc(schema.CustomObjectTypeVersion.last_modified))
    )


def custom_object_search_query(
    keyword: str, user_info: UserInfo, filter_params: FilterParamsType
) -> sql.selectable.Select:
    "Database Search query for custom_object."

    custom_object = sql.alias(schema.CustomObject)
    query = (
        sql.select(
            [
                custom_object.c.uuid,
                schema.CustomObjectVersion.title.label("summary"),
                sql.func.coalesce(
                    schema.CustomObjectVersion.subtitle, ""
                ).label("description"),
                sql.literal("custom_object").label("type"),
                sql.cast(schema.CustomObjectType.uuid, sqltypes.String).label(
                    "parent_uuid"
                ),
            ]
        )
        .select_from(
            sql.join(
                custom_object,
                schema.CustomObjectVersion,
                sql.and_(
                    schema.CustomObjectVersion.id
                    == custom_object.c.custom_object_version_id,
                    sql.or_(
                        schema.CustomObjectVersion.date_deleted.is_(None),
                        schema.CustomObjectVersion.date_deleted
                        > datetime.datetime.now(datetime.timezone.utc),
                    ),
                ),
            )
            .join(
                schema.CustomObjectTypeVersion,
                schema.CustomObjectTypeVersion.id
                == schema.CustomObjectVersion.custom_object_type_version_id,
            )
            .join(
                schema.CustomObjectType,
                schema.CustomObjectType.id
                == schema.CustomObjectTypeVersion.custom_object_type_id,
            )
        )
        .where(
            sql.and_(
                allowed_object_v2_subquery(
                    user_info=user_info,
                    authorization=AuthorizationLevel.read,
                    object_alias=custom_object,
                ),
                sql.or_(
                    schema.CustomObjectVersion.title.ilike(f"%{keyword}%"),
                    schema.CustomObjectVersion.subtitle.ilike(f"%{keyword}%"),
                    schema.CustomObjectVersion.external_reference.ilike(
                        f"%{keyword}%"
                    ),
                ),
            )
        )
        .order_by(sql.desc(schema.CustomObjectVersion.last_modified))
    )

    if "relationships.custom_object_type.id" in filter_params:
        custom_object_type_uuid = filter_params[
            "relationships.custom_object_type.id"
        ]
        query = query.where(
            schema.CustomObjectType.uuid == custom_object_type_uuid
        )

    if filter_params.get("relationships.custom_object.active") != "false":
        query = query.where(schema.CustomObjectVersion.status == "active")

    return query


def document_search_query(
    keyword: str, user_info: UserInfo, filter_params: FilterParamsType
) -> sql.selectable.Select:
    "Database query to search for documents"

    base_conditions = [
        schema.File.date_deleted.is_(None),
        schema.File.destroyed.is_(False),
        schema.File.active_version.is_(True),
        user_allowed_cases_subquery(user_info, "search"),
    ]
    search_conditions = []

    # Case id (all documents in a specific case)
    try:
        number_keyword = int(keyword)
        search_conditions.append(schema.File.case_id == number_keyword)
    except ValueError:
        pass

    # Filename
    search_conditions.append(
        (schema.File.name + schema.File.extension).ilike(f"%{keyword}%")
    )

    # Description
    search_conditions.append(
        schema.FileMetaData.description.ilike(f"%{keyword}%")
    )

    # Full-text document search
    prepared_search_term = prepare_search_term(search_term=keyword)
    search_conditions.append(
        schema.File.search_index.match(prepared_search_term)
    )

    base_conditions.append(sql.or_(*search_conditions))

    return (
        sql.select(
            [
                schema.File.uuid,
                (schema.File.name + schema.File.extension).label("summary"),
                sql.func.concat(
                    sql.cast(schema.File.case_id, sqltypes.String),
                    " | " + schema.FileMetaData.description,
                ).label("description"),
                sql.literal("document").label("type"),
                sql.cast(schema.Case.uuid, sqltypes.String).label(
                    "parent_uuid"
                ),
            ],
        )
        .select_from(
            sql.join(
                schema.File,
                schema.FileMetaData,
                schema.File.metadata_id == schema.FileMetaData.id,
                isouter=True,
            ).join(schema.Case, schema.Case.id == schema.File.case_id)
        )
        .where(sql.and_(*base_conditions))
        .order_by(sql.desc(schema.File.id))
    )


def object_v1_search_query(
    keyword: str, user_info: UserInfo, filter_params: FilterParamsType
) -> sql.selectable.Select:
    "Database search query for 'old' (v1 style) custom objects"

    class_object_data = sql.alias(schema.ObjectData)
    object_object_data = sql.alias(schema.ObjectData)

    prepared_search_term = prepare_search_term(keyword)

    return (
        sql.select(
            [
                object_object_data.c.uuid.label("uuid"),
                sql.func.coalesce(
                    sql.cast(object_object_data.c.properties, postgresql.JSON)[
                        "label"
                    ].astext,
                    "Geen objecttitel",
                ).label("summary"),
                sql.cast(class_object_data.c.properties, postgresql.JSON)[
                    "values"
                ]["name"]["human_value"].astext.label("description"),
                sql.literal("object_v1").label("type"),
                sql.literal(None).label("parent_uuid"),
            ]
        )
        .select_from(
            sql.join(
                object_object_data,
                class_object_data,
                sql.and_(
                    object_object_data.c.class_uuid
                    == class_object_data.c.uuid,
                    class_object_data.c.object_class == "type",
                ),
            )
        )
        .where(object_object_data.c.text_vector.match(prepared_search_term))
        .where(
            allowed_object_v1_subquery(
                object_alias=object_object_data,
                class_alias=class_object_data,
                user_info=user_info,
            )
        )
        .order_by(sql.desc(object_object_data.c.date_modified))
    )


def saved_search_search_query(
    keyword: str, user_info: UserInfo, filter_params: FilterParamsType
) -> sql.selectable.Select:
    "Database search query for saved queries"

    object_object_data = sql.alias(schema.ObjectData)

    prepared_search_term = prepare_search_term(keyword)

    return (
        sql.select(
            [
                object_object_data.c.uuid.label("uuid"),
                sql.func.coalesce(
                    sql.cast(object_object_data.c.properties, postgresql.JSON)[
                        "label"
                    ].astext,
                    "Geen naam",
                ).label("summary"),
                sql.literal("").label("description"),
                sql.literal("saved_search").label("type"),
                sql.literal(None).label("parent_uuid"),
            ]
        )
        .select_from(object_object_data)
        .where(object_object_data.c.text_vector.match(prepared_search_term))
        .where(
            sql.and_(
                allowed_object_v1_subquery(
                    object_alias=object_object_data,
                    class_alias=None,
                    user_info=user_info,
                ),
                object_object_data.c.object_class == "saved_search",
            )
        )
        .order_by(sql.desc(object_object_data.c.date_modified))
    )


def employee_search_query(
    keyword: str, user_info: UserInfo, filter_params: FilterParamsType
) -> sql.selectable.Select:
    """Database search query for employees."""

    return (
        sql.select(
            [
                schema.Subject.uuid,
                sql.cast(schema.Subject.properties, postgresql.JSON)[
                    "displayname"
                ].astext.label("summary"),
                schema.Group.name.label("description"),
                sql.literal("employee").label("type"),
                sql.literal(None).label("parent_uuid"),
            ]
        )
        .select_from(
            sql.join(
                schema.Subject,
                schema.Group,
                schema.Group.id == schema.Subject.group_ids[1],
            )
        )
        .where(
            sql.and_(
                schema.Subject.subject_type == "employee",
                sql.or_(
                    sql.cast(schema.Subject.properties, postgresql.JSON)[
                        "displayname"
                    ].astext.ilike(f"%{keyword}%"),
                    sql.cast(schema.Subject.properties, postgresql.JSON)[
                        "mail"
                    ].astext.ilike(f"%{keyword}%"),
                ),
                sql.or_(
                    sql.func.array_length(schema.Subject.role_ids, 1).isnot(
                        None
                    ),
                    sql.func.array_length(schema.Subject.role_ids, 1) > 0,
                ),
            )
        )
        .order_by(
            sql.desc(schema.Subject.last_modified), sql.desc(schema.Subject.id)
        )
    )


def organization_search_query(
    keyword: str, user_info: UserInfo, filter_params: FilterParamsType
) -> sql.selectable.Select:
    """Database search query for organizations"""

    # organization search query
    return (
        sql.select(
            [
                schema.Bedrijf.uuid,
                schema.Bedrijf.handelsnaam.label("summary"),
                sql.case(
                    [
                        # Company with a "vestigingsadres" in NL
                        (
                            schema.Bedrijf.vestiging_straatnaam.isnot(None),
                            sql.func.concat(
                                schema.Bedrijf.vestiging_straatnaam,
                                " "
                                + sql.cast(
                                    schema.Bedrijf.vestiging_huisnummer,
                                    sqltypes.String,
                                ),
                                schema.Bedrijf.vestiging_huisletter,
                                "-"
                                + sql.func.nullif(
                                    schema.Bedrijf.vestiging_huisnummertoevoeging,
                                    "",
                                ),
                                ",",
                                " " + schema.Bedrijf.vestiging_postcode,
                                " " + schema.Bedrijf.vestiging_woonplaats,
                            ),
                        ),
                        # Company with a "vestigingsadres" outside NL
                        (
                            schema.Bedrijf.vestiging_adres_buitenland1.isnot(
                                None
                            ),
                            sql.func.concat(
                                schema.Bedrijf.vestiging_adres_buitenland1,
                                ", "
                                + schema.Bedrijf.vestiging_adres_buitenland2,
                                ", "
                                + schema.Bedrijf.vestiging_adres_buitenland3,
                            ),
                        ),
                        # Company with a "correspondentieadres" in NL
                        (
                            schema.Bedrijf.correspondentie_straatnaam.isnot(
                                None
                            ),
                            sql.func.concat(
                                schema.Bedrijf.correspondentie_straatnaam,
                                " "
                                + sql.cast(
                                    schema.Bedrijf.correspondentie_huisnummer,
                                    sqltypes.String,
                                ),
                                schema.Bedrijf.correspondentie_huisletter,
                                "-"
                                + sql.func.nullif(
                                    schema.Bedrijf.correspondentie_huisnummertoevoeging,
                                    "",
                                ),
                                ",",
                                " " + schema.Bedrijf.correspondentie_postcode,
                                " "
                                + schema.Bedrijf.correspondentie_woonplaats,
                            ),
                        ),
                        # Company with a "correspondentieadres" outside NL
                        (
                            schema.Bedrijf.correspondentie_adres_buitenland1.isnot(
                                None
                            ),
                            sql.func.concat(
                                schema.Bedrijf.correspondentie_adres_buitenland1,
                                ", "
                                + schema.Bedrijf.correspondentie_adres_buitenland2,
                                ", "
                                + schema.Bedrijf.correspondentie_adres_buitenland3,
                            ),
                        ),
                    ],
                    else_="Adres onbekend",
                ).label("description"),
                sql.literal("organization").label("type"),
                sql.literal(None).label("parent_uuid"),
            ]
        )
        .where(
            sql.and_(
                schema.Bedrijf.search_term.ilike(f"%{keyword}%"),
                schema.Bedrijf.deleted_on.is_(None),
            )
        )
        .order_by(sql.desc(schema.Bedrijf.id))
    )


def person_search_query(
    keyword: str, user_info: UserInfo, filter_params: FilterParamsType
) -> sql.Select:
    """Database search query for persons"""
    keyword_escaped = "%".join(map(escape_term_for_like, keyword.split()))

    return (
        sql.select(
            [
                schema.NatuurlijkPersoon.uuid,
                sql.func.concat(
                    schema.NatuurlijkPersoon.adellijke_titel + " ",
                    schema.NatuurlijkPersoon.voorletters + " ",
                    schema.NatuurlijkPersoon.surname,
                    " ("
                    + sql.func.to_char(
                        sql.cast(
                            schema.NatuurlijkPersoon.geboortedatum,
                            sqltypes.DateTime,
                        )
                        .op("AT TIME ZONE")("UTC")
                        .op("AT TIME ZONE")("Europe/Amsterdam"),
                        "DD-MM-YYYY",
                    )
                    + ")",
                ).label("summary"),
                sql.func.coalesce(
                    sql.func.concat(
                        schema.Adres.straatnaam
                        + " "
                        + sql.cast(schema.Adres.huisnummer, sqltypes.String),
                        schema.Adres.huisletter,
                        "-" + schema.Adres.huisnummertoevoeging,
                        ", ",
                        schema.Adres.postcode + " ",
                        schema.Adres.woonplaats,
                    ),
                    "Adres onbekend",
                ).label("description"),
                sql.literal("person").label("type"),
                sql.literal(None).label("parent_uuid"),
            ]
        )
        .select_from(
            sql.join(
                schema.NatuurlijkPersoon,
                schema.Adres,
                schema.NatuurlijkPersoon.adres_id == schema.Adres.id,
                isouter=True,
            )
        )
        .where(
            sql.and_(
                schema.NatuurlijkPersoon.search_term.ilike(
                    f"%{keyword_escaped}%"
                ),
                schema.NatuurlijkPersoon.active.is_(True),
                schema.NatuurlijkPersoon.deleted_on.is_(None),
            )
        )
        .order_by(sql.desc(schema.NatuurlijkPersoon.id))
    )


def case_type_search_query(
    keyword: str,
    user_info: UserInfo,
    filter_params: FilterParamsType,
) -> sql.Select:

    """Database search query for case_types"""

    query = (
        sql.select(
            [
                schema.Zaaktype.uuid,
                schema.ZaaktypeNode.titel.label("summary"),
                sql.literal("").label("description"),
                sql.literal("case_type").label("type"),
                sql.literal(None).label("parent_uuid"),
            ]
        )
        .select_from(
            sql.join(
                schema.ZaaktypeNode,
                schema.Zaaktype,
                sql.and_(
                    schema.Zaaktype.zaaktype_node_id == schema.ZaaktypeNode.id,
                    schema.Zaaktype.active.is_(True),
                ),
            )
        )
        .where(
            sql.and_(
                schema.Zaaktype.deleted.is_(None),
                schema.Zaaktype.active.is_(True),
                sql.or_(
                    schema.ZaaktypeNode.titel.ilike(f"%{keyword}%"),
                    schema.ZaaktypeNode.zaaktype_omschrijving.ilike(
                        f"%{keyword}%"
                    ),
                ),
            )
        )
    )

    requestor_type_mapping = {
        "person": ["natuurlijk_persoon", "natuurlijk_persoon_na"],
        "organization": ["niet_natuurlijk_persoon"],
        "employee": ["medewerker"],
    }

    if "relationships.case_type.requestor_type" in filter_params:
        requestor_type = filter_params[
            "relationships.case_type.requestor_type"
        ]

        query = query.where(
            sql.exists(
                sql.select(sql.literal(1))
                .select_from(schema.ZaaktypeBetrokkenen)
                .where(
                    sql.and_(
                        schema.ZaaktypeBetrokkenen.zaaktype_node_id
                        == schema.ZaaktypeNode.id,
                        schema.ZaaktypeBetrokkenen.betrokkene_type.in_(
                            requestor_type_mapping[requestor_type]
                        ),
                    ),
                )
            )
        )

    query = query.order_by(sql.desc(schema.ZaaktypeNode.last_modified))
    return query
