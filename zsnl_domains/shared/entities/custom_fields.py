# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty import entity
from pydantic import Field
from typing import Optional


class CustomFields(entity.Entity):
    entity_type = "custom_field"

    custom_fields: Optional[dict] = Field(
        ..., title="Custom fields for the case"
    )
