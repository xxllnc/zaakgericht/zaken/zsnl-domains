# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty import entity
from pydantic import Field
from typing import List, Optional
from uuid import UUID
from zsnl_domains.shared.custom_field.types import base as typebase


class ObjectFile(entity.ValueObject):
    label: str = Field(..., title="The file name including the extension")
    value: UUID = Field(..., title="The uuid of the file")


class CustomFieldDocument(typebase.CustomFieldValueBase):
    type: str = Field(
        "document", title="A value formatted as a file formatted object"
    )
    value: Optional[List[ObjectFile]] = Field(
        ...,
        title="The value for this field formatted as a file object",
    )
