# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from pydantic import Field
from typing import List, Union
from zsnl_domains.shared.custom_field.types import base as typebase


class CustomFieldTypeSelect(typebase.CustomFieldValueBase):
    type: str = Field("select", title="A value formatted as text")
    value: Union[str, List[str]] = Field(
        ..., title="A list of values for this custom field"
    )

    @classmethod
    def get_validators(cls, custom_field):
        return {"option_validator": cls._get_option_validator(custom_field)}
