# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from ..entities.employee_settings import (
    EmployeeSettings,
    NotificationSettings,
    RelatedEmployee,
    Signature,
)
from minty.cqrs import UserInfo
from minty.cqrs.events import Event
from minty.exceptions import Conflict, NotFound
from minty.repository import Repository
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from sqlalchemy.dialects import postgresql as sa_pg
from uuid import UUID
from zsnl_domains.database import schema


class EmployeeSettingsRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "EmployeeSettings"
    _events_to_calls = {
        "NotificationSettingsSet": "_save_notification_settings",
        "SignatureSaved": "_save_signature",
        "SignatureDeleted": "_delete_signature",
        "PhoneExtensionChanged": "_save_phone_extension",
    }

    def find_by_uuid(
        self,
        uuid: UUID,
        user_info: UserInfo,
    ) -> EmployeeSettings:
        """Find employee settings by uuid."""

        # Show notification_settings only if logged in user has admin rights
        # or logged in user is the requested employee

        hide_notification_settings = not (
            user_info.permissions.get("admin", False)
            or str(user_info.user_uuid) == str(uuid)
        )

        qry_stmt = (
            sql.select(
                [
                    schema.Subject.uuid.label("employee_uuid"),
                    sql.cast(schema.Subject.settings, sa_pg.JSON)[
                        "notifications"
                    ].label("notification_settings"),
                    sql.cast(schema.Subject.settings, sa_pg.JSON)["kcc"][
                        "extension"
                    ].astext.label("phone_extension"),
                    schema.Filestore.uuid.label("signature_uuid"),
                ]
            )
            .select_from(
                sql.join(
                    schema.Subject,
                    schema.Filestore,
                    schema.Filestore.id
                    == sql.cast(schema.Subject.settings, sa_pg.JSON)[
                        "signature_filestore_id"
                    ].astext.cast(sqltypes.Integer),
                    isouter=True,
                )
            )
            .where(
                sql.and_(
                    schema.Subject.uuid == uuid,
                    schema.Subject.subject_type == "employee",
                )
            )
        )

        query_result = self.session.execute(qry_stmt).fetchone()

        if not query_result:
            raise NotFound(
                f"No notification settings found for employee with uuid '{uuid}'",
                "employee_settings/not_found",
            )

        return self._sqla_to_entity(
            query_result=query_result,
            hide_notification_settings=hide_notification_settings,
        )

    def _sqla_to_entity(
        self,
        query_result,
        hide_notification_settings,
    ):
        """Initialize EmployeeSettings Entity from sqla object."""

        if hide_notification_settings:
            notification_settings = None
        else:
            settings_dict = query_result.notification_settings or {
                "new_document": False,
                "new_assigned_case": False,
                "case_term_exceeded": False,
                "new_ext_pip_message": False,
                "new_attribute_proposal": False,
                "case_suspension_term_exceeded": False,
            }

            updated_settings = {
                k: (False if v is None else v)
                for (k, v) in settings_dict.items()
            }
            notification_settings = NotificationSettings(**updated_settings)

        signature = (
            Signature(
                **{
                    "entity_id": query_result.signature_uuid,
                    "uuid": query_result.signature_uuid,
                }
            )
            if query_result.signature_uuid
            else None
        )
        employee = RelatedEmployee(
            **{
                "uuid": query_result.employee_uuid,
                "entity_id": query_result.employee_uuid,
            }
        )

        employee_settings = EmployeeSettings(
            notification_settings=notification_settings,
            signature=signature,
            employee=employee,
            phone_extension=query_result.phone_extension,
            # Services, etc. needed by the entity
            _event_service=self.event_service,
        )

        return employee_settings

    def _save_notification_settings(
        self, event: Event, user_info: UserInfo = None, dry_run: bool = False
    ):
        changes = event.format_changes()
        notification_settings = changes["notification_settings"]
        employee_uuid = event.entity_data["employee"]["uuid"]

        # Get settings for an employee from Subject table
        subject_settings = self.session.execute(
            sql.select([schema.Subject.settings]).where(
                sql.and_(
                    schema.Subject.uuid == employee_uuid,
                    schema.Subject.subject_type == "employee",
                )
            )
        ).fetchone()[0]

        # Set value for notifications in settings
        subject_settings["notifications"] = notification_settings

        # Update value of settings for employee in Subject table
        self.session.execute(
            sql.update(schema.Subject)
            .where(
                sql.and_(
                    schema.Subject.uuid == employee_uuid,
                    schema.Subject.subject_type == "employee",
                )
            )
            .values(settings=subject_settings)
            .execution_options(synchronize_session=False)
        )

    def _save_signature(
        self, event: Event, user_info: UserInfo, dry_run: bool = False
    ):
        changes = event.format_changes()
        signature_filestore_uuid = changes["signature"]["uuid"]
        employee_uuid = event.entity_data["employee"]["uuid"]

        # Set value for signature_filestore_id in settings
        file_store = self.session.execute(
            sql.select([schema.Filestore.id, schema.Filestore.mimetype]).where(
                schema.Filestore.uuid == signature_filestore_uuid
            )
        ).fetchone()

        if not file_store:
            raise NotFound(
                f"File not found with uuid '{signature_filestore_uuid}'",
                "contact/save_signature/file_not_found",
            )

        if file_store.mimetype not in ["image/png", "image/jpeg", "image/gif"]:
            raise Conflict(
                f"File of type '{file_store.mimetype}' is not supported for signature",
                "contact/save_signature/file_not_supported",
            )

        # Update value of settings for employee in Subject table
        self.session.execute(
            sql.update(schema.Subject)
            .where(
                sql.and_(
                    schema.Subject.uuid == employee_uuid,
                    schema.Subject.subject_type == "employee",
                )
            )
            .values(
                settings=sql.cast(schema.Subject.settings, sa_pg.JSONB)
                + sql.cast(
                    {"signature_filestore_id": file_store.id}, sa_pg.JSONB
                )
            )
            .execution_options(synchronize_session=False)
        )

    def _delete_signature(
        self, event: Event, user_info: UserInfo = None, dry_run: bool = False
    ):
        employee_uuid = event.entity_data["employee"]["uuid"]

        # Update value of settings for an employee to set signature as None
        self.session.execute(
            sql.update(schema.Subject)
            .where(
                sql.and_(
                    schema.Subject.uuid == employee_uuid,
                    schema.Subject.subject_type == "employee",
                )
            )
            .values(
                settings=sql.cast(schema.Subject.settings, sa_pg.JSONB)
                + sql.cast({"signature_filestore_id": None}, sa_pg.JSONB)
            )
            .execution_options(synchronize_session=False)
        )

    def _save_phone_extension(
        self, event, user_info: UserInfo, dry_run: bool = False
    ):
        changes = event.format_changes()
        phone_extension = changes["phone_extension"]
        employee_uuid = event.entity_data["employee"]["uuid"]

        self.session.execute(
            sql.update(schema.Subject)
            .where(
                sql.and_(
                    schema.Subject.uuid == employee_uuid,
                    schema.Subject.subject_type == "employee",
                )
            )
            .values(
                settings=sql.cast(schema.Subject.settings, sa_pg.JSONB)
                + sql.cast(
                    {"kcc": {"extension": phone_extension}}, sa_pg.JSONB
                )
            )
            .execution_options(synchronize_session=False)
        )
