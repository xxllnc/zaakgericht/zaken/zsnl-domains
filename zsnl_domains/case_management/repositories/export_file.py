# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from .. import entities
from ..entities import ExportFile
from datetime import datetime, timezone
from minty.repository import Repository
from sqlalchemy import sql
from typing import Dict, List
from uuid import UUID
from zsnl_domains.database import schema

export_file_query = sql.select(
    [
        schema.ExportQueue.uuid.label("uuid"),
        schema.ExportQueue.subject_uuid.label("subject_uuid"),
        schema.ExportQueue.expires.label("expires"),
        schema.ExportQueue.downloaded.label("downloaded"),
        schema.ExportQueue.filestore_uuid.label("filestore_uuid"),
        schema.Filestore.original_name.label("original_name"),
    ]
).select_from(
    sql.join(
        schema.ExportQueue,
        schema.Filestore,
        schema.ExportQueue.filestore_id == schema.Filestore.id,
    )
)

DEFAULT_SORT_ORDER = [sql.asc(schema.ExportQueue.expires)]


class ExportFileRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "ExportFile"
    _events_to_calls: Dict[str, str] = {}

    def get_export_file_list(
        self, page: int, page_size: int, user_uuid: UUID
    ) -> List[ExportFile]:
        """Get list of Export Files.
        :return: List[ExportFile]
        :rtype: list
        """

        offset = self._calculate_offset(page, page_size)

        # Get only user specific files
        query = export_file_query.where(
            sql.and_(
                schema.ExportQueue.subject_uuid == user_uuid,
                schema.ExportQueue.expires > datetime.now(timezone.utc),
            )
        )

        export_files = self.session.execute(
            query.order_by(*DEFAULT_SORT_ORDER).limit(page_size).offset(offset)
        ).fetchall()

        return [
            self._entity_from_row(row=export_files_row)
            for export_files_row in export_files
        ]

    def _entity_from_row(self, row) -> ExportFile:

        export_file = entities.ExportFile(
            uuid=row.uuid,
            entity_id=row.filestore_uuid,
            name=row.original_name,
            expires_at=row.expires,
            downloads=row.downloaded,
        )
        return export_file
