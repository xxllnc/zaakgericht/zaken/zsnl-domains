# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import datetime
from ... import DatabaseRepositoryBase
from ...shared.types import ComparisonFilterCondition
from minty.cqrs import UserInfo
from minty.entity import EntityCollection
from minty.repository import Repository
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from sqlalchemy.engine.row import Row
from sqlalchemy.sql import selectable
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import (
    case_search_result,
    total_result_count,
)
from zsnl_domains.database import schema
from zsnl_domains.shared.entities.case import ValidCaseUrgency
from zsnl_domains.shared.repositories import case_acl
from zsnl_domains.shared.util import escape_term_for_like, prepare_search_term

CASE_ALIAS = sql.alias(schema.Case)
ZAAK_BETROKKENEN_ASSIGNEE_ALIAS = sql.alias(schema.ZaakBetrokkenen)
ZAAK_BETROKKENEN_COORDINATOR_ALIAS = sql.alias(schema.ZaakBetrokkenen)
ZAAK_BETROKKENEN_REQUESTOR_ALIAS = sql.alias(schema.ZaakBetrokkenen)
ZAAK_ZAAKTYPE_RESULTATEN = sql.alias(schema.ZaaktypeResultaten)
ZAAK_BAG = sql.alias(schema.ZaakBag)


SORT_ORDER = {
    case_search_result.CaseSearchOrder.number_asc: [
        sql.asc(CASE_ALIAS.c.id),
    ],
    case_search_result.CaseSearchOrder.number_desc: [
        sql.desc(CASE_ALIAS.c.id)
    ],
    case_search_result.CaseSearchOrder.unread_message_count_asc: [
        sql.asc(schema.CaseMeta.unread_communication_count),
        sql.desc(CASE_ALIAS.c.id),
    ],
    case_search_result.CaseSearchOrder.unread_message_count_desc: [
        sql.desc(schema.CaseMeta.unread_communication_count),
        sql.desc(CASE_ALIAS.c.id),
    ],
    case_search_result.CaseSearchOrder.unaccepted_files_count_asc: [
        sql.asc(schema.CaseMeta.unaccepted_files_count),
        sql.desc(CASE_ALIAS.c.id),
    ],
    case_search_result.CaseSearchOrder.unaccepted_files_count_desc: [
        sql.desc(schema.CaseMeta.unaccepted_files_count),
        sql.desc(CASE_ALIAS.c.id),
    ],
    case_search_result.CaseSearchOrder.unaccepted_attribute_update_count_asc: [
        sql.asc(schema.CaseMeta.unaccepted_attribute_update_count),
        sql.desc(CASE_ALIAS.c.id),
    ],
    case_search_result.CaseSearchOrder.unaccepted_attribute_update_count_desc: [
        sql.desc(schema.CaseMeta.unaccepted_attribute_update_count),
        sql.desc(CASE_ALIAS.c.id),
    ],
    case_search_result.CaseSearchOrder.registration_date_asc: [
        sql.asc(CASE_ALIAS.c.registratiedatum),
        sql.desc(CASE_ALIAS.c.id),
    ],
    case_search_result.CaseSearchOrder.registration_date_desc: [
        sql.desc(CASE_ALIAS.c.registratiedatum),
        sql.desc(CASE_ALIAS.c.id),
    ],
    case_search_result.CaseSearchOrder.completion_date_asc: [
        sql.asc(CASE_ALIAS.c.afhandeldatum),
        sql.desc(CASE_ALIAS.c.id),
    ],
    case_search_result.CaseSearchOrder.completion_date_desc: [
        sql.desc(CASE_ALIAS.c.afhandeldatum),
        sql.desc(CASE_ALIAS.c.id),
    ],
}


class CaseSearchResultRepository(Repository, DatabaseRepositoryBase):
    def _get_repo(self, domain, repo_name):
        repo = domain.REQUIRED_REPOSITORIES[repo_name]
        return repo(
            infrastructure_factory=self.infrastructure_factory,
            context=self.context,
            event_service=self.event_service,
        )

    custom_fields_query = sql.func.array(
        sql.select(
            [
                sql.func.json_build_object(
                    "name",
                    schema.BibliotheekKenmerk.naam,
                    "magic_string",
                    schema.BibliotheekKenmerk.magic_string,
                    "value",
                    schema.ZaakKenmerk.value,
                    "type",
                    schema.BibliotheekKenmerk.value_type,
                    "is_multiple",
                    schema.BibliotheekKenmerk.type_multiple,
                )
            ]
        )
        .select_from(
            sql.join(
                schema.ZaakKenmerk,
                schema.BibliotheekKenmerk,
                schema.ZaakKenmerk.bibliotheek_kenmerken_id
                == schema.BibliotheekKenmerk.id,
            )
        )
        .where(schema.ZaakKenmerk.zaak_id == CASE_ALIAS.c.id)
        .scalar_subquery()
    ).label("custom_fields")

    file_custom_fields_query = sql.func.array(
        sql.select(
            [
                sql.func.json_build_object(
                    "name",
                    schema.BibliotheekKenmerk.naam,
                    "magic_string",
                    schema.BibliotheekKenmerk.magic_string,
                    "type",
                    schema.BibliotheekKenmerk.value_type,
                    "value",
                    sql.func.array(
                        sql.select(
                            sql.func.json_build_object(
                                "md5",
                                schema.Filestore.md5,
                                "size",
                                schema.Filestore.size,
                                "uuid",
                                schema.Filestore.uuid,
                                "filename",
                                schema.File.name + schema.File.extension,
                                "mimetype",
                                schema.Filestore.mimetype,
                                "is_archivable",
                                schema.Filestore.is_archivable,
                                "original_name",
                                schema.Filestore.original_name,
                                "thumbnail_uuid",
                                schema.Filestore.thumbnail_uuid,
                            ),
                        )
                        .select_from(
                            sql.join(
                                schema.FileCaseDocument,
                                schema.File,
                                sql.and_(
                                    schema.FileCaseDocument.file_id
                                    == schema.File.id,
                                    schema.FileCaseDocument.case_id
                                    == CASE_ALIAS.c.id,
                                ),
                            ).join(
                                schema.Filestore,
                                schema.File.filestore_id
                                == schema.Filestore.id,
                            )
                        )
                        .where(
                            schema.FileCaseDocument.bibliotheek_kenmerken_id
                            == schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id,
                        )
                        .scalar_subquery()
                    ),
                )
            ]
        )
        .select_from(
            sql.join(
                schema.ZaaktypeKenmerk,
                schema.BibliotheekKenmerk,
                sql.and_(
                    schema.ZaaktypeKenmerk.bibliotheek_kenmerken_id
                    == schema.BibliotheekKenmerk.id,
                    schema.BibliotheekKenmerk.value_type == "file",
                ),
            ).join(
                schema.ZaaktypeNode,
                schema.ZaaktypeNode.id
                == schema.ZaaktypeKenmerk.zaaktype_node_id,
            )
        )
        .where(
            schema.ZaaktypeNode.id == CASE_ALIAS.c.zaaktype_node_id,
        )
        .scalar_subquery()
    ).label("file_custom_fields")

    _COUNT_PHASES_QUERY = (
        sql.select([sql.func.count(schema.ZaaktypeStatus.id)])
        .where(
            schema.ZaaktypeStatus.zaaktype_node_id == schema.ZaaktypeNode.id
        )
        .scalar_subquery()
        .label("count_phases")
    )

    _CASE_SEACH_BASE_QUERY = sql.select(
        [
            CASE_ALIAS.c.id,
            CASE_ALIAS.c.uuid,
            CASE_ALIAS.c.status,
            CASE_ALIAS.c.vernietigingsdatum,
            CASE_ALIAS.c.archival_state,
            CASE_ALIAS.c.registratiedatum,
            CASE_ALIAS.c.afhandeldatum,
            sql.func.get_date_progress_from_case(
                CASE_ALIAS.c.afhandeldatum,
                CASE_ALIAS.c.streefafhandeldatum,
                CASE_ALIAS.c.registratiedatum,
                CASE_ALIAS.c.status,
            ).label("percentage_days_left"),
            CASE_ALIAS.c.onderwerp,
            (
                sql.cast(CASE_ALIAS.c.milestone, sqltypes.Float)
                / _COUNT_PHASES_QUERY
            ).label("progress"),
            schema.ZaaktypeNode.titel,
            schema.CaseMeta.unaccepted_files_count,
            schema.CaseMeta.unread_communication_count,
            schema.CaseMeta.unaccepted_attribute_update_count,
            CASE_ALIAS.c.requestor_v1_json["preview"].label("requestor_name"),
            CASE_ALIAS.c.requestor_v1_json["instance"]["subject"][
                "type"
            ].label("requestor_type"),
            CASE_ALIAS.c.requestor_v1_json["reference"].label(
                "requestor_uuid"
            ),
            CASE_ALIAS.c.assignee_v1_json["preview"].label("assignee_name"),
            CASE_ALIAS.c.assignee_v1_json["reference"].label("assignee_uuid"),
            CASE_ALIAS.c.coordinator_v1_json["preview"].label(
                "coordinator_name"
            ),
            CASE_ALIAS.c.coordinator_v1_json["reference"].label(
                "coordinator_uuid"
            ),
            custom_fields_query,
            file_custom_fields_query,
        ]
    ).select_from(
        sql.join(
            CASE_ALIAS,
            schema.ZaaktypeNode,
            CASE_ALIAS.c.zaaktype_node_id == schema.ZaaktypeNode.id,
        )
        .join(
            schema.CaseMeta,
            CASE_ALIAS.c.id == schema.CaseMeta.zaak_id,
        )
        .join(schema.Zaaktype, CASE_ALIAS.c.zaaktype_id == schema.Zaaktype.id)
        .join(
            ZAAK_BETROKKENEN_ASSIGNEE_ALIAS,
            ZAAK_BETROKKENEN_ASSIGNEE_ALIAS.c.id == CASE_ALIAS.c.behandelaar,
            isouter=True,
        )
        .join(
            ZAAK_BETROKKENEN_COORDINATOR_ALIAS,
            ZAAK_BETROKKENEN_COORDINATOR_ALIAS.c.id
            == CASE_ALIAS.c.coordinator,
            isouter=True,
        )
        .join(
            ZAAK_BETROKKENEN_REQUESTOR_ALIAS,
            ZAAK_BETROKKENEN_REQUESTOR_ALIAS.c.id == CASE_ALIAS.c.aanvrager,
        )
        .join(
            ZAAK_ZAAKTYPE_RESULTATEN,
            ZAAK_ZAAKTYPE_RESULTATEN.c.id == CASE_ALIAS.c.resultaat_id,
            isouter=True,
        )
        .join(
            ZAAK_BAG,
            ZAAK_BAG.c.id == CASE_ALIAS.c.locatie_zaak,
            isouter=True,
        )
    )

    _for_entity = "CaseSearchResult"

    def search(
        self,
        user_info: UserInfo,
        permission: str,
        page: int,
        page_size: int,
        filters: dict,
        sort: case_search_result.CaseSearchOrder,
    ) -> EntityCollection[case_search_result.CaseSearchResult]:
        """Return a EntityCollection of the CaseSearchResult entity based on the search parameters.
        :return: EntityCollection of CaseSearchResult
        """
        offset = self._calculate_offset(page, page_size)

        qry_stmt = self._get_search_query(
            case_alias=CASE_ALIAS,
            user_info=user_info,
            permission=permission,
            filters=filters,
        )
        sort_order = SORT_ORDER[sort]
        qry_stmt = (
            qry_stmt.order_by(*sort_order).limit(page_size).offset(offset)
        )

        case_rows = self.session.execute(qry_stmt).fetchall()
        entities = [self._entity_from_row(row) for row in case_rows]

        return EntityCollection(entities=entities)

    def search_total_results(
        self,
        user_info: UserInfo,
        permission: str,
        filters: dict,
    ):
        """Get total count of Case based on given filter."""
        qry_stmt = self._get_search_query(
            case_alias=CASE_ALIAS,
            user_info=user_info,
            permission=permission,
            filters=filters,
        )
        total_results = total_result_count.TotalResultCount(
            total_results=self._get_count(qry_stmt)
        )

        return total_results

    def _get_search_query(
        self,
        case_alias: selectable.Alias,
        user_info: UserInfo,
        permission: str,
        filters: dict,
    ):
        case_search_query = self._CASE_SEACH_BASE_QUERY.where(
            case_acl.user_allowed_cases_subquery(
                user_info, permission, case_alias=case_alias.c
            )
        )

        # Update filters dict with only requested filters(Remove None)
        updated_filters = {k: v for k, v in filters.items() if v is not None}
        filters.clear()
        filters.update(updated_filters)

        for filter in filters:
            case_search_query = FILTERS_MAPPING[filter](
                self, case_search_query, filters[filter]
            )
        return case_search_query

    def _apply_filter_status(self, case_search_query, filter):
        return case_search_query.where(CASE_ALIAS.c.status.in_(filter))

    def _apply_filter_case_type_uuids(self, case_search_query, filter):
        return case_search_query.where(schema.Zaaktype.uuid.in_(filter))

    def _apply_filter_requestor_uuids(self, case_search_query, filter):
        return case_search_query.where(
            ZAAK_BETROKKENEN_REQUESTOR_ALIAS.c.subject_id.in_(filter)
        )

    def _apply_filter_assignee_uuids(self, case_search_query, filter):
        return case_search_query.where(
            ZAAK_BETROKKENEN_ASSIGNEE_ALIAS.c.subject_id.in_(filter)
        )

    def _apply_filter_coordinator_uuids(self, case_search_query, filter):
        return case_search_query.where(
            ZAAK_BETROKKENEN_COORDINATOR_ALIAS.c.subject_id.in_(filter)
        )

    def _apply_filter_registration_date(self, case_search_query, filter):
        return self.apply_comparison_filter(
            query=case_search_query,
            column=CASE_ALIAS.c.registratiedatum,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
        )

    def _apply_filter_completion_date(self, case_search_query, filter):
        return self.apply_comparison_filter(
            query=case_search_query,
            column=CASE_ALIAS.c.afhandeldatum,
            comparison_filters=[
                ComparisonFilterCondition[datetime.datetime].from_str(
                    filter_value
                )
                for filter_value in filter
            ],
        )

    def _apply_filter_payment_status(self, case_search_query, filter):
        return case_search_query.where(CASE_ALIAS.c.payment_status.in_(filter))

    def _apply_filter_channel_of_contact(self, case_search_query, filter):
        return case_search_query.where(CASE_ALIAS.c.contactkanaal.in_(filter))

    def _apply_filter_confidentiality(self, case_search_query, filter):
        return case_search_query.where(
            CASE_ALIAS.c.confidentiality.in_(filter)
        )

    def _apply_filter_archival_state(self, case_search_query, filter):
        return case_search_query.where(CASE_ALIAS.c.archival_state.in_(filter))

    def _apply_filter_retention_period_source_date(
        self, case_search_query, filter
    ):
        return case_search_query.where(
            ZAAK_ZAAKTYPE_RESULTATEN.c.ingang.in_(filter)
        )

    def _apply_filter_case_location(self, case_search_query, filter):
        location_ids = list(filter)
        nummeraanduiding_bag_ids = openbareruimte_bag_ids = []
        for id in location_ids:
            if "nummeraanduiding-" in id:
                nummeraanduiding_bag_ids.append(
                    id.lower().replace("nummeraanduiding-", "")
                )
            elif "openbareruimte-" in id:
                openbareruimte_bag_ids.append(
                    id.lower().replace("openbareruimte-", "")
                )

        case_search_query = case_search_query.where(
            sql.or_(
                sql.and_(
                    ZAAK_BAG.c.bag_id.in_(nummeraanduiding_bag_ids),
                    ZAAK_BAG.c.bag_type == "nummeraanduiding",
                ),
                sql.and_(
                    ZAAK_BAG.c.bag_id.in_(openbareruimte_bag_ids),
                    ZAAK_BAG.c.bag_type == "openbareruimte",
                ),
            )
        )

        return case_search_query

    def _apply_filter_num_unread_messages(self, case_search_query, filter):
        return self.apply_comparison_filter(
            query=case_search_query,
            column=schema.CaseMeta.unread_communication_count,
            comparison_filters=[
                ComparisonFilterCondition[int].from_str(filter_value)
                for filter_value in filter
            ],
        )

    def _apply_filter_num_unaccepted_files(self, case_search_query, filter):
        return self.apply_comparison_filter(
            query=case_search_query,
            column=schema.CaseMeta.unaccepted_files_count,
            comparison_filters=[
                ComparisonFilterCondition[int].from_str(filter_value)
                for filter_value in filter
            ],
        )

    def _apply_filter_num_unaccepted_updates(self, case_search_query, filter):
        return self.apply_comparison_filter(
            query=case_search_query,
            column=schema.CaseMeta.unaccepted_attribute_update_count,
            comparison_filters=[
                ComparisonFilterCondition[int].from_str(filter_value)
                for filter_value in filter
            ],
        )

    def _apply_filter_result(self, case_search_query, filter):
        return case_search_query.where(CASE_ALIAS.c.resultaat.in_(filter))

    def _apply_filter_keyword(self, case_search_query, filter):
        prepared_search_term = prepare_search_term(search_term=filter)
        return case_search_query.where(
            sql.and_(
                CASE_ALIAS.c.id == schema.CaseMeta.zaak_id,
                schema.CaseMeta.text_vector.match(prepared_search_term),
            )
        )

    def _apply_filter_period_of_preservation_active(
        self, case_search_query, filter
    ):
        return case_search_query.where(
            sql.and_(ZAAK_ZAAKTYPE_RESULTATEN.c.trigger_archival.is_(filter))
        )

    def _apply_filter_subject(self, case_search_query, filter):
        escaped_keyword = escape_term_for_like(filter)
        return case_search_query.where(
            CASE_ALIAS.c.onderwerp.ilike(f"%{escaped_keyword}%", escape="~"),
        )

    def _apply_filter_urgency(self, case_search_query, filter):
        urgency_query_conditions = {
            ValidCaseUrgency.normal: sql.func.coalesce(
                CASE_ALIAS.c.afhandeldatum, sql.func.now()
            )
            <= CASE_ALIAS.c.urgency_date_medium,
            ValidCaseUrgency.high: sql.and_(
                sql.func.coalesce(CASE_ALIAS.c.afhandeldatum, sql.func.now())
                >= CASE_ALIAS.c.urgency_date_high,
                sql.func.coalesce(CASE_ALIAS.c.afhandeldatum, sql.func.now())
                < CASE_ALIAS.c.streefafhandeldatum,
            ),
            ValidCaseUrgency.late: sql.func.coalesce(
                CASE_ALIAS.c.afhandeldatum, sql.func.now()
            )
            >= CASE_ALIAS.c.streefafhandeldatum,
            ValidCaseUrgency.medium: sql.and_(
                sql.func.coalesce(CASE_ALIAS.c.afhandeldatum, sql.func.now())
                > CASE_ALIAS.c.urgency_date_medium,
                sql.func.coalesce(CASE_ALIAS.c.afhandeldatum, sql.func.now())
                < CASE_ALIAS.c.urgency_date_high,
            ),
        }
        urgencies_list = {"normal", "high", "late", "medium"}

        if len(urgencies_list.difference(filter)) > 0:
            needed_filters = urgencies_list.intersection(filter)
            case_search_query = case_search_query.where(
                sql.or_(
                    urgency_query_conditions[item] for item in needed_filters
                )
            )
            case_search_query = case_search_query.where(
                CASE_ALIAS.c.status != "stalled"
            )
        return case_search_query

    def _entity_from_row(
        self, row: Row
    ) -> case_search_result.CaseSearchResult:
        """Initialize CaseSearchResult Entity from a database row"""

        contact_type_matpping = {
            "person": "person",
            "employee": "employee",
            "company": "organization",
        }
        requestor = {
            "entity_id": row.requestor_uuid,
            "entity_type": contact_type_matpping[row.requestor_type],
            "uuid": row.requestor_uuid,
            "type": contact_type_matpping[row.requestor_type],
            "name": row.requestor_name,
        }
        assignee = (
            None
            if not row.assignee_uuid
            else {
                "entity_id": row.assignee_uuid,
                "uuid": row.assignee_uuid,
                "name": row.assignee_name,
                "type": "employee",
            }
        )
        coordinator = (
            None
            if not row.coordinator_uuid
            else {
                "entity_id": row.coordinator_uuid,
                "uuid": row.coordinator_uuid,
                "name": row.coordinator_name,
                "type": "employee",
            }
        )
        custom_fields_repo = self._get_repo(case_management, "custom_fields")
        return case_search_result.CaseSearchResult(
            entity_id=row.uuid,
            uuid=row.uuid,
            number=row.id,
            status=row.status,
            destruction_date=row.vernietigingsdatum,
            archival_state=row.archival_state,
            case_type_title=row.titel,
            subject=row.onderwerp,
            percentage_days_left=row.percentage_days_left,
            progress=row.progress * 100,
            unread_message_count=row.unread_communication_count,
            unaccepted_files_count=row.unaccepted_files_count,
            unaccepted_attribute_update_count=row.unaccepted_attribute_update_count,
            requestor=requestor,
            assignee=assignee,
            coordinator=coordinator,
            registration_date=row.registratiedatum,
            completion_date=row.afhandeldatum,
            custom_fields=custom_fields_repo.format_custom_fields_for_case_basic(
                custom_fields=row.custom_fields,
                file_custom_fields=row.file_custom_fields,
            ).custom_fields,
        )


FILTERS_MAPPING = {
    "filter_status": CaseSearchResultRepository._apply_filter_status,
    "case_type_uuids": CaseSearchResultRepository._apply_filter_case_type_uuids,
    "requestor_uuids": CaseSearchResultRepository._apply_filter_requestor_uuids,
    "assignee_uuids": CaseSearchResultRepository._apply_filter_assignee_uuids,
    "coordinator_uuids": CaseSearchResultRepository._apply_filter_coordinator_uuids,
    "filter_registration_date": CaseSearchResultRepository._apply_filter_registration_date,
    "filter_completion_date": CaseSearchResultRepository._apply_filter_completion_date,
    "filter_payment_status": CaseSearchResultRepository._apply_filter_payment_status,
    "filter_channel_of_contact": CaseSearchResultRepository._apply_filter_channel_of_contact,
    "filter_confidentiality": CaseSearchResultRepository._apply_filter_confidentiality,
    "filter_archival_state": CaseSearchResultRepository._apply_filter_archival_state,
    "filter_retention_period_source_date": CaseSearchResultRepository._apply_filter_retention_period_source_date,
    "filter_result": CaseSearchResultRepository._apply_filter_result,
    "filter_case_location": CaseSearchResultRepository._apply_filter_case_location,
    "filter_num_unread_messages": CaseSearchResultRepository._apply_filter_num_unread_messages,
    "filter_num_unaccepted_files": CaseSearchResultRepository._apply_filter_num_unaccepted_files,
    "filter_num_unaccepted_updates": CaseSearchResultRepository._apply_filter_num_unaccepted_updates,
    "filter_keyword": CaseSearchResultRepository._apply_filter_keyword,
    "filter_period_of_preservation_active": CaseSearchResultRepository._apply_filter_period_of_preservation_active,
    "filter_subject": CaseSearchResultRepository._apply_filter_subject,
    "filter_urgency": CaseSearchResultRepository._apply_filter_urgency,
}
