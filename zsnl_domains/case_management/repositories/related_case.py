# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from .. import entities
from minty.entity import EntityCollection
from minty.repository import Repository
from sqlalchemy import sql
from sqlalchemy import types as sqltypes
from uuid import UUID
from zsnl_domains.database import schema
from zsnl_domains.shared.repositories.case_acl import allowed_cases_subquery


class RelatedCaseRepository(Repository, DatabaseRepositoryBase):
    def find_for_custom_object(
        self, object_uuid: UUID, user_uuid: UUID
    ) -> EntityCollection[entities.RelatedCase]:
        """
        Find related cases for custom_object.

        :param object_uuid: UUID of the custom_object
        :type object_uuid: UUID
        :return: cases related to a custom_object
        :rtype: List[RelatedCase]
        """

        select_stmt = self._select_stmt_case()
        select_stmt = select_stmt.where(
            sql.and_(
                schema.CustomObject.uuid == object_uuid,
                schema.CustomObjectRelationship.custom_object_id
                == schema.CustomObject.id,
                schema.CustomObjectRelationship.relationship_type == "case",
                schema.CustomObjectRelationship.related_uuid
                == schema.Case.uuid,
                allowed_cases_subquery(self.session, user_uuid, "read"),
            )
        )

        rows = []
        for row in self.session.execute(select_stmt).fetchall():
            rows.append(self._inflate_row_to_entity(row))
        return EntityCollection[entities.RelatedCase](rows)

    def _inflate_row_to_entity(self, row):
        """Inflate database row to RelatedCase Entity.

        :param row: Database Row
        :type row: object
        :return: RelatedCase
        :rtype: RelatedCase
        """
        if not row:
            return None

        mapping = {
            "uuid": "uuid",
            "entity_id": "uuid",
            "number": "id",
            "result": "result",
            "status": "status",
            "progress": "progress",
            "entity_meta_summary": "summary",
        }

        assignee_mapping = {
            "uuid": "assignee_uuid",
            "entity_id": "assignee_uuid",
            "entity_meta_summary": "assignee_name",
        }

        case_type_version_mapping = {
            "uuid": "case_type_version_uuid",
            "entity_id": "case_type_version_uuid",
            "entity_meta_summary": "case_type_version_title",
        }

        entity_data = {}
        for key, objkey in mapping.items():
            entity_data[key] = getattr(row, objkey)

        entity_data["case_type"] = {}
        for key, objkey in case_type_version_mapping.items():
            entity_data["case_type"][key] = getattr(row, objkey)

        if row.assignee_uuid:
            entity_data["assignee"] = {}
            for key, objkey in assignee_mapping.items():
                entity_data["assignee"][key] = getattr(row, objkey)

        return entities.RelatedCase.parse_obj(
            {**entity_data, "_event_service": self.event_service}
        )

    def _select_stmt_case(self):
        """Sql select statement for case.

        Or:

        SELECT
            zaak.uuid,
            zaak.id,
            zaak.onderwerp AS summary,
            zaak.resultaat AS result,
            zaak.status,
            CAST(zaak.milestone AS FLOAT) / (SELECT count(zaaktype_status.id) AS count_1 FROM zaaktype_status WHERE zaaktype_status.zaaktype_node_id = zaaktype_node.id) AS progress,
            zaaktype_node.uuid AS case_type_version_uuid,
            zaaktype_node.titel AS case_type_version_title,
            subject.uuid AS assignee_uuid,
            CAST(subject.properties AS JSON)[:param_1] AS assignee_name,
        FROM
            custom_object,
            custom_object_relationship,
            zaaktype_node,
            JOIN zaak ON zaaktype_node.id = zaak.zaaktype_node_id
            LEFT OUTER JOIN subject ON subject.id = zaak.behandelaar_gm_id
        """
        count_phases = (
            sql.select([sql.func.count(schema.ZaaktypeStatus.id)])
            .where(
                schema.ZaaktypeStatus.zaaktype_node_id
                == schema.ZaaktypeNode.id
            )
            .scalar_subquery()
            .label("count_phases")
        )

        return sql.select(
            [
                schema.Case.uuid,
                schema.Case.id,
                schema.Case.onderwerp.label("summary"),
                schema.Case.resultaat.label("result"),
                schema.Case.status,
                (
                    sql.cast(schema.Case.milestone, sqltypes.Float)
                    / count_phases
                ).label("progress"),
                schema.ZaaktypeNode.uuid.label("case_type_version_uuid"),
                schema.ZaaktypeNode.titel.label("case_type_version_title"),
                schema.Subject.uuid.label("assignee_uuid"),
                sql.cast(schema.Subject.properties, sqltypes.JSON)[
                    "displayname"
                ].label("assignee_name"),
            ]
        ).select_from(
            sql.join(
                schema.Case,
                schema.ZaaktypeNode,
                schema.ZaaktypeNode.id == schema.Case.zaaktype_node_id,
            ).join(
                schema.Subject,
                schema.Subject.id == schema.Case.behandelaar_gm_id,
                isouter=True,
            )
        )
