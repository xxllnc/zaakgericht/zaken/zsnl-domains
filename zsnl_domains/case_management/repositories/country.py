# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from .. import entities
from ..entities import Country
from minty.repository import Repository
from sqlalchemy import sql
from typing import Dict, List
from zsnl_domains.database import schema

countries_list_query = sql.select(
    [
        schema.CountryCode.uuid.label("uuid"),
        schema.CountryCode.label.label("name"),
        schema.CountryCode.dutch_code.label("code"),
    ]
).select_from(schema.CountryCode)


class CountryRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "Country"
    _events_to_calls: Dict[str, str] = {}

    def get_countries_list(self) -> List[Country]:
        """Get list of countries.
        :return: List[Country]
        :rtype: list
        """

        countries = self.session.execute(countries_list_query).fetchall()

        return [self._entity_from_row(row=country) for country in countries]

    def _entity_from_row(self, row) -> Country:

        country = entities.Country(
            uuid=row.uuid,
            name=row.name,
            code=row.code,
        )
        return country
