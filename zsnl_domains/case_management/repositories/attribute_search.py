# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from ... import DatabaseRepositoryBase
from ..entities import attribute_search as search_entities
from minty.cqrs import UserInfo
from minty.entity import EntityCollection
from minty.repository import Repository
from sqlalchemy import sql
from zsnl_domains.database import schema
from zsnl_domains.shared.util import escape_term_for_like


class AttributeSearchRepository(Repository, DatabaseRepositoryBase):
    _for_entity = "AttributeSearch"

    def search(
        self,
        page: int,
        page_size: int,
        filters: dict,
        user_info: UserInfo,
    ) -> EntityCollection[search_entities.AttributeSearch]:
        """Get list of attributes based on given filter."""

        query = self._search_query_stmt(
            filters=filters,
            user_info=user_info,
        )
        offset = self._calculate_offset(page, page_size)

        attributes_list = self.session.execute(
            query.limit(page_size).offset(offset)
        ).fetchall()

        rows = [self._entity_from_row(row=row) for row in attributes_list]
        return EntityCollection[search_entities.AttributeSearch](rows)

    def _search_query_stmt(self, filters: dict, user_info):
        query = sql.select(
            [
                schema.BibliotheekKenmerk.uuid,
                schema.BibliotheekKenmerk.description,
                schema.BibliotheekKenmerk.naam.label("label"),
                schema.BibliotheekKenmerk.magic_string,
                schema.BibliotheekKenmerk.value_type,
            ]
        )
        query = query.where(schema.BibliotheekKenmerk.deleted.is_(None))
        if filters is not None:
            query_stmt = self._apply_filters(
                query,
                filters=filters,
            )

        return query_stmt

    def _apply_filters(
        self,
        query,
        filters: dict,
    ):
        updated_filters = {k: v for k, v in filters.items() if v is not None}
        filters.clear()
        filters.update(updated_filters)

        if "filter_keyword" in filters:
            escaped_keyword = escape_term_for_like(filters["filter_keyword"])
            query = query.where(
                schema.BibliotheekKenmerk.naam.ilike(
                    f"%{escaped_keyword}%", escape="~"
                )
            )
        return query

    def _entity_from_row(self, row) -> search_entities.AttributeSearch:
        return search_entities.AttributeSearch.parse_obj(
            {
                "entity_id": row.uuid,
                "label": row.label,
                "magic_string": row.magic_string,
                "description": row.description,
                "type": row.value_type,
            }
        )
