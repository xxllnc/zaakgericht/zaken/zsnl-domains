# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import logging
from ...shared.services.template import TemplateService, jinja_template
from ..entities import Case
from typing import Callable

logger = logging.getLogger(__name__)

contact_mapping = {
    "name": "naam",
    "phone_number": "tel",
    "mobile_number": "mobiel",
    "email": "email",
    "salutation": "aanhef",
    "salutation1": "aanhef1",
    "salutation2": "aanhef2",
    "initials": "initialen",
    "first_names": "voornaam",
    "surname_prefix": "voorvoegsel",
    "surname": "achternaam",
    "naamgebruik": "naamgebruik",
    "last_name": "achternaam",
    "family_name": "geslachtsnaam",
    "full_name": "volledigenaam",
    "date_of_birth": "geboortedatum",
    "place_of_birth": "geboorteplaats",
    "country_of_birth": "geboorteland",
    "date_of_marriage": "huwelijksdatum",
    "date_of_divorce": "datum_ontbinding_partnerschap",
    "date_of_death": "overlijdensdatum",
    "is_secret": "indicatie_geheim",
    "street": "straat",
    "house_number": "huisnummer",
    "zipcode": "postcode",
    "place_of_residence": "woonplaats",
    "correspondence_street": "correspondentie_straat",
    "correspondence_house_number": "correspondentie_huisnummer",
    "correspondence_zipcode": "correspondentie_postcode",
    "correspondence_place_of_residence": "correspondentie_woonplaats",
    "residence_street": "verblijf_straat",
    "residence_house_number": "verblijf_huisnummer",
    "residence_zipcode": "verblijf_postcode",
    "residence_place_of_residence": "verblijf_woonplaats",
    "country_of_residence": "verblijf_land",
    "foreign_residence_address_line1": "verblijf_buitenland1",
    "foreign_residence_address_line2": "verblijf_buitenland2",
    "foreign_residence_address_line3": "verblijf_buitenland3",
    "trade_name": "handelsnaam",
    "coc": "kvknummer",
    "establishment_number": "vestigingsnummer",
    "title": "functie",
    "department": "afdeling",
    "gender": "geslacht",
    "investigation": "in_onderzoek",
    "type_of_business_entity": "rechtsvorm",
    "login": "login",
    "status": "status",
    "bsn": "burgerservicenummer",
    "has_briefadres": "heeft_briefadres",
    "type": "type",
    "a_number": "anummer",
}


payment_status_mapping = {
    "success": "Geslaagd",
    "pending": "Wachten op bevestiging",
    "failed": "Niet geslaagd",
    "offline": "Later betalen",
}
case_type_magic_strings = [
    "name",
    "id",
    "motivation",
    "archive_classification_code",
    "registration_bag",
    "objection_and_appeal",
    "goal",
    "lead_time_service",
    "lead_time_legal",
    "eform",
    "generic_category",
    "initiator_type",
    "identification",
    "lex_silencio_positivo",
    "principle_national",
    "description",
    "publicity",
    "suspension",
    "publication",
    "text_for_publication",
    "process_description",
    "keywords",
    "initiator_source",
    "supervisor",
    "supervisor_relation",
    "adjourn_period",
    "extension",
    "extension_period",
    "designation_of_confidentiality",
    "penalty",
    "principle_local",
    "wkpb",
    "version",
    "version_date_of_creation",
    "version_date_of_expiration",
]
case_type_mapping = {
    "name": "zaaktype",
    "id": "zaaktype_id",
    "motivation": "aanleiding",
    "archive_classification_code": "archiefclassicatiecode",
    "registration_bag": "bag",
    "objection_and_appeal": "bezwaar_en_beroep_mogelijk",
    "goal": "doel",
    "lead_time_service": "doorlooptijd_service",
    "lead_time_legal": "doorlooptijd_wettelijk",
    "eform": "e_formulier",
    "generic_category": "generieke_categorie",
    "initiator_type": "handelingsinitiator",
    "identification": "identificatie",
    "lex_silencio_positivo": "lex_silencio_positivo",
    "principle_national": "lokale_grondslag",
    "description": "omschrijving_of_toelichting",
    "publicity": "openbaarheid",
    "suspension": "opschorten_mogelijk",
    "publication": "publicatie",
    "text_for_publication": "publicatietekst",
    "process_description": "procesbeschrijving",
    "keywords": "trefwoorden",
    "initiator_source": "trigger",
    "supervisor": "verantwoordelijke",
    "supervisor_relation": "verantwoordingsrelatie",
    "adjourn_period": "verdagingstermijn",
    "extension": "verlenging_mogelijk",
    "extension_period": "verlengingstermijn",
    "designation_of_confidentiality": "vertrouwelijkheidsaanduiding",
    "penalty": "wet_dwangsom",
    "principle_local": "wettelijke_grondslag",
    "wkpb": "wkpb",
    "version": "zaaktype_versie",
    "version_date_of_creation": "zaaktype_versie_begindatum",
    "version_date_of_expiration": "zaaktype_versie_einddatum",
}
case_type_price_mapping = {
    "web": "tarief_web",
    "counter": "tarief_balie",
    "telephone": "tarief_telefoon",
    "email": "tarief_email",
    "employee": "tarief_behandelaar",
    "post": "tarief_post",
}

system_attributes_legacy_name_mapping = {
    "completion_date": "date_of_completion",
    "archival_state": "archival_state",
    "contact_channel": "channel_of_contact",
    "summary": "subject",
    "public_summary": "subject_external",
    "stalled_since_date": "stalled_since",
    "stalled_until_date": "stalled_until",
    "registration_date": "date_of_registration",
    "start_date": "startdate",
    "target_completion_date": "date_target",
    "destruction_date": "date_destruction",
    "confidentiality": "confidentiality",
    "id": "number",
    "milestone": "milestone",
    "case_type_phase": "phase",
    "department": "route_ou",
    "role": "route_role",
    "unaccepted_files_count": "num_unaccepted_files",
    "unaccepted_attribute_update_count": "num_unaccepted_updates",
    "parent_uuid": "parent_uuid",
    "child_uuids": "child_uuids",
    "child_uuids": "child_uuids",
    "related_uuids": "related_uuids",
    "destructable": "destructable",
}

system_attributes_dutch_name_mapping = {
    "department": "afdeling",
    "completion_date": "afhandeldatum",
    "date_of_completion_full": "afhandeldatum_volledig",
    "aggregation_scope": "aggregatieniveau",
    "type_of_archiving": "archiefnominatie",
    "archival_state": "archiefstatus",
    "period_of_preservation": "bewaartermijn",
    "contact_channel": "contactkanaal",
    "lead_time_real": "zaak_doorlooptijd",
    "summary": "zaak_onderwerp",
    "public_summary": "zaak_onderwerp_extern",
    "stalled_since_date": "opgeschort_sinds",
    "stalled_until_date": "opgeschort_tot",
    "suspension_rationale": "reden_opschorting",
    "premature_completion_rationale": "reden_vroegtijdig_afhandelen",
    "registration_date": "registratiedatum",
    "date_of_registration_full": "registratiedatum_volledig",
    "start_date": "startdatum",
    "result": "resultaat",
    "result_description": "resultaat_omschrijving",
    "result_explanation": "resultaat_toelichting",
    "target_completion_date": "streefafhandeldatum",
    "destruction_date": "uiterste_vernietigingsdatum",
    "confidentiality": "vertrouwelijkheid",
    "price": "zaak_bedrag",
    "related_cases": "zaak_relaties",
    "id": "zaaknummer",
    "number_parent": "pid",
    "number_master": "zaaknummer_hoofdzaak",
    "number_relations": "relates_to",
    "payment_status": "betaalstatus",
    "days_left": "dagen",
    "number_previous": "vervolg_van",
    "documents": "zaak_documenten",
    "case_documents": "zaak_dossierdocumenten",
    "case_type_phase": "zaak_fase",
    "milestone": "zaak_mijlpaal",
    "active_selection_list": "actieve_selectielijst",
    "progress_status": "voortgang_status",
    "progress_days": "voortgang",
}


class CaseTemplateService(TemplateService):
    def __init__(self, user_name_retriever: Callable[[], str]):
        self.user_name_retriever = user_name_retriever

    def _prepare_entity(self, case: Case):
        attributes = case.entity_dict()
        relationships = {}
        case_variables = {}

        for relationship_key in case.entity_relationships:
            relationship = attributes.pop(relationship_key)
            relationships[relationship_key] = relationship

        template_variables = {
            field: self._format_custom_field_value(value)
            for (field, value) in attributes["custom_fields"].items()
        }

        primary_contacts = self._generate_primary_contacts(relationships)
        template_variables.update(primary_contacts)
        case_variables.update(primary_contacts["case"])

        secondary_contacts = self._generate_secondary_contacts(relationships)
        template_variables.update(secondary_contacts)
        case_variables.update(secondary_contacts["case"])

        case_type_attributes = self._generate_case_type_attributes(
            relationships
        )
        template_variables.update(case_type_attributes)
        case_variables.update(case_type_attributes["case"])

        system_attributes = self._generate_system_attributes(attributes)
        template_variables.update(system_attributes)
        case_variables.update(system_attributes["case"])

        template_variables["case"] = case_variables
        template_variables["attributes"] = attributes
        template_variables["relationships"] = relationships
        template_variables["relationships"]["case_type"] = relationships[
            "case_type_version"
        ]

        template_variables.update(self._get_generic_attributes())
        return template_variables

    def _generate_primary_contacts(self, relationships: dict) -> dict:
        contacts = {}
        contacts["case"] = {}
        if (
            "requestor" in relationships
            and relationships["requestor"] is not None
        ):
            contacts["case"]["requestor"] = relationships["requestor"]
            contacts["aanvrager"] = relationships["requestor"]
            for key, value in relationships["requestor"].items():
                if key in contact_mapping:
                    name = "aanvrager_" + contact_mapping[key]
                    contacts[name] = value

        if (
            "assignee" in relationships
            and relationships["assignee"] is not None
        ):
            contacts["case"]["assignee"] = relationships["assignee"]
            for key, value in relationships["assignee"].items():
                if key in contact_mapping:
                    name = "behandelaar_" + contact_mapping[key]
                    contacts[name] = value

        if (
            "coordinator" in relationships
            and relationships["coordinator"] is not None
        ):
            contacts["case"]["coordinator"] = relationships["coordinator"]
            for key, value in relationships["coordinator"].items():
                if key in contact_mapping:
                    name = "coordinator_" + contact_mapping[key]
                    contacts[name] = value

        return contacts

    def _generate_secondary_contacts(self, relationships: dict) -> dict:
        contacts = {}
        contacts["case"] = {}
        if "related_contacts" in relationships:
            for contact in relationships["related_contacts"]:
                contact_attributes = contact.entity_dict()
                prefix = contact_attributes["magic_string_prefix"]
                contacts["case"][prefix] = contact_attributes
                for key, value in contact_attributes.items():
                    if key in contact_mapping:
                        dutch_name = prefix + "_" + contact_mapping[key]
                        contacts[dutch_name] = value
        return contacts

    def _generate_case_type_attributes(self, relationships):
        case_type_attributes = {}
        case_type_attributes["case"] = {}
        case_type_attributes["case"]["casetype"] = {}
        case_type_attributes["case"]["casetype"]["price"] = ""
        english_magic_strings = {}

        if (
            "case_type_version" in relationships
            and relationships["case_type_version"] is not None
        ):
            case_type_data = relationships["case_type_version"]

            metadata = case_type_data["metadata"]
            payment = case_type_data["settings"]["payment"]

            english_magic_strings = {
                "name": case_type_data["name"],
                "id": case_type_data["id"],
                "registration_bag": metadata["bag"],
                "motivation": metadata["motivation"],
                "objection_and_appeal": metadata[
                    "possibility_for_objection_and_appeal"
                ],
                "goal": metadata["purpose"],
                "lead_time_service": case_type_data["terms"][
                    "lead_time_service"
                ]["value"],
                "lead_time_legal": case_type_data["terms"]["lead_time_legal"][
                    "value"
                ],
                "eform": metadata["e_webform"],
                "initiator_type": case_type_data["initiator_type"],
                "identification": case_type_data["identification"],
                "lex_silencio_positivo": metadata["lex_silencio_positivo"],
                "principle_national": metadata["legal_basis"],
                "description": case_type_data["description"],
                "publicity": case_type_data["settings"]["is_public"],
                "suspension": metadata["may_postpone"],
                "publication": metadata["publication"],
                "text_for_publication": metadata["publication_text"],
                "process_description": case_type_data["metadata"][
                    "process_description"
                ],
                "price": {
                    "web": payment["webform"]["amount"],
                    "counter": payment["frontdesk"]["amount"],
                    "telephone": payment["phone"]["amount"],
                    "email": payment["mail"]["amount"],
                    "employee": payment["assignee"]["amount"],
                    "post": payment["post"]["amount"],
                },
                "keywords": case_type_data["tags"],
                "initiator_source": case_type_data["initiator_source"],
                "supervisor": metadata["responsible_subject"],
                "supervisor_relation": metadata["responsible_relationship"],
                "adjourn_period": metadata["extension_period"],
                "extension": metadata["may_extend"],
                "extension_period": metadata["extension_period"],
                "designation_of_confidentiality": metadata[
                    "designation_of_confidentiality"
                ],
                "penalty": metadata["penalty_law"],
                "principle_local": metadata["local_basis"],
                "wkpb": metadata["wkpb_applies"],
                "version": case_type_data["version"],
                "version_date_of_creation": case_type_data["created"],
                "version_date_of_expiration": case_type_data["deleted"],
                "archive_classification_code": metadata[
                    "archive_classification_code"
                ],
                "generic_category": case_type_data["catalog_folder"]["name"],
            }

        for key, value in english_magic_strings["price"].items():
            name = case_type_price_mapping[key]
            case_type_attributes[name] = value

        for key, value in english_magic_strings.items():
            if key in case_type_mapping:
                case_type_attributes[case_type_mapping[key]] = value
        case_type_attributes["case"]["casetype"] = english_magic_strings
        return case_type_attributes

    def _generate_system_attributes(self, attributes: dict) -> dict:
        system_attributes = {}
        system_attributes["case"] = {}
        system_attributes["case"]["department"] = ""
        system_attributes["case"]["result"] = ""

        case_location_value = attributes["case_location"]
        system_attributes["case"]["case_location"] = {
            "ligplaats": case_location_value,
            "nummeraanduiding": case_location_value,
            "openbareruimte": case_location_value,
            "pand": case_location_value,
            "standplaats": case_location_value,
            "verblijfsobject": case_location_value,
            "woonplaats": case_location_value,
        }
        system_attributes["zaaklocatie_ligplaats"] = case_location_value
        system_attributes["zaaklocatie_nummeraanduiding"] = case_location_value
        system_attributes["zaaklocatie_openbareruimte"] = case_location_value
        system_attributes["zaaklocatie_pand"] = case_location_value
        system_attributes["zaaklocatie_standplaats"] = case_location_value
        system_attributes["zaaklocatie_verblijfsobject"] = case_location_value
        system_attributes["zaaklocatie_woonplaats"] = case_location_value

        for key, value in attributes.items():
            if key == "case_location":
                # case_locations are processed above because of a . in the system_attribute
                continue
            if key in system_attributes_legacy_name_mapping:
                legacy_name = system_attributes_legacy_name_mapping[key]
                system_attributes["case"][legacy_name] = value
            else:
                system_attributes["case"][key] = value

            if key in system_attributes_dutch_name_mapping:
                dutch_name = system_attributes_dutch_name_mapping[key]
                system_attributes[dutch_name] = value

        system_attributes["bedrag_web"] = system_attributes["zaak_bedrag"]
        if "department" in attributes and attributes["department"] is not None:
            system_attributes["case"]["department"] = attributes["department"][
                "name"
            ]
            system_attributes["afdeling"] = attributes["department"]["name"]

        if "result" in attributes and attributes["result"] is not None:
            system_attributes["case"]["result"] = attributes["result"][
                "result"
            ]
            system_attributes["resultaat"] = attributes["result"]["result"]
        if "payment" in attributes and attributes["payment"] is not None:
            payment_status = attributes["payment"]["status"]
            if payment_status is not None:
                system_attributes["case"]["payment_status"] = payment_status
                system_attributes["betaalstatus"] = payment_status_mapping[
                    payment_status
                ]

        return system_attributes

    def _get_generic_attributes(self) -> dict:
        generic_attributes = {}
        generic_attributes["system"] = {}

        current_date = datetime.datetime.now(datetime.timezone.utc)

        generic_attributes["system"]["current_date"] = current_date
        generic_attributes["sjabloon_aanmaakdatum"] = current_date

        return generic_attributes

    def _process_user_variables(self, template: str) -> dict:
        user_variables = {}
        # only fill the user variables if the "user" magic_string is in the
        # template to prevent unnecessary queries
        user_var = next(
            filter(
                lambda var: "user.name" in var or "gebruiker_naam" in var,
                jinja_template.TEMPLATE_TAG_PATTERN.findall(template),
            ),
            None,
        )

        if user_var:
            user_name = self.user_name_retriever()
            user_variables["user"] = {}
            user_variables["user"]["name"] = user_name
            user_variables["gebruiker_naam"] = user_name
        return user_variables

    def render_value(self, case: Case, template: str):
        template = getattr(case.case_type_version, template)

        if template is None:
            template = ""

        template_variables = self._prepare_entity(case)
        template_variables.update(
            self._process_user_variables(template=template)
        )
        return self.render(template, template_variables)

    def _format_custom_field_value(self, value):
        if "value" in value and isinstance(value["value"], str):
            value["value"] = self._process_string(value["value"])

        return value

    def _format_date(self, date_value):
        if isinstance(date_value, (datetime.datetime)):
            date_value = date_value.date()
        val = date_value.strftime("%d-%m-%Y")
        return val

    def _process_string(self, template_value):
        try:
            date_value = datetime.datetime.strptime(template_value, "%Y-%m-%d")
            if isinstance(date_value, datetime.datetime):
                return self._format_date(date_value)
        except ValueError:
            return template_value
