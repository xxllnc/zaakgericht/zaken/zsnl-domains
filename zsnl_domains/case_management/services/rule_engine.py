# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
from ..entities import Case
from ..entities.case_type_version import (
    CasetypeRule,
    RuleAction,
    RuleCondition,
    StaticRuleAction,
)
from . import rule_actions, rule_conditions
from minty.exceptions import NotFound
from typing import List, Optional, Type, Union

conditions_mapping = {
    cls.name(cls): cls
    for cls in rule_conditions.BaseCondition.__subclasses__()
}


class RuleEngine:
    __allowed_actions: List[StaticRuleAction] = []
    loopcount = 0

    def __init__(self, allowed_actions: List[StaticRuleAction]):
        self.__allowed_actions = allowed_actions

    def _get_rule_action(
        self, name: Union[StaticRuleAction, str]
    ) -> Optional[Type[rule_actions.BaseAction]]:
        rule_action = next(
            filter(
                lambda ra: ra.name == name,
                rule_actions.BaseAction.__subclasses__(),
            ),
            None,
        )
        if not rule_action:
            raise NotImplementedError(
                f"Action '{name}' is not implemented in v2 rule_engine",
                "rule_engine/action_not_implemented",
            )
        return (
            rule_action if self._is_allowed_to_execute(action=name) else None
        )

    def _execute_rule_action(self, case: Case, actions: List[RuleAction]):
        self.loopcount = self.loopcount + 1
        if self.loopcount <= 5:
            for action in actions:
                if action.action:
                    rule_action = self._get_rule_action(name=action.action)
                    if rule_action:
                        rule_action(case=case).run(action=action)

    def _check_condition(self, case, condition: RuleCondition):
        if conditions_mapping.get(condition.kenmerk, None):
            return conditions_mapping[condition.kenmerk](case=case).match(
                value=condition.value
            )
        else:
            raise NotImplementedError(
                f"Condition '{condition.kenmerk}' is not implemented in v2 rule_engine",
                "rule_engine/condition_not_implemented",
            )

    def _is_allowed_to_execute(
        self, action: Union[str, StaticRuleAction]
    ) -> bool:
        return action in self.__allowed_actions

    def execute_rules(self, rules: List[CasetypeRule], case: Case):
        for rule in rules:
            if rule.condition_type == "or":
                rule_condition_matcher = any
            elif rule.condition_type == "and":
                rule_condition_matcher = all
            else:
                raise NotFound(
                    f"Condition type {rule.condition_type} condition is not allowed in v2 rule_engine",
                    "rule_engine/condition_type_not_allowed",
                )

            if rule_condition_matcher(
                [
                    self._check_condition(case, condition)
                    for condition in rule.conditions
                ]
            ):
                self._execute_rule_action(case, rule.match_actions)
            else:
                self._execute_rule_action(case, rule.nomatch_actions)
