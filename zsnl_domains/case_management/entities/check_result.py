# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic import Field


class CheckResult(Entity):
    """
    Entity that represents the result of a boolean check.

    Example usage:
        * A query that checks whether the user can perform a certain action.
    """

    entity_type = "check_result"
    check_result: bool = Field(..., title="Check Result")
