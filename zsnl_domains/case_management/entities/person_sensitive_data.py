# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity, Field
from typing import Optional
from uuid import UUID


class PersonSensitiveData(Entity):
    """Contains sensitive information about person"""

    entity_type = "person_sensitive_data"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Internal identifier of the person")
    personal_number: Optional[str] = Field(
        None,
        title="Burger service number of the person",
    )
