# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic import Field
from typing import Optional
from uuid import UUID, uuid4


class CaseRelation(Entity):
    entity_type = "case_relation"
    entity_id__fields = ["uuid"]

    relation_type: Optional[str] = Field(
        None, title="Type of relation between cases"
    )
    uuid: Optional[UUID] = Field(
        None, title="Internal identifier for the case relation"
    )
    object1_uuid: Optional[UUID] = Field(
        None, title="Indenitider for the first case"
    )
    object2_uuid: Optional[UUID] = Field(
        None, title="Indenitider for the second case"
    )
    relationship_type1: Optional[str] = Field(
        None, title="Type of relationship wrt first case"
    )
    relationship_type2: Optional[str] = Field(
        None, title="Type of relationship wrt second case"
    )
    owner_uuid: Optional[UUID] = Field(
        None, title="Identifier for the case relation owner"
    )
    blocks_deletion: Optional[bool] = Field(
        None, title="Boolean indicating blocks deletion"
    )
    summary1: Optional[str] = Field(None, title="Summary of first case")
    summary2: Optional[str] = Field(None, title="Summary of second case")
    case_id_a: Optional[int] = Field(None, title="Id of first case")
    case_id_b: Optional[int] = Field(None, title="Id of second case")
    order_seq_a: Optional[int] = Field(
        None, title="Sequesnce number of first case"
    )
    order_seq_b: Optional[int] = Field(
        None, title="Sequesnce number of second case"
    )
    current_case_uuid: Optional[UUID] = Field(
        None, title="Identifier of current case"
    )

    @Entity.event(name="CaseRelationCreated", fire_always=True)
    def create(self, object1_uuid, object2_uuid):
        self.uuid = str(uuid4())
        self.object1_uuid = object1_uuid
        self.object2_uuid = object2_uuid
        self.current_case_uuid = object1_uuid

    @Entity.event(name="CaseRelationReordered", fire_always=True)
    def reorder(self, current_case_uuid, new_index):
        self.current_case_uuid = current_case_uuid
        if self.current_case_uuid == self.object1_uuid:
            self.order_seq_b = new_index
        else:
            self.order_seq_a = new_index

    @Entity.event(
        name="CaseRelationDeleted",
        fire_always=True,
        extra_fields=[
            "case_id_a",
            "case_id_b",
            "object1_uuid",
            "object2_uuid",
        ],
    )
    def delete(self, current_case_uuid):
        self.current_case_uuid = current_case_uuid
