# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import enum
import minty.cqrs
from ._shared import AuthorizationLevel, ContactType
from datetime import date, datetime
from minty import entity
from pydantic import Field, conint, constr
from pydantic.generics import GenericModel
from typing import (
    Annotated,
    Generic,
    List,
    Literal,
    Optional,
    Set,
    Tuple,
    TypeVar,
    Union,
)
from uuid import UUID
from zsnl_domains.case_management.entities.custom_object import (
    ValidArchiveStatus,
    ValidObjectStatus,
)
from zsnl_domains.shared import types
from zsnl_domains.shared.entities.case import (
    ValidCaseArchivalState,
    ValidCaseConfidentiality,
    ValidCasePaymentStatus,
    ValidCaseResult,
    ValidCaseRetentionPeriodSourceDate,
    ValidCaseStatus,
    ValidContactChannel,
)

ISO8601_PERIOD_REGEX = (
    r"^([?+-])?"
    r"P(?!\b)"
    r"([?0-9]+([,.][0-9]+)?Y)s?"
    r"([?0-9]+([,.][0-9]+)?M)?"
    r"([?0-9]+([,.][0-9]+)?W)?"
    r"([?0-9]+([,.][0-9]+)?D)?"
    r"((T)([?0-9]+([,.][0-9]+)?H)?"
    r"([?0-9]+([,.][0-9]+)?M)?"
    r"([?0-9]+([,.][0-9]+)?S)?)?$"
)

SimpleFilterType = TypeVar("SimpleFilterType")
MinZeroInteger = conint(ge=0)


class SimpleFilterObject(GenericModel, Generic[SimpleFilterType]):
    label: Optional[str] = Field(
        None, title="Human-readable preview value of the filter value"
    )
    value: SimpleFilterType = Field(..., title="Value to filter for")

    class Config:
        @staticmethod
        def schema_extra(schema):
            for prop in schema.get("properties", {}).values():
                if (
                    "type" in prop
                    and prop["type"] == "array"
                    and "items" in schema["properties"]["value"]["items"]
                ):
                    updated_array = {
                        "oneOf": schema["properties"]["value"]["items"][
                            "items"
                        ]
                    }
                    schema["properties"]["value"]["items"][
                        "items"
                    ] = updated_array


CustomISODuration = constr(regex=ISO8601_PERIOD_REGEX)


class AbsoluteDate(entity.ValueObject):
    type: Literal["absolute"]
    value: Optional[datetime]
    operator: types.ComparisonFilterOperator


class RelativeDate(entity.ValueObject):
    type: Literal["relative"]
    value: CustomISODuration
    operator: types.ComparisonFilterOperator


class RangeDate(entity.ValueObject):
    type: Literal["range"]
    start_value: datetime
    end_value: datetime
    time_set_by_user: bool


class CustomObjectTypeFilter(entity.ValueObject):
    type: Literal["relationship.custom_object_type"]
    parameters: SimpleFilterObject[UUID]


class CustomObjectStatusFilter(entity.ValueObject):
    type: Literal["attributes.status"]
    parameters: SimpleFilterObject[ValidObjectStatus]


class CustomObjectArchiveStatusFilter(entity.ValueObject):
    type: Literal["attributes.archive_status"]
    parameters: SimpleFilterObject[ValidArchiveStatus]


class customFieldParameters(entity.ValueObject):
    operator: Optional[str]
    magic_string: str
    type: str
    value: str


class ContactParameters(entity.ValueObject):
    label: str
    value: UUID
    type: ContactType


class CustomFieldFilter(entity.ValueObject):
    type: Literal["custom_field"]
    parameters: customFieldParameters


class CustomObjectLastModifiedFilter(entity.ValueObject):
    type: Literal["attributes.last_modified"]
    parameters: SimpleFilterObject[
        List[
            Annotated[
                Union[
                    AbsoluteDate,
                    RangeDate,
                    RelativeDate,
                ],
                Field(discriminator="type"),
            ],
        ]
    ]


class CaseTypeUuidFilter(entity.ValueObject):
    type: Literal["relationship.case_type.id"]
    parameters: SimpleFilterObject[UUID]


class CaseStatusFilter(entity.ValueObject):
    type: Literal["attributes.status"]
    parameters: SimpleFilterObject[ValidCaseStatus]


class CaseAssigneeUuidsFilter(entity.ValueObject):
    type: Literal["relationship.assignee.id"]
    parameters: List[ContactParameters]


class CaseCoordinatorUuidsFilter(entity.ValueObject):
    type: Literal["relationship.coordinator.id"]
    parameters: List[ContactParameters]


class CaseRequestorUuidsFilter(entity.ValueObject):
    type: Literal["relationship.requestor.id"]
    parameters: List[ContactParameters]


class CaseRegistrationDateFilter(entity.ValueObject):
    type: Literal["attributes.registration_date"]
    parameters: SimpleFilterObject[
        List[Tuple[types.ComparisonFilterOperator, date]]
    ]


class CaseCompletionDateFilter(entity.ValueObject):
    type: Literal["attributes.completion_date"]
    parameters: SimpleFilterObject[
        List[Tuple[types.ComparisonFilterOperator, date]]
    ]


class CasePaymentStatusFilter(entity.ValueObject):
    type: Literal["attributes.payment_status"]
    parameters: SimpleFilterObject[ValidCasePaymentStatus]


class CaseChannelOfContactFilter(entity.ValueObject):
    type: Literal["attributes.channel_of_contact"]
    parameters: SimpleFilterObject[ValidContactChannel]


class CaseConfidentialityFilter(entity.ValueObject):
    type: Literal["attributes.confidentiality"]
    parameters: SimpleFilterObject[ValidCaseConfidentiality]


class CaseArchivalStateFilter(entity.ValueObject):
    type: Literal["attributes.archival_state"]
    parameters: SimpleFilterObject[ValidCaseArchivalState]


class CaseRetentionPeriodSourceDateFilter(entity.ValueObject):
    type: Literal["attributes.retention_period_source_date"]
    parameters: SimpleFilterObject[ValidCaseRetentionPeriodSourceDate]


class CaseResultFilter(entity.ValueObject):
    type: Literal["attributes.result"]
    value: SimpleFilterObject[ValidCaseResult]


class CaseLocationFilter(entity.ValueObject):
    type: Literal["attributes.case_location"]
    value: SimpleFilterObject[str]


class CaseNumUnreadMessagesFilter(entity.ValueObject):
    type: Literal["attributes.num_unread_messages"]
    value: SimpleFilterObject[
        List[Tuple[types.ComparisonFilterOperator, MinZeroInteger]]
    ]


class CaseNumUnacceptedFilesFilter(entity.ValueObject):
    type: Literal["attributes.num_unaccepted_files"]
    value: SimpleFilterObject[
        List[Tuple[types.ComparisonFilterOperator, MinZeroInteger]]
    ]


class CaseNumUnacceptedUpdatesFilter(entity.ValueObject):
    type: Literal["attributes.num_unaccepted_updates"]
    value: SimpleFilterObject[
        List[Tuple[types.ComparisonFilterOperator, MinZeroInteger]]
    ]


class CasePeriodOfPreservationActiveFilter(entity.ValueObject):
    type: Literal["attributes.period_of_preservation_active"]
    value: SimpleFilterObject[str]


class CaseSubjectFilter(entity.ValueObject):
    type: Literal["attributes.subject"]
    value: SimpleFilterObject[str]


class KeywordFilter(entity.ValueObject):
    type: Literal["keyword"]
    parameters: SimpleFilterObject[Optional[str]]


class CustomObjectSearchFilterDefinition(entity.ValueObject):
    kind_type: Literal["custom_object"]
    filters: List[
        Annotated[
            Union[
                KeywordFilter,
                CustomFieldFilter,
                CustomObjectTypeFilter,
                Optional[CustomObjectStatusFilter],
                Optional[CustomObjectArchiveStatusFilter],
                Optional[CustomObjectLastModifiedFilter],
            ],
            Field(discriminator="type"),
        ]
    ]

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


class CaseSearchFilterDefinition(entity.ValueObject):
    kind_type: Literal["case"]
    custom_fields_operator: Optional[Union[Literal["and"], Literal["or"]]]
    filters: List[
        Annotated[
            Union[
                KeywordFilter,
                CustomFieldFilter,
                CaseTypeUuidFilter,
                CaseStatusFilter,
                CaseAssigneeUuidsFilter,
                CaseCoordinatorUuidsFilter,
                CaseRequestorUuidsFilter,
                CaseRegistrationDateFilter,
                CaseCompletionDateFilter,
                CasePaymentStatusFilter,
                CaseChannelOfContactFilter,
                CaseConfidentialityFilter,
                CaseArchivalStateFilter,
                CaseRetentionPeriodSourceDateFilter,
                CaseResultFilter,
                CaseLocationFilter,
                CaseNumUnreadMessagesFilter,
                CaseNumUnacceptedFilesFilter,
                CaseNumUnacceptedUpdatesFilter,
                CasePeriodOfPreservationActiveFilter,
                CaseSubjectFilter,
            ],
            Field(discriminator="type"),
        ]
    ]

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


class SavedSearchPermission(str, enum.Enum):
    """Permissions a user can give to a group/role on a saved search."""

    read = "read"
    write = "write"


class SortOrder(str, enum.Enum):
    asc = "asc"
    desc = "desc"


class SavedSearchKind(str, enum.Enum):
    case = "case"
    custom_object = "custom_object"


class SavedSearchPermissionDefinition(entity.ValueObject):
    group_id: UUID = Field(
        ..., title="Identifier of the group this permission applies to"
    )
    role_id: UUID = Field(
        ..., title="Identifier of the role this permission applies to"
    )
    permission: Set[SavedSearchPermission] = Field(
        ...,
        title="Permissions the specified group + role has for this saved search",
    )


class SavedSearchColumnDefinition(entity.ValueObject):
    source: List[str] = Field(
        ...,
        title="Path to retrieve the field from the case or custom_object entity",
    )
    label: str = Field(..., title="Human-readable name for this column")
    type: str = Field(
        ...,
        title="Type of the field; can be used to determine how to show the 'raw' value",
    )
    magic_string: Optional[str] = Field(
        None, title="Magic string for this column"
    )

    class Config:
        allow_population_by_field_name = True

    def dict(self, exclude_unset=True, **kwargs):
        return super().dict(exclude_unset=exclude_unset, **kwargs)


class SavedSearchOwner(entity.Entity):
    entity_type = "employee"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class SavedSearch(entity.Entity):
    """
    Represents a saved state of the "advanced search" screen.

    Users can create (and save) multiple search queries, for quick access
    while working with the system.
    """

    entity_type = "saved_search"
    entity_id__fields = ["uuid"]
    entity_relationships = ["owner"]
    entity_meta__fields = ["entity_meta_summary", "entity_meta_authorizations"]

    entity_meta_authorizations: Optional[Set[AuthorizationLevel]] = Field(
        None,
        title="The set of rights/authorizations the current user has for the saved search",
    )

    uuid: UUID = Field(..., title="Identifier for this saved search")
    name: str = Field(..., title="Unique name of this search")

    owner: SavedSearchOwner = Field(..., title="Owner of this saved search")

    kind: SavedSearchKind = Field(
        ..., title="The kind of entity this saved search searches for"
    )

    filters: Annotated[
        Union[
            CustomObjectSearchFilterDefinition,
            CaseSearchFilterDefinition,
        ],
        Field(discriminator="kind_type"),
    ]
    permissions: List[SavedSearchPermissionDefinition] = Field(
        ..., title="Group/role/permission"
    )

    columns: List[SavedSearchColumnDefinition] = Field(
        ..., title="Columns visible when using this search"
    )
    sort_column: str = Field(
        ..., title="Column to sort the result on, when using this search"
    )
    sort_order: SortOrder = Field(
        ..., title="Order for sorting results (ascending or descending)"
    )
    date_deleted: Optional[date] = Field(
        None, title="Date the saved search is deleted"
    )

    @classmethod
    @entity.Entity.event(name="SavedSearchCreated", fire_always=True)
    def create(
        cls,
        uuid: UUID,
        name: str,
        kind: SavedSearchKind,
        owner: SavedSearchOwner,
        filters: Union[
            CustomObjectSearchFilterDefinition,
            CaseSearchFilterDefinition,
        ],
        permissions: List[SavedSearchPermissionDefinition],
        columns: List[SavedSearchColumnDefinition],
        sort_column: str,
        sort_order: SortOrder,
        event_service: minty.cqrs.EventService,
    ):
        return cls.parse_obj(
            {
                "entity_id": uuid,
                "uuid": uuid,
                "name": name,
                "kind": kind,
                "owner": owner,
                "filters": filters,
                "permissions": permissions,
                "columns": columns,
                "sort_column": sort_column,
                "sort_order": sort_order,
                "_event_service": event_service,
            }
        )

    @entity.Entity.event(name="SavedSearchDeleted", fire_always=True)
    def delete(self):
        self.date_deleted = date.today()

    @entity.Entity.event(name="SavedSearchUpdated", fire_always=True)
    def update(
        self,
        name: str,
        filters: Union[
            CustomObjectSearchFilterDefinition,
            CaseSearchFilterDefinition,
        ],
        columns: List[SavedSearchColumnDefinition],
        sort_column: str,
        sort_order: SortOrder,
    ):
        self.name = name
        self.filters = filters
        self.columns = columns
        self.sort_column = sort_column
        self.sort_order = sort_order

    @entity.Entity.event(
        name="SavedSearchPermissionsUpdated", fire_always=True
    )
    def update_permissions(
        self, permissions: List[SavedSearchPermissionDefinition]
    ):
        self.permissions = permissions
