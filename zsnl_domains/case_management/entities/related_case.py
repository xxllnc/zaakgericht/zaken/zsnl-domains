# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.entity import Entity
from pydantic import Field
from typing import Optional
from uuid import UUID
from zsnl_domains.shared.entities.case import ValidCaseStatus


class RelatedCaseAssignee(Entity):
    """Assignee for the related_case"""

    entity_type = "employee"
    entity_id__fields = ["uuid"]

    uuid: Optional[UUID] = Field(None, title="Internal identifier of assignee")


class RelatedCaseTypeVersion(Entity):
    """Assignee for the related_case"""

    entity_type = "case_type_version"
    entity_id__fields = ["uuid"]

    uuid: Optional[UUID] = Field(None, title="Internal identifier of assignee")


class RelatedCase(Entity):
    """Content of related_case"""

    entity_type = "related_case"
    entity_relationships = ["assignee", "case_type"]
    entity_id__fields = ["uuid"]

    # Properties
    uuid: UUID = Field(..., title="Internal identifier of the case")
    number: int = Field(..., title="Case ID")
    result: Optional[str] = Field(None, title="Result of the case")
    status: ValidCaseStatus = Field(..., title="Status of the case")
    progress: float = Field(..., title="Progress status of the case")

    # Relationship
    assignee: Optional[RelatedCaseAssignee] = Field(
        None, title="Assignee of the case"
    )

    case_type: RelatedCaseTypeVersion = Field(
        ..., title="Case type of the case"
    )
