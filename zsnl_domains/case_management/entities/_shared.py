# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import enum
import string
from minty.entity import Entity, ValueObject
from pydantic import Field, validator
from typing import Optional, Union
from uuid import UUID


class AuthorizationLevel(str, enum.Enum):
    read = "read"
    readwrite = "readwrite"
    admin = "admin"


class CaseAuthorizationLevel(str, enum.Enum):
    search = "search"
    read = "read"
    write = "write"
    manage = "manage"


class RelatedCaseType(Entity):
    """
    Reference to a case_type entity
    """

    entity_type: str = Field("case_type", const=True)
    entity_id__fields = ["uuid"]
    entity_meta__fields = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the case type")


class RelatedCaseTypeVersion(Entity):
    """
    Reference to a case_type_version entity
    """

    entity_type: str = Field("case_type_version", const=True)
    entity_id__fields = ["uuid"]
    entity_meta__fields = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the case type version")


class RelatedDepartment(Entity):
    """Reference to a department entity"""

    entity_type: str = Field("department", const=True)
    entity_id__fields = ["uuid"]
    entity_meta__fields = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class RelatedRole(Entity):
    """Reference to a Role entity"""

    entity_type: str = Field("role", const=True)
    entity_id__fields = ["uuid"]
    entity_meta__fields = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class RelatedEmployee(Entity):
    """Reference to an employee entity"""

    entity_type: str = Field("employee", const=True)
    entity_id__fields = ["uuid"]
    entity_meta__fields = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the employee")


class RelatedPerson(Entity):
    """Reference to a person entity"""

    entity_type: str = Field("person", const=True)
    entity_id__fields = ["uuid"]
    entity_meta__fields = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the person")


class RelatedOrganization(Entity):
    """Reference to an organization entity"""

    entity_type: str = Field("organization", const=True)
    entity_id__fields = ["uuid"]
    entity_meta__fields = ["entity_meta_summary"]

    uuid: UUID = Field(..., title="Identifier of the organization")


class BaseAddress(ValueObject):
    country: str = Field(..., title="Country")


class DutchAddress(BaseAddress):
    street: str = Field(..., title="Street name")
    street_number: int = Field(..., title="House number")
    street_number_letter: Optional[str] = Field(
        None, title="House letter (part of house number)"
    )
    street_number_suffix: Optional[str] = Field(
        None, title="House number extension"
    )
    zipcode: Optional[str] = Field(None, title="Postal code (zipcode)")
    city: str = Field(..., title="City")
    is_foreign: bool = Field(
        False,
        const=True,
        title="Indicates that this is an address outside the Netherlands",
    )

    @validator("street_number_letter")
    def is_valid_street_number_letter(cls, v):
        if v in [None, "", " "]:
            return None

        if len(v) == 1 and v[0] in string.ascii_letters:
            return v

        raise ValueError("House letter should be 1 letter (A-Z)")


class InternationalAddress(BaseAddress):
    address_line_1: str = Field(..., title="First line of the address")
    address_line_2: Optional[str] = Field(
        None, title="Second line of the address"
    )
    address_line_3: Optional[str] = Field(
        None, title="Third line of the address"
    )
    is_foreign: bool = Field(
        True,
        const=True,
        title="Indicates that this is an address outside the Netherlands",
    )


class InvalidatedAddress(BaseAddress):
    address_line_1: Optional[str] = Field(
        None, title="First line of the address"
    )
    address_line_2: Optional[str] = Field(
        None, title="Second line of the address"
    )
    address_line_3: Optional[str] = Field(
        None, title="Third line of the address"
    )
    street: Optional[str] = Field(None, title="Street name")
    street_number: Optional[int] = Field(None, title="House number")
    street_number_letter: Optional[str] = Field(
        None, title="House letter (part of house number)"
    )
    street_number_suffix: Optional[str] = Field(
        None, title="House number extension"
    )
    zipcode: Optional[str] = Field(None, title="Postal code (zipcode)")
    city: Optional[str] = Field(None, title="City")
    is_foreign: bool = Field(
        False,
        title="Indicates that this is an address outside the Netherlands",
    )


Address = Union[DutchAddress, InternationalAddress]

ContactAddress = Union[DutchAddress, InternationalAddress, InvalidatedAddress]


class ValidPreferredContactChannel(str, enum.Enum):
    pip = "pip"
    email = "email"
    phone = "phone"
    mail = "mail"


class ContactInformation(ValueObject):
    email: Optional[str] = Field(None, title="Email address of the contact")
    phone_number: Optional[str] = Field(None, title="Preferred phone number")
    mobile_number: Optional[str] = Field(None, title="Mobile phone number")
    internal_note: Optional[str] = Field(None, title="Internal note")
    preferred_contact_channel: Optional[ValidPreferredContactChannel] = Field(
        "pip", title="Preffered contact channel"
    )


class RelatedCustomObject(Entity):
    entity_type = "custom_object"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of the custom object")


class ValidGender(str, enum.Enum):
    male = "M"
    female = "F"
    other = "X"


class CaseConfidentiality(str, enum.Enum):
    public = "public"
    internal = "internal"
    confidential = "confidential"


class CaseStatus(str, enum.Enum):
    new = "new"
    open = "open"
    stalled = "stalled"
    resolved = "resolved"


class CasePayment(ValueObject):
    amount: Optional[float] = Field(None, title="payment amount")
    status: Optional[str] = Field(None, title="payment status")


class CaseResultArchivalAttributes(ValueObject):
    state: Optional[str] = Field(None, title="archival state")
    selection_list: Optional[str] = Field(None, title="selection list")


class CaseResult(ValueObject):
    result: str = Field(None, title="Short name (slug) for the result")
    result_name: str = Field(None, title="Custom name of the result")
    result_uuid: UUID = Field(
        None, title="UUID of the result in the case type"
    )
    archival_attributes: Optional[CaseResultArchivalAttributes] = Field(
        None, title="Archival attributes for the result"
    )


class ContactType(str, enum.Enum):
    person = "person"
    organization = "organization"
    employee = "employee"


def is_valid_bsn(bsn: str) -> bool:
    if 6 < len(bsn) < 10:
        bsn = bsn.zfill(9)
    else:
        return False
    sum = 0
    for i in range(9, 0, -1):
        if i > 1:
            sum += i * int(bsn[9 - i])
        elif i == 1:
            sum -= i * int(bsn[8])

    if sum and sum % 11 == 0:
        return True

    return False
