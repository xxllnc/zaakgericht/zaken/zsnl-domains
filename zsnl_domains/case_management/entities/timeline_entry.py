# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.entity import Entity
from pydantic import Field
from typing import Optional
from uuid import UUID


class TimelineRelatedCase(Entity):
    """Case related to contact"""

    entity_type = "case"
    entity_id__fields = ["uuid"]

    case_id: Optional[str] = Field(..., title="Id of the case")


class TimelineEntry(Entity):
    """Entity represents properties of timeline entry"""

    entity_type = "timeline_entry"
    entity_relationships = ["case"]
    entity_id__fields = ["uuid"]

    case: Optional[TimelineRelatedCase] = Field(None, title="case information")
    uuid: Optional[UUID] = Field(..., title="UUID of the event")
    type: str = Field(..., title="Type of the the event")
    date: datetime = Field(..., title="Date of the event")
    user: Optional[str] = Field(..., title="User of the event")
    description: Optional[str] = Field(..., title="Description of the event")
    exception: Optional[str] = Field(
        title="Exception information of the event"
    )

    def to_json(self):
        return self.json(
            exclude={
                "entity_id": ...,
                "entity_type": ...,
                "entity_relationships": ...,
                "entity_meta_summary": ...,
                "entity_id__fields": ...,
                "entity_meta__fields": ...,
                "entity_changelog": ...,
                "entity_data": ...,
                "_event_service": ...,
                "case": {
                    "entity_id": ...,
                    "entity_type": ...,
                    "entity_relationships": ...,
                    "entity_meta_summary": ...,
                    "entity_id__fields": ...,
                    "entity_meta__fields": ...,
                    "entity_changelog": ...,
                    "entity_data": ...,
                },
            }
        )
