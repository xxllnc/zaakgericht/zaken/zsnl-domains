# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import logging
import re
from ._shared import (
    Address,
    ContactAddress,
    ContactInformation,
    RelatedCustomObject,
)
from minty.entity import Entity, ValueObject
from minty.exceptions import Conflict
from pydantic import Field, validator
from typing import List, Optional
from uuid import UUID
from zsnl_domains.shared.util import get_entity_code_from_type_of_business

logger = logging.getLogger(__name__)


class OrganizationActivity(ValueObject):
    code: str = Field(..., title="Activity code")
    description: str = Field(..., title="Description of the activity")


class OrganizationContactPerson(ValueObject):
    initials: Optional[str] = Field(
        None, title="Initials in the name of contact person"
    )
    insertions: Optional[str] = Field(
        None, title="Insertions in the name of contact person"
    )
    family_name: Optional[str] = Field(
        None, title="Family name of the contact person"
    )
    first_name: Optional[str] = Field(
        None, title="First name of the contact person"
    )


class Organization(Entity):
    entity_type = "organization"
    entity_id__fields = ["uuid"]
    entity_relationships = ["related_custom_object"]

    uuid: UUID = Field(..., title="Identifier of this organization")
    name: str = Field(..., title="Organization name")
    source: str = Field(
        ...,
        title="Source of this organization",
        description="""
        The source of the data in this entity. Will be 'Zaaksysteem' if it was
        created manually, or name of the integration if it was imported from
        elsewhere.
        """,
    )

    date_founded: Optional[datetime.date] = Field(
        ..., title="Date the organization was founded"
    )
    date_registered: Optional[datetime.date] = Field(
        ..., title="Date the organization was first registered"
    )
    date_ceased: Optional[datetime.date] = Field(
        ..., title="Date the organization ceased operations"
    )

    rsin: Optional[int] = Field(..., title="RSIN of the organization")
    oin: Optional[int] = Field(
        ..., title="OIN (Organization Identification Number)"
    )

    coc_number: Optional[str] = Field(
        ...,
        title="Chamber of Commerce (Kamer van Koophandel, KvK) number of the organization",
    )
    coc_location_number: Optional[int] = Field(
        None,
        title="Branch identification number ('Vestigingsnummer')",
    )
    organization_type: Optional[str] = Field(..., title="Type of organization")
    main_activity: Optional[OrganizationActivity] = Field(
        ..., title="Code and descripton of the organization's main activity"
    )
    secondary_activities: List[OrganizationActivity] = Field(
        ...,
        title="Codes and descriptons of the organization's other activities",
    )

    location_address: Optional[ContactAddress] = Field(
        ..., title="Main address"
    )
    correspondence_address: Optional[ContactAddress] = Field(
        ..., title="Correspondence address"
    )
    contact_information: ContactInformation = Field(
        ..., title="Contact information"
    )
    related_custom_object: Optional[RelatedCustomObject] = Field(
        ..., title="UUID of the custom object with extra information"
    )
    preferred_contact_channel: Optional[str] = Field(
        None, title="Preferred channel of contact for this organization"
    )
    contact_person: Optional[OrganizationContactPerson] = Field(
        None, title="Contact person for this organization"
    )

    has_valid_address: bool = Field(
        ..., title="True if the contact has a valid address"
    )
    is_active: bool = Field(True, title="Organization is active/inactive")

    authenticated: Optional[int] = Field(
        ..., title="Indicates if the organization is authenticated"
    )

    @validator("coc_number")
    def validate_coc_number(cls, coc_number):
        if coc_number is None:
            return None

        if not re.match(r"^[0-9]{1,8}$", coc_number):
            raise ValueError("Invalid KVK", "coc_number/invalid")

        return coc_number

    @Entity.event(name="RelatedCustomObjectSet", fire_always=True)
    def set_related_custom_object(self, custom_object_uuid: Optional[UUID]):
        self.related_custom_object = (
            None
            if custom_object_uuid is None
            else RelatedCustomObject(
                entity_id=custom_object_uuid, uuid=custom_object_uuid
            )
        )

    @Entity.event(name="ContactInformationSaved", fire_always=True)
    def save_contact_information(self, contact_information: dict):
        self.contact_information = contact_information

    @Entity.event(name="OrganizationUpdated", fire_always=True)
    def update_non_authentic(
        self,
        update_data,
        address: Optional[Address],
        correspondence_address: Optional[Address],
    ):
        if self.authenticated:
            raise Conflict("Can not update Authenticated Organization")

        self.name = update_data.name if update_data.name else self.name
        self.coc_number = update_data.coc_number
        self.coc_location_number = update_data.coc_location_number
        self.contact_person = OrganizationContactPerson(
            **self._map_contact_person_data(update_data)
        )
        if update_data.organization_type:
            self.organization_type = get_entity_code_from_type_of_business(
                update_data.organization_type
            )
        self.location_address = address if address else self.location_address

        self.correspondence_address = correspondence_address

    def _build_initials_from_name(self, first_name) -> str:
        """Create a person's initials from their first name."""
        initials = "".join(
            [namepart[0] + "." for namepart in first_name.split(" ")]
        )

        return initials

    def _map_contact_person_data(self, update_data):
        update_contact_name = (
            update_data.contact_first_name
            if update_data.contact_first_name
            else self.contact_person.first_name
        )
        update_contact_family_name = (
            update_data.contact_last_name
            if update_data.contact_last_name
            else self.contact_person.family_name
        )
        update_contact_insertions = (
            update_data.contact_insertions
            if update_data.contact_insertions is not None
            else self.contact_person.insertions
        )
        return {
            "family_name": update_contact_family_name,
            "first_name": update_contact_name,
            "initials": self._build_initials_from_name(update_contact_name),
            "insertions": update_contact_insertions,
        }


class OrganizationLimited(Entity):
    entity_type = "organization"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Identifier of this organization")
    has_correspondence_address: bool = Field(
        None, title="True if organization has Correspondence address"
    )

    has_valid_address: bool = Field(
        ..., title="True if the contact has a valid address"
    )
    is_ceased: bool = Field(False, title="If organization is ceased")
    coc_number: str = Field(
        None,
        title="Chamber of Commerce (Kamer van Koophandel, KvK) number of the organization",
    )
    coc_location_number: int = Field(
        None,
        title="Branch identification number ('Vestigingsnummer')",
    )
