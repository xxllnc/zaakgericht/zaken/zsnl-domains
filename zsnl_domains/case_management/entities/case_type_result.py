# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import date
from dateutil.relativedelta import relativedelta
from minty.entity import Entity
from pydantic import Field
from typing import Optional
from uuid import UUID


class CaseTypeResult(Entity):
    entity_type = "case_type_result"

    uuid: UUID = Field(..., title="Uuid of the case_type_result")
    name: Optional[str] = Field(..., title="Name of the case_type_result")
    result: str = Field(..., title="Result of the case_type_result")
    trigger_archival: bool = Field(..., title="Indicator for archive case")
    preservation_term_label: str = Field(
        ..., title="Label of the result_preservation_term"
    )
    preservation_term_unit: Optional[str] = Field(
        ..., title="Unit of the result_preservation_term"
    )
    preservation_term_unit_amount: Optional[int] = Field(
        ..., title="Unit amount of the result_preservation_term"
    )

    def _calculate_destruction_date(self, afhandeldatum: Optional[date]):
        unit = self.preservation_term_unit
        amount = self.preservation_term_unit_amount
        if not unit or not afhandeldatum:
            # in case of 'Bewaren'
            return None

        delta = None
        if unit == "week":
            delta = relativedelta(weeks=amount)
        elif unit == "month":
            delta = relativedelta(months=amount)
        elif unit == "year":
            delta = relativedelta(years=amount)
        else:
            raise NotImplementedError(
                f"Calculate desctruction_date does not support unit {unit}"
            )

        return afhandeldatum + delta
