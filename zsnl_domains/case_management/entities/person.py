# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2


import minty.exceptions
from ._shared import (
    Address,
    ContactAddress,
    ContactInformation,
    RelatedCustomObject,
    ValidGender,
)
from datetime import datetime
from minty.cqrs import UserInfo
from minty.entity import Entity
from minty.exceptions import Conflict
from pydantic import Field, validator
from typing import Optional
from uuid import UUID
from zsnl_domains.case_management.entities._shared import is_valid_bsn
from zsnl_domains.shared.util import get_landcode_from_country_name


class ContactInformationPerson(ContactInformation):
    is_an_anonymous_contact_person: Optional[bool] = Field(
        False, title="Is the contact person is anonymous"
    )


class Person(Entity):
    """Content of a user defined Custom Object"""

    entity_type = "person"
    entity_id__fields = ["uuid"]

    # Properties
    authenticated: bool = Field(
        ..., title="Boolean indicates if the person is authenticated"
    )
    source: Optional[str] = Field(None, title="Source of the person")
    uuid: UUID = Field(..., title="Internal identifier of the person")
    first_names: Optional[str] = Field(None, title="First names of person")
    insertions: Optional[str] = Field(
        None, title="Insertions in the name of the person"
    )
    family_name: Optional[str] = Field(None, title="Family name of person")
    surname: str = Field("", title="Surname of person")
    name: str = Field(..., title="Name of the person")
    noble_title: Optional[str] = Field(None, title="Title of person")

    date_of_birth: Optional[datetime] = Field(
        None, title="Date of birth of person"
    )
    date_of_death: Optional[datetime] = Field(
        None, title="Date of death of person"
    )
    gender: Optional[str] = Field(None, title="Gender of the person")

    has_valid_address: bool = Field(
        ..., title="True if the contact has a valid address"
    )

    inside_municipality: Optional[bool] = Field(
        None,
        title="Boolean that indicates if the person lives within the municipality or not",
    )

    residence_address: Optional[ContactAddress] = Field(
        None, title="Residence address of the person"
    )
    correspondence_address: Optional[ContactAddress] = Field(
        None, title="Correspondence address of the person"
    )

    contact_information: ContactInformationPerson = Field(
        None, title="Contact info of the person"
    )

    entity_relationships = [
        "related_custom_object",
    ]

    related_custom_object: Optional[RelatedCustomObject] = Field(
        None, title="Custom object related to this person"
    )

    is_active: bool = Field(True, title="Mark user as active/inactive")

    is_secret: Optional[bool] = Field(None, title="If its secret value")

    bsn: Optional[str] = Field(None, title="BSN number for the person")

    initials: Optional[str] = Field(None, title="Initials of Person")

    country_code: Optional[str] = Field(
        None, title="Country Code of the address"
    )
    external_identifier: Optional[str] = Field(
        None, title="External identifier for the person"
    )

    @validator("bsn")
    def validate_bsn(cls, v):
        if v is not None:
            if not is_valid_bsn(v):
                raise ValueError("Invalid BSN", "bsn/invalid")
        return v

    # Events
    @Entity.event(name="RelatedCustomObjectSet", fire_always=True)
    def set_related_custom_object(self, custom_object_uuid: Optional[UUID]):
        self.related_custom_object = (
            None
            if custom_object_uuid is None
            else RelatedCustomObject(
                entity_id=custom_object_uuid,
                uuid=custom_object_uuid,
            )
        )

    @Entity.event(name="BsnRetrieved", fire_always=True, extra_fields=["name"])
    def create_log_for_bsn_retrieved(self):
        pass

    @Entity.event(
        name="ContactInformationSaved", fire_always=True, extra_fields=["name"]
    )
    def save_contact_information(
        self, contact_information: dict, user_info: UserInfo
    ):
        if not user_info.permissions.get("admin", False):
            if (
                self.contact_information.is_an_anonymous_contact_person
                != contact_information.get("is_an_anonymous_contact_person")
            ):
                raise minty.exceptions.Forbidden(
                    "Only Admin can set the contact as anonymous.",
                    "person/set_anonymous_not_allowed",
                )
        if self.contact_information.is_an_anonymous_contact_person and (
            contact_information.get("is_an_anonymous_contact_person", None)
            is not False
        ):
            raise minty.exceptions.Conflict(
                "Can not update details of anonymous person",
                "person/update_anonymous_not_allowed",
            )
        self.contact_information = ContactInformationPerson(
            **contact_information
        )

    @Entity.event(
        name="PersonUpdated", fire_always=True, extra_fields=["name"]
    )
    def update_non_authentic(
        self,
        first_name: str,
        family_name: str,
        surname_prefix: Optional[str],
        noble_title: Optional[str],
        gender: Optional[ValidGender],
        inside_municipality: Optional[bool],
        address: Optional[Address],
        correspondence_address: Optional[Address],
    ):
        if self.authenticated:
            raise Conflict("Can not update Authenticated Contact")

        self.first_names = first_name
        self.family_name = family_name
        self.surname = (
            surname_prefix + " " + family_name
            if surname_prefix
            else family_name
        )
        self.initials = (
            self._build_initials_from_name() if self.first_names else None
        )
        self.inside_municipality = inside_municipality

        if surname_prefix is not None:
            self.insertions = surname_prefix
        if noble_title is not None:
            self.noble_title = noble_title
        if gender is not None:
            self.gender = self._decide_gender_value(gender)

        if address:
            self.residence_address = address
            self.country_code = get_landcode_from_country_name(address.country)

        self.correspondence_address = correspondence_address

    def _build_initials_from_name(self) -> str:
        """Create a person's initials from their first name."""
        initials = "".join(
            [
                namepart[0] + "."
                for namepart in self.first_names.split(" ")
                if namepart
            ]
        )

        return initials

    def _decide_gender_value(self, gender):
        if gender == "M" or gender == "X":
            gender = gender
        elif gender == "F":
            gender = "V"
        else:
            gender = None
        return gender

    @Entity.event(
        name="NonAuthenticBsnUpdated", fire_always=True, extra_fields=["name"]
    )
    def update_bsn_non_authentic(self, bsn: str):
        if self.authenticated:
            raise Conflict("Can not update Authenticated Contact")
        self.bsn = bsn if bsn else self.bsn


class PersonLimited(Entity):

    entity_type = "person"
    entity_id__fields = ["uuid"]

    uuid: UUID = Field(..., title="Internal identifier of the person")

    has_correspondence_address: bool = Field(
        None, title="Correspondence address of the person"
    )

    has_valid_address: bool = Field(
        ..., title="True if the contact has a valid address"
    )
    is_secret: bool = Field(False, title="If its secret value")
    is_deceased: bool = Field(False, title="If person is deceased")
    is_under_investigation: bool = Field(
        False, title="If person is under investigation"
    )
