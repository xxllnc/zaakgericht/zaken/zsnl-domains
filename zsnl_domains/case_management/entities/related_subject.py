# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from enum import Enum
from minty.entity import Entity, Field
from typing import List, Optional
from uuid import UUID


class RelatedSubjectTypes(str, Enum):
    person = "person"
    organization = "organization"
    employee = "employee"


class RelatedSubjectRole(Entity):
    entity_type = "role"
    uuid: UUID = Field(..., title="UUID of the role")

    entity_id__fields = ["uuid"]


class RelatedSubject(Entity):
    """Content of related_subject"""

    entity_type = "related_subject"
    entity_id__fields = ["uuid"]

    _related_entity_map = {"roles": RelatedSubjectRole}

    # Properties
    uuid: UUID = Field(..., title="Internal identifier of the subject")
    type: RelatedSubjectTypes = Field(
        ..., title="Type of the subject (person/employee/organization)"
    )
    roles: Optional[List[RelatedSubjectRole]] = Field(
        None, title="Roles of the subject"
    )
