# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ...shared.types import ComparisonFilterConditionStr
from ...shared.validators import split_comma_separated_field
from ..entities import custom_object as object_entities
from ..entities import custom_object_search_result as search_entities
from ..repositories import CustomObjectSearchResultRepository
from minty.entity import EntityCollection
from pydantic import BaseModel, Field, validate_arguments, validator
from typing import List, Optional, Set, cast
from uuid import UUID


class CustomObjectSearchResultFilter(BaseModel):
    custom_object_type_uuid: UUID = Field(
        ..., alias="relationship.custom_object_type.id"
    )

    filter_status: Optional[Set[object_entities.ValidObjectStatus]] = Field(
        None, alias="attributes.status"
    )

    filter_archive_status: Optional[
        Set[object_entities.ValidArchiveStatus]
    ] = Field(None, alias="attributes.archive_status")

    filter_last_modified: Optional[List[ComparisonFilterConditionStr]] = Field(
        None, alias="attributes.last_modified"
    )

    filter_keyword: Optional[str] = Field(None, alias="keyword")

    class Config:
        validate_all = True

    _split_comma_separated_fields = validator(
        "filter_status",
        "filter_archive_status",
        "filter_last_modified",
        allow_reuse=True,
        pre=True,
    )(split_comma_separated_field)


class CustomObjectSearchResult(minty.cqrs.SplitQueryBase):
    name = "search_custom_objects"

    @validate_arguments
    def __call__(
        self,
        page: int,
        page_size: int,
        filters: CustomObjectSearchResultFilter,
        sort: Optional[
            search_entities.CustomObjectSearchOrder
        ] = search_entities.CustomObjectSearchOrder.title_asc,
    ) -> EntityCollection[search_entities.CustomObjectSearchResult]:
        """
        Get a list of filtered custom object search results.
        """

        custom_object_search_repo = cast(
            CustomObjectSearchResultRepository,
            self.get_repository("custom_object_search"),
        )

        result = custom_object_search_repo.search(
            page=page,
            page_size=page_size,
            sort=sort,
            user_info=self.qry.user_info,
            **filters.dict(),
        )
        return result


class CustomObjectSearchTotalResults(minty.cqrs.SplitQueryBase):
    name = "search_custom_object_total_results"

    @validate_arguments
    def __call__(self, filters: CustomObjectSearchResultFilter):

        custom_object_search_repo = cast(
            CustomObjectSearchResultRepository,
            self.get_repository("custom_object_search"),
        )

        total_result = custom_object_search_repo.search_total_results_count(
            user_info=self.qry.user_info,
            **filters.dict(),
        )

        return total_result
