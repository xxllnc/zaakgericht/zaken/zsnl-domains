# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..entities import CaseBasic
from ..repositories import CaseBasicRepository
from pydantic import validate_arguments
from typing import cast
from uuid import UUID


class Get(minty.cqrs.SplitQueryBase):
    name = "get_case_basic_by_uuid"

    @validate_arguments
    def __call__(self, case_uuid: UUID) -> CaseBasic:
        """
        Retrieve a CaseBasic entity for the case with UUID `case_uuid`
        """

        repo = cast(CaseBasicRepository, self.get_repository("case_basic"))
        case = repo.find_case_basic_by_uuid(
            case_uuid=case_uuid,
            user_info=self.qry.user_info,
            permission="read",
        )
        return case
