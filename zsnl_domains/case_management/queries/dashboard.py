# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.entity
from .. import repositories
from ..entities import dashboard as entity
from pydantic import validate_arguments
from typing import cast


class GetDashboard(minty.cqrs.SplitQueryBase):
    name = "get_dashboard"

    @validate_arguments
    def __call__(self) -> entity.Dashboard:
        repo = cast(
            repositories.DashboardRepository,
            self.get_repository("dashboard"),
        )
        uuid = self.qry.user_info.user_uuid
        return repo.get(
            uuid=uuid,
            user_info=self.qry.user_info,
        )
