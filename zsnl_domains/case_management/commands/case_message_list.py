# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import CaseMessageListRepository, SubjectRepository
from pydantic import validate_arguments
from typing import cast
from uuid import UUID


class ReassignCaseMessages(minty.cqrs.SplitCommandBase):
    name = "reassign_case_messages"

    @validate_arguments
    def __call__(self, case_uuid: UUID, subject_uuid: UUID):
        """
        Reassign all messages related to a case to a specific subject.

        :param case_uuid: UUID of the case to reassign messages for
        :param subject_uuid: UUID of the subject to assign messages to. Should
            be an employee
        """
        cml_repo = cast(
            CaseMessageListRepository, self.get_repository("case_message_list")
        )
        subject_repo = cast(SubjectRepository, self.get_repository("subject"))
        subject = subject_repo.find_subject_by_uuid(subject_uuid=subject_uuid)

        messages = cml_repo.get_messages_for_case(case_uuid=case_uuid)
        messages.assign_to_user(subject)

        cml_repo.save()
