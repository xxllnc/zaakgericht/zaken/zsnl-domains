# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
from ..repositories import CaseRepository
from minty.validation import validate_with
from pkgutil import get_data
from typing import cast
from uuid import UUID

# All of the code in here will be replaced once the creation of subcases
# has been moved to Python [20191012]


class EnqueueSubcases(minty.cqrs.SplitCommandBase):
    name = "enqueue_subcases"

    @validate_with(get_data(__name__, "validation/enqueue_subcases.json"))
    def __call__(self, case_uuid: UUID):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.unsafe_find_case_by_uuid(case_uuid)
        case.enqueue_subcases()
        repo.save()


class CreateSubcases(minty.cqrs.SplitCommandBase):
    name = "create_subcases"

    @validate_with(get_data(__name__, "validation/create_subcases.json"))
    def __call__(self, case_uuid: UUID, queue_ids):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.unsafe_find_case_by_uuid(case_uuid)
        case.create_subcases(queue_ids)
        repo.save()


class EnqueueDocuments(minty.cqrs.SplitCommandBase):
    name = "enqueue_documents"

    @validate_with(get_data(__name__, "validation/enqueue_documents.json"))
    def __call__(self, case_uuid: UUID):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.unsafe_find_case_by_uuid(case_uuid)
        case.enqueue_documents()
        repo.save()


class GenerateDocuments(minty.cqrs.SplitCommandBase):
    name = "generate_documents"

    @validate_with(get_data(__name__, "validation/generate_documents.json"))
    def __call__(self, case_uuid: UUID, queue_ids):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.unsafe_find_case_by_uuid(case_uuid)
        case.generate_documents(queue_ids)
        repo.save()


class EnqueueEmails(minty.cqrs.SplitCommandBase):
    name = "enqueue_emails"

    @validate_with(get_data(__name__, "validation/enqueue_emails.json"))
    def __call__(self, case_uuid: UUID):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.unsafe_find_case_by_uuid(case_uuid)
        case.enqueue_emails()
        repo.save()


class GenerateEmails(minty.cqrs.SplitCommandBase):
    name = "generate_emails"

    @validate_with(get_data(__name__, "validation/generate_emails.json"))
    def __call__(self, case_uuid: UUID, queue_ids):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.unsafe_find_case_by_uuid(case_uuid)
        case.generate_emails(queue_ids)
        repo.save()


class EnqueueSubjects(minty.cqrs.SplitCommandBase):
    name = "enqueue_subjects"

    @validate_with(get_data(__name__, "validation/enqueue_subjects.json"))
    def __call__(self, case_uuid: UUID):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.unsafe_find_case_by_uuid(case_uuid)
        case.enqueue_subjects()
        repo.save()


class GenerateSubjects(minty.cqrs.SplitCommandBase):
    name = "generate_subjects"

    @validate_with(get_data(__name__, "validation/generate_subjects.json"))
    def __call__(self, case_uuid: UUID, queue_ids):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.unsafe_find_case_by_uuid(case_uuid)
        case.generate_subjects(queue_ids)
        repo.save()


class TransitionCase(minty.cqrs.SplitCommandBase):
    name = "transition_case"

    @validate_with(get_data(__name__, "validation/transition_case.json"))
    def __call__(self, case_uuid: str):
        repo = cast(CaseRepository, self.get_repository("case"))
        case = repo.unsafe_find_case_by_uuid(case_uuid=UUID(case_uuid))
        case.transition_case()
        repo.save()
