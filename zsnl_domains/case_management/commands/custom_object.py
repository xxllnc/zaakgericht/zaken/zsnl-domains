# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2
import json
import minty.cqrs
import minty.exceptions
from ..entities import Case
from ..entities import case_type_version as case_type_entity
from ..entities import custom_object as entity
from ..repositories import (
    CaseRepository,
    CaseTypeRepository,
    CustomObjectRepository,
    CustomObjectTypeRepository,
)
from ..services import CustomObjectTemplateService
from minty.exceptions import NotFound
from minty.validation import validate_with
from pkgutil import get_data
from typing import List, Optional, cast
from uuid import UUID, uuid4


class CreateCustomObject(minty.cqrs.SplitCommandBase):
    name = "create_custom_object"

    @validate_with(get_data(__name__, "validation/create_custom_object.json"))
    def __call__(self, **kwargs):
        """
        Create a custom object of a given object type.

        See validations json for more information about the possible parameters
        """
        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )

        repo.create(
            **kwargs,
            template_service=CustomObjectTemplateService(),
            user_info=self.cmd.user_info,
        )

        repo.save()


class UpdateCustomObject(minty.cqrs.SplitCommandBase):
    name = "update_custom_object"

    @validate_with(get_data(__name__, "validation/update_custom_object.json"))
    def __call__(self, **kwargs):
        """
        Updates a custom object from a given object type

        See validations json for more information about the possible parameters
        """

        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )
        custom_object = repo.find_by_uuid(
            uuid=kwargs["existing_uuid"],
            user_info=self.cmd.user_info,
            authorization=entity.AuthorizationLevel.readwrite,
        )

        if not custom_object:
            raise NotFound(
                f"Could not find or rights are insufficient for 'custom_object' with uuid {kwargs['existing_uuid']}",
                "case_management/custom_object/object/not_found",
            )

        params = kwargs
        del params["existing_uuid"]

        for relationshipname, relationshipdata in params.pop(
            "related_to", {}
        ).items():
            if not relationshipdata or len(relationshipdata) < 1:
                params[f"{relationshipname}"] = []
                continue

            if relationshipname == "cases":
                params["cases"] = [
                    {"uuid": case_uuid, "entity_id": case_uuid}
                    for case_uuid in relationshipdata
                ]

            if relationshipname == "custom_objects":
                params["custom_objects"] = [
                    {
                        "uuid": custom_object_uuid,
                        "entity_id": custom_object_uuid,
                    }
                    for custom_object_uuid in relationshipdata
                ]

        if (
            custom_object.status == entity.ValidObjectStatus.inactive
            and not self._user_has_permission("admin")
        ):
            raise minty.exceptions.Forbidden(
                "Current user does not have rights to update inactive objects"
            )

        custom_object = custom_object.update(
            **params, template_service=CustomObjectTemplateService()
        )
        repo.save()


class DeleteCustomObject(minty.cqrs.SplitCommandBase):
    name = "delete_custom_object"

    @validate_with(get_data(__name__, "validation/delete_custom_object.json"))
    def __call__(self, **kwargs):
        """Delete custom object with given uuid."""

        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )
        custom_object = repo.find_by_uuid(uuid=kwargs["custom_object_uuid"])

        if not custom_object:
            raise NotFound(
                f'Could not find "custom_object" with uuid {kwargs["custom_object_uuid"]}'
            )
        custom_object.delete()

        repo.save()


class RelateCustomObjectTo(minty.cqrs.SplitCommandBase):
    name = "relate_custom_object_to"

    @validate_with(
        get_data(__name__, "validation/relate_custom_object_to.json")
    )
    def __call__(self, **kwargs):
        """
        Relate another entity to a given custom object.

        See validations json for more information about the possible parameters
        """
        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )

        custom_object = repo.find_by_uuid(
            uuid=kwargs["custom_object_uuid"],
            user_info=self.cmd.user_info,
            authorization=entity.AuthorizationLevel.readwrite,
        )

        if not custom_object:
            raise NotFound(
                "Could not find or rights are insufficient for 'custom_object' with uuid {}".format(
                    kwargs["custom_object_uuid"]
                ),
                "case_management/custom_object/object/not_found",
            )

        if "cases" in kwargs:
            custom_object.relate_to(
                relationship_type="case", related_uuids=kwargs["cases"]
            )

        repo.save()


class UnrelateCustomObjectFrom(minty.cqrs.SplitCommandBase):
    name = "unrelate_custom_object_from"

    @validate_with(
        get_data(__name__, "validation/unrelate_custom_object_from.json")
    )
    def __call__(self, **kwargs):
        """
        Unrelate another entity to a given custom object.

        See validations json for more information about the possible parameters
        """

        repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )
        custom_object = repo.find_by_uuid(
            uuid=kwargs["custom_object_uuid"],
            user_info=self.cmd.user_info,
            authorization=entity.AuthorizationLevel.readwrite,
        )

        if not custom_object:
            raise NotFound(
                "Could not find or rights are insufficient for 'custom_object' with uuid {}".format(
                    kwargs["custom_object_uuid"],
                ),
                "case_management/custom_object/object/not_found",
            )

        if "cases" in kwargs:
            custom_object.unrelate_from(
                relationship_type="case", related_uuids=kwargs["cases"]
            )

        repo.save()


class CustomObjectActionTrigger(minty.cqrs.SplitCommandBase):
    name = "custom_object_action_trigger"

    @validate_with(
        get_data(__name__, "validation/custom_object_action_trigger.json")
    )
    def __call__(
        self,
        case_uuid: str,
        action_type: str,
        relation_field_magic_string: str,
        custom_object_fields: dict,
        status: bool,
    ) -> None:
        """
        Handles a fase action custom_object event.
        based on action_type perform these actions:
        "create": Create a new object containing the custom_object_fields and relate it to a custom_field
        "edit":   Update all related objects in a custom field

        See validations json for more information about the possible parameters
        """
        case_repo = cast(CaseRepository, self.get_repository("case"))
        case_type_repo = cast(
            CaseTypeRepository, self.get_repository("case_type")
        )

        active_status = (
            entity.ValidObjectStatus.active
            if status
            else entity.ValidObjectStatus.inactive
        )

        case = case_repo.find_case_by_uuid(
            case_uuid=UUID(case_uuid),
            user_info=self.cmd.user_info,
            permission="read",
        )

        if case.case_type_version is None:
            raise minty.exceptions.Conflict(
                "Case found without a case type version?"
            )

        case_type = case_type_repo.find_case_type_version_by_uuid(
            version_uuid=case.case_type_version.uuid
        )

        object_custom_fields = {
            key: {"value": value} if value != "" else None
            for key, value in custom_object_fields.items()
        }

        if case_type:
            self.__transform_custom_fields(
                case=case,
                case_type=case_type,
                object_custom_fields=object_custom_fields,
            )

        # contains the uuids of the objects that are affected in this cmd
        updated_custom_objects: List[UUID] = []
        if action_type == "create" and self._allowed_to_add_object(
            case=case,
            case_type=case_type,
            magic_string=relation_field_magic_string,
        ):
            updated_custom_objects.append(
                self._create_custom_object_for_relation_attribute(
                    case=case,
                    case_type=case_type,
                    relation_field_magic_string=relation_field_magic_string,
                    object_custom_fields=object_custom_fields,
                    status=active_status,
                )
            )
        elif action_type == "edit":
            updated_custom_objects.extend(
                self._update_custom_objects_in_relation_field(
                    case=case,
                    case_type=case_type,
                    custom_field_magic_string=relation_field_magic_string,
                    custom_field_data=object_custom_fields,
                    status=active_status,
                )
            )

        if updated_custom_objects:
            field_value = self._create_attribute_value(
                case=case,
                case_type=case_type,
                magic_string=relation_field_magic_string,
                affected_object_uuids=updated_custom_objects,
            )

            case.update_custom_field(
                magic_string=relation_field_magic_string, new_value=field_value
            )
            case_repo.save()

    def __transform_custom_fields(
        self,
        case: Case,
        case_type: case_type_entity.CaseTypeVersionEntity,
        object_custom_fields: dict,
    ) -> None:
        # address_v2 fields are stored as a json string, transform it to a dict
        for key in case_type.get_custom_fields_of_type("address_v2").keys():
            if key in object_custom_fields and object_custom_fields[key]:
                object_custom_fields[key]["value"] = json.loads(
                    object_custom_fields[key]["value"]
                )

        for key in case_type.get_custom_fields_of_type("file").keys():
            if key in object_custom_fields and object_custom_fields[key]:
                # transform value for file customfields
                object_custom_fields[key] = object_custom_fields[key]["value"]

        for relation_field in case_type.get_custom_fields_of_type(
            "relationship"
        ):
            if (
                relation_field in object_custom_fields
                and object_custom_fields[relation_field] is not None
                and not object_custom_fields[relation_field]["value"].strip()
            ):
                # in some cases the relationship value is send in this format:
                # {'value': ' '}. Update this value to none for relationship fields
                object_custom_fields[relation_field] = None

            # if the relationfield is available in the attributemapping,
            # parse the correct value
            if (
                relation_field in object_custom_fields
                and object_custom_fields[relation_field] is not None
            ):
                object_custom_fields[relation_field] = json.loads(
                    object_custom_fields[relation_field]["value"]
                )
                self.convert_relation_type_to_subject(
                    object_custom_fields[relation_field]
                )

        for relation_field in case_type.get_custom_fields_of_type("geojson"):
            # parse the geojson fields from a str to a dict
            if (
                relation_field in object_custom_fields
                and object_custom_fields[relation_field] is not None
            ):
                object_custom_fields[relation_field]["value"] = json.loads(
                    object_custom_fields[relation_field]["value"]
                )

    def convert_relation_type_to_subject(self, relation_value: dict):
        # currently relationfields of the type subject can contain persons,
        # organizations and employees. The type is available in the
        # specifics.relationship_type field.
        # However, in custom_ojbects, this relationship_type must have the
        # value of "subject". This function converts the relationship_type
        if (
            "specifics" in relation_value
            and relation_value["specifics"] is not None
        ):
            if relation_value["specifics"]["relationship_type"] in [
                "person",
                "organization",
                "employee",
            ]:
                relation_value["specifics"]["relationship_type"] = "subject"

    def _create_attribute_value(
        self,
        case: Case,
        case_type: case_type_entity.CaseTypeVersionEntity,
        magic_string: str,
        affected_object_uuids: list[UUID],
    ):
        custom_object_repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )
        # get current attribute value
        # add / update affected_object
        # return new value
        result = {}
        if self._is_relation_field_multivalue(
            case_type=case_type, magic_string=magic_string
        ):
            multi_value_field_template = {
                "type": "relationship",
                "value": [],
                "specifics": None,
            }
            result = (
                case.custom_fields[magic_string]["value"].copy()
                if case.custom_fields
                and magic_string in case.custom_fields
                and case.custom_fields[magic_string] not in (None, {})
                else multi_value_field_template
            )

            # store all related object values in dict with uuid as key
            object_values_dict: dict[str, dict] = {
                object_value["value"]: object_value
                for object_value in result["value"]
            }
            for uuid in affected_object_uuids:
                object_values_dict[
                    str(uuid)
                ] = self._build_custom_object_value(
                    repo=custom_object_repo, custom_object_uuid=uuid
                )
            result["value"] = list(object_values_dict.values())
        else:
            # single value attribute can only contain 1 value. Overwrite it
            result = self._build_custom_object_value(
                repo=custom_object_repo,
                custom_object_uuid=affected_object_uuids[0],
            )

        return result

    def _build_custom_object_value(
        self, repo: CustomObjectRepository, custom_object_uuid: UUID
    ):
        custom_object = repo.find_by_uuid(
            uuid=custom_object_uuid,
            user_info=self.cmd.user_info,
            authorization=entity.AuthorizationLevel.read,
        )
        if not custom_object:
            raise NotFound(
                f'Could not find "custom_object" with uuid {custom_object_uuid}'
            )
        return {
            "type": "relationship",
            "value": str(custom_object.version_independent_uuid),
            "specifics": {
                "relationship_type": "custom_object",
                "metadata": {
                    "description": custom_object.subtitle,
                    "summary": custom_object.title,
                },
            },
        }

    def _is_relation_field_multivalue(
        self,
        case_type: case_type_entity.CaseTypeVersionEntity,
        magic_string: str,
    ):
        relation_field_type: Optional[
            case_type_entity.CustomFieldRelatedToCaseType
        ] = case_type.get_custom_fields_of_type("relationship")[magic_string]
        return (
            False
            if relation_field_type.is_multiple is None
            else relation_field_type.is_multiple
        )

    def _allowed_to_add_object(
        self,
        case: Case,
        case_type: case_type_entity.CaseTypeVersionEntity,
        magic_string: str,
    ):
        cur_num_objects_in_field = (
            len(case.custom_fields[magic_string]["value"]["value"])
            if case.custom_fields and magic_string in case.custom_fields
            else 0
        )
        return (
            self._is_relation_field_multivalue(
                case_type=case_type, magic_string=magic_string
            )
            or cur_num_objects_in_field == 0
        )

    def _create_custom_object_for_relation_attribute(
        self,
        case: Case,
        case_type: case_type_entity.CaseTypeVersionEntity,
        relation_field_magic_string: str,
        object_custom_fields: dict,
        status: entity.ValidObjectStatus,
    ) -> UUID:
        """create a new custom object
        a. Get the custom_object_type
        b. create the object
        return the uuid of the created object"""

        custom_object_type_repo = cast(
            CustomObjectTypeRepository,
            self.get_repository("custom_object_type"),
        )
        relation_field_type: Optional[
            case_type_entity.CustomFieldRelatedToCaseType
        ] = (
            case_type.get_custom_fields_of_type("relationship")[
                relation_field_magic_string
            ]
            if case.case_type
            else None
        )
        if (
            not relation_field_type
            or not relation_field_type.relationship_uuid
        ):
            raise NotFound(
                f'Could not find "relation_field_type" with magic string {relation_field_magic_string}'
            )

        custom_object_type = (
            custom_object_type_repo.find_by_version_independent_uuid(
                uuid=relation_field_type.relationship_uuid,
                user_info=self.cmd.user_info,
                authorization=entity.AuthorizationLevel.read,
            )
        )
        new_uuid = uuid4()
        self.cmd.create_custom_object(
            uuid=str(new_uuid),
            custom_object_type_uuid=str(custom_object_type.uuid),
            custom_fields=object_custom_fields,
            status=status.value,
        )
        return new_uuid

    def _update_custom_objects_in_relation_field(
        self,
        case: Case,
        case_type: case_type_entity.CaseTypeVersionEntity,
        custom_field_magic_string: str,
        custom_field_data: dict,
        status: entity.ValidObjectStatus,
    ) -> List[UUID]:
        """Epdate all custom object with custom_field_data that are related in
        custom_field_magic_string attribute.
        return all UUID's of the objects that are updated"""
        # haal zaak op
        # check kenmerk in zaak (obv magic_string custom field)
        # je hebt nu een lijst objecten of in elk geval, referenties ernaar
        # haal objecten op
        # update objecten
        # update zaak met nieuwe objecttitels
        all_updated_objects: List[UUID] = []

        custom_object_repo = cast(
            CustomObjectRepository, self.get_repository("custom_object")
        )

        if (
            case.custom_fields
            and custom_field_magic_string in case.custom_fields
        ):
            custom_objects_data = (
                case.custom_fields[custom_field_magic_string]["value"]["value"]
                if self._is_relation_field_multivalue(
                    case_type=case_type, magic_string=custom_field_magic_string
                )
                else [case.custom_fields[custom_field_magic_string]["value"]]
            )

            for related_custom_object in custom_objects_data:
                existing_uuid = related_custom_object["value"]
                exisiting_custom_object = custom_object_repo.find_by_uuid(
                    uuid=existing_uuid
                )

                if (
                    exisiting_custom_object
                    and not exisiting_custom_object.status
                    == entity.ValidObjectStatus.inactive
                ):
                    self.cmd.update_custom_object(
                        existing_uuid=existing_uuid,
                        custom_fields=custom_field_data,
                        uuid=str(uuid4()),
                        status=status,
                    )
                    all_updated_objects.append(existing_uuid)

        return all_updated_objects
