# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import datetime
import email
import pytest
from collections import namedtuple
from email.message import EmailMessage
from email.policy import default as DefaultMailPolicy
from minty.cqrs import test
from minty.exceptions import Conflict, Forbidden, NotFound
from unittest import mock
from uuid import UUID, uuid4
from zsnl_domains import communication
from zsnl_domains.communication.commands import (
    Commands,
    _get_case_from_message_subject,
    _get_participants,
    _get_thread_contact_from_participants,
    _get_thread_from_message,
    _mark_message_read,
)
from zsnl_domains.communication.entities import (
    Case,
    Contact,
    ExternalMessage,
    File,
    Thread,
)

UserInfo = namedtuple("UserInfo", "user_uuid permissions")


MESSAGE_WITH_NULLS = """Return-Path: <bug@development.zaaksysteem.nl>
To: Zaaksysteem Bug <bug@development.zaaksysteem.nl>
From: Zaaksysteem Bug <bug@development.zaaksysteem.nl>
Subject: This demonstrates the NUL bug
Message-ID: <b9eba5dd-faba-442f-889d-e375cc402677@development.zaaksysteem.nl>
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: quoted-printable
MIME-Version: 1.0

Here's NUL byte: =00
"""

COMPLEX_MESSAGE1 = """MIME-Version: 1.0
Date: Fri, 25 Oct 2019 08:51:44 +0200
Message-ID: <CAO3nP7oyP+ta2GazELypDncNnTkVvMJQeX-EqwcroO5TyBK5Xg@mail.gmail.com>
Subject: Testing 123
From: Martijn <martijn@mintlab.nl>
To: test@development.zaaksysteem.nl
Content-Type: multipart/alternative; boundary="00000000000048e3700595b6950a"

--00000000000048e3700595b6950a
Content-Type: text/plain; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

Test van inkomende mail!

=F0=9F=8E=83=F0=9F=8E=83=F0=9F=8E=83

Met vriendelijke groet,

Martijn



H.J.E. Wenckebachweg 90 4.01
1114 AD Amsterdam
T: 020 - 737 000 5
M: 06 - 482 537 94

--00000000000048e3700595b6950a
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: quoted-printable

This is the HTML part, which should not be found.
--00000000000048e3700595b6950a--"""

COMPLEX_MESSAGE2 = """MIME-Version: 1.0
Date: Fri, 25 Oct 2019 08:51:44 +0200
Message-ID: <test@example.com>
From: Somebody <somebody@example.com>
To: Somebody Else <somebody@example.org>
Subject: Some kind of signed message
Content-Type: multipart/mixed; boundary="outer"

--outer
MIME-Version: 1.0
Content-Type: multipart/alternative; boundary="inner1"
Content-Transfer-Encoding: 7bit


--inner1
Content-Type: text/plain; charset="UTF-8"
Content-Transfer-Encoding: 8bit
Content-Disposition: inline

This is email text.

--inner1
Content-Type: text/html
Content-Transfer-Encoding: 8bit
Content-Disposition: inline

<html><body>This is email HTML body</body></html>

--inner1--

--outer
Content-ID:
MIME-Version: 1.0
Content-Type: multipart/signed; name="smime.p7m"
Content-Transfer-Encoding: 8bit
Content-Disposition: attachment; filename="smime.p7m"

Content-Type: multipart/signed; protocol="application/x-pkcs7-signature"; micalg="sha-256"; boundary="----signature"

--signature
Content-Type: multipart/alternative; boundary="inner2"
Content-Transfer-Encoding: 8bit

This is a multi-part message in MIME format.
--inner2
Content-Type: text/plain; charset="UTF-8"

This is a piece of text inside the multipart/signed part.

--inner2
Content-Type: text/html
Content-Transfer-Encoding: 8bit
Content-Disposition: inline

<html><body>This is email HTML in the multipart/signed part.</body></html>

--inner2--

--signature
Content-Type: application/x-pkcs7-signature; name="smime.p7s"
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename="smime.p7s"

BASE64 SIGNATURE GOES HERE
--signature--

--outer--
"""

MESSAGE_WITHOUT_SUBJECT = """Return-Path: <bug@development.zaaksysteem.nl>
To: Zaaksysteem Bug <bug@development.zaaksysteem.nl>
From: Zaaksysteem Bug <bug@development.zaaksysteem.nl>
Message-ID: <b9eba5dd-faba-442f-889d-e375cc402677@development.zaaksysteem.nl>
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: quoted-printable
MIME-Version: 1.0
"""


class MockInfrastructureFactory:
    def __init__(self, mock_infra):
        self.infrastructure = mock_infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infrastructure[infrastructure_name]


UserInfo = namedtuple("UserInfo", "user_uuid permissions")


class TestCommandClass:
    def setup_method(self):
        self.mock_repo_factory = mock.MagicMock()
        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.command = Commands(
            repository_factory=self.mock_repo_factory,
            context=None,
            user_uuid=uuid4(),
            event_service=None,
        )
        self.command.user_info = user_info

    @mock.patch("zsnl_domains.communication.commands.uuid4")
    def test_import_email_with_attachments(self, uuid4_mock):

        file_uuid = uuid4()
        uuid4_mock.return_value = file_uuid

        cmd = self.command
        self.mock_repo_factory.reset_mock()
        mock_repositories = {
            "attachment": mock.MagicMock(),
            "case": mock.MagicMock(),
            "document_file": mock.MagicMock(),
            "file": mock.MagicMock(),
            "message": mock.MagicMock(),
            "thread": mock.MagicMock(),
        }

        def get_repository(name, context=None, event_service=None):
            return mock_repositories[name]

        with open("tests/data/test.eml") as message:
            email_msg = email.message_from_file(
                message, policy=DefaultMailPolicy
            )

        mock_repositories["file"].parse_as_message.return_value = email_msg
        mock_repositories["file"].upload.return_value = {
            "uuid": file_uuid,
            "md5": "MD5HASH",
            "size": 12344,
            "mime_type": "application/pdf",
            "storage_location": "Minio",
        }
        mock_repositories["message"].get_email_configuration.return_value = {
            "subject_prefix": "PREFIX"
        }

        self.mock_repo_factory.get_repository = get_repository

        cmd.import_email(str(uuid4()))
        mock_repositories["document_file"].create_file.assert_called_with(
            file_uuid=file_uuid,
            filename="index.jpeg",
            mimetype="application/pdf",
            size=12344,
            storage_location="Minio",
            md5="MD5HASH",
        )
        with open("tests/data/email_with_email.eml") as message:
            email_msg = email.message_from_file(
                message, policy=DefaultMailPolicy
            )

        mock_repositories["file"].parse_as_message.return_value = email_msg
        mock_repositories["file"].upload.return_value = {
            "uuid": file_uuid,
            "md5": "MD5HASH",
            "size": 12344,
            "mime_type": "application/pdf",
            "storage_location": "Minio",
        }

        self.mock_repo_factory.get_repository = get_repository

        cmd.import_email(str(uuid4()))
        assert mock_repositories["document_file"].create_file.call_args_list[
            3
        ] == mock.call(
            file_uuid=file_uuid,
            filename="test.eml",
            mimetype="application/pdf",
            size=12344,
            storage_location="Minio",
            md5="MD5HASH",
        )
        assert mock_repositories["document_file"].create_file.call_args_list[
            4
        ] == mock.call(
            file_uuid=file_uuid,
            filename=".txt",
            mimetype="application/pdf",
            size=12344,
            storage_location="Minio",
            md5="MD5HASH",
        )

    @mock.patch("zsnl_domains.communication.commands.uuid4")
    def test_import_email_with_attachments_invalid(self, uuid4_mock):

        file_uuid = uuid4()
        uuid4_mock.return_value = file_uuid

        cmd = self.command
        self.mock_repo_factory.reset_mock()
        mock_repositories = {
            "attachment": mock.MagicMock(),
            "case": mock.MagicMock(),
            "document_file": mock.MagicMock(),
            "file": mock.MagicMock(),
            "message": mock.MagicMock(),
            "thread": mock.MagicMock(),
        }

        def get_repository(name, context=None, event_service=None):
            return mock_repositories[name]

        with open("tests/data/test.eml") as message:
            email_msg = email.message_from_file(
                message, policy=DefaultMailPolicy
            )

        mock_repositories["file"].parse_as_message.return_value = email_msg
        mock_repositories["file"].upload.return_value = {
            "uuid": file_uuid,
            "md5": "MD5HASH",
            "size": 12344,
            "mime_type": "application/pdf",
            "storage_location": "Minio",
        }
        mock_repositories["message"].get_email_configuration.return_value = {
            "subject_prefix": "PREFIX"
        }
        mock_repositories["document_file"].create_file.side_effect = Forbidden

        self.mock_repo_factory.get_repository = get_repository

        cmd.import_email(str(uuid4()))

        assert mock_repositories["document_file"].create_file.call_args_list[
            0
        ] == mock.call(
            file_uuid=file_uuid,
            filename="Dropped Text.txt",
            mimetype="application/pdf",
            size=12344,
            storage_location="Minio",
            md5="MD5HASH",
        )
        assert mock_repositories["document_file"].create_file.call_args_list[
            1
        ] == mock.call(
            file_uuid=file_uuid,
            filename=".txt",
            mimetype="application/pdf",
            size=12344,
            storage_location="Minio",
            md5="MD5HASH",
        )

        assert mock_repositories["document_file"].create_file.call_args_list[
            2
        ] == mock.call(
            file_uuid=file_uuid,
            filename="index.jpeg",
            mimetype="application/pdf",
            size=12344,
            storage_location="Minio",
            md5="MD5HASH",
        )

        with open("tests/data/email_with_email.eml") as message:
            email_msg = email.message_from_file(
                message, policy=DefaultMailPolicy
            )

        mock_repositories["file"].parse_as_message.return_value = email_msg
        mock_repositories["file"].upload.return_value = {
            "uuid": file_uuid,
            "md5": "MD5HASH",
            "size": 12344,
            "mime_type": "application/pdf",
            "storage_location": "Minio",
        }

        self.mock_repo_factory.get_repository = get_repository

        mock_repositories["document_file"].reset_mock()

        cmd.import_email(str(uuid4()))

        assert mock_repositories["document_file"].create_file.call_args_list[
            0
        ] == mock.call(
            file_uuid=file_uuid,
            filename="test.eml",
            mimetype="application/pdf",
            size=12344,
            storage_location="Minio",
            md5="MD5HASH",
        )
        assert mock_repositories["document_file"].create_file.call_args_list[
            1
        ] == mock.call(
            file_uuid=file_uuid,
            filename=".txt",
            mimetype="application/pdf",
            size=12344,
            storage_location="Minio",
            md5="MD5HASH",
        )

    def test_import_email(self):
        cmd = self.command
        self.mock_repo_factory.reset_mock()

        mock_repositories = {
            "attachment": mock.MagicMock(),
            "case": mock.MagicMock(),
            "document_file": mock.MagicMock(),
            "file": mock.MagicMock(),
            "message": mock.MagicMock(),
            "thread": mock.MagicMock(),
        }
        fake_message = email.message_from_string(
            "From: Me <me@example.com>\n"
            "To: You <you@example.com>\n"
            "Cc: Someone <someone@example.com>\n"
            "Cc: Third Person <third@example.com>\n"
            "Subject: You know\n"
            "Content-Type: text/plain\n"
            "\n"
            "This is the body\n",
            policy=DefaultMailPolicy,
        )

        thread_uuid = uuid4()
        thread = mock.MagicMock()
        thread.uuid = thread_uuid

        mock_repositories["file"].parse_as_message.return_value = fake_message
        mock_repositories["message"].get_email_configuration.return_value = {
            "subject_prefix": "PREFIX"
        }
        mock_repositories["thread"].create.return_value = thread

        def get_repository(name, context=None, event_service=None):
            return mock_repositories[name]

        self.mock_repo_factory.get_repository = get_repository

        cmd.import_email(str(uuid4()))

        thread_repo = mock_repositories["thread"]
        thread_calls = thread_repo.create.call_args_list
        assert len(thread_calls) == 1
        (thread_call_args, thread_call_kwargs) = thread_calls[0]
        assert thread_call_args == ()
        assert thread_call_kwargs["case"] is None
        assert thread_call_kwargs["contact"] is None
        assert thread_call_kwargs["thread_type"] == "external"
        assert isinstance(thread_call_kwargs["new_thread_uuid"], UUID)
        thread_repo.save.assert_called()

        message_repo = mock_repositories["message"]
        message_calls = message_repo.create_external_message.call_args_list
        assert len(message_calls) == 1
        (message_calls_args, message_calls_kwargs) = message_calls[0]
        assert message_calls_args == ()
        assert message_calls_kwargs["content"] == "This is the body\n"
        assert message_calls_kwargs["created_by"] is None
        assert message_calls_kwargs["external_message_type"] == "email"
        assert message_calls_kwargs["subject"] == "You know"
        assert message_calls_kwargs["participants"] == [
            {
                "role": "to",
                "display_name": "You",
                "address": "you@example.com",
            },
            {
                "role": "cc",
                "display_name": "Someone",
                "address": "someone@example.com",
            },
            {
                "role": "cc",
                "display_name": "Third Person",
                "address": "third@example.com",
            },
            {
                "role": "from",
                "display_name": "Me",
                "address": "me@example.com",
            },
        ]
        assert message_calls_kwargs["thread_uuid"] == thread_uuid
        assert isinstance(message_calls_kwargs["external_message_uuid"], UUID)
        message_repo.save.assert_called()

    def test_import_email_nul_byte(self):
        cmd = self.command
        self.mock_repo_factory.reset_mock()

        mock_repositories = {
            "attachment": mock.MagicMock(),
            "case": mock.MagicMock(),
            "document_file": mock.MagicMock(),
            "file": mock.MagicMock(),
            "message": mock.MagicMock(),
            "thread": mock.MagicMock(),
        }
        fake_message = email.message_from_string(
            MESSAGE_WITH_NULLS,
            policy=DefaultMailPolicy,
        )

        thread_uuid = uuid4()
        thread = mock.MagicMock()
        thread.uuid = thread_uuid

        mock_repositories["file"].parse_as_message.return_value = fake_message
        mock_repositories["message"].get_email_configuration.return_value = {
            "subject_prefix": "PREFIX"
        }
        mock_repositories["thread"].create.return_value = thread

        def get_repository(name, context=None, event_service=None):
            return mock_repositories[name]

        self.mock_repo_factory.get_repository = get_repository

        cmd.import_email(str(uuid4()))

        thread_repo = mock_repositories["thread"]
        thread_calls = thread_repo.create.call_args_list
        assert len(thread_calls) == 1
        (thread_call_args, thread_call_kwargs) = thread_calls[0]
        assert thread_call_args == ()
        assert thread_call_kwargs["case"] is None
        assert thread_call_kwargs["contact"] is None
        assert thread_call_kwargs["thread_type"] == "external"
        assert isinstance(thread_call_kwargs["new_thread_uuid"], UUID)
        thread_repo.save.assert_called()

        message_repo = mock_repositories["message"]
        message_calls = message_repo.create_external_message.call_args_list
        assert len(message_calls) == 1
        (message_calls_args, message_calls_kwargs) = message_calls[0]
        assert message_calls_args == ()
        assert message_calls_kwargs["content"] == """Here's NUL byte: \n"""
        assert message_calls_kwargs["created_by"] is None
        assert message_calls_kwargs["external_message_type"] == "email"
        assert (
            message_calls_kwargs["subject"] == "This demonstrates the NUL bug"
        )
        assert message_calls_kwargs["participants"] == [
            {
                "role": "to",
                "display_name": "Zaaksysteem Bug",
                "address": "bug@development.zaaksysteem.nl",
            },
            {
                "role": "from",
                "display_name": "Zaaksysteem Bug",
                "address": "bug@development.zaaksysteem.nl",
            },
        ]
        assert message_calls_kwargs["thread_uuid"] == thread_uuid
        assert isinstance(message_calls_kwargs["external_message_uuid"], UUID)
        message_repo.save.assert_called()

    def test_import_email_no_subject(self):
        cmd = self.command
        self.mock_repo_factory.reset_mock()

        mock_repositories = {
            "attachment": mock.MagicMock(),
            "case": mock.MagicMock(),
            "document_file": mock.MagicMock(),
            "file": mock.MagicMock(),
            "message": mock.MagicMock(),
            "thread": mock.MagicMock(),
        }
        fake_message = email.message_from_string(
            MESSAGE_WITHOUT_SUBJECT,
            policy=DefaultMailPolicy,
        )

        thread_uuid = uuid4()
        thread = mock.MagicMock()
        thread.uuid = thread_uuid

        mock_repositories["file"].parse_as_message.return_value = fake_message
        mock_repositories["message"].get_email_configuration.return_value = {
            "subject_prefix": "PREFIX"
        }
        mock_repositories["thread"].create.return_value = thread

        def get_repository(name, context=None, event_service=None):
            return mock_repositories[name]

        self.mock_repo_factory.get_repository = get_repository

        cmd.import_email(str(uuid4()))

        thread_repo = mock_repositories["thread"]
        thread_calls = thread_repo.create.call_args_list
        assert len(thread_calls) == 1
        (thread_call_args, thread_call_kwargs) = thread_calls[0]
        assert thread_call_args == ()
        assert thread_call_kwargs["case"] is None
        assert thread_call_kwargs["contact"] is None
        assert thread_call_kwargs["thread_type"] == "external"
        assert isinstance(thread_call_kwargs["new_thread_uuid"], UUID)
        thread_repo.save.assert_called()

        message_repo = mock_repositories["message"]
        message_calls = message_repo.create_external_message.call_args_list
        assert len(message_calls) == 1
        (message_calls_args, message_calls_kwargs) = message_calls[0]
        assert message_calls_args == ()
        assert message_calls_kwargs["created_by"] is None
        assert message_calls_kwargs["external_message_type"] == "email"
        assert message_calls_kwargs["subject"] is None

    def test_import_email_complex(self):
        cmd = self.command
        self.mock_repo_factory.reset_mock()

        mock_repositories = {
            "attachment": mock.MagicMock(),
            "case": mock.MagicMock(),
            "document_file": mock.MagicMock(),
            "file": mock.MagicMock(),
            "message": mock.MagicMock(),
            "thread": mock.MagicMock(),
        }
        fake_message = email.message_from_string(
            COMPLEX_MESSAGE1,
            policy=DefaultMailPolicy,
        )

        thread_uuid = uuid4()
        thread = mock.MagicMock()
        thread.uuid = thread_uuid

        mock_repositories["file"].parse_as_message.return_value = fake_message
        mock_repositories["message"].get_email_configuration.return_value = {
            "subject_prefix": "PREFIX"
        }
        mock_repositories["thread"].create.return_value = thread

        def get_repository(name, context=None, event_service=None):
            return mock_repositories[name]

        self.mock_repo_factory.get_repository = get_repository

        cmd.import_email(str(uuid4()))

        thread_repo = mock_repositories["thread"]
        thread_calls = thread_repo.create.call_args_list
        assert len(thread_calls) == 1
        (thread_call_args, thread_call_kwargs) = thread_calls[0]
        assert thread_call_args == ()
        assert thread_call_kwargs["case"] is None
        assert thread_call_kwargs["contact"] is None
        assert thread_call_kwargs["thread_type"] == "external"
        assert isinstance(thread_call_kwargs["new_thread_uuid"], UUID)
        thread_repo.save.assert_called()

        message_repo = mock_repositories["message"]
        message_calls = message_repo.create_external_message.call_args_list
        assert len(message_calls) == 1
        (message_calls_args, message_calls_kwargs) = message_calls[0]
        assert message_calls_args == ()
        assert (
            message_calls_kwargs["content"]
            == """Test van inkomende mail!

🎃🎃🎃

Met vriendelijke groet,

Martijn



H.J.E. Wenckebachweg 90 4.01
1114 AD Amsterdam
T: 020 - 737 000 5
M: 06 - 482 537 94\n"""
        )
        assert message_calls_kwargs["created_by"] is None
        assert message_calls_kwargs["external_message_type"] == "email"
        assert message_calls_kwargs["subject"] == "Testing 123"
        assert message_calls_kwargs["participants"] == [
            {
                "role": "to",
                "display_name": "",
                "address": "test@development.zaaksysteem.nl",
            },
            {
                "role": "from",
                "display_name": "Martijn",
                "address": "martijn@mintlab.nl",
            },
        ]
        assert message_calls_kwargs["thread_uuid"] == thread_uuid
        assert isinstance(message_calls_kwargs["external_message_uuid"], UUID)
        message_repo.save.assert_called()

    def test_import_email_complex_smime(self):
        cmd = self.command
        self.mock_repo_factory.reset_mock()

        mock_repositories = {
            "attachment": mock.MagicMock(),
            "case": mock.MagicMock(),
            "document_file": mock.MagicMock(),
            "file": mock.MagicMock(),
            "message": mock.MagicMock(),
            "thread": mock.MagicMock(),
        }
        fake_message = email.message_from_string(
            COMPLEX_MESSAGE2,
            policy=DefaultMailPolicy,
        )

        thread_uuid = uuid4()
        thread = mock.MagicMock()
        thread.uuid = thread_uuid

        mock_repositories["file"].parse_as_message.return_value = fake_message
        mock_repositories["message"].get_email_configuration.return_value = {
            "subject_prefix": "PREFIX"
        }
        mock_repositories["thread"].create.return_value = thread

        def get_repository(name, context=None, event_service=None):
            return mock_repositories[name]

        self.mock_repo_factory.get_repository = get_repository

        cmd.import_email(str(uuid4()))

        file_repo = mock_repositories["document_file"]
        file_repo_calls = file_repo.create_file.call_args_list
        assert len(file_repo_calls) == 1

        (file_repo_call_args, file_repo_call_kwargs) = file_repo_calls[0]
        assert file_repo_call_args == ()
        assert file_repo_call_kwargs["filename"] == "smime.p7m"

    def test_create_contact_moment(self):
        cmd = self.command
        thread_uuid = str(uuid4())
        contact_uuid = str(uuid4())
        case_uuid = str(uuid4())
        cm_uuid = str(uuid4())

        channel = "email"
        content = "content"
        direction = "incoming"

        cmd.create_contact_moment(
            thread_uuid=thread_uuid,
            contact_uuid=contact_uuid,
            channel=channel,
            content=content,
            direction=direction,
            case_uuid=case_uuid,
            contact_moment_uuid=cm_uuid,
        )

        self.mock_repo_factory.get_repository.assert_called_with(
            name="message", context=None, event_service=None
        )
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.save.assert_called()
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.save.assert_called()

    def test_create_note(self):

        cmd = self.command
        thread_uuid = str(uuid4())
        contact_uuid = str(uuid4())
        case_uuid = str(uuid4())
        note_uuid = str(uuid4())

        content = "content"

        cmd.create_note(
            thread_uuid=thread_uuid,
            note_uuid=note_uuid,
            contact_uuid=contact_uuid,
            case_uuid=None,
            content=content,
        )

        self.mock_repo_factory.get_repository.assert_called_with(
            name="message", context=None, event_service=None
        )
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.save.assert_called()
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.save.assert_called()

        cmd.create_note(
            thread_uuid=thread_uuid,
            note_uuid=note_uuid,
            case_uuid=case_uuid,
            contact_uuid=None,
            content=content,
        )

        self.mock_repo_factory.get_repository.assert_called_with(
            name="message", context=None, event_service=None
        )
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.save.assert_called()
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.save.assert_called()

    def test_create_external_message_case_resolved(self):
        case = namedtuple("Case", "status")

        self.mock_repo_factory.get_repository(
            "case"
        ).get_case_for_pip_user.return_value = case(status="resolved")

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}
        self.command.user_info = user_info
        with pytest.raises(Conflict) as excinfo:
            self.command.create_external_message(
                thread_uuid=str(uuid4()),
                subject="email",
                content="some_content",
                message_type="pip",
                case_uuid=str(uuid4()),
                external_message_uuid=str(uuid4()),
                attachments=[{"id": str(uuid4()), "filename": "foo.docx"}],
                direction="unspecified",
            )
        assert excinfo.value.args == (
            "Not allowed to create message for 'resolved' case.",
            "communication/case/case_resolved",
        )

        user_info.permissions = {}
        self.mock_repo_factory.get_repository(
            "case"
        ).find_case_by_uuid.return_value = case(status="resolved")

        with pytest.raises(Conflict) as excinfo:
            self.command.create_external_message(
                thread_uuid=str(uuid4()),
                subject="email",
                content="some_content",
                message_type="pip",
                case_uuid=str(uuid4()),
                external_message_uuid=str(uuid4()),
                attachments=[{"id": str(uuid4()), "filename": "foo.docx"}],
                direction="unspecified",
            )
        assert excinfo.value.args == (
            "Not allowed to create message for 'resolved' case.",
            "communication/case/case_resolved",
        )

    def test_create_external_message(self):
        cmd = self.command
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        external_message_uuid = str(uuid4())

        subject = "email"
        content = "content"
        external_message_type = "pip"

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}
        cmd.user_info = user_info

        cmd.create_external_message(
            thread_uuid=thread_uuid,
            subject=subject,
            content=content,
            message_type=external_message_type,
            case_uuid=case_uuid,
            external_message_uuid=external_message_uuid,
            attachments=[{"id": str(uuid4()), "filename": "foo.docx"}],
            participants=[],
            direction="unspecified",
        )

        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.save.assert_called()

    def test_create_external_message_new(self):
        cmd = self.command
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        external_message_uuid = str(uuid4())

        subject = "email"
        content = "content"
        external_message_type = "pip"

        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = None

        case_repo = self.mock_repo_factory.get_repository("case")
        contact_repo = self.mock_repo_factory.get_repository("contact")

        user_info = mock.MagicMock()
        user_info.permissions = {}
        cmd.user_info = user_info

        cmd.create_external_message(
            thread_uuid=thread_uuid,
            subject=subject,
            content=content,
            message_type=external_message_type,
            case_uuid=case_uuid,
            external_message_uuid=external_message_uuid,
            attachments=[],
            direction="unspecified",
        )

        thread_repo.create.assert_called_once_with(
            new_thread_uuid=thread_uuid,
            thread_type="external",
            contact=contact_repo.get_requestor_contact_from_case(),
            case=case_repo.find_case_by_uuid(),
        )

        thread_repo.save.assert_called()
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.save.assert_called()

    def test_create_external_message_new_email(self):
        cmd = self.command
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        external_message_uuid = str(uuid4())

        subject = "email"
        content = "content"
        external_message_type = "email"
        participants = [
            {"address": "test@test.com", "role": "to", "display_name": "test"}
        ]

        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = None

        case_repo = self.mock_repo_factory.get_repository("case")

        user_info = mock.MagicMock()
        user_info.permissions = {}
        cmd.user_info = user_info

        cmd.create_external_message(
            thread_uuid=thread_uuid,
            subject=subject,
            content=content,
            message_type=external_message_type,
            case_uuid=case_uuid,
            external_message_uuid=external_message_uuid,
            attachments=[],
            direction="incoming",
            participants=participants,
        )

        thread_repo.create.assert_called_once_with(
            new_thread_uuid=thread_uuid,
            thread_type="external",
            contact=None,
            case=case_repo.find_case_by_uuid(),
        )

        thread_repo.save.assert_called()
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.save.assert_called()

    def test_link_thread_to_case(self):
        cmd = self.command

        user_info = mock.MagicMock()
        user_info.uuid = uuid4()
        user_info.permissions = {"pip_user": False}
        cmd.user_info = user_info

        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        message_uuid = uuid4()

        external_message_type = "email"
        case = Case(
            id=2,
            uuid=case_uuid,
            case_type_name=None,
            description=None,
            description_public=None,
            status="open",
        )

        thread = Thread(
            id=3,
            uuid=thread_uuid,
            case=None,
            last_message_cache={"message_type": "email"},
            unread_employee_count=0,
            unread_pip_count=0,
        )
        thread.event_service = mock.MagicMock()

        message_created_by = Contact(uuid=uuid4(), name="test")
        external_message = ExternalMessage(
            uuid=message_uuid,
            thread_uuid=thread_uuid,
            case_uuid=None,
            created_by=message_created_by,
            created_by_displayname="test",
            message_slug=None,
            last_modified="2019-04-06",
            created_date="2019-04-06",
            content="test_content",
            subject="test_subject",
            external_message_type="email",
            attachments=[],
            participants=[],
            direction="outgoing",
            original_message_file=None,
            read_employee="2018-05-05",
            read_pip=None,
        )
        external_message.event_service = mock.MagicMock()

        case_repo = self.mock_repo_factory.get_repository("case")
        case_repo.find_case_by_uuid.return_value = case

        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = thread

        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.get_messages_by_thread_uuid.return_value = [
            external_message
        ]

        cmd.link_thread_to_case(
            thread_uuid=thread_uuid,
            case_uuid=case_uuid,
            external_message_type=external_message_type,
        )

        thread_repo.save.assert_called()
        message_repo.save.assert_called()

    def test_link_thread_to_case_no_thread(self):
        cmd = self.command
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        external_message_type = "email"
        case = Case(
            id=2,
            uuid=case_uuid,
            case_type_name=None,
            description=None,
            description_public=None,
            status="open",
        )
        thread_repo = self.mock_repo_factory.get_repository("case")
        thread_repo.find_case_by_uuid.return_value = case

        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = None

        with pytest.raises(Conflict):
            cmd.link_thread_to_case(
                thread_uuid=thread_uuid,
                case_uuid=case_uuid,
                external_message_type=external_message_type,
            )

    def test_link_thread_resolved_case(self):
        case = Case(
            id=2,
            uuid=str(uuid4()),
            case_type_name=None,
            description=None,
            description_public=None,
            status="resolved",
        )
        self.mock_repo_factory.get_repository(
            "case"
        ).find_case_by_uuid.return_value = case

        with pytest.raises(Conflict) as excinfo:
            self.command.link_thread_to_case(
                thread_uuid=str(uuid4()),
                case_uuid=str(uuid4()),
                external_message_type="email",
            )
        assert excinfo.value.args == (
            "Not allowed to link thread to 'resolved' case.",
            "communication/case/case_resolved",
        )

    def test_create_external_message_pip_with_participants(self):
        cmd = self.command
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        external_message_uuid = str(uuid4())

        subject = "email"
        content = "content"
        external_message_type = "pip"

        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = None

        user_info = mock.MagicMock()
        user_info.permissions = {}
        cmd.user_info = user_info

        with pytest.raises(Conflict):
            cmd.create_external_message(
                thread_uuid=thread_uuid,
                subject=subject,
                content=content,
                message_type=external_message_type,
                case_uuid=case_uuid,
                external_message_uuid=external_message_uuid,
                attachments=[],
                participants=[
                    {
                        "role": "to",
                        "display_name": "Piet",
                        "address": "piet@frit.es",
                    }
                ],
                direction="incoming",
            )

    def test_delete_message_no_permission(self):
        cmd = self.command
        message_uuid = str(uuid4())
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = None

        with pytest.raises(Conflict) as excinfo:
            cmd.delete_message(message_uuid=message_uuid)

        assert excinfo.value.args == (
            f"No permission to delete message with uuid: '{message_uuid}'.",
            "communication/message/no_permission_for_delete",
        )

    def test_delete_message_resolved_case(self):
        cmd = self.command
        message_uuid = str(uuid4())
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread = mock.MagicMock()
        thread.case.status = "resolved"
        thread_repo.get_thread_by_uuid.return_value = thread

        with pytest.raises(Conflict) as excinfo:
            cmd.delete_message(message_uuid=message_uuid)

        assert excinfo.value.args == (
            "Can not delete messages from case with status: 'resolved'.",
            "communication/case/case_resolved",
        )

    def test_delete_message(self):
        cmd = self.command
        self.mock_repo_factory.reset()
        message_uuid = str(uuid4())

        message = mock.MagicMock()
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.get_message_by_uuid.return_value = message

        thread = mock.MagicMock()
        thread.number_of_messages = 1
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = thread

        cmd.delete_message(message_uuid=message_uuid)

        message.delete.assert_called_once()
        thread.delete.assert_called_once()

        message_repo.save.assert_called()
        thread_repo.save.assert_called()

    def test_delete_message_external(self):
        cmd = self.command
        self.mock_repo_factory.reset()
        message_uuid = str(uuid4())

        message = mock.MagicMock()
        message.message_type = "external"
        message.attachments = [1, 2, 3]
        message.read_pip = None
        message.read_employee = None

        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.get_message_by_uuid.return_value = message

        thread = mock.MagicMock()
        thread.number_of_messages = 3
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = thread

        cmd.delete_message(message_uuid=message_uuid)

        message.delete.assert_called_once()
        calls = [mock.call(context="pip"), mock.call(context="employee")]
        thread.decrement_unread_count.assert_has_calls(calls)
        thread.decrement_attachment_count.assert_called()

        message_repo.save.assert_called()
        thread_repo.save.assert_called()

    def test_create_external_message_email_as_pipuser(self):
        cmd = self.command
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        external_message_uuid = str(uuid4())

        subject = "email"
        content = "content"
        external_message_type = "email"

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}
        cmd.user_info = user_info

        with pytest.raises(Forbidden):
            cmd.create_external_message(
                thread_uuid=thread_uuid,
                subject=subject,
                content=content,
                message_type=external_message_type,
                case_uuid=case_uuid,
                external_message_uuid=external_message_uuid,
                attachments=[{"id": str(uuid4()), "filename": "foo.docx"}],
                participants=[],
                direction="outgoing",
            )

    def test_create_external_message_email(self):
        cmd = self.command
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        external_message_uuid = str(uuid4())

        subject = "email"
        content = "content"
        external_message_type = "email"

        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = None

        user_info = mock.MagicMock()
        user_info.permissions = {}
        cmd.user_info = user_info

        cmd.create_external_message(
            thread_uuid=thread_uuid,
            subject=subject,
            content=content,
            message_type=external_message_type,
            case_uuid=case_uuid,
            external_message_uuid=external_message_uuid,
            attachments=[],
            participants=[
                {
                    "role": "to",
                    "display_name": "Piet",
                    "address": "piet@frit.es",
                }
            ],
            direction="incoming",
        )

        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.create_external_message.assert_called_once_with(
            external_message_uuid=external_message_uuid,
            case_id=self.mock_repo_factory.get_repository("case")
            .find_case_by_uuid()
            .id,
            thread_uuid=thread_uuid,
            subject=subject,
            content=content,
            created_by=message_repo.get_contact_by_uuid(),
            external_message_type="email",
            participants=[
                {
                    "role": "to",
                    "display_name": "Piet",
                    "address": "piet@frit.es",
                }
            ],
            direction="incoming",
            original_message_file=None,
            message_date=None,
            creator_type="employee",
        )

    def test_create_external_message_errors(self):
        cmd = self.command
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        external_message_uuid = str(uuid4())

        subject = "email"
        content = "content"

        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = None

        user_info = mock.MagicMock()
        user_info.permissions = {}
        cmd.user_info = user_info

        with pytest.raises(Conflict):
            cmd.create_external_message(
                thread_uuid=thread_uuid,
                subject=subject,
                content=content,
                case_uuid=case_uuid,
                external_message_uuid=external_message_uuid,
                attachments=[],
                participants=[],
                message_type="pip",
                direction="incoming",
            )

        with pytest.raises(Conflict):
            cmd.create_external_message(
                thread_uuid=thread_uuid,
                subject=subject,
                content=content,
                case_uuid=case_uuid,
                external_message_uuid=external_message_uuid,
                attachments=[],
                participants=[
                    {
                        "role": "to",
                        "display_name": "Piet",
                        "address": "piet@frit.es",
                    }
                ],
                message_type="email",
                direction="unspecified",
            )

    def test_send_email(self):
        cmd = self.command
        message_uuid = str(uuid4())

        message = mock.MagicMock()
        message.uuid = message_uuid

        cmd.get_repository(
            "message"
        ).get_message_by_uuid.return_value = message

        cmd.send_email(message_uuid=message_uuid)

        cmd.get_repository(
            "message"
        ).get_message_by_uuid.assert_called_once_with(message_uuid)
        message.send.assert_called_once_with()

    def test_get_thread_from_message_with_caseid_in_subject(self):
        thread_uuid = str(uuid4())
        message = EmailMessage()
        message["subject"] = "[PREFIX 1337 abcde] Rest of the subject"

        thread_repository = mock.MagicMock()
        case_repository = mock.MagicMock()

        thread = mock.MagicMock()
        thread.uuid = thread_uuid

        thread_repository.get_thread_by_partial_uuid.return_value = thread
        thread_repository.get_thread_by_partial_uuid().case.id = 42

        found_thread_info = _get_thread_from_message(
            subject_prefix="PREFIX",
            message=message,
            thread_repository=thread_repository,
            case_repository=case_repository,
        )

        assert found_thread_info == {"thread": thread, "case_id": 42}
        thread_repository.get_thread_by_partial_uuid.assert_called_with(
            "abcde"
        )

    def test_get_thread_from_message(self):
        thread_uuid = str(uuid4())
        message = EmailMessage()
        message["subject"] = "[PREFIX abcde] Rest of the subject"

        thread_repository = mock.MagicMock()
        case_repository = mock.MagicMock()

        thread = mock.MagicMock()
        thread.uuid = thread_uuid

        thread_repository.get_thread_by_partial_uuid.return_value = thread
        # thread_repository.get_thread_by_partial_uuid().uuid = thread_uuid
        thread_repository.get_thread_by_partial_uuid().case.id = 42

        found_thread_info = _get_thread_from_message(
            subject_prefix="PREFIX",
            message=message,
            thread_repository=thread_repository,
            case_repository=case_repository,
        )

        assert found_thread_info == {"thread": thread, "case_id": 42}
        thread_repository.get_thread_by_partial_uuid.assert_called_with(
            "abcde"
        )

    @mock.patch("zsnl_domains.communication.commands.uuid4")
    def test_get_thread_from_message_exception(self, mock_uuid4):
        thread_id = mock_uuid4.return_value = uuid4()

        message = EmailMessage()
        message["subject"] = "[PREFIX abcde] Rest of the subject"

        thread_repository = mock.MagicMock()
        case_repository = mock.MagicMock()

        thread = mock.MagicMock()
        thread.uuid = thread_id

        thread_repository.create.return_value = thread
        thread_repository.get_thread_by_partial_uuid.side_effect = Conflict()

        found_thread_info = _get_thread_from_message(
            subject_prefix="PREFIX",
            message=message,
            thread_repository=thread_repository,
            case_repository=case_repository,
        )

        assert found_thread_info == {"thread": thread, "case_id": None}
        thread_repository.get_thread_by_partial_uuid.assert_called_with(
            "abcde"
        )

        thread_repository.create.assert_called_once_with(
            new_thread_uuid=thread_id,
            contact=None,
            case=None,
            thread_type="external",
        )

    @mock.patch(
        "zsnl_domains.communication.commands._get_case_from_message_subject"
    )
    @mock.patch("zsnl_domains.communication.commands.uuid4")
    def test_get_thread_from_message_old_style(
        self, mock_uuid4, mock_get_case
    ):
        mock_case = Case(
            id=123,
            uuid=uuid4(),
            description=None,
            description_public=None,
            case_type_name=None,
            status="open",
        )
        thread_id = mock_uuid4.return_value = uuid4()
        mock_get_case.return_value = mock_case

        message = EmailMessage()
        message["subject"] = "[PREFIX 123-abcde] Rest of the subject"

        thread_repository = mock.MagicMock()
        case_repository = mock.MagicMock()

        thread = mock.MagicMock()
        thread.uuid = thread_id

        thread_repository.create.return_value = thread

        found_thread_info = _get_thread_from_message(
            subject_prefix="PREFIX",
            message=message,
            thread_repository=thread_repository,
            case_repository=case_repository,
        )

        thread_repository.get_thread_by_partial_uuid.assert_not_called()

        mock_get_case.assert_called_once_with(
            subject=message["subject"],
            subject_prefix="PREFIX",
            case_repository=case_repository,
        )

        thread_repository.create.assert_called_once_with(
            new_thread_uuid=thread_id,
            contact=None,
            case=mock_case,
            thread_type="external",
        )

        assert found_thread_info == {"thread": thread, "case_id": mock_case.id}

    def test_get_case_from_message_subject(self):
        case_repository = mock.MagicMock()
        case_uuid = uuid4()

        case = Case(
            id=123,
            uuid=case_uuid,
            description="X",
            description_public=None,
            case_type_name="Y",
            status="open",
        )

        case_repository.get_case_by_id.return_value = case

        found_case = _get_case_from_message_subject(
            subject=f"[PREFIX 123-{str(case_uuid)[-6:]}] Some email subject",
            subject_prefix="PREFIX",
            case_repository=case_repository,
        )

        assert found_case == case

    def test_get_case_from_message_subject_found_wronguuid(self):
        case_repository = mock.MagicMock()
        case_uuid = UUID("3bbaa7a1-0802-4d97-8f05-42e3fb996bc5")

        case = Case(
            id=123,
            uuid=case_uuid,
            description="X",
            description_public=None,
            case_type_name="Y",
            status="open",
        )

        case_repository.get_case_by_id.return_value = case

        found_case = _get_case_from_message_subject(
            subject="[PREFIX 123-fffff] Some email subject",
            subject_prefix="PREFIX",
            case_repository=case_repository,
        )

        assert found_case is None

    def test_get_case_from_message_subject_notfound(self):
        case_repository = mock.MagicMock()

        case_repository.get_case_by_id.side_effect = NotFound

        found_case = _get_case_from_message_subject(
            subject="[PREFIX 123-fffff] Some email subject",
            subject_prefix="PREFIX",
            case_repository=case_repository,
        )

        assert found_case is None

    def test_mark_messages_read_pip(self):
        cmd = self.command
        thread_uuid = uuid4()
        message_uuid = str(uuid4())

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}
        self.command.user_info = user_info

        self.mock_repo_factory.reset()
        message_uuids = [message_uuid]
        timestamp = str(datetime.datetime.now())

        message = mock.MagicMock()
        message.uuid = message_uuid
        message.thread_uuid = thread_uuid
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.get_messages_by_uuid.return_value = [message]

        thread = mock.MagicMock()
        thread.uuid = thread_uuid
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_for_pip_by_uuid.return_value = thread

        cmd.mark_messages_read(
            message_uuids=message_uuids, context="pip", timestamp=timestamp
        )

        message.mark_read.assert_called_once_with(
            context="pip", timestamp=timestamp
        )
        thread.decrement_unread_count.assert_called_once_with(context="pip")
        message_repo.save.assert_called()
        thread_repo.save.assert_called()

    def test_mark_messages_read_employee(self):
        cmd = self.command
        thread_uuid = uuid4()
        message_uuid = str(uuid4())

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.command.user_info = user_info

        self.mock_repo_factory.reset()
        message_uuids = [message_uuid]
        timestamp = str(datetime.datetime.now())

        message = mock.MagicMock()
        message.uuid = message_uuid
        message.thread_uuid = thread_uuid
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.get_messages_by_uuid.return_value = [message]

        thread = mock.MagicMock()
        thread.uuid = thread_uuid
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = thread

        cmd.mark_messages_read(
            message_uuids=message_uuids,
            context="employee",
            timestamp=timestamp,
        )

        message.mark_read.assert_called_once_with(
            context="employee", timestamp=timestamp
        )
        thread.decrement_unread_count.assert_called_once_with(
            context="employee"
        )
        message_repo.save.assert_called()
        thread_repo.save.assert_called()

    def test_mark_messages_unread_employee(self):

        cmd = self.command
        thread_uuid = uuid4()
        message_uuid = str(uuid4())

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": True}
        self.command.user_info = user_info

        self.mock_repo_factory.reset()
        message_uuids = [message_uuid]
        timestamp = datetime.datetime.now()

        message = mock.MagicMock()
        message.read_employee = timestamp
        message.read_pip = timestamp
        message.uuid = message_uuid
        message.thread_uuid = thread_uuid

        thread = mock.MagicMock()
        thread.uuid = thread_uuid

        message_repo = self.mock_repo_factory.get_repository("message")
        thread_repo = self.mock_repo_factory.get_repository("thread")
        message_repo.get_messages_by_uuid.return_value = [message]
        thread_repo.get_thread_by_uuid.return_value = thread

        cmd.mark_messages_unread(
            message_uuids=message_uuids,
            context="employee",
        )

        message.mark_unread.assert_called_once_with(context="employee")
        thread.increment_unread_count.assert_called_once_with(
            context="employee"
        )

        message_repo.save.assert_called()
        thread_repo.save.assert_called()

    def test_mark_messages_unread_pip(self):

        cmd = self.command
        thread_uuid = uuid4()
        message_uuid = str(uuid4())

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.command.user_info = user_info

        self.mock_repo_factory.reset()
        message_uuids = [message_uuid]
        timestamp = datetime.datetime.now()

        message = mock.MagicMock()
        message.read_employee = timestamp
        message.read_pip = timestamp
        message.uuid = message_uuid
        message.thread_uuid = thread_uuid

        thread = mock.MagicMock()
        thread.uuid = thread_uuid

        message_repo = self.mock_repo_factory.get_repository("message")
        thread_repo = self.mock_repo_factory.get_repository("thread")
        message_repo.get_messages_by_uuid.return_value = [message]
        thread_repo.get_thread_for_pip_by_uuid.return_value = thread

        cmd.mark_messages_unread(
            message_uuids=message_uuids,
            context="pip",
        )

        message.mark_unread.assert_called_once_with(context="pip")
        thread.increment_unread_count.assert_called_once_with(context="pip")

        message_repo.save.assert_called()
        thread_repo.save.assert_called()

    def test_mark_messages_read_exception(self):
        cmd = self.command
        thread_uuid = uuid4()
        message_uuid = str(uuid4())

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.command.user_info = user_info

        self.mock_repo_factory.reset()
        message_uuids = [message_uuid]
        timestamp = str(datetime.datetime.now())

        message = mock.MagicMock()
        message.uuid = message_uuid
        message.thread_uuid = thread_uuid
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.get_messages_by_uuid.return_value = [message]

        thread = mock.MagicMock()
        thread.uuid = thread_uuid
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value.return_value = thread

        with pytest.raises(Conflict) as excinfo:
            thread_repo.get_thread_by_uuid.return_value = None
            cmd.mark_messages_read(
                message_uuids=message_uuids,
                context="employee",
                timestamp=timestamp,
            )
        assert excinfo.value.args == (
            f"No permission to mark messages read for thread with uuid: '{thread_uuid}'.",
            "communication/message/no_permission_to_mark_read",
        )

        with pytest.raises(Conflict) as excinfo:
            thread_repo.get_messages_by_uuid.return_value = []
            cmd.mark_messages_read(
                message_uuids=message_uuids,
                context="employee",
                timestamp=timestamp,
            )
        assert excinfo.value.args == (
            "User do not have access to any of the messages",
            "communication/message/no_permission_to_mark_read",
        )

        with pytest.raises(Conflict) as excinfo:
            message_1 = mock.MagicMock()
            message_1.uuid = uuid4()
            message_1.thread_uuid = uuid4()
            message_repo.get_messages_by_uuid.return_value = [
                message,
                message_1,
            ]
            thread_repo.get_thread_by_uuid.return_value = thread
            cmd.mark_messages_read(
                message_uuids=message_uuids,
                context="employee",
                timestamp=timestamp,
            )
        assert excinfo.value.args == (
            f"Message with uuid {message_1.uuid} does not belong to thread with uuid {thread_uuid}",
            "communication/message/no_permission_to_mark_read",
        )

    def test_mark_messages_unread_exception(self):
        cmd = self.command
        thread_uuid = uuid4()
        message_uuid = str(uuid4())

        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.command.user_info = user_info

        self.mock_repo_factory.reset()
        message_uuids = [message_uuid]

        message = mock.MagicMock()
        message.uuid = message_uuid
        message.thread_uuid = thread_uuid
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.get_messages_by_uuid.return_value = [message]

        thread = mock.MagicMock()
        thread.uuid = thread_uuid
        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value.return_value = thread

        with pytest.raises(Conflict) as excinfo:
            thread_repo.get_thread_by_uuid.return_value = None
            cmd.mark_messages_unread(
                message_uuids=message_uuids,
                context="employee",
            )
        assert excinfo.value.args == (
            f"No permission to mark messages unread for thread with uuid: '{thread_uuid}'.",
            "communication/message/mark_unread/no_permission",
        )

        with pytest.raises(Conflict) as excinfo:
            thread_repo.get_messages_by_uuid.return_value = []
            cmd.mark_messages_unread(
                message_uuids=message_uuids,
                context="employee",
            )
        assert excinfo.value.args == (
            "User does not have access to any of the messages",
            "communication/message/mark_unread/no_permission",
        )

        with pytest.raises(Conflict) as excinfo:
            message_1 = mock.MagicMock()
            message_1.uuid = uuid4()
            message_1.thread_uuid = uuid4()
            message_repo.get_messages_by_uuid.return_value = [
                message,
                message_1,
            ]
            thread_repo.get_thread_by_uuid.return_value = thread
            cmd.mark_messages_unread(
                message_uuids=message_uuids,
                context="employee",
            )
        assert excinfo.value.args == (
            f"Message with uuid {message_1.uuid} does not belong to thread with uuid {thread_uuid}",
            "communication/message/mark_unread/thread_id_mismatch",
        )

    def test_create_new_email_thread_with_case_assignee_as_participant(self):
        """
        Test create email external message thread when the 'ontvanger' of email id case_assignee.
        """

        cmd = self.command
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        external_message_uuid = str(uuid4())

        subject = "email"
        content = "content"
        external_message_type = "email"

        requestor_uuid = uuid4()
        assignee_uuid = uuid4()
        case_requestor = Contact(
            uuid=requestor_uuid,
            name="1",
            id=1,
            address=None,
            email_address="test1@test.com",
        )
        case_assignee = Contact(
            uuid=assignee_uuid,
            name="test2",
            id=2,
            address=None,
            email_address="test2@test.com",
        )
        subject_relations = [case_requestor, case_assignee]

        contact_repo = self.mock_repo_factory.get_repository("contact")
        contact_repo.get_requestor_contact_from_case.return_value = (
            case_requestor
        )
        contact_repo.get_subject_relation_contacts_from_case.return_value = (
            subject_relations
        )

        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = None

        case_repo = self.mock_repo_factory.get_repository("case")

        user_info = mock.MagicMock()
        user_info.permissions = {}
        cmd.user_info = user_info

        participants = [
            {
                "address": "test2@test.com",
                "role": "to",
                "display_name": "test2",
                "uuid": str(assignee_uuid),
            }
        ]

        cmd.create_external_message(
            thread_uuid=thread_uuid,
            subject=subject,
            content=content,
            message_type=external_message_type,
            case_uuid=case_uuid,
            external_message_uuid=external_message_uuid,
            attachments=[],
            direction="incoming",
            participants=participants,
        )

        thread_repo.create.assert_called_once_with(
            new_thread_uuid=thread_uuid,
            thread_type="external",
            contact=case_assignee,
            case=case_repo.find_case_by_uuid(),
        )

        thread_repo.save.assert_called()
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.save.assert_called()

    def test_create_new_email_thread_with_requestor_as_participant(self):
        """
        Test create email external message thread when the 'ontvanger' of email is case_requestor.
        """

        cmd = self.command
        thread_uuid = str(uuid4())
        case_uuid = str(uuid4())
        external_message_uuid = str(uuid4())

        subject = "email"
        content = "content"
        external_message_type = "email"
        requestor_uuid = uuid4()
        participants = [
            {
                "address": "test@test.com",
                "role": "to",
                "display_name": "test",
                "uuid": str(requestor_uuid),
            }
        ]

        contact_repo = self.mock_repo_factory.get_repository("contact")
        case_requestor = Contact(
            uuid=requestor_uuid,
            name="test",
            id=3,
            address=None,
            email_address="test@test.com",
        )
        contact_repo.get_requestor_contact_from_case.return_value = (
            case_requestor
        )
        contact_repo.get_subject_relation_contacts_from_case.return_value = [
            case_requestor
        ]

        thread_repo = self.mock_repo_factory.get_repository("thread")
        thread_repo.get_thread_by_uuid.return_value = None

        case_repo = self.mock_repo_factory.get_repository("case")

        user_info = mock.MagicMock()
        user_info.permissions = {}
        cmd.user_info = user_info

        cmd.create_external_message(
            thread_uuid=thread_uuid,
            subject=subject,
            content=content,
            message_type=external_message_type,
            case_uuid=case_uuid,
            external_message_uuid=external_message_uuid,
            attachments=[],
            direction="incoming",
            participants=participants,
        )

        thread_repo.create.assert_called_once_with(
            new_thread_uuid=thread_uuid,
            thread_type="external",
            contact=case_requestor,
            case=case_repo.find_case_by_uuid(),
        )

        thread_repo.save.assert_called()
        message_repo = self.mock_repo_factory.get_repository("message")
        message_repo.save.assert_called()


class TestCommandImportMessageClass:
    def setup_method(self):
        self.mock_repo_factory = mock.MagicMock()
        self.command = Commands(
            repository_factory=self.mock_repo_factory,
            context=None,
            user_uuid=None,
            event_service=None,
        )
        user_info = mock.MagicMock()
        user_info.permissions = {"pip_user": False}
        self.command.user_info = user_info
        self.mock_repositories = {
            "attachment": mock.MagicMock(),
            "case": mock.MagicMock(),
            "document_file": mock.MagicMock(),
            "file": mock.MagicMock(),
            "message": mock.MagicMock(),
            "thread": mock.MagicMock(),
            "contact": mock.MagicMock(),
        }

        def get_repository(name, context=None, event_service=None):
            return self.mock_repositories[name]

        self.mock_repo_factory.get_repository = get_repository

    def test_process_email_file_incorrect_mimetype(self):
        case_uuid = str(uuid4())
        file_uuid = str(uuid4())
        f = File(
            uuid=file_uuid,
            filename="test.pdf",
            md5="1234-md5",
            size=124,
            mimetype="application/pdf",
            date_created="asda",
            storage_location="s3",
        )

        self.mock_repo_factory.get_repository(
            "file"
        ).get_file_by_uuid.return_value = f
        with pytest.raises(Conflict):
            self.command.process_email_file(
                case_uuid=case_uuid, file_uuid=file_uuid
            )

    @mock.patch(
        "zsnl_domains.communication.commands._upload_and_create_attachment"
    )
    def test_process_email_file(self, mock_attachment_upload):
        case_uuid = str(uuid4())
        file_uuid = str(uuid4())
        c = Case(
            id=1234,
            uuid=case_uuid,
            description="description",
            description_public="description_public",
            status="open",
            case_type_name="some case_type",
        )
        f = File(
            uuid=file_uuid,
            filename="test.eml",
            md5="1234-md5",
            size=124,
            mimetype="message/rfc822",
            date_created="asda",
            storage_location="s3",
        )
        mock_attachment_upload.filename = "File name goes here.exe"
        mock_attachment_upload.file_uuid = str(uuid4())
        with open("tests/data/102-attachment_mail_example.mail", "rb") as fh:
            msg = email.message_from_binary_file(fh, policy=DefaultMailPolicy)
            self.mock_repo_factory.get_repository(
                "file"
            ).get_file_by_uuid.return_value = f
            self.mock_repo_factory.get_repository(
                "case"
            ).find_case_by_uuid.return_value = c
            self.mock_repo_factory.get_repository(
                "file"
            ).parse_as_message.return_value = msg

            self.command.process_email_file(
                case_uuid=case_uuid, file_uuid=file_uuid
            )

        self.mock_repo_factory.get_repository("thread").create.assert_called()
        self.mock_repo_factory.get_repository(
            "message"
        ).create_external_message.assert_called()
        self.mock_repo_factory.get_repository(
            "attachment"
        ).attach_file_to_message.assert_called()
        self.mock_repo_factory.get_repository("thread").save.assert_called()
        self.mock_repo_factory.get_repository("message").save.assert_called()
        self.mock_repo_factory.get_repository(
            "document_file"
        ).save.assert_called()
        self.mock_repo_factory.get_repository(
            "attachment"
        ).save.assert_called()

    @mock.patch(
        "zsnl_domains.communication.commands._upload_and_create_attachment"
    )
    def test_process_email_file_error(self, mock_attachment_upload):
        case_uuid = str(uuid4())
        file_uuid = str(uuid4())
        c = Case(
            id=1234,
            uuid=case_uuid,
            description="description",
            description_public="description_public",
            status="open",
            case_type_name="some case_type",
        )
        f = File(
            uuid=file_uuid,
            filename="test.eml",
            md5="1234-md5",
            size=124,
            mimetype="message/rfc822",
            date_created="asda",
            storage_location="s3",
        )
        mock_attachment_upload.side_effect = Forbidden

        with open("tests/data/email_with_email.eml", "rb") as fh:
            msg = email.message_from_binary_file(fh, policy=DefaultMailPolicy)
            self.mock_repo_factory.get_repository(
                "file"
            ).get_file_by_uuid.return_value = f
            self.mock_repo_factory.get_repository(
                "case"
            ).find_case_by_uuid.return_value = c
            self.mock_repo_factory.get_repository(
                "file"
            ).parse_as_message.return_value = msg

            self.command.process_email_file(
                case_uuid=case_uuid, file_uuid=file_uuid
            )

        self.mock_repo_factory.get_repository("thread").create.assert_called()
        self.mock_repo_factory.get_repository(
            "message"
        ).create_external_message.assert_called()
        self.mock_repo_factory.get_repository(
            "attachment"
        ).attach_file_to_message.assert_not_called()
        self.mock_repo_factory.get_repository("thread").save.assert_called()
        self.mock_repo_factory.get_repository("message").save.assert_called()
        self.mock_repo_factory.get_repository(
            "document_file"
        ).save.assert_called()
        self.mock_repo_factory.get_repository(
            "attachment"
        ).save.assert_called()

    @mock.patch(
        "zsnl_domains.communication.commands._upload_and_create_attachment"
    )
    def test_process_email_file_smime(self, mock_attachment_upload):
        case_uuid = str(uuid4())
        file_uuid = str(uuid4())
        c = Case(
            id=1234,
            uuid=case_uuid,
            description="description",
            description_public="description_public",
            status="open",
            case_type_name="some case_type",
        )
        f = File(
            uuid=file_uuid,
            filename="test.eml",
            md5="1234-md5",
            size=124,
            mimetype="message/rfc822",
            date_created="asda",
            storage_location="s3",
        )
        mock_attachment_upload.filename = "File name goes here.exe"
        mock_attachment_upload.file_uuid = str(uuid4())

        msg = email.message_from_string(
            COMPLEX_MESSAGE2, policy=DefaultMailPolicy
        )
        self.mock_repo_factory.get_repository(
            "file"
        ).get_file_by_uuid.return_value = f
        self.mock_repo_factory.get_repository(
            "case"
        ).find_case_by_uuid.return_value = c
        self.mock_repo_factory.get_repository(
            "file"
        ).parse_as_message.return_value = msg

        self.command.process_email_file(
            case_uuid=case_uuid, file_uuid=file_uuid
        )

        self.mock_repo_factory.get_repository("thread").create.assert_called()
        self.mock_repo_factory.get_repository(
            "message"
        ).create_external_message.assert_called()
        self.mock_repo_factory.get_repository(
            "attachment"
        ).attach_file_to_message.assert_called()
        self.mock_repo_factory.get_repository("thread").save.assert_called()
        self.mock_repo_factory.get_repository("message").save.assert_called()
        self.mock_repo_factory.get_repository(
            "document_file"
        ).save.assert_called()
        self.mock_repo_factory.get_repository(
            "attachment"
        ).save.assert_called()

    def test_process_email_file_msg(self):
        case_uuid = str(uuid4())
        file_uuid = str(uuid4())
        c = Case(
            id=1234,
            uuid=case_uuid,
            description="description",
            description_public="description_public",
            status="open",
            case_type_name="some case_type",
        )
        f = File(
            uuid=file_uuid,
            filename="test.msg",
            md5="1234-md5",
            size=124,
            mimetype="application/vnd.ms-outlook",
            date_created="asda",
            storage_location="s3",
        )
        self.mock_repo_factory.get_repository(
            "file"
        ).get_file_by_uuid.return_value = f
        self.mock_repo_factory.get_repository(
            "case"
        ).find_case_by_uuid.return_value = c
        self.mock_repo_factory.get_repository(
            "file"
        ).parse_as_message_outlook.return_value = email.message_from_string(
            "From: Me <me@example.com>\n"
            "To: You <you@example.com>\n"
            "Cc: Someone <someone@example.com>\n"
            "Cc: Third Person <third@example.com>\n"
            "Subject: You know\n"
            "Content-Type: multipart/mixed; boundary=abc\n"
            "Date: Sat, 30 Apr 2005 19:28:29\n"
            "\n"
            "--abc\n"
            "Content-Type: text/plain\n"
            "\n"
            "This is the body\n"
            "--abc\n"
            "Content-Type: image/png\n"
            "\n"
            "Not really a PNG"
            "--abc--\n",
            policy=DefaultMailPolicy,
        )

        # Code under test:
        self.command.process_email_file(
            case_uuid=case_uuid, file_uuid=file_uuid
        )

        self.mock_repo_factory.get_repository("thread").create.assert_called()
        self.mock_repo_factory.get_repository(
            "message"
        ).create_external_message.assert_called()

        thread_calls = self.mock_repo_factory.get_repository(
            "message"
        ).create_external_message.call_args_list
        (thread_call_args, thread_call_kwargs) = thread_calls[0]

        assert thread_call_kwargs["message_date"] == datetime.datetime(
            2005, 4, 30, 19, 28, 29, tzinfo=datetime.timezone.utc
        )
        self.mock_repo_factory.get_repository(
            "attachment"
        ).attach_file_to_message.assert_called()
        self.mock_repo_factory.get_repository("thread").save.assert_called()
        self.mock_repo_factory.get_repository("message").save.assert_called()
        self.mock_repo_factory.get_repository(
            "document_file"
        ).save.assert_called()
        self.mock_repo_factory.get_repository(
            "attachment"
        ).save.assert_called()


class TestCommandHelperFunctions:
    def test__get_participants_message_email_message(self):
        fake_message = email.message_from_string(
            "From: Me <me@example.com>\n"
            "To: You <you@example.com>\n"
            "Cc: Someone <someone@example.com>\n"
            "Cc: Third Person <third@example.com>\n"
            "Subject: You know\n"
            "Content-Type: text/plain\n"
            "\n"
            "This is the body\n",
            policy=DefaultMailPolicy,
        )
        participants = _get_participants(email_message=fake_message)

        assert participants == [
            {
                "role": "to",
                "display_name": "You",
                "address": "you@example.com",
            },
            {
                "role": "cc",
                "display_name": "Someone",
                "address": "someone@example.com",
            },
            {
                "role": "cc",
                "display_name": "Third Person",
                "address": "third@example.com",
            },
            {
                "role": "from",
                "display_name": "Me",
                "address": "me@example.com",
            },
        ]

    def test_mark_message_read(self):
        message = mock.MagicMock()
        message.direction = "incoming"

        thread = mock.MagicMock()

        _mark_message_read(
            message=message,
            thread=thread,
            context="pip",
            timestamp="test_timestamp",
        )
        message.mark_read.assert_called_with(
            context="pip", timestamp="test_timestamp"
        )
        thread.decrement_unread_count.assert_called_once_with(context="pip")

    def test__get_thread_contact_from_participants(self):
        requestor_uuid = uuid4()
        assignee_uuid = uuid4()
        coordinator_uuid = uuid4()

        case_requestor = Contact(
            uuid=requestor_uuid,
            name="test1",
            id=1,
            address=None,
            email_address="test1@test.com",
        )
        case_assignee = Contact(
            uuid=assignee_uuid,
            name="test2",
            id=2,
            address=None,
            email_address="test2@test.com",
        )
        case_coordinator = Contact(
            uuid=coordinator_uuid,
            name="test3",
            id=3,
            address=None,
            email_address="test3@test.com",
        )

        case_subject_relations = [
            case_requestor,
            case_assignee,
            case_coordinator,
        ]

        res = _get_thread_contact_from_participants(
            participants=[],
            case_requestor=case_requestor,
            case_subject_relations=case_subject_relations,
        )
        assert res is None

        participants = [
            {
                "address": "test1@test.com",
                "role": "to",
                "display_name": "test1",
                "uuid": str(case_requestor.uuid),
            }
        ]
        res = _get_thread_contact_from_participants(
            participants=participants,
            case_requestor=case_requestor,
            case_subject_relations=case_subject_relations,
        )
        assert res == case_requestor

        participants = [
            {
                "address": "test2@test.com",
                "role": "to",
                "display_name": "test2",
                "uuid": str(case_assignee.uuid),
            }
        ]
        res = _get_thread_contact_from_participants(
            participants=participants,
            case_requestor=case_requestor,
            case_subject_relations=case_subject_relations,
        )
        assert res == case_assignee

        participants = [
            {
                "address": "test2@test.com",
                "role": "to",
                "display_name": "test2",
                "uuid": str(case_assignee.uuid),
            },
            {
                "address": "test3@test.com",
                "role": "to",
                "display_name": "test3",
                "uuid": str(case_coordinator.uuid),
            },
        ]
        res = _get_thread_contact_from_participants(
            participants=participants,
            case_requestor=case_requestor,
            case_subject_relations=case_subject_relations,
        )
        assert res is None


class Test_as_user_want_to_generate_pdf_derivative_for_an_attachment(
    test.TestBase
):
    def setup_method(self):
        inframocks = {"s3": mock.MagicMock(), "converter": mock.MagicMock()}
        self.load_command_instance(communication, inframocks)

        self.attachment_uuid = str(uuid4())

        self.mock_get_attachment = mock.MagicMock()
        self.attachment = namedtuple(
            "AttachedFile",
            [
                "uuid",
                "original_name",
                "size",
                "mimetype",
                "md5",
                "date_created",
                "storage_location",
                "attachment_uuid",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
            ],
        )(
            uuid=uuid4(),
            original_name="testdoc1",
            size="123",
            mimetype="application/msword",
            md5="123",
            date_created="2019-05-02",
            storage_location=["Minio"],
            attachment_uuid=self.attachment_uuid,
            preview_uuid=None,
            preview_mimetype=None,
            preview_storage_location=None,
        )

        self.mock_get_attachment.fetchone.return_value = self.attachment
        self.insert_attachment_derivative_file = mock.MagicMock()
        self.insert_attachment_derivative = mock.MagicMock()

        self.s3 = inframocks["s3"]
        self.converter = inframocks["converter"]

    def test_generate_pdf_derivative(self):
        mock_rows = [
            self.mock_get_attachment,
            self.insert_attachment_derivative_file,
            self.insert_attachment_derivative,
        ]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        self.s3.upload.return_value = {
            "storage_location": "Minio",
            "mime_type": "application/pdf",
            "md5": "123",
            "size": 123,
        }

        self.converter.convert.return_value = "Test string".encode("ascii")
        self.cmd.generate_pdf_derivative(attachment_uuid=self.attachment_uuid)

    def test_generate_pdf_derivative_attachment_not_found(self):
        self.mock_get_attachment.fetchone.return_value = None
        mock_rows = [self.mock_get_attachment]

        def mock_execute(query):
            return mock_rows.pop(0)

        self.session.execute = mock_execute
        self.session.reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.cmd.generate_pdf_derivative(
                attachment_uuid=self.attachment_uuid
            )

        assert excinfo.value.args == (
            f"Attachment with uuid {self.attachment_uuid} not found.",
            "communication/attachment/not_found",
        )
