# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from minty.exceptions import NotFound
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication.entities import AttachedFile, MessageAttachment
from zsnl_domains.communication.repositories import AttachmentRepository


class InfraFactoryMock:
    def __init__(self, infra):
        self.infra = infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infra[infrastructure_name]


class TestAttachmentRepository:
    def setup_method(self):
        with mock.patch("sqlalchemy.orm.Session") as mock_session:
            self.mock_infra = mock.MagicMock()
            self.mock_storage = mock.MagicMock()
            self.infra = InfraFactoryMock(
                infra={"database": mock_session, "s3": self.mock_storage}
            )
            self.event_service = mock.MagicMock()
            self.attach_repo = AttachmentRepository(
                infrastructure_factory=self.infra,
                context=None,
                event_service=self.event_service,
            )
            self.db = self.attach_repo._get_infrastructure("database")

    def test_attach_file_to_message(self):
        file_uuid = uuid4()
        message_uuid = uuid4()
        result = self.attach_repo.attach_file_to_message(
            file_uuid=file_uuid, filename="foo.pdf", message_uuid=message_uuid
        )
        assert isinstance(result, MessageAttachment)

    @mock.patch(
        "zsnl_domains.communication.repositories.attachment.AttachmentRepository._insert_file_attachment"
    )
    def test_save(self, mock_insert_file_attach):
        event = mock.MagicMock()
        event.entity_type = "MessageAttachment"
        event.event_name = "AttachedToMessage"
        self.event_service.event_list = [event]
        self.attach_repo.save()

        mock_insert_file_attach.assert_called_with(event=event)

    def test__insert_file_attachment(self):
        event = mock.MagicMock()
        file_uuid = str(uuid4())
        message_uuid = str(uuid4())
        attachment_uuid = str(uuid4())

        event.changes = [
            {"key": "file_uuid", "new_value": file_uuid, "old_value": None},
            {
                "key": "message_uuid",
                "new_value": message_uuid,
                "old_value": None,
            },
            {"key": "filename", "new_value": "test.pdf", "old_value": None},
            {
                "key": "attachment_uuid",
                "new_value": attachment_uuid,
                "old_value": None,
            },
        ]

        self.attach_repo._insert_file_attachment(event)
        self.db.execute.assert_called()

    def test_generate_download_url(self):
        f = AttachedFile(
            uuid=uuid4(),
            filename="test.png",
            size=3456,
            mimetype="img/png",
            md5="84f6119b40b50523533e37d68e251e6e",
            date_created="2019-09-19T15:45:41.423244",
            storage_location="LocalS3",
        )
        self.mock_storage.get_download_url.return_value = "https://some_url.nl"
        url = self.attach_repo.generate_download_url(f)
        assert url == "https://some_url.nl"

    @mock.patch(
        "zsnl_domains.communication.repositories.attachment.AttachmentRepository._transform_to_file_entity"
    )
    def test_get_file_by_uuid(self, mock_transform):
        att_uuid = str(uuid4())
        user_uuid = str(uuid4())
        is_pip_user = False

        self.db.reset()
        self.attach_repo.get_file_by_uuid(
            attachment_uuid=att_uuid,
            user_uuid=user_uuid,
            is_pip_user=is_pip_user,
        )
        self.db.execute.assert_called()
        mock_transform.assert_called_once()

    def test_get_file_by_uuid_exception(self):
        att_uuid = str(uuid4())
        user_uuid = str(uuid4())
        is_pip_user = False

        self.db.reset()
        self.db.execute().fetchone.return_value = None

        with pytest.raises(NotFound):
            self.attach_repo.get_file_by_uuid(
                attachment_uuid=att_uuid,
                user_uuid=user_uuid,
                is_pip_user=is_pip_user,
            )

    def test__transform_to_file_entity(self):
        Result = namedtuple(
            "Result",
            "uuid original_name size mimetype md5 date_created storage_location attachment_uuid",
        )
        res = Result(
            uuid=uuid4(),
            original_name="test.png",
            size=3456,
            mimetype="img/png",
            md5="84f6119b40b50523533e37d68e251e6e",
            date_created="2019-09-19T15:45:41.423244",
            storage_location="LocalS3",
            attachment_uuid=uuid4(),
        )
        f = self.attach_repo._transform_to_file_entity(res)
        assert isinstance(f, AttachedFile)
        assert f.uuid == res.uuid
        assert f.filename == res.original_name
        assert f.size == res.size
        assert f.mimetype == res.mimetype
        assert f.md5 == res.md5
        assert f.date_created == res.date_created
        assert f.storage_location == res.storage_location
