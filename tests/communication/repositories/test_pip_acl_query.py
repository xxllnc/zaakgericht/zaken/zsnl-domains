# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import sqlalchemy
from uuid import uuid4
from zsnl_domains.communication.repositories.pip_acl_query import pip_acl_query


class TestPIPQueries:
    def test_pip_acl_query(self):

        res = pip_acl_query(user_uuid=uuid4()).subquery()

        assert {str(col) for col in res.columns} == {"anon_1.uuid"}

        assert isinstance(res, sqlalchemy.sql.selectable.Subquery)
