# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication.entities import Contact, ContactMoment


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestContactMomentEntity:
    message_id = 123
    uuid = uuid4()
    thread_uuid = uuid4()
    contact_uuid = uuid4()
    created_by_uuid = uuid4()
    thread_type = "employee"
    contact_name = "admin"
    channel = "email"
    display_name = "admin"
    created = last_modified = message_date = datetime.now().isoformat()
    last_message = {"slug": "test test test"}
    type = "contact_moment"
    message_slug = "contact message slug"
    direction = "direction"
    contact = Contact(uuid=contact_uuid, name=contact_name)
    created_by = Contact(uuid=created_by_uuid, name=contact_name)

    def setup_method(self):
        self.contact_moment = ContactMoment(
            uuid=self.uuid,
            thread_uuid=self.thread_uuid,
            case_uuid=None,
            message_slug=self.message_slug,
            created_date=self.created,
            message_date=self.message_date,
            last_modified=self.last_modified,
            content=self.message_slug,
            channel=self.channel,
            direction=self.direction,
            recipient=self.contact,
            created_by=self.created_by,
            created_by_displayname="test created by",
            recipient_displayname="test recipient",
        )
        self.contact_moment.event_service = mock.MagicMock()

    def test_contact_initialisation(self):
        assert self.contact_moment.uuid == self.uuid
        assert self.contact_moment.thread_uuid == self.thread_uuid
        assert self.contact_moment.case_uuid is None
        assert self.contact_moment.entity_id == self.uuid
        assert self.contact_moment.message_type == self.type

        assert self.contact_moment.recipient.name == self.contact_name
        assert self.contact_moment.recipient.uuid == self.contact_uuid
        assert self.contact_moment.created_by.uuid == self.created_by_uuid
        assert self.contact_moment.created_by.name == self.contact_name

        assert self.contact_moment.created_date == self.created
        assert self.contact_moment.last_modified == self.last_modified

        assert self.contact_moment.message_slug == self.message_slug
        assert self.contact_moment.content == self.message_slug
        assert self.contact_moment.direction == self.direction
        assert self.contact_moment.channel == self.channel

    def test_create(self):
        thread_uuid = uuid4()
        recipient = Contact(uuid=uuid4(), name="test recipient")
        created = Contact(uuid=uuid4(), name="test created by")
        self.contact_moment.create(
            thread_uuid=thread_uuid,
            created_by=created,
            recipient=recipient,
            content="new content",
            channel="phone",
            direction="incoming",
        )

        assert self.contact_moment.thread_uuid == thread_uuid
        assert self.contact_moment.recipient_displayname == recipient.name
        assert self.contact_moment.created_by_displayname == created.name

        assert self.contact_moment.message_slug == "new content"
        assert self.contact_moment.content == "new content"
        assert self.contact_moment.direction == "incoming"
        assert self.contact_moment.channel == "phone"
