# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime, timezone
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication.entities import Contact, Note


class TestNoteEntity:
    note_uuid = uuid4()
    thread_uuid = uuid4()
    case_uuid = uuid4()
    content = "test"
    created_by_uuid = uuid4()
    contact_name = "admin"
    now = datetime.now(timezone.utc)

    created_by = Contact(uuid=created_by_uuid, name=contact_name)

    def setup_method(self):
        self.note = Note(
            uuid=self.note_uuid,
            thread_uuid=self.thread_uuid,
            case_uuid=self.case_uuid,
            created_by=self.created_by,
            message_slug="message_slug",
            last_modified=self.now,
            created_date=self.now,
            message_date=self.now,
            content="some content",
            created_by_displayname="test",
        )

    def test_case_initialisation(self):
        assert self.note.message_slug == "message_slug"
        assert self.note.content == "some content"
        assert self.note.message_type == "note"
        assert self.note.created_by == self.created_by
        assert self.note.uuid == self.note_uuid
        assert self.note.thread_uuid == self.thread_uuid
        assert self.note.case_uuid == self.case_uuid
        assert self.note.created_date == self.now
        assert self.note.last_modified == self.now
        assert self.note.entity_id == self.note_uuid
        assert self.note.created_by_displayname == "test"

    def test_create(self):

        thread_uuid = uuid4()
        created = Contact(uuid=uuid4(), name="test created by")

        args = {
            "thread_uuid": thread_uuid,
            "created_by": created,
            "content": "some new content",
        }
        self.note.event_service = mock.MagicMock()

        self.note.create(**args)
        assert self.note.thread_uuid == thread_uuid
        assert self.note.uuid == self.note_uuid

        assert self.note.created_by == created

        assert self.note.created_by_displayname == created.name

        assert self.note.content == "some new content"
