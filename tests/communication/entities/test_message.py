# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from datetime import datetime
from minty.exceptions import Conflict
from unittest import mock
from uuid import uuid4
from zsnl_domains.communication.entities import Contact, Message


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestMessageEntity:
    message_id = 123
    uuid = uuid4()
    thread_uuid = uuid4()
    case_uuid = uuid4()
    created_by_uuid = uuid4()
    contact_name = "admin"
    created = last_modified = datetime.now().isoformat()
    type = "contactmoment"
    message_slug = "contact message slug"
    created_by = Contact(uuid=created_by_uuid, name=contact_name)

    def setup_method(self):
        self.message = Message(
            uuid=self.uuid,
            thread_uuid=self.thread_uuid,
            case_uuid=self.case_uuid,
            message_slug=self.message_slug,
            message_type=self.type,
            created_date=self.created,
            last_modified=self.last_modified,
            created_by=self.created_by,
            created_by_displayname="test",
        )
        self.message.event_service = mock.MagicMock()

    def test_contact_initialisation(self):
        assert self.message.uuid == self.uuid
        assert self.message.thread_uuid == self.thread_uuid
        assert self.message.case_uuid == self.case_uuid
        assert self.message.entity_id == self.uuid
        assert self.message.message_type == self.type

        assert self.message.created_by.uuid == self.created_by_uuid
        assert self.message.created_by.name == self.contact_name

        assert self.message.created_date == self.created
        assert self.message.last_modified == self.last_modified
        assert self.message.message_slug == self.message_slug
        assert self.message.created_by_displayname == "test"

    def test_create(self):
        with pytest.raises(NotImplementedError):
            self.message.create()

    def test_delete(self):
        self.message.delete()
        self.message.event_service.log_event.assert_called_with(
            entity_type=self.message.__class__.__name__,
            entity_id=self.message.entity_id,
            event_name="MessageDeleted",
            changes=[],
            entity_data={
                "thread_uuid": str(self.message.thread_uuid),
                "case_uuid": str(self.message.case_uuid),
                "message_type": self.message.message_type,
            },
        )

    def test_mark_read(self):
        timestamp = datetime.now()
        with pytest.raises(Conflict) as excinfo:
            self.message.mark_read(context="pip", timestamp=timestamp)

        assert excinfo.value.args == (
            "Cannot mark read for contactmoment",
            "message/cannot_mark_read",
        )

    def test_mark_read_for_not_external_message_raises_conflict(self):

        with pytest.raises(Conflict) as exception_info:
            self.message.message_type = "not_external"
            self.message.mark_unread(context="pip")

        assert exception_info.value.args == (
            f"Cannot mark messages unread for type '{self.message.message_type}'",
            "message/cannot_mark_unread",
        )
