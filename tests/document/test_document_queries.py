# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from minty import exceptions
from minty.cqrs import EventService
from minty.cqrs.test import TestBase
from minty.exceptions import Forbidden, NotFound
from minty.repository import RepositoryFactory
from sqlalchemy.dialects import postgresql
from unittest import mock
from unittest.mock import patch
from uuid import uuid4
from zsnl_domains import document
from zsnl_domains.document import DirectoryEntryRepository, DocumentRepository
from zsnl_domains.document.commands import Commands
from zsnl_domains.document.entities import DirectoryEntry
from zsnl_domains.document.queries import Queries

UserInfo = namedtuple("UserInfo", "user_uuid permissions")


class MockInfrastructureFactory:
    def __init__(self, mock_infra):
        self.infrastructure = mock_infra

    def get_infrastructure(self, context, infrastructure_name):
        return self.infrastructure[infrastructure_name]


class MockRepositoryFactory:
    def __init__(self, infra_factory):
        self.infrastructure_factory = infra_factory
        self.repositories = {}

    def get_repository(self, name, context):
        mock_repo = self.repositories[name]
        return mock_repo


class TestDocumentQueries:
    def setup_method(self):
        with mock.patch(
            "zsnl_domains.document.repositories.directory.DirectoryRepository",
            spec=True,
        ) as directoryRepoMock, mock.patch(
            "zsnl_domains.document.repositories.directory_entry.DirectoryEntryRepository",
            spec=True,
        ) as directoryEntryRepoMock, mock.patch(
            "zsnl_domains.document.repositories.document.DocumentRepository",
            spec=True,
        ) as documentRepoMock:
            documentRepoMock.get_document_by_uuid.return_value = {
                "document_id": 1
            }
            repo_factory = MockRepositoryFactory(infra_factory={})
            repo_factory.repositories["document"] = documentRepoMock
            repo_factory.repositories[
                "directory_entry"
            ] = directoryEntryRepoMock
            repo_factory.repositories["directory"] = directoryRepoMock
            user_info = mock.MagicMock()
            user_info.user_uuid = "d3887763-675c-4988-9347-c5e1448fd20d"
            self.querymock = Queries(
                repository_factory=repo_factory,
                context=None,
                user_uuid="d3887763-675c-4988-9347-c5e1448fd20d",
            )
            self.querymock.user_info = user_info
            self.document_entity = mock.MagicMock()
            self.directory_entry_entity = mock.MagicMock()
            self.directory_entity = mock.MagicMock()

    def test_get_document_by_uuid(self):
        res = self.querymock.get_document_by_uuid(document_uuid=str(uuid4()))
        assert res["document_id"] == 1

    def test_search_document(self):
        document_repo = self.querymock.repository_factory.repositories[
            "document"
        ]
        document_repo.search_document.return_value = ["test"]

        case_uuid = str(uuid4())
        user_uuid = self.querymock.user_uuid
        res = self.querymock.search_document(
            case_uuid=case_uuid, keyword="test", filter=None
        )
        document_repo.search_document.assert_called_once_with(
            case_uuid=case_uuid,
            user_uuid=user_uuid,
            keyword="test",
            filter=None,
        )
        assert res == ["test"]

    def test_get_directory_entries_for_case(self):
        directory_entry_repo = self.querymock.repository_factory.repositories[
            "directory_entry"
        ]
        directory_entry_repo.get_directory_entries_for_case.return_value = [
            "test"
        ]

        case_uuid = str(uuid4())
        directory_uuid = str(uuid4())
        user_info = self.querymock.user_info
        res = self.querymock.get_directory_entries_for_case(
            case_uuid=case_uuid,
            directory_uuid=directory_uuid,
            search_term="Artifact",
        )
        directory_entry_repo.get_directory_entries_for_case.assert_called_once_with(
            user_info=user_info,
            case_uuid=case_uuid,
            directory_uuid=directory_uuid,
            search_term="Artifact",
            no_empty_folders=True,
        )
        assert res == ["test"]

    def test_get_parent_directories_for_directory(self):
        directory_repo = self.querymock.repository_factory.repositories[
            "directory"
        ]
        directory_repo.get_parent_directories_for_directory.return_value = [
            self.directory_entity
        ]

        directory_uuid = str(uuid4())
        res = self.querymock.get_parent_directories_for_directory(
            directory_uuid=directory_uuid
        )
        directory_repo.get_parent_directories_for_directory.assert_called_once_with(
            directory_uuid=directory_uuid
        )
        assert res == [self.directory_entity]


class TestDocumentGeneral:
    def test_get_query_instance(self):
        repo = {}
        context = None
        qry = document.get_query_instance(
            repository_factory=repo,
            context=context,
            user_uuid="0b71102a-2714-421d-9fe1-e4b085a0eee8",
        )
        assert isinstance(qry, Queries)

    def test_get_command_instance(self):
        repo = {}
        context = None
        cmd = document.get_command_instance(
            repository_factory=repo,
            context=context,
            user_uuid="0b71102a-2714-421d-9fe1-e4b085a0eee8",
            event_service="event_service",
        )
        assert isinstance(cmd, Commands)


class TestDownloadDocumentAsPIPUser(TestBase):
    def setup_method(self):

        #  create "zsnl_domains.document.queries.Queries" object
        #  needs: repository factory
        #         - maken met een "mock infrastructure factory" die mock infrastructure teruggeeft
        #           (1x: database, 1x: s3)
        # call function
        # check everything (assert_

        self.mock_infrastructures = {
            "database": mock.MagicMock(),
            "s3": mock.MagicMock(),
        }

        self.document_uuid = uuid4()
        self.database_query_result = namedtuple(
            "DocumentData",
            [
                "id",
                "document_uuid",
                "filename",
                "extension",
                "store_uuid",
                "directory_uuid",
                "case_uuid",
                "case_display_number",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
                "accepted",
                "date_modified",
                "thumbnail",
                "creator_uuid",
                "creator_displayname",
                "type",
                "properties",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
                "description",
                "origin",
                "origin_date",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
                "intake_owner_uuid",
                "intake_group_uuid",
                "intake_role_uuid",
            ],
        )(
            id=3,
            document_uuid=self.document_uuid,
            filename="test_doc.pdf",
            extension=".pdf",
            store_uuid=uuid4(),
            directory_uuid=uuid4(),
            case_uuid=uuid4(),
            case_display_number=123,
            mimetype="application/pdf",
            size=1,
            storage_location=["minio"],
            md5="fake_md5",
            is_archivable=True,
            virus_scan_status="ok",
            accepted=True,
            date_modified="2019-05-06",
            thumbnail="fake_thumbnail",
            creator_uuid=uuid4(),
            creator_displayname="A.Admin",
            type="pdf",
            properties='{"displayname": "A.Admin"}',
            preview_uuid=uuid4(),
            preview_storage_location=["minio"],
            preview_mimetype="application/pdf",
            description="des",
            origin="Intern",
            origin_date="2015-3-15",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

        self.user_info = namedtuple("UserInfo", "user_uuid permissions")

        repo_factory = RepositoryFactory(infra_factory=self.mock_infra)
        repo_factory.register_repository("document", DocumentRepository)

        self.query_instance = Queries(
            repository_factory=repo_factory,
            context="testcontext",
            user_uuid=self.user_info.user_uuid,
        )

    def test_get_document_as_pip_user(self):

        self.user_info = UserInfo(
            user_uuid=str(uuid4()), permissions={"pip_user": True}
        )
        self.mock_infrastructures[
            "database"
        ].execute().fetchone.return_value = self.database_query_result
        self.mock_infrastructures[
            "s3"
        ].get_download_url.return_value = "https://example.com/example.txt"

        self.mock_infrastructures["database"].reset_mock()

        result = self.query_instance.get_document_download_link(
            document_uuid=str(self.document_uuid), user_info=self.user_info
        )
        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list

        # From the first (only) execute(), get the 0th element of the "args"
        select_statement = db_execute_calls[0][0][0]
        compiled = select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 1
        assert result == "https://example.com/example.txt"
        assert str(compiled) == (
            "SELECT file.uuid AS document_uuid, filestore.uuid AS store_uuid, filestore.mimetype, filestore.size, filestore.storage_location, filestore.md5, filestore.is_archivable, filestore.virus_scan_status, directory.uuid AS directory_uuid, zaak.uuid AS case_uuid, zaak.id AS case_display_number, file.name AS filename, file.extension, file.accepted, file.version, groups.uuid AS intake_group_uuid, roles.uuid AS intake_role_uuid, get_subject_by_legacy_id(file.intake_owner) AS intake_owner_uuid, get_subject_by_legacy_id(file.created_by) AS creator_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.created_by)) AS creator_displayname, file.date_modified, file.id, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_1)s) AS preview_uuid, (SELECT filestore_1.storage_location \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_storage_location, (SELECT filestore_1.mimetype \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_3)s) AS preview_mimetype, file_metadata.description, file_metadata.origin, file_metadata.origin_date, file_metadata.trust_level AS confidentiality, file_metadata.document_category, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_5)s) AS thumbnail_mimetype, (SELECT filestore_2.storage_location \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_6)s) AS thumbnail_storage_location, array((SELECT json_build_object(%(json_build_object_2)s, zaaktype_document_kenmerken_map.case_document_uuid, %(json_build_object_3)s, zaaktype_document_kenmerken_map.name, %(json_build_object_4)s, zaaktype_document_kenmerken_map.public_name, %(json_build_object_5)s, zaaktype_document_kenmerken_map.magic_string) AS json_build_object_1 \n"
            "FROM zaaktype_document_kenmerken_map JOIN (file_case_document JOIN zaak ON zaak.id = file_case_document.case_id) ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = file_case_document.bibliotheek_kenmerken_id AND zaaktype_document_kenmerken_map.zaaktype_node_id = zaak.zaaktype_node_id \n"
            "WHERE file_case_document.file_id = file.id AND file.case_id = zaak.id)) AS labels \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id LEFT OUTER JOIN groups ON groups.id = file.intake_group_id LEFT OUTER JOIN roles ON roles.id = file.intake_role_id \n"
            "WHERE file.active_version IS true AND file.uuid = %(uuid_1)s AND file.destroyed IS false AND (EXISTS (SELECT 1 \n"
            "FROM zaak_betrokkenen JOIN zaak ON zaak.id = zaak_betrokkenen.zaak_id JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id \n"
            "WHERE zaak_betrokkenen.subject_id = %(subject_id_1)s AND zaak.deleted IS NULL AND zaak.status IN (%(status_1_1)s, %(status_1_2)s, %(status_1_3)s, %(status_1_4)s) AND zaaktype_node.prevent_pip IS NOT true AND (zaak_betrokkenen.pip_authorized IS true OR zaak.aanvrager = zaak_betrokkenen.id)))"
        )

    def test_get_document_as_normal_user(self):
        self.user_info = UserInfo(
            user_uuid=str(uuid4()), permissions={"pip_user": False}
        )
        self.mock_infrastructures[
            "database"
        ].execute().fetchone.return_value = self.database_query_result
        self.mock_infrastructures[
            "s3"
        ].get_download_url.return_value = "https://example.com/example.txt"

        self.mock_infrastructures["database"].reset_mock()

        result = self.query_instance.get_document_download_link(
            document_uuid=str(self.document_uuid), user_info=self.user_info
        )

        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = db_execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        select_statement_2 = db_execute_calls[1][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 2
        assert (
            str(select_statement_1) == "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )

        assert (
            str(select_statement_2)
            == "SELECT file.uuid AS document_uuid, filestore.uuid AS store_uuid, filestore.mimetype, filestore.size, filestore.storage_location, filestore.md5, filestore.is_archivable, filestore.virus_scan_status, directory.uuid AS directory_uuid, zaak.uuid AS case_uuid, zaak.id AS case_display_number, file.name AS filename, file.extension, file.accepted, file.version, groups.uuid AS intake_group_uuid, roles.uuid AS intake_role_uuid, get_subject_by_legacy_id(file.intake_owner) AS intake_owner_uuid, get_subject_by_legacy_id(file.created_by) AS creator_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.created_by)) AS creator_displayname, file.date_modified, file.id, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_1)s) AS preview_uuid, (SELECT filestore_1.storage_location \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_storage_location, (SELECT filestore_1.mimetype \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_3)s) AS preview_mimetype, file_metadata.description, file_metadata.origin, file_metadata.origin_date, file_metadata.trust_level AS confidentiality, file_metadata.document_category, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_5)s) AS thumbnail_mimetype, (SELECT filestore_2.storage_location \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_6)s) AS thumbnail_storage_location, array((SELECT json_build_object(%(json_build_object_2)s, zaaktype_document_kenmerken_map.case_document_uuid, %(json_build_object_3)s, zaaktype_document_kenmerken_map.name, %(json_build_object_4)s, zaaktype_document_kenmerken_map.public_name, %(json_build_object_5)s, zaaktype_document_kenmerken_map.magic_string) AS json_build_object_1 \n"
            "FROM zaaktype_document_kenmerken_map JOIN (file_case_document JOIN zaak ON zaak.id = file_case_document.case_id) ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = file_case_document.bibliotheek_kenmerken_id AND zaaktype_document_kenmerken_map.zaaktype_node_id = zaak.zaaktype_node_id \n"
            "WHERE file_case_document.file_id = file.id AND file.case_id = zaak.id)) AS labels \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id LEFT OUTER JOIN groups ON groups.id = file.intake_group_id LEFT OUTER JOIN roles ON roles.id = file.intake_role_id \n"
            "WHERE file.active_version IS true AND file.uuid = %(uuid_1)s AND file.destroyed IS false AND (file.case_id IS NULL OR zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) OR file.uuid = %(uuid_3)s AND zaak.confidentiality != %(confidentiality_1)s AND file.confidential IS true AND (EXISTS (SELECT 1 \n"
            "FROM zaak JOIN zaaktype_authorisation ON zaaktype_authorisation.zaaktype_id = zaak.zaaktype_id AND zaaktype_authorisation.confidential IS true AND zaaktype_authorisation.recht = %(recht_1)s JOIN subject_position_matrix ON subject_position_matrix.role_id = zaaktype_authorisation.role_id AND subject_position_matrix.group_id = zaaktype_authorisation.ou_id JOIN subject ON subject.id = subject_position_matrix.subject_id \n"
            "WHERE zaak.id = file.case_id AND subject.uuid = %(uuid_4)s)))"
        )
        assert result == "https://example.com/example.txt"

    def test_get_document_unscanned__not_allowed(self):
        self.user_info = UserInfo(
            user_uuid=str(uuid4()), permissions={"pip_user": False}
        )
        self.mock_infrastructures[
            "database"
        ].execute().fetchone.return_value = namedtuple(
            "DocumentData",
            [
                "id",
                "document_uuid",
                "filename",
                "extension",
                "store_uuid",
                "directory_uuid",
                "case_uuid",
                "case_display_number",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
                "accepted",
                "date_modified",
                "thumbnail",
                "creator_uuid",
                "creator_displayname",
                "type",
                "properties",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
                "description",
                "origin",
                "origin_date",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
                "intake_owner_uuid",
                "intake_group_uuid",
                "intake_role_uuid",
            ],
        )(
            id=3,
            document_uuid=self.document_uuid,
            filename="img.png",
            extension=".png",
            store_uuid=uuid4(),
            directory_uuid=uuid4(),
            case_uuid=uuid4(),
            case_display_number=123,
            mimetype="img/png",
            size=1,
            storage_location=["minio"],
            md5="fake_md5",
            is_archivable=True,
            virus_scan_status="pending",
            accepted=True,
            date_modified="2019-05-06",
            thumbnail="fake_thumbnail",
            creator_uuid=uuid4(),
            creator_displayname="A.Admin",
            type="pdf",
            properties='{"displayname": "A.Admin"}',
            preview_uuid=None,
            preview_storage_location=None,
            preview_mimetype=None,
            description="des",
            origin="Intern",
            origin_date="2015-12-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )
        self.mock_infrastructures[
            "s3"
        ].get_download_url.return_value = "https://example.com/example.txt"

        self.mock_infrastructures["database"].reset_mock()

        with pytest.raises(Forbidden) as excinfo:
            self.query_instance.get_document_download_link(
                document_uuid=str(self.document_uuid), user_info=self.user_info
            )

        assert excinfo.value.args == (
            f"Document with uuid '{self.document_uuid}'is not scanned, can not be downloaded",
            "document/download/not_allowed",
        )


class TestGetDirectoryEntriesForCase(TestBase):
    def setup_method(self):
        self.mock_infrastructures = {"database": mock.MagicMock()}

        self.user_info = mock.MagicMock()
        self.user_info.user_uuid = str(uuid4())
        self.user_info.permissions = {"pip_user": False}

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

        repo_factory = RepositoryFactory(infra_factory=self.mock_infra)
        repo_factory.register_repository(
            "directory_entry", DirectoryEntryRepository
        )

        self.query_instance = Queries(
            repository_factory=repo_factory,
            context="testcontext",
            user_uuid=self.user_info.user_uuid,
        )
        self.query_instance.user_info = self.user_info

        self.database_query_result = namedtuple(
            "ResultProxy",
            [
                "uuid",
                "type",
                "name",
                "case_uuid",
                "case_display_number",
                "parent_directory_uuid",
                "parent_directory_display_name",
                "modified_by_uuid",
                "modified_by_display_name",
                "mimetype",
                "accepted",
                "rejection_reason",
                "rejected_by_display_name",
                "document_number",
                "description",
                "extension",
                "last_modified_date_time",
                "preview_uuid",
                "intake_owner",
                "intake_role_id",
                "intake_role_name",
                "intake_group_name",
                "intake_group_id",
                "subject_role_ids",
                "subject_group_ids",
                "intake_owner_display_name",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
            ],
        )(
            uuid=uuid4(),
            type="document",
            name="test_doc",
            case_uuid=uuid4(),
            case_display_number=12,
            parent_directory_uuid=uuid4(),
            parent_directory_display_name="test_dir",
            modified_by_uuid=uuid4(),
            modified_by_display_name="A.Admin",
            mimetype="application/pdf",
            accepted=True,
            document_number=1,
            rejection_reason="",
            rejected_by_display_name="",
            description="doc",
            extension="pdf",
            last_modified_date_time="2019-06-05",
            preview_uuid=None,
            intake_owner="",
            intake_role_id="",
            intake_role_name="",
            intake_group_name="",
            intake_group_id="",
            intake_owner_display_name="",
            subject_role_ids=[],
            subject_group_ids=[],
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
        )

    def test_get_directory_entries_for_case_for_pip_user(self):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = [self.database_query_result]
        self.mock_infrastructures["database"].reset_mock()
        self.query_instance.user_info.permissions["pip_user"] = True

        result = self.query_instance.get_directory_entries_for_case(
            case_uuid=str(uuid4()), no_empty_folders=False
        )

        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement = db_execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 1
        assert (
            str(select_statement)
            == "SELECT file.uuid AS uuid, file.name AS name, %(param_1)s AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_3)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND (EXISTS (SELECT 1 \n"
            "FROM zaak_betrokkenen JOIN zaak ON zaak.id = zaak_betrokkenen.zaak_id JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id \n"
            "WHERE zaak_betrokkenen.subject_id = %(subject_id_1)s AND zaak.deleted IS NULL AND zaak.status IN (%(status_1_1)s, %(status_1_2)s, %(status_1_3)s, %(status_1_4)s) AND zaaktype_node.prevent_pip IS NOT true AND (zaak_betrokkenen.pip_authorized IS true OR zaak.aanvrager = zaak_betrokkenen.id))) AND file.publish_pip IS true AND zaak.uuid = %(uuid_1)s UNION ALL SELECT directory.uuid AS uuid, directory.name AS name, %(param_5)s AS type, %(param_6)s AS description, %(param_7)s AS extension, %(param_8)s AS mimetype, %(param_9)s AS accepted, %(param_10)s AS last_modified_date_time, %(param_11)s AS document_number, zaak.uuid AS case_uuid, %(param_12)s AS rejection_reason, %(param_13)s AS rejected_by_display_name, %(param_14)s AS intake_owner, %(param_15)s AS intake_role_id, %(param_16)s AS intake_group_id, %(param_17)s AS intake_group_name, %(param_18)s AS intake_role_name, %(param_19)s AS intake_owner_display_name, directory.case_id AS case_display_number, directory_1.uuid AS parent_directory_uuid, directory_1.name AS parent_directory_display_name, %(param_20)s AS modified_by_uuid, %(param_21)s AS modified_by_display_name, %(param_22)s AS preview_uuid, %(param_23)s AS thumbnail_uuid, %(param_24)s AS thumbnail_mimetype \n"
            "FROM directory LEFT OUTER JOIN directory AS directory_1 ON directory.path[array_upper(directory.path, %(array_upper_1)s)] = directory_1.id LEFT OUTER JOIN zaak ON zaak.id = directory.case_id \n"
            "WHERE (EXISTS (SELECT 1 \n"
            "FROM zaak_betrokkenen JOIN zaak ON zaak.id = zaak_betrokkenen.zaak_id JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id \n"
            "WHERE zaak_betrokkenen.subject_id = %(subject_id_2)s AND zaak.deleted IS NULL AND zaak.status IN (%(status_2_1)s, %(status_2_2)s, %(status_2_3)s, %(status_2_4)s) AND zaaktype_node.prevent_pip IS NOT true AND (zaak_betrokkenen.pip_authorized IS true OR zaak.aanvrager = zaak_betrokkenen.id))) AND zaak.uuid = %(uuid_2)s"
        )

        assert isinstance(result[0], DirectoryEntry)
        assert result[0].uuid == self.database_query_result.uuid
        assert result[0].entity_id == self.database_query_result.uuid
        assert result[0].name == self.database_query_result.name
        assert result[0].entry_type == self.database_query_result.type

        assert result[0].case == {
            "uuid": self.database_query_result.case_uuid,
            "display_number": self.database_query_result.case_display_number,
        }
        assert result[0].modified_by == {
            "uuid": self.database_query_result.modified_by_uuid,
            "display_name": self.database_query_result.modified_by_display_name,
        }

        assert result[0].mimetype == self.database_query_result.mimetype
        assert result[0].accepted == self.database_query_result.accepted
        assert (
            result[0].last_modified_date_time
            == self.database_query_result.last_modified_date_time
        )

    def test_get_directory_entries_for_case_for_employee(self):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = []
        self.mock_infrastructures["database"].reset_mock()
        self.query_instance.user_info.permissions["pip_user"] = False

        result = self.query_instance.get_directory_entries_for_case(
            case_uuid=str(uuid4())
        )

        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = db_execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        select_statement_2 = db_execute_calls[1][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        select_statement_3 = db_execute_calls[2][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 3
        assert str(select_statement_1) == (
            "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )
        assert str(select_statement_2) == (
            "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )
        assert str(select_statement_3) == (
            "SELECT file.uuid AS uuid, file.name AS name, %(param_1)s AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_3)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.uuid = %(uuid_2)s UNION ALL SELECT directory.uuid AS uuid, directory.name AS name, %(param_5)s AS type, %(param_6)s AS description, %(param_7)s AS extension, %(param_8)s AS mimetype, %(param_9)s AS accepted, %(param_10)s AS last_modified_date_time, %(param_11)s AS document_number, zaak.uuid AS case_uuid, %(param_12)s AS rejection_reason, %(param_13)s AS rejected_by_display_name, %(param_14)s AS intake_owner, %(param_15)s AS intake_role_id, %(param_16)s AS intake_group_id, %(param_17)s AS intake_group_name, %(param_18)s AS intake_role_name, %(param_19)s AS intake_owner_display_name, directory.case_id AS case_display_number, directory_1.uuid AS parent_directory_uuid, directory_1.name AS parent_directory_display_name, %(param_20)s AS modified_by_uuid, %(param_21)s AS modified_by_display_name, %(param_22)s AS preview_uuid, %(param_23)s AS thumbnail_uuid, %(param_24)s AS thumbnail_mimetype \n"
            "FROM directory LEFT OUTER JOIN directory AS directory_1 ON directory.path[array_upper(directory.path, %(array_upper_1)s)] = directory_1.id LEFT OUTER JOIN zaak ON zaak.id = directory.case_id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_3)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_3)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_3)s) AND zaak.aanvrager_type = %(aanvrager_type_2)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_2)s AND case_acl.subject_uuid = %(subject_uuid_2)s))) AND zaak.uuid = %(uuid_4)s"
        )
        assert result == []

    def test_get_directory_entries_for_case_with_search_term(self):
        self.mock_infrastructures["database"].reset_mock()

        self.query_instance.get_directory_entries_for_case(
            case_uuid=str(uuid4()), search_term="search_term testen"
        )

        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = db_execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        select_statement_2 = db_execute_calls[1][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        select_statement_3 = db_execute_calls[2][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 3
        assert str(select_statement_1) == (
            "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )
        assert str(select_statement_2) == (
            "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )
        assert str(select_statement_3) == (
            "SELECT file.uuid AS uuid, file.name AS name, %(param_1)s AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_3)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND (file.name || file.extension ILIKE %(param_5)s ESCAPE '~' OR file.search_index @@ to_tsquery(%(search_index_1)s)) AND zaak.uuid = %(uuid_2)s UNION ALL SELECT directory.uuid AS uuid, directory.name AS name, %(param_6)s AS type, %(param_7)s AS description, %(param_8)s AS extension, %(param_9)s AS mimetype, %(param_10)s AS accepted, %(param_11)s AS last_modified_date_time, %(param_12)s AS document_number, zaak.uuid AS case_uuid, %(param_13)s AS rejection_reason, %(param_14)s AS rejected_by_display_name, %(param_15)s AS intake_owner, %(param_16)s AS intake_role_id, %(param_17)s AS intake_group_id, %(param_18)s AS intake_group_name, %(param_19)s AS intake_role_name, %(param_20)s AS intake_owner_display_name, directory.case_id AS case_display_number, directory_1.uuid AS parent_directory_uuid, directory_1.name AS parent_directory_display_name, %(param_21)s AS modified_by_uuid, %(param_22)s AS modified_by_display_name, %(param_23)s AS preview_uuid, %(param_24)s AS thumbnail_uuid, %(param_25)s AS thumbnail_mimetype \n"
            "FROM directory LEFT OUTER JOIN directory AS directory_1 ON directory.path[array_upper(directory.path, %(array_upper_1)s)] = directory_1.id LEFT OUTER JOIN zaak ON zaak.id = directory.case_id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_3)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_3)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_3)s) AND zaak.aanvrager_type = %(aanvrager_type_2)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_2)s AND case_acl.subject_uuid = %(subject_uuid_2)s))) AND directory.name ILIKE %(name_1)s ESCAPE '~' AND zaak.uuid = %(uuid_4)s"
        )

    def test_get_directory_entries_for_case_for_directory(self):

        self.mock_infrastructures["database"].reset_mock()

        self.query_instance.get_directory_entries_for_case(
            case_uuid=str(uuid4()), directory_uuid=str(uuid4())
        )

        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = db_execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        select_statement_2 = db_execute_calls[1][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        select_statement_3 = db_execute_calls[2][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 3
        assert str(select_statement_1) == (
            "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )
        assert str(select_statement_2) == (
            "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )
        assert str(select_statement_3) == (
            "SELECT file.uuid AS uuid, file.name AS name, %(param_1)s AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_3)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND directory.uuid = %(uuid_2)s AND zaak.uuid = %(uuid_3)s UNION ALL SELECT directory.uuid AS uuid, directory.name AS name, %(param_5)s AS type, %(param_6)s AS description, %(param_7)s AS extension, %(param_8)s AS mimetype, %(param_9)s AS accepted, %(param_10)s AS last_modified_date_time, %(param_11)s AS document_number, zaak.uuid AS case_uuid, %(param_12)s AS rejection_reason, %(param_13)s AS rejected_by_display_name, %(param_14)s AS intake_owner, %(param_15)s AS intake_role_id, %(param_16)s AS intake_group_id, %(param_17)s AS intake_group_name, %(param_18)s AS intake_role_name, %(param_19)s AS intake_owner_display_name, directory.case_id AS case_display_number, directory_1.uuid AS parent_directory_uuid, directory_1.name AS parent_directory_display_name, %(param_20)s AS modified_by_uuid, %(param_21)s AS modified_by_display_name, %(param_22)s AS preview_uuid, %(param_23)s AS thumbnail_uuid, %(param_24)s AS thumbnail_mimetype \n"
            "FROM directory LEFT OUTER JOIN directory AS directory_1 ON directory.path[array_upper(directory.path, %(array_upper_1)s)] = directory_1.id LEFT OUTER JOIN zaak ON zaak.id = directory.case_id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_2)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_4)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_4)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_4)s) AND zaak.aanvrager_type = %(aanvrager_type_2)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_2)s AND case_acl.subject_uuid = %(subject_uuid_2)s))) AND directory_1.uuid = %(uuid_5)s AND zaak.uuid = %(uuid_6)s"
        )

    def test_get_directory_entries_for_case_for_pip_user_without_empty_folders(
        self,
    ):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = [self.database_query_result]
        self.mock_infrastructures["database"].reset_mock()
        self.query_instance.user_info.permissions["pip_user"] = True

        self.query_instance.get_directory_entries_for_case(
            case_uuid=str(uuid4())
        )

        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement = db_execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 1
        assert str(select_statement) == (
            "WITH RECURSIVE cte(id, path, depth) AS \n"
            "(SELECT directory.id AS id, directory.path AS path, %(param_25)s AS depth \n"
            "FROM directory, file \n"
            "WHERE directory.id = file.directory_id AND file.publish_pip IS true UNION ALL SELECT directory_2.id AS id, directory_2.path AS path, cte.depth + %(depth_1)s AS anon_1 \n"
            "FROM directory AS directory_2, cte \n"
            "WHERE directory_2.id = any(cte.path) AND cte.depth <= %(depth_2)s)\n"
            " SELECT file.uuid AS uuid, file.name AS name, %(param_1)s AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_3)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND (EXISTS (SELECT 1 \n"
            "FROM zaak_betrokkenen JOIN zaak ON zaak.id = zaak_betrokkenen.zaak_id JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id \n"
            "WHERE zaak_betrokkenen.subject_id = %(subject_id_1)s AND zaak.deleted IS NULL AND zaak.status IN (%(status_1_1)s, %(status_1_2)s, %(status_1_3)s, %(status_1_4)s) AND zaaktype_node.prevent_pip IS NOT true AND (zaak_betrokkenen.pip_authorized IS true OR zaak.aanvrager = zaak_betrokkenen.id))) AND file.publish_pip IS true AND zaak.uuid = %(uuid_1)s UNION ALL SELECT directory.uuid AS uuid, directory.name AS name, %(param_5)s AS type, %(param_6)s AS description, %(param_7)s AS extension, %(param_8)s AS mimetype, %(param_9)s AS accepted, %(param_10)s AS last_modified_date_time, %(param_11)s AS document_number, zaak.uuid AS case_uuid, %(param_12)s AS rejection_reason, %(param_13)s AS rejected_by_display_name, %(param_14)s AS intake_owner, %(param_15)s AS intake_role_id, %(param_16)s AS intake_group_id, %(param_17)s AS intake_group_name, %(param_18)s AS intake_role_name, %(param_19)s AS intake_owner_display_name, directory.case_id AS case_display_number, directory_1.uuid AS parent_directory_uuid, directory_1.name AS parent_directory_display_name, %(param_20)s AS modified_by_uuid, %(param_21)s AS modified_by_display_name, %(param_22)s AS preview_uuid, %(param_23)s AS thumbnail_uuid, %(param_24)s AS thumbnail_mimetype \n"
            "FROM directory LEFT OUTER JOIN directory AS directory_1 ON directory.path[array_upper(directory.path, %(array_upper_1)s)] = directory_1.id LEFT OUTER JOIN zaak ON zaak.id = directory.case_id \n"
            "WHERE (EXISTS (SELECT 1 \n"
            "FROM zaak_betrokkenen JOIN zaak ON zaak.id = zaak_betrokkenen.zaak_id JOIN zaaktype_node ON zaaktype_node.id = zaak.zaaktype_node_id \n"
            "WHERE zaak_betrokkenen.subject_id = %(subject_id_2)s AND zaak.deleted IS NULL AND zaak.status IN (%(status_2_1)s, %(status_2_2)s, %(status_2_3)s, %(status_2_4)s) AND zaaktype_node.prevent_pip IS NOT true AND (zaak_betrokkenen.pip_authorized IS true OR zaak.aanvrager = zaak_betrokkenen.id))) AND directory.id IN (SELECT cte.id \n"
            "FROM cte) AND zaak.uuid = %(uuid_2)s"
        )


class TestGetDirectoryEntriesForIntake(TestBase):
    def setup_method(self):
        self.mock_infrastructures = {"database": mock.MagicMock()}

        self.user_info = mock.MagicMock()
        self.user_info.user_uuid = str(uuid4())

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

        repo_factory = RepositoryFactory(infra_factory=self.mock_infra)
        repo_factory.register_repository(
            "directory_entry", DirectoryEntryRepository
        )

        self.query_instance = Queries(
            repository_factory=repo_factory,
            context="testcontext",
            user_uuid=self.user_info.user_uuid,
        )

        self.database_query_result = namedtuple(
            "ResultProxy",
            [
                "uuid",
                "type",
                "name",
                "case_uuid",
                "case_display_number",
                "parent_directory_uuid",
                "parent_directory_display_name",
                "modified_by_uuid",
                "modified_by_display_name",
                "mimetype",
                "accepted",
                "document_number",
                "rejection_reason",
                "rejected_by_display_name",
                "description",
                "extension",
                "last_modified_date_time",
                "preview_uuid",
                "intake_owner",
                "intake_role_id",
                "intake_role_name",
                "intake_group_name",
                "intake_group_id",
                "subject_role_ids",
                "subject_group_ids",
                "intake_owner_display_name",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
            ],
        )(
            uuid=uuid4(),
            type="document",
            name="test_doc",
            case_uuid=uuid4(),
            case_display_number=12,
            parent_directory_uuid=uuid4(),
            parent_directory_display_name="test_dir",
            modified_by_uuid=uuid4(),
            modified_by_display_name="A.Admin",
            mimetype="application/pdf",
            accepted=True,
            document_number=2,
            rejection_reason="A valid rejection reason",
            rejected_by_display_name="B. Heerder",
            description="test doc",
            extension="pdf",
            last_modified_date_time="2019-09-05",
            preview_uuid=None,
            intake_owner="",
            intake_role_id="",
            intake_role_name="",
            intake_group_name="",
            intake_group_id="",
            intake_owner_display_name="",
            subject_role_ids=[],
            subject_group_ids=[],
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
        )

    def test_get_directory_entries_for_intake(self):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = [self.database_query_result]

        page = 1
        page_size = 10

        self.mock_infrastructures["database"].reset_mock()
        self.user_info.permissions = ["documenten_intake_all"]
        self.query_instance.user_info = self.user_info
        result = self.query_instance.get_directory_entries_for_intake(
            page=page, page_size=page_size
        )
        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = str(db_execute_calls[0][0][0])

        assert len(db_execute_calls) == 1

        assert (
            select_statement_1
            == "SELECT file.uuid AS uuid, file.name AS name, :param_1 AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = :type_1 AND filestore.mimetype != :mimetype_1 AND filestore_1.id = file_derivative_1.filestore_id\n"
            " LIMIT :param_2) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_3) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_4) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND file.case_id IS NULL AND file.skip_intake IS false AND file.queue IS true ORDER BY file.id ASC\n"
            " LIMIT :param_5 OFFSET :param_6"
        )

        assert isinstance(result[0], DirectoryEntry)
        assert result[0].uuid == self.database_query_result.uuid
        assert result[0].name == self.database_query_result.name
        assert result[0].entry_type == self.database_query_result.type

        assert result[0].rejection_reason == "A valid rejection reason"

        assert result[0].case == {
            "uuid": self.database_query_result.case_uuid,
            "display_number": self.database_query_result.case_display_number,
        }
        assert result[0].modified_by == {
            "uuid": self.database_query_result.modified_by_uuid,
            "display_name": self.database_query_result.modified_by_display_name,
        }

        assert result[0].mimetype == self.database_query_result.mimetype
        assert result[0].accepted == self.database_query_result.accepted
        assert (
            result[0].last_modified_date_time
            == self.database_query_result.last_modified_date_time
        )

    def test_get_directory_entries_for_intake_with_document_intake_subject_perms(
        self,
    ):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = [self.database_query_result]

        page = 1
        page_size = 10

        self.mock_infrastructures["database"].reset_mock()
        self.user_info.permissions = ["documenten_intake_subject"]
        self.query_instance.user_info = self.user_info
        result = self.query_instance.get_directory_entries_for_intake(
            page=page, page_size=page_size
        )
        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = str(db_execute_calls[0][0][0])
        # select_statement_2 = str(db_execute_calls[1][0][0]).replace("\n", "")

        assert len(db_execute_calls) == 1

        assert (
            select_statement_1
            == "SELECT file.uuid AS uuid, file.name AS name, :param_1 AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = :type_1 AND filestore.mimetype != :mimetype_1 AND filestore_1.id = file_derivative_1.filestore_id\n"
            " LIMIT :param_2) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_3) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_4) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND file.case_id IS NULL AND file.skip_intake IS false AND file.queue IS true AND ((EXISTS (SELECT :param_5 AS anon_1 \n"
            "FROM subject_position_matrix JOIN subject ON subject.id = subject_position_matrix.subject_id \n"
            "WHERE subject_position_matrix.role_id = file.intake_role_id AND subject_position_matrix.group_id = file.intake_group_id AND subject.uuid = :uuid_1)) OR get_subject_by_legacy_id(file.intake_owner) = :get_subject_by_legacy_id_1) ORDER BY file.id ASC\n"
            " LIMIT :param_6 OFFSET :param_7"
        )

        assert isinstance(result[0], DirectoryEntry)
        assert result[0].uuid == self.database_query_result.uuid
        assert result[0].name == self.database_query_result.name
        assert result[0].entry_type == self.database_query_result.type

        assert result[0].case == {
            "uuid": self.database_query_result.case_uuid,
            "display_number": self.database_query_result.case_display_number,
        }
        assert result[0].modified_by == {
            "uuid": self.database_query_result.modified_by_uuid,
            "display_name": self.database_query_result.modified_by_display_name,
        }

        assert result[0].mimetype == self.database_query_result.mimetype
        assert result[0].accepted == self.database_query_result.accepted
        assert (
            result[0].last_modified_date_time
            == self.database_query_result.last_modified_date_time
        )

    def test_get_directory_entries_for_intake_without_intake_permissions(self):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = [self.database_query_result]

        page = 1
        page_size = 100

        self.mock_infrastructures["database"].reset_mock()
        self.user_info.permissions = []
        self.query_instance.user_info = self.user_info
        self.query_instance.get_directory_entries_for_intake(
            page=page, page_size=page_size
        )
        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list

        assert len(db_execute_calls) == 0

    def test_get_directory_entries_for_intake_with_search_term(self):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = []

        page = 1
        page_size = 10
        sort = "attributes.name"

        self.mock_infrastructures["database"].reset_mock()
        self.user_info.permissions = ["documenten_intake_all"]
        self.query_instance.user_info = self.user_info
        result = self.query_instance.get_directory_entries_for_intake(
            search_term="search_term",
            page=page,
            page_size=page_size,
            sort=sort,
        )
        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = str(db_execute_calls[0][0][0])

        assert len(db_execute_calls) == 1
        assert result == []

        assert select_statement_1 == (
            "SELECT file.uuid AS uuid, file.name AS name, :param_1 AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = :type_1 AND filestore.mimetype != :mimetype_1 AND filestore_1.id = file_derivative_1.filestore_id\n"
            " LIMIT :param_2) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_3) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_4) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND (lower(file.name || file.extension) LIKE lower(:param_5) ESCAPE '~' OR file.search_index MATCH :search_index_1) AND file.case_id IS NULL AND file.skip_intake IS false AND file.queue IS true ORDER BY file.name ASC, file.id ASC\n"
            " LIMIT :param_6 OFFSET :param_7"
        )

    def test_get_directory_entries_for_intake_assigned_to_user(self):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = [self.database_query_result]

        page = 1
        page_size = 10

        self.mock_infrastructures["database"].reset_mock()
        self.user_info.permissions = ["documenten_intake_all"]
        self.query_instance.user_info = self.user_info
        result = self.query_instance.get_directory_entries_for_intake(
            page=page, page_size=page_size, assigned=True
        )
        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = str(db_execute_calls[0][0][0])

        assert len(db_execute_calls) == 1

        assert (
            select_statement_1
            == "SELECT file.uuid AS uuid, file.name AS name, :param_1 AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = :type_1 AND filestore.mimetype != :mimetype_1 AND filestore_1.id = file_derivative_1.filestore_id\n"
            " LIMIT :param_2) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_3) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_4) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND file.case_id IS NULL AND file.skip_intake IS false AND file.queue IS true AND (file.intake_owner IS NOT NULL OR file.intake_group_id IS NOT NULL AND file.intake_role_id IS NOT NULL) ORDER BY file.id ASC\n"
            " LIMIT :param_5 OFFSET :param_6"
        )

        assert isinstance(result[0], DirectoryEntry)
        assert result[0].uuid == self.database_query_result.uuid
        assert result[0].name == self.database_query_result.name
        assert result[0].entry_type == self.database_query_result.type

        assert result[0].rejection_reason == "A valid rejection reason"

        assert result[0].case == {
            "uuid": self.database_query_result.case_uuid,
            "display_number": self.database_query_result.case_display_number,
        }
        assert result[0].modified_by == {
            "uuid": self.database_query_result.modified_by_uuid,
            "display_name": self.database_query_result.modified_by_display_name,
        }

        assert result[0].mimetype == self.database_query_result.mimetype
        assert result[0].accepted == self.database_query_result.accepted
        assert (
            result[0].last_modified_date_time
            == self.database_query_result.last_modified_date_time
        )

    def test_get_directory_entries_for_intake_unassigned(self):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = [self.database_query_result]

        page = 1
        page_size = 10

        self.mock_infrastructures["database"].reset_mock()
        self.user_info.permissions = ["documenten_intake_all"]
        self.query_instance.user_info = self.user_info
        result = self.query_instance.get_directory_entries_for_intake(
            page=page, page_size=page_size, assigned=False
        )
        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = str(db_execute_calls[0][0][0])

        assert len(db_execute_calls) == 1

        assert (
            select_statement_1
            == "SELECT file.uuid AS uuid, file.name AS name, :param_1 AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = :type_1 AND filestore.mimetype != :mimetype_1 AND filestore_1.id = file_derivative_1.filestore_id\n"
            " LIMIT :param_2) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_3) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_4) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND file.case_id IS NULL AND file.skip_intake IS false AND file.queue IS true AND file.intake_owner IS NULL AND file.intake_group_id IS NULL AND file.intake_role_id IS NULL ORDER BY file.id ASC\n"
            " LIMIT :param_5 OFFSET :param_6"
        )

        assert isinstance(result[0], DirectoryEntry)
        assert result[0].uuid == self.database_query_result.uuid
        assert result[0].name == self.database_query_result.name
        assert result[0].entry_type == self.database_query_result.type

        assert result[0].rejection_reason == "A valid rejection reason"

        assert result[0].case == {
            "uuid": self.database_query_result.case_uuid,
            "display_number": self.database_query_result.case_display_number,
        }
        assert result[0].modified_by == {
            "uuid": self.database_query_result.modified_by_uuid,
            "display_name": self.database_query_result.modified_by_display_name,
        }

        assert result[0].mimetype == self.database_query_result.mimetype
        assert result[0].accepted == self.database_query_result.accepted
        assert (
            result[0].last_modified_date_time
            == self.database_query_result.last_modified_date_time
        )

    def test_get_directory_entries_for_intake_assigned_to_self(self):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = [self.database_query_result]

        page = 1
        page_size = 10

        self.mock_infrastructures["database"].reset_mock()
        self.user_info.permissions = ["documenten_intake_all"]
        self.query_instance.user_info = self.user_info
        result = self.query_instance.get_directory_entries_for_intake(
            page=page,
            page_size=page_size,
            assigned=False,
            assigned_to_self=True,
        )
        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = str(db_execute_calls[0][0][0])
        select_statement_2 = str(db_execute_calls[1][0][0])

        assert len(db_execute_calls) == 2
        assert (
            select_statement_1 == "SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = :uuid_1"
        )

        assert (
            select_statement_2
            == "SELECT file.uuid AS uuid, file.name AS name, :param_1 AS type, file_metadata.description AS description, file.extension AS extension, filestore.mimetype AS mimetype, file.accepted AS accepted, file.date_modified AS last_modified_date_time, file.id AS document_number, zaak.uuid AS case_uuid, file.rejection_reason AS rejection_reason, file.rejected_by_display_name AS rejected_by_display_name, file.intake_owner AS intake_owner, roles.id AS intake_role_id, groups.id AS intake_group_id, groups.name AS intake_group_name, roles.name AS intake_role_name, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.intake_owner)) AS intake_owner_display_name, zaak.id AS case_display_number, directory.uuid AS parent_directory_uuid, directory.name AS parent_directory_display_name, get_subject_by_legacy_id(file.modified_by) AS modified_by_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.modified_by)) AS modified_by_display_name, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = :type_1 AND filestore.mimetype != :mimetype_1 AND filestore_1.id = file_derivative_1.filestore_id\n"
            " LIMIT :param_2) AS preview_uuid, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_3) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = :type_2 AND filestore_2.id = file_derivative_2.filestore_id\n"
            " LIMIT :param_4) AS thumbnail_mimetype \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN groups ON file.intake_group_id = groups.id LEFT OUTER JOIN roles ON file.intake_role_id = roles.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id \n"
            "WHERE file.active_version IS true AND file.date_deleted IS NULL AND file.destroyed IS false AND file.case_id IS NULL AND file.skip_intake IS false AND file.queue IS true AND file.intake_owner = :intake_owner_1 ORDER BY file.id ASC\n"
            " LIMIT :param_5 OFFSET :param_6"
        )

        assert isinstance(result[0], DirectoryEntry)
        assert result[0].uuid == self.database_query_result.uuid
        assert result[0].name == self.database_query_result.name
        assert result[0].entry_type == self.database_query_result.type

        assert result[0].rejection_reason == "A valid rejection reason"

        assert result[0].case == {
            "uuid": self.database_query_result.case_uuid,
            "display_number": self.database_query_result.case_display_number,
        }
        assert result[0].modified_by == {
            "uuid": self.database_query_result.modified_by_uuid,
            "display_name": self.database_query_result.modified_by_display_name,
        }

        assert result[0].mimetype == self.database_query_result.mimetype
        assert result[0].accepted == self.database_query_result.accepted
        assert (
            result[0].last_modified_date_time
            == self.database_query_result.last_modified_date_time
        )

    def test_get_directory_entries_for_intake_assigned_both_true_raises_exception(
        self,
    ):
        self.mock_infrastructures[
            "database"
        ].execute().fetchall.return_value = [self.database_query_result]

        page = 1
        page_size = 10

        self.mock_infrastructures["database"].reset_mock()
        self.user_info.permissions = ["documenten_intake_all"]
        self.query_instance.user_info = self.user_info
        with pytest.raises(exceptions.ValidationError) as excinfo:
            self.query_instance.get_directory_entries_for_intake(
                page=page,
                page_size=page_size,
                assigned=True,
                assigned_to_self=True,
            )
        assert excinfo.value.args == (
            "Cannot request both assigned filtes at the same time",
        )


class TestGetPreviewUrl(TestBase):
    def setup_method(self):
        self.mock_infrastructures = {
            "database": mock.MagicMock(),
            "s3": mock.MagicMock(),
        }

        self.user_info = mock.MagicMock()
        self.user_info.user_uuid = str(uuid4())
        self.user_info.permissions = {"pip_user": False}

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

        repo_factory = RepositoryFactory(infra_factory=self.mock_infra)
        repo_factory.register_repository("document", DocumentRepository)

        self.query_instance = Queries(
            repository_factory=repo_factory,
            context="testcontext",
            user_uuid=self.user_info.user_uuid,
        )
        self.query_instance.user_info = self.user_info

        self.document_uuid = str(uuid4())
        self.database_query_result = namedtuple(
            "DocumentData",
            [
                "id",
                "document_uuid",
                "filename",
                "extension",
                "store_uuid",
                "directory_uuid",
                "case_uuid",
                "case_display_number",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
                "accepted",
                "date_modified",
                "thumbnail",
                "creator_uuid",
                "creator_displayname",
                "type",
                "properties",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
                "description",
                "origin",
                "origin_date",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
                "intake_owner_uuid",
                "intake_group_uuid",
                "intake_role_uuid",
            ],
        )(
            id=3,
            document_uuid=self.document_uuid,
            filename="test_doc",
            extension=".pdf",
            store_uuid=uuid4(),
            directory_uuid=uuid4(),
            case_uuid=uuid4(),
            case_display_number=123,
            mimetype="application/pdf",
            size=1,
            storage_location=["minio"],
            md5="fake_md5",
            is_archivable=True,
            virus_scan_status="ok",
            accepted=True,
            date_modified="2019-05-06",
            thumbnail="fake_thumbnail",
            creator_uuid=uuid4(),
            creator_displayname="A.Admin",
            type="pdf",
            properties='{"displayname": "A.Admin"}',
            preview_uuid=uuid4(),
            preview_storage_location=["minio"],
            preview_mimetype="application/pdf",
            description="des",
            origin="Intern",
            origin_date="2015-3-15",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )

    @patch(
        "zsnl_domains.document.repositories.document.DocumentRepository.generate_preview_url"
    )
    def test_get_document_preview_link_generate_preview_url_called_correctly(
        self, mock_generate_preview_url
    ):
        self.load_query_instance(document)

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )
        # Create an EventService
        self.event_service = EventService(
            correlation_id=str(uuid4()),
            domain="test_domain",
            context="test_context",
            user_uuid=self.user_info.user_uuid,
        )
        document_repo = DocumentRepository(
            infrastructure_factory=self.mock_infra,
            context="test_context",
            event_service=self.event_service,
        )

        self.mock_infrastructures[
            "database"
        ].execute().fetchone.return_value = self.database_query_result

        self.mock_infrastructures["database"].reset_mock()

        self.query_instance.get_document_preview_link(
            document_uuid=self.document_uuid, user_info=self.user_info
        )

        document_repo.generate_preview_url.assert_called_once_with(
            preview_uuid=self.database_query_result.store_uuid,
            preview_storage_location=self.database_query_result.storage_location[
                0
            ],
            preview_mime_type=self.database_query_result.mimetype,
            preview_filename=self.database_query_result.filename
            + self.database_query_result.extension,
        )

    def test_get_document_preview_link(self):
        self.mock_infrastructures[
            "database"
        ].execute().fetchone.return_value = self.database_query_result
        self.mock_infrastructures[
            "s3"
        ].get_download_url.return_value = "https://example.com/example.txt"

        self.mock_infrastructures["database"].reset_mock()

        result = self.query_instance.get_document_preview_link(
            document_uuid=self.document_uuid, user_info=self.user_info
        )

        db_execute_calls = self.mock_infrastructures[
            "database"
        ].execute.call_args_list
        select_statement_1 = db_execute_calls[0][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        select_statement_2 = db_execute_calls[1][0][0].compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert len(db_execute_calls) == 2
        assert str(select_statement_1) == (
            "SELECT subject.uuid \n"
            "FROM subject \n"
            "WHERE subject.role_ids && array((SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.name IN (%(name_1_1)s, %(name_1_2)s))) AND subject.uuid = %(uuid_1)s"
        )
        assert str(select_statement_2) == (
            "SELECT file.uuid AS document_uuid, filestore.uuid AS store_uuid, filestore.mimetype, filestore.size, filestore.storage_location, filestore.md5, filestore.is_archivable, filestore.virus_scan_status, directory.uuid AS directory_uuid, zaak.uuid AS case_uuid, zaak.id AS case_display_number, file.name AS filename, file.extension, file.accepted, file.version, groups.uuid AS intake_group_uuid, roles.uuid AS intake_role_uuid, get_subject_by_legacy_id(file.intake_owner) AS intake_owner_uuid, get_subject_by_legacy_id(file.created_by) AS creator_uuid, get_subject_display_name_by_uuid(get_subject_by_legacy_id(file.created_by)) AS creator_displayname, file.date_modified, file.id, (SELECT filestore_1.uuid \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_1)s) AS preview_uuid, (SELECT filestore_1.storage_location \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_2)s) AS preview_storage_location, (SELECT filestore_1.mimetype \n"
            "FROM filestore AS filestore_1, file_derivative AS file_derivative_1 \n"
            "WHERE file_derivative_1.file_id = file.id AND file_derivative_1.type = %(type_1)s AND filestore.mimetype != %(mimetype_1)s AND filestore_1.id = file_derivative_1.filestore_id \n"
            " LIMIT %(param_3)s) AS preview_mimetype, file_metadata.description, file_metadata.origin, file_metadata.origin_date, file_metadata.trust_level AS confidentiality, file_metadata.document_category, (SELECT filestore_2.uuid \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_4)s) AS thumbnail_uuid, (SELECT filestore_2.mimetype \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_5)s) AS thumbnail_mimetype, (SELECT filestore_2.storage_location \n"
            "FROM filestore AS filestore_2, file_derivative AS file_derivative_2 \n"
            "WHERE file_derivative_2.file_id = file.id AND file_derivative_2.type = %(type_2)s AND filestore_2.id = file_derivative_2.filestore_id \n"
            " LIMIT %(param_6)s) AS thumbnail_storage_location, array((SELECT json_build_object(%(json_build_object_2)s, zaaktype_document_kenmerken_map.case_document_uuid, %(json_build_object_3)s, zaaktype_document_kenmerken_map.name, %(json_build_object_4)s, zaaktype_document_kenmerken_map.public_name, %(json_build_object_5)s, zaaktype_document_kenmerken_map.magic_string) AS json_build_object_1 \n"
            "FROM zaaktype_document_kenmerken_map JOIN (file_case_document JOIN zaak ON zaak.id = file_case_document.case_id) ON zaaktype_document_kenmerken_map.bibliotheek_kenmerken_id = file_case_document.bibliotheek_kenmerken_id AND zaaktype_document_kenmerken_map.zaaktype_node_id = zaak.zaaktype_node_id \n"
            "WHERE file_case_document.file_id = file.id AND file.case_id = zaak.id)) AS labels \n"
            "FROM file JOIN filestore ON file.filestore_id = filestore.id LEFT OUTER JOIN directory ON file.directory_id = directory.id LEFT OUTER JOIN zaak ON file.case_id = zaak.id LEFT OUTER JOIN file_metadata ON file_metadata.id = file.metadata_id LEFT OUTER JOIN groups ON groups.id = file.intake_group_id LEFT OUTER JOIN roles ON roles.id = file.intake_role_id \n"
            "WHERE file.active_version IS true AND file.uuid = %(uuid_1)s AND file.destroyed IS false AND (file.case_id IS NULL OR zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) OR file.uuid = %(uuid_3)s AND zaak.confidentiality != %(confidentiality_1)s AND file.confidential IS true AND (EXISTS (SELECT 1 \n"
            "FROM zaak JOIN zaaktype_authorisation ON zaaktype_authorisation.zaaktype_id = zaak.zaaktype_id AND zaaktype_authorisation.confidential IS true AND zaaktype_authorisation.recht = %(recht_1)s JOIN subject_position_matrix ON subject_position_matrix.role_id = zaaktype_authorisation.role_id AND subject_position_matrix.group_id = zaaktype_authorisation.ou_id JOIN subject ON subject.id = subject_position_matrix.subject_id \n"
            "WHERE zaak.id = file.case_id AND subject.uuid = %(uuid_4)s)))"
        )
        assert result == "https://example.com/example.txt"

    def test_get_document_preview_link_no_preview_found(self):

        self.mock_infrastructures[
            "database"
        ].execute().fetchone.return_value = namedtuple(
            "DocumentData",
            [
                "id",
                "document_uuid",
                "filename",
                "extension",
                "store_uuid",
                "directory_uuid",
                "case_uuid",
                "case_display_number",
                "mimetype",
                "size",
                "storage_location",
                "md5",
                "is_archivable",
                "virus_scan_status",
                "accepted",
                "date_modified",
                "thumbnail",
                "creator_uuid",
                "creator_displayname",
                "type",
                "properties",
                "preview_uuid",
                "preview_storage_location",
                "preview_mimetype",
                "description",
                "origin",
                "origin_date",
                "thumbnail_uuid",
                "thumbnail_storage_location",
                "thumbnail_mimetype",
                "intake_owner_uuid",
                "intake_group_uuid",
                "intake_role_uuid",
            ],
        )(
            id=3,
            document_uuid=self.document_uuid,
            filename="img.png",
            extension=".png",
            store_uuid=uuid4(),
            directory_uuid=uuid4(),
            case_uuid=uuid4(),
            case_display_number=123,
            mimetype="img/png",
            size=1,
            storage_location=["minio"],
            md5="fake_md5",
            is_archivable=True,
            virus_scan_status="ok",
            accepted=True,
            date_modified="2019-05-06",
            thumbnail="fake_thumbnail",
            creator_uuid=uuid4(),
            creator_displayname="A.Admin",
            type="pdf",
            properties='{"displayname": "A.Admin"}',
            preview_uuid=None,
            preview_storage_location=None,
            preview_mimetype=None,
            description="des",
            origin="Intern",
            origin_date="2015-12-03",
            thumbnail_uuid=None,
            thumbnail_storage_location=None,
            thumbnail_mimetype=None,
            intake_owner_uuid=None,
            intake_group_uuid=None,
            intake_role_uuid=None,
        )
        self.mock_infrastructures[
            "s3"
        ].get_download_url.return_value = "https://example.com/example.txt"

        self.mock_infrastructures["database"].reset_mock()

        with pytest.raises(NotFound) as excinfo:
            self.query_instance.get_document_preview_link(
                document_uuid=self.document_uuid, user_info=self.user_info
            )

        assert excinfo.value.args == (
            f"No preview found for document with uuid {self.document_uuid}",
            "document/preview_not_found",
        )


class DocumentEditUrl(TestBase):
    def setup_method(self):
        self.mock_infrastructures = {
            "database": mock.MagicMock(),
            "s3": mock.MagicMock(),
            "zoho": mock.MagicMock(),
        }

        self.user_info = mock.MagicMock()
        self.user_info.user_uuid = str(uuid4())
        self.user_info.permissions = {"pip_user": False}

        self.mock_infra = MockInfrastructureFactory(
            mock_infra=self.mock_infrastructures
        )

    def test_get_edit_document_url(self):

        self.user_info = UserInfo(
            user_uuid=str(uuid4()), permissions={"pip_user": True}
        )

        self.mock_infrastructures[
            "zoho"
        ].edit_document.return_value = "https://example.com/example.txt"
