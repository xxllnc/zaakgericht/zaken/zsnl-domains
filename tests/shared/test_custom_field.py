# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pydantic
import pytest
import random
import string
from pydantic import ValidationError
from typing import List
from uuid import uuid4
from zsnl_domains.shared import custom_field
from zsnl_domains.shared.custom_field.types import base
from zsnl_domains.shared.custom_field.types.geojson import GeoJSON


class TestCustomFieldType:
    fields = [
        "text",
        "richtext",
        "option",
        "select",
        "textarea",
        "email",
        "url",
        "bankaccount",
        "valuta",
        "numeric",
    ]
    single_field_schema = {
        "custom_fields": [
            {
                "is_required": False,
                "custom_field_type": "text",
                "magic_string": "ztc_summary",
                "label": "Custom label for text",
                "name": "Text name",
            },
            {
                "is_required": False,
                "custom_field_type": "textarea",
                "magic_string": "ztc_textarea",
                "label": "Custom label for textarea",
                "name": "Textarea name",
            },
            {
                "is_required": False,
                "custom_field_type": "richtext",
                "magic_string": "ztc_richtext",
                "label": "Custom label for richtext",
                "name": "Richtext name",
            },
            {
                "is_required": False,
                "custom_field_type": "email",
                "magic_string": "ztc_email",
                "label": "Custom label for email",
                "name": "Email name",
            },
            {
                "is_required": False,
                "custom_field_type": "url",
                "magic_string": "ztc_url",
                "label": "Custom label for url",
                "name": "URL name",
            },
            {
                "is_required": False,
                "custom_field_type": "bankaccount",
                "magic_string": "ztc_iban",
                "label": "Custom label for IBAN",
                "name": "IBAN name",
            },
            {
                "is_required": False,
                "custom_field_type": "valuta",
                "magic_string": "ztc_currency",
                "label": "Custom label for currency",
                "name": "Currency name",
            },
            {
                "is_required": False,
                "custom_field_type": "numeric",
                "magic_string": "age_value",
                "label": "Custom label for numeric value",
                "name": "Age Value",
            },
        ]
    }

    multi_field_schema = {
        "custom_fields": [
            {
                "is_required": False,
                "custom_field_type": "checkbox",
                "magic_string": "ztc_checkbox",
                "label": "Custom label for checkbox",
                "options": ["step1", "step2"],
                "name": "Checkbox name",
            }
        ]
    }

    options_schema = {
        "custom_fields": [
            {
                "is_required": False,
                "custom_field_type": "option",
                "magic_string": "ztc_option",
                "label": "Custom label for option",
                "options": ["step1", "step2"],
                "name": "Option name",
            }
        ]
    }

    def test_type_checking_for_single_fields(self):
        cf_model = custom_field.get_custom_fields_model(
            definition=custom_field.CustomFieldDefinition.parse_obj(
                self.single_field_schema
            )
        )

        values = {}
        for field in self.single_field_schema["custom_fields"]:
            value = "".join(
                random.choice(string.ascii_letters) for i in range(25)
            )

            if field["custom_field_type"] in ("select"):
                value = "step1"

            values[field["magic_string"]] = {"value": value}

        cfields = cf_model.parse_obj(values)
        for field in self.single_field_schema["custom_fields"]:
            attr = getattr(cfields, field["magic_string"])
            assert isinstance(attr, base.CustomFieldValueBase)
            assert attr.value

    def test_type_checking_for_multi_fields(self):
        cf_model = custom_field.get_custom_fields_model(
            definition=custom_field.CustomFieldDefinition.parse_obj(
                self.multi_field_schema
            )
        )

        values = {}
        for field in self.multi_field_schema["custom_fields"]:
            value = "".join(
                random.choice(string.ascii_letters) for i in range(25)
            )

            if field["custom_field_type"] in ("checkbox"):
                value = "step1"

            values[field["magic_string"]] = {"value": [value]}

        cfields = cf_model.parse_obj(values)
        for field in self.multi_field_schema["custom_fields"]:
            attr = getattr(cfields, field["magic_string"])
            assert isinstance(attr, base.CustomFieldValueMultiValueBase)
            assert isinstance(attr.value, List)
            assert len(attr.value) == 1

        values = {}
        for field in self.multi_field_schema["custom_fields"]:
            value = "".join(
                random.choice(string.ascii_letters) for i in range(25)
            )

            values[field["magic_string"]] = {"value": [value, value]}

        cfields = cf_model.parse_obj(values)
        for field in self.multi_field_schema["custom_fields"]:
            attr = getattr(cfields, field["magic_string"])
            assert len(attr.value) == 2

    def test_type_checking_for_date_field(self):
        cf_model = custom_field.get_custom_fields_model(
            definition=custom_field.CustomFieldDefinition.parse_obj(
                {
                    "custom_fields": [
                        {
                            "is_required": False,
                            "custom_field_type": "date",
                            "magic_string": "ztc_date",
                            "label": "Custom label fordate",
                            "name": "fordate name",
                        }
                    ]
                }
            )
        )

        with pytest.raises(ValidationError) as excinfo:
            cf_model.parse_obj({"ztc_date": {"value": "bogus"}})

        assert "Invalid isoformat" in str(excinfo.value)

        with pytest.raises(ValidationError) as excinfo:
            assert cf_model.parse_obj(
                {"ztc_date": {"value": "2020-04-01T12:00"}}
            )

        assert "Invalid isoformat" in str(excinfo.value)

        cf_model.parse_obj({"ztc_date": {"value": "2020-04-01"}})

    def test_validation_of_options_fields(self):
        definition = custom_field.CustomFieldDefinition.parse_obj(
            self.options_schema
        )
        cf_model = custom_field.get_custom_fields_model(definition=definition)

        good_values = {"ztc_option": "step1"}
        wrong_values = {"ztc_option": "stepnot"}

        for field in self.options_schema["custom_fields"]:
            fieldvalue = cf_model.parse_obj(
                {
                    field["magic_string"]: {
                        "value": good_values[field["magic_string"]]
                    }
                }
            )

            attr = getattr(fieldvalue, field["magic_string"])
            assert attr.value

            with pytest.raises(ValidationError) as excinfo:
                cf_model.parse_obj(
                    {
                        field["magic_string"]: {
                            "value": wrong_values[field["magic_string"]]
                        }
                    }
                )

            assert "must be one of the following options" in str(excinfo.value)

    def test_validation_of_different_options_fields(self):
        definition = custom_field.CustomFieldDefinition.parse_obj(
            {
                "custom_fields": [
                    {
                        "is_required": False,
                        "custom_field_type": "option",
                        "magic_string": "ztc_option",
                        "label": "Custom label for option",
                        "options": ["step1", "step2"],
                        "name": "Option name",
                    },
                    {
                        "is_required": False,
                        "custom_field_type": "select",
                        "magic_string": "ztc_select",
                        "label": "Custom label for select",
                        "options": ["step3", "step4"],
                        "name": "Select name",
                    },
                    {
                        "is_required": False,
                        "custom_field_type": "select",
                        "magic_string": "ztc_select2",
                        "label": "Custom label for select2",
                        "options": ["step5", "step6"],
                        "name": "Select2 name",
                    },
                ]
            }
        )

        cf_model = custom_field.get_custom_fields_model(definition=definition)
        assert cf_model.parse_obj(
            {"ztc_select2": {"value": ["step5", "step6"]}}
        )
        assert cf_model.parse_obj({"ztc_option": {"value": "step1"}})
        assert cf_model.parse_obj(
            {"ztc_select": {"value": ["step3", "step4"]}}
        )

    def test_is_required_created_required_attribute(self):
        definition = custom_field.CustomFieldDefinition.parse_obj(
            {
                "custom_fields": [
                    {
                        "is_required": True,
                        "custom_field_type": "option",
                        "magic_string": "ztc_option",
                        "label": "Custom label for option",
                        "options": ["step1", "step2"],
                        "name": "Option name",
                    }
                ]
            }
        )

        cf_model = custom_field.get_custom_fields_model(definition=definition)

        with pytest.raises(pydantic.ValidationError) as excinfo:
            cf_model.parse_obj({"ztc_no_required": {"value": "step5"}})

        assert "field required" in str(excinfo.value)

        assert cf_model.parse_obj({"ztc_option": {"value": "step1"}})

    def test_except_on_unknown_custom_field_type(self):
        with pytest.raises(pydantic.ValidationError) as excinfo:
            custom_field.CustomFieldDefinition.parse_obj(
                {
                    "custom_fields": [
                        {
                            "is_required": False,
                            "custom_field_type": "unknown",
                            "magic_string": "ztc_option",
                            "label": "Custom label for option",
                            "options": ["step1", "step2"],
                            "name": "Option name",
                        }
                    ]
                }
            )

        assert (
            "1 validation error for CustomFieldDefinition\ncustom_fields -> 0 -> custom_field_type\n  value is not a valid enumeration"
            in str(excinfo.value)
        )

    def test_except_on_missing_options(self):
        with pytest.raises(pydantic.ValidationError) as excinfo:
            custom_field.CustomFieldDefinition.parse_obj(
                {
                    "custom_fields": [
                        {
                            "is_required": False,
                            "custom_field_type": "option",
                            "magic_string": "ztc_option",
                            "label": "Custom label for option",
                            "options": [],
                            "name": "Option name",
                        }
                    ]
                }
            )

        assert "should have at least one option" in str(excinfo.value)

    def test_except_on_incorrect_definition(self):
        with pytest.raises(KeyError) as excinfo:
            custom_field.get_custom_fields_model(
                definition={"incorrect_one": True}
            )

        assert "Input parameter schema must be of type" in str(excinfo.value)

    def test_type_checking_for_geojson_field(self):
        cf_model = custom_field.get_custom_fields_model(
            definition=custom_field.CustomFieldDefinition.parse_obj(
                {
                    "custom_fields": [
                        {
                            "is_required": False,
                            "custom_field_type": "geojson",
                            "magic_string": "ztc_geojson",
                            "label": "Custom label for geojson",
                            "name": "GeoJSON name",
                        }
                    ]
                }
            )
        )

        with pytest.raises(ValidationError) as excinfo:
            cf_model.parse_obj({"ztc_geojson": {"value": "bogus"}})

        assert "value_error.jsondecode" in str(excinfo.value)

        with pytest.raises(ValidationError) as excinfo:
            cf_model.parse_obj(
                {
                    "ztc_geojson": {
                        "value": '{"type":"Feature","geometry": { "blaat":"Point","coordinates":[125.6, 10.1]} }'
                    }
                }
            )

        assert "valid GeoJSON structure" in str(excinfo.value)

        v = cf_model.parse_obj(
            {
                "ztc_geojson": {
                    "value": '{"type":"Feature","geometry": { "type":"Point","coordinates":[125.6, 10.1]} }'
                }
            }
        )

        assert isinstance(v.ztc_geojson.value, GeoJSON)
        assert v.ztc_geojson.value["type"] == "Feature"

        with pytest.raises(ValidationError) as excinfo:
            cf_model.parse_obj(
                {
                    "ztc_geojson": {
                        "value": {
                            "type": "Featuree",
                            "geometry": {
                                "type": "Point",
                                "coordinates": [125.6, 10.1],
                            },
                        }
                    }
                }
            )

        assert "nvalid GeoJSON format" in str(excinfo.value)

        v = cf_model.parse_obj(
            {
                "ztc_geojson": {
                    "value": {
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [125.6, 10.1],
                        },
                    }
                }
            }
        )

        assert isinstance(v.ztc_geojson.value, GeoJSON)
        assert v.ztc_geojson.value["type"] == "Feature"

    def test_type_checking_for_relationship_field(self):
        cf_model = custom_field.get_custom_fields_model(
            definition=custom_field.CustomFieldDefinition.parse_obj(
                {
                    "custom_fields": [
                        {
                            "is_required": False,
                            "custom_field_type": "relationship",
                            "magic_string": "ztc_document",
                            "label": "Custom label for relationship",
                            "name": "Relationship name",
                            "metadata": {
                                "type": "custom_object",
                                "uuid": uuid4(),
                                "name": "Testname",
                            },
                        }
                    ]
                }
            )
        )

        with pytest.raises(ValidationError) as excinfo:
            cf_model.parse_obj({"ztc_document": {"value": "bogus"}})

        assert "value is not a valid uuid" in str(excinfo.value)

        with pytest.raises(ValidationError) as excinfo:
            cf_model.parse_obj(
                {"ztc_document": {"value": {"filestore_bla": "bla"}}}
            )

        assert "value is not a valid uuid" in str(excinfo.value)

        # Document
        obj = cf_model.parse_obj(
            {
                "ztc_document": {
                    "value": uuid4(),
                    "specifics": {
                        "relationship_type": "document",
                        "metadata": {"filename": "test.txt"},
                    },
                }
            }
        )

        assert obj.ztc_document.specifics.metadata.filename == "test.txt"

        obj = cf_model.parse_obj(
            {
                "ztc_document": {
                    "value": uuid4(),
                    "specifics": {
                        "relationship_type": "custom_object",
                        "metadata": {
                            "summary": "contract",
                            "description": "contract details",
                        },
                    },
                }
            }
        )

        assert obj.ztc_document.specifics.metadata.summary == "contract"
        assert (
            obj.ztc_document.specifics.metadata.description
            == "contract details"
        )
