# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from tests.case_management.test_entity_util import (  # noqa: F401; pylint: disable=unused-variable
    test_case,
)
