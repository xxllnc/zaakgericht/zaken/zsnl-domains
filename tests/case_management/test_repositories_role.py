# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.exceptions import NotFound
from unittest import mock
from uuid import uuid4
from zsnl_domains.case_management.repositories.role import RoleRepository


class TestDepartmentRepository:
    def test_find_sunnyday(self):
        mock_db = mock.Mock()
        mock_db.execute().fetchone.return_value = "db row"
        mock_db.reset_mock()
        mock_self = mock.Mock(session=mock_db)
        mock_self._entity_from_row.return_value = "entity"

        role_uuid = uuid4()
        result = RoleRepository.find(mock_self, role_uuid)
        mock_self._entity_from_row.assert_called_once_with(row="db row")
        assert result == "entity"

    def test_find_notfound(self):
        mock_db = mock.Mock()
        mock_db.execute().fetchone.return_value = None
        mock_self = mock.Mock(session=mock_db)

        role_uuid = uuid4()

        with pytest.raises(NotFound):
            RoleRepository.find(mock_self, role_uuid)
