# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from datetime import datetime
from minty.cqrs import UserInfo, test
from minty.exceptions import ValidationError
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class TestAs_An_admin_or_zaakbeheerder_I_want_to_export_timeline_for_a_contact(
    test.TestBase
):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = UserInfo(user_uuid=uuid4(), permissions={})

    def test_export_timeline_for_person(self):
        person_uuid = uuid4()

        self.cmd.request_export(
            type="person",
            uuid=str(person_uuid),
        )
        self.assert_has_event_name("TimelineExportRequested")


class TestAs_An_admin_or_zaakbeheerder_I_want_to_run_timeline_export_for_a_contact(
    test.TestBase
):
    def setup_method(self):
        mock_s3_infra = mock.MagicMock()
        mock_s3_infra.upload.return_value = {
            "mime_type": "application/json",
            "md5": "123456",
            "size": "32",
            "storage_location": "minio",
        }
        self.load_command_instance(case_management, {"s3": mock_s3_infra})

        mock_entry_uuids = [uuid4(), uuid4()]
        mock_entries = [mock.Mock(), mock.Mock()]
        mock_entries[0].configure_mock(
            id=10,
            type="case/pip/feedback",
            created=datetime(
                year=2020, month=10, day=2, hour=10, minute=12, second=16
            ),
            description="Notitie toegevoegd",
            user="auser",
            exception="someting terribly went wrong",
            uuid=mock_entry_uuids[0],
            entity_meta_summary="Notitie toegevoegd",
            entity_id=mock_entry_uuids[0],
            case_uuid=uuid4(),
            zaak_id=10,
        )
        mock_entries[1].configure_mock(
            id=11,
            type="case/document/create",
            created=datetime(
                year=2020, month=10, day=1, hour=13, minute=44, second=27
            ),
            description='Document "testdoc-1" toegevoegd',
            user="auser",
            exception="someting terribly went wrong2",
            uuid=mock_entry_uuids[1],
            entity_meta_summary='Document "testdoc-1" toegevoegd',
            entity_id=mock_entry_uuids[1],
            case_uuid=uuid4(),
            zaak_id=1,
        )
        self.session.execute().fetchall.side_effect = [mock_entries, []]
        self.session.execute.reset_mock()

        self.cmd.user_info = UserInfo(
            user_uuid=uuid4(),
            permissions={"admin": False},
        )

    def test_run_timeline_export_for_person(self):
        contact_uuid = str(uuid4())
        self.cmd.run_export(
            type="person",
            uuid=contact_uuid,
        )

        self.assert_has_event_name("TimelineExportCreated")

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 6

        zaak_betrokkene_select_statement = call_list[0][0][0]
        assert str(
            zaak_betrokkene_select_statement.compile(
                dialect=postgresql.dialect()
            )
        ) == (
            "SELECT %(param_1)s AS type, natuurlijk_persoon.id \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_1)s"
        )

        logging_entries_select_statement = call_list[1][0][0]

        assert str(
            logging_entries_select_statement.compile(
                dialect=postgresql.dialect(),
                compile_kwargs={"render_postcompile": True},
            )
        ) == (
            "WITH anon_1 AS \n"
            '(SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, %(param_3)s AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_4)s AS attribute_value \n'
            "FROM logging \n"
            'WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_1_1)s, %(event_type_1_2)s, %(event_type_1_3)s, %(event_type_1_4)s, %(event_type_1_5)s, %(event_type_1_6)s, %(event_type_1_7)s, %(event_type_1_8)s, %(event_type_1_9)s, %(event_type_1_10)s, %(event_type_1_11)s, %(event_type_1_12)s, %(event_type_1_13)s, %(event_type_1_14)s, %(event_type_1_15)s, %(event_type_1_16)s, %(event_type_1_17)s, %(event_type_1_18)s, %(event_type_1_19)s, %(event_type_1_20)s, %(event_type_1_21)s, %(event_type_1_22)s, %(event_type_1_23)s, %(event_type_1_24)s, %(event_type_1_25)s, %(event_type_1_26)s, %(event_type_1_27)s, %(event_type_1_28)s, %(event_type_1_29)s, %(event_type_1_30)s, %(event_type_1_31)s, %(event_type_1_32)s, %(event_type_1_33)s, %(event_type_1_34)s, %(event_type_1_35)s, %(event_type_1_36)s, %(event_type_1_37)s, %(event_type_1_38)s, %(event_type_1_39)s, %(event_type_1_40)s, %(event_type_1_41)s, %(event_type_1_42)s) AND ((CAST(logging.event_data AS JSON) ->> %(param_5)s) = %(param_6)s OR logging.created_for = %(created_for_1)s) UNION SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id AS zaak_id, CAST(logging.event_data AS JSON) ->> %(param_7)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_8)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_9)s AS attribute_value \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) JOIN zaak_betrokkenen ON zaak.aanvrager = zaak_betrokkenen.id AND zaak_betrokkenen.subject_id = %(subject_id_1)s \n"
            "WHERE logging.restricted IS false AND logging.event_type IN (%(event_type_2_1)s, %(event_type_2_2)s, %(event_type_2_3)s, %(event_type_2_4)s, %(event_type_2_5)s, %(event_type_2_6)s, %(event_type_2_7)s, %(event_type_2_8)s, %(event_type_2_9)s, %(event_type_2_10)s, %(event_type_2_11)s, %(event_type_2_12)s, %(event_type_2_13)s, %(event_type_2_14)s, %(event_type_2_15)s, %(event_type_2_16)s, %(event_type_2_17)s, %(event_type_2_18)s, %(event_type_2_19)s, %(event_type_2_20)s, %(event_type_2_21)s, %(event_type_2_22)s, %(event_type_2_23)s, %(event_type_2_24)s, %(event_type_2_25)s, %(event_type_2_26)s, %(event_type_2_27)s, %(event_type_2_28)s, %(event_type_2_29)s, %(event_type_2_30)s, %(event_type_2_31)s, %(event_type_2_32)s, %(event_type_2_33)s, %(event_type_2_34)s, %(event_type_2_35)s, %(event_type_2_36)s, %(event_type_2_37)s, %(event_type_2_38)s, %(event_type_2_39)s, %(event_type_2_40)s, %(event_type_2_41)s, %(event_type_2_42)s))\n"
            ' SELECT anon_1.uuid, anon_1.type, anon_1.description, anon_1."user", anon_1.created, anon_1.zaak_id, anon_1.comment, anon_1.exception, anon_1.case_uuid, anon_1.attribute_value \n'
            "FROM anon_1 ORDER BY created DESC \n"
            " LIMIT %(param_10)s OFFSET %(param_11)s"
        )

        insert_filestore_statement = call_list[4][0][0]
        insert_compiled = insert_filestore_statement.compile(
            dialect=postgresql.dialect()
        )

        assert insert_compiled.params["mimetype"] == "application/json"
        assert insert_compiled.params["size"] == "32"
        assert insert_compiled.params["storage_location"] == ["minio"]
        assert str(insert_compiled) == (
            "INSERT INTO filestore (uuid, original_name, size, mimetype, md5, storage_location, is_archivable, virus_scan_status) VALUES (%(uuid)s, %(original_name)s, %(size)s, %(mimetype)s, %(md5)s, %(storage_location)s::VARCHAR[], %(is_archivable)s, %(virus_scan_status)s) RETURNING filestore.id"
        )

        insert_export_queue_statement = call_list[5][0][0]
        insert_compiled = insert_export_queue_statement.compile(
            dialect=postgresql.dialect()
        )
        assert (
            insert_compiled.params["subject_uuid"]
            == self.cmd.user_info.user_uuid
        )
        assert str(insert_compiled) == (
            "INSERT INTO export_queue (uuid, subject_id, subject_uuid, expires, token, filestore_id, filestore_uuid, downloaded) VALUES (%(uuid)s, (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s AND subject.subject_type = %(subject_type_1)s), %(subject_uuid)s, %(expires)s, %(token)s, %(filestore_id)s, %(filestore_uuid)s, %(downloaded)s) RETURNING export_queue.id"
        )

    def test_run_timeline_export_for_employee(self):
        contact_uuid = str(uuid4())
        self.cmd.run_export(
            type="employee",
            uuid=contact_uuid,
            period_start="2021-02-03T12:53:00+01:00",
            period_end="2021-02-04T12:53:00Z",
        )

        self.assert_has_event_name("TimelineExportCreated")

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 6

        zaak_betrokkene_select_statement = call_list[0][0][0]
        assert str(
            zaak_betrokkene_select_statement.compile(
                dialect=postgresql.dialect()
            )
        ) == (
            "SELECT %(param_1)s AS type, subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s AND subject.subject_type = %(subject_type_1)s"
        )

    def test_run_timeline_export_for_organization(self):
        contact_uuid = str(uuid4())
        self.cmd.run_export(
            type="organization",
            uuid=contact_uuid,
        )

        self.assert_has_event_name("TimelineExportCreated")

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 6

        zaak_betrokkene_select_statement = call_list[0][0][0]
        assert str(
            zaak_betrokkene_select_statement.compile(
                dialect=postgresql.dialect()
            )
        ) == (
            "SELECT %(param_1)s AS type, bedrijf.id \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.uuid = %(uuid_1)s"
        )

    def test_run_timeline_export_for_case(self):
        case_uuid = str(uuid4())
        self.cmd.run_export(
            type="case",
            uuid=case_uuid,
        )

        self.assert_has_event_name("TimelineExportCreated")

        call_list = self.session.execute.call_args_list

        assert len(call_list) == 4

        logging_entries_select_statement = call_list[0][0][0]
        compiled = logging_entries_select_statement.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert str(compiled) == (
            'SELECT logging.uuid AS uuid, logging.event_type AS type, logging.onderwerp AS description, logging.created_by_name_cache AS "user", coalesce(logging.created, logging.last_modified) AS created, logging.zaak_id, CAST(logging.event_data AS JSON) ->> %(param_1)s AS comment, CAST(logging.event_data AS JSON) ->> %(param_2)s AS exception, zaak.uuid AS case_uuid, CAST(logging.event_data AS JSON) ->> %(param_3)s AS attribute_value \n'
            "FROM logging JOIN zaak ON logging.zaak_id = zaak.id \n"
            "WHERE zaak.uuid = %(uuid_1)s AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_2)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) ORDER BY logging.created DESC \n"
            " LIMIT %(param_4)s OFFSET %(param_5)s"
        )

        insert_filestore_statement = call_list[2][0][0]
        insert_compiled = insert_filestore_statement.compile(
            dialect=postgresql.dialect()
        )

        assert insert_compiled.params["mimetype"] == "application/json"
        assert insert_compiled.params["size"] == "32"
        assert insert_compiled.params["storage_location"] == ["minio"]
        assert str(insert_compiled) == (
            "INSERT INTO filestore (uuid, original_name, size, mimetype, md5, storage_location, is_archivable, virus_scan_status) VALUES (%(uuid)s, %(original_name)s, %(size)s, %(mimetype)s, %(md5)s, %(storage_location)s::VARCHAR[], %(is_archivable)s, %(virus_scan_status)s) RETURNING filestore.id"
        )

        insert_export_queue_statement = call_list[3][0][0]
        insert_compiled = insert_export_queue_statement.compile(
            dialect=postgresql.dialect()
        )
        assert (
            insert_compiled.params["subject_uuid"]
            == self.cmd.user_info.user_uuid
        )
        assert str(insert_compiled) == (
            "INSERT INTO export_queue (uuid, subject_id, subject_uuid, expires, token, filestore_id, filestore_uuid, downloaded) VALUES (%(uuid)s, (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s AND subject.subject_type = %(subject_type_1)s), %(subject_uuid)s, %(expires)s, %(token)s, %(filestore_id)s, %(filestore_uuid)s, %(downloaded)s) RETURNING export_queue.id"
        )

    def test_run_timeline_export_fails_for_invalid_type(self):
        contact_uuid = str(uuid4())
        with pytest.raises(ValidationError) as excinfo:
            self.cmd.run_export(
                type="object",
                uuid=contact_uuid,
            )
        assert (
            excinfo.value.args[0][0]["message"]
            == "'object' is not one of ['person', 'organization', 'employee', 'case']"
        )
