# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.cqrs.test
import minty.exceptions
import pytest
import sqlalchemy.exc as sql_exc
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import Task

RETRIEVE_TASK_QUERY = (
    "SELECT zaak.uuid AS case_uuid, zaak.id AS case_id, zaak.milestone AS case_milestone, zaak.uuid AS case_status, checklist.case_milestone AS phase, checklist_item.uuid, checklist_item.label, checklist_item.description, checklist_item.due_date, checklist_item.state, checklist_item.user_defined, subject.uuid AS assignee_uuid, get_display_name_for_employee(hstore(ARRAY[%(param_1)s], ARRAY[subject.properties])) AS assignee_display_name, subject.properties AS assignee_properties, natuurlijk_persoon.uuid AS assignee_person_uuid, get_display_name_for_person(hstore(ARRAY[%(param_2)s, %(param_3)s, %(param_4)s], ARRAY[natuurlijk_persoon.voorletters, natuurlijk_persoon.naamgebruik, natuurlijk_persoon.geslachtsnaam])) AS assignee_person_display_name, bedrijf.uuid AS assignee_organization_uuid, get_display_name_for_company(hstore(ARRAY[%(param_5)s], ARRAY[bedrijf.handelsnaam])) AS assignee_organization_display_name, groups.name AS department_name, groups.uuid AS department_uuid, zaaktype_node.titel AS case_type_title, zaaktype.uuid AS case_type_uuid, CAST(coalesce(CAST(zaaktype_node.properties AS JSON) ->> %(param_6)s, %(coalesce_1)s) AS BOOLEAN) AS allow_external_task_assignment \n"
    "FROM checklist JOIN checklist_item ON checklist.id = checklist_item.checklist_id JOIN zaak ON zaak.id = checklist.case_id JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id JOIN zaaktype ON zaak.zaaktype_id = zaaktype.id LEFT OUTER JOIN subject ON checklist_item.assignee_id = subject.id LEFT OUTER JOIN natuurlijk_persoon ON checklist_item.assignee_person_id = natuurlijk_persoon.id LEFT OUTER JOIN bedrijf ON checklist_item.assignee_organization_id = bedrijf.id JOIN groups ON zaak.route_ou = groups.id \n"
    "WHERE checklist_item.uuid = %(uuid_1)s AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
    "FROM subject \n"
    "WHERE subject.uuid = %(uuid_2)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
    "FROM subject \n"
    "WHERE subject.uuid = %(uuid_2)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
    "FROM subject \n"
    "WHERE subject.uuid = %(uuid_2)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
    "FROM case_acl \n"
    "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s)))"
)


@pytest.fixture
def task_db_row_maker():
    def db_row_maker(**kwargs):
        db_row = mock.Mock(name="mock_task_row")

        task_values = {
            "case_uuid": uuid4(),
            "case_id": 123,
            "case_milestone": 3,
            "case_status": "open",
            "phase": 3,
            "uuid": uuid4(),
            "label": "A Task",
            "description": "Longer description",
            "due_date": None,
            "state": False,
            "user_defined": True,
            "assignee_uuid": None,
            "assignee_display_name": None,
            "assignee_person_uuid": None,
            "assignee_person_display_name": None,
            "assignee_organization_uuid": None,
            "assignee_organization_display_name": None,
            "allow_external_task_assignment": False,
            **kwargs,
        }

        db_row.configure_mock(**task_values)
        return db_row

    return db_row_maker


class TaskTestBase(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.cmd.user_uuid = self.user_info.user_uuid
        self.cmd.user_info = self.user_info


class Test_Get_Task_List(minty.cqrs.test.TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.user_uuid = uuid4()
        self.user_info = minty.cqrs.UserInfo(
            user_uuid=self.user_uuid,
            permissions={"admin": False},
        )
        self.qry.user_uuid = self.user_info.user_uuid
        self.qry.user_info = self.user_info

    def test_get_task_list_minimal(self, task_db_row_maker):
        self.session.execute().fetchall.side_effect = [
            [
                task_db_row_maker(
                    assignee_person_uuid=uuid4(),
                    assignee_person_display_name="Piet Friet",
                ),
                task_db_row_maker(
                    assignee_organization_uuid=uuid4(),
                    assignee_organization_display_name="Piet Friet BV",
                ),
            ]
        ]
        self.session.reset_mock()
        result = self.qry.get_task_list(page=10, page_size=10)

        assert len(result) == 2
        assert isinstance(result[0], Task)

        t1: Task = result[0]
        t2: Task = result[1]

        assert t1.assignee is not None
        assert t1.assignee.type == "person"
        assert t1.assignee.display_name == "Piet Friet"
        assert t2.assignee is not None
        assert t2.assignee.type == "organization"
        assert t2.assignee.display_name == "Piet Friet BV"

        query = self.session.execute.call_args_list[0][0][0]
        query_compiled = query.compile(dialect=postgresql.dialect())

        assert str(query_compiled) == (
            "SELECT zaak.uuid AS case_uuid, zaak.id AS case_id, zaak.milestone AS case_milestone, zaak.uuid AS case_status, checklist.case_milestone AS phase, checklist_item.uuid, checklist_item.label, checklist_item.description, checklist_item.due_date, checklist_item.state, checklist_item.user_defined, subject.uuid AS assignee_uuid, get_display_name_for_employee(hstore(ARRAY[%(param_1)s], ARRAY[subject.properties])) AS assignee_display_name, subject.properties AS assignee_properties, natuurlijk_persoon.uuid AS assignee_person_uuid, get_display_name_for_person(hstore(ARRAY[%(param_2)s, %(param_3)s, %(param_4)s], ARRAY[natuurlijk_persoon.voorletters, natuurlijk_persoon.naamgebruik, natuurlijk_persoon.geslachtsnaam])) AS assignee_person_display_name, bedrijf.uuid AS assignee_organization_uuid, get_display_name_for_company(hstore(ARRAY[%(param_5)s], ARRAY[bedrijf.handelsnaam])) AS assignee_organization_display_name, groups.name AS department_name, groups.uuid AS department_uuid, zaaktype_node.titel AS case_type_title, zaaktype.uuid AS case_type_uuid, CAST(coalesce(CAST(zaaktype_node.properties AS JSON) ->> %(param_6)s, %(coalesce_1)s) AS BOOLEAN) AS allow_external_task_assignment \n"
            "FROM checklist JOIN checklist_item ON checklist.id = checklist_item.checklist_id JOIN zaak ON zaak.id = checklist.case_id JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id JOIN zaaktype ON zaak.zaaktype_id = zaaktype.id LEFT OUTER JOIN subject ON checklist_item.assignee_id = subject.id LEFT OUTER JOIN natuurlijk_persoon ON checklist_item.assignee_person_id = natuurlijk_persoon.id LEFT OUTER JOIN bedrijf ON checklist_item.assignee_organization_id = bedrijf.id JOIN groups ON zaak.route_ou = groups.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) ORDER BY checklist.case_milestone, checklist_item.sequence, checklist_item.id \n"
            " LIMIT %(param_7)s OFFSET %(param_8)s"
        )
        assert query_compiled.params == {
            "aanvrager_type_1": "medewerker",
            "coalesce_1": "0",
            "param_1": "properties",
            "param_2": "voorletters",
            "param_3": "naamgebruik",
            "param_4": "geslachtsnaam",
            "param_5": "handelsnaam",
            "param_6": "allow_external_task_assignment",
            "param_7": 10,
            "param_8": 90,
            "permission_1": "read",
            "status_1": "deleted",
            "subject_uuid_1": self.user_uuid,
            "uuid_1": self.user_uuid,
        }

    def test_get_task_list_full(self, task_db_row_maker):
        self.session.execute().fetchall.side_effect = [
            [
                task_db_row_maker(
                    assignee_person_uuid=uuid4(),
                    assignee_person_display_name="Piet Friet",
                ),
                task_db_row_maker(
                    assignee_organization_uuid=uuid4(),
                    assignee_organization_display_name="Piet Friet BV",
                ),
            ]
        ]
        self.session.reset_mock()

        assignee_uuid = uuid4()
        case_uuid = uuid4()
        case_id = 1234
        case_type_uuid = uuid4()
        department_uuid = uuid4()

        result = self.qry.get_task_list(
            page=10,
            page_size=10,
            assignee_uuids=[assignee_uuid],
            case_uuids=[case_uuid],
            case_ids=[case_id],
            case_type_uuids=[case_type_uuid],
            department_uuids=[department_uuid],
            title_words=["ti%tle", "words"],
            keyword="keyword",
            phase=3,
            completed=False,
            sort="relationships.case.number",
        )

        assert len(result) == 2
        assert isinstance(result[0], Task)

        t1: Task = result[0]
        t2: Task = result[1]

        assert t1.assignee is not None
        assert t1.assignee.type == "person"
        assert t1.assignee.display_name == "Piet Friet"
        assert t2.assignee is not None
        assert t2.assignee.type == "organization"
        assert t2.assignee.display_name == "Piet Friet BV"

        query = self.session.execute.call_args_list[0][0][0]
        query_compiled = query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(query_compiled) == (
            "SELECT zaak.uuid AS case_uuid, zaak.id AS case_id, zaak.milestone AS case_milestone, zaak.uuid AS case_status, checklist.case_milestone AS phase, checklist_item.uuid, checklist_item.label, checklist_item.description, checklist_item.due_date, checklist_item.state, checklist_item.user_defined, subject.uuid AS assignee_uuid, get_display_name_for_employee(hstore(ARRAY[%(param_1)s], ARRAY[subject.properties])) AS assignee_display_name, subject.properties AS assignee_properties, natuurlijk_persoon.uuid AS assignee_person_uuid, get_display_name_for_person(hstore(ARRAY[%(param_2)s, %(param_3)s, %(param_4)s], ARRAY[natuurlijk_persoon.voorletters, natuurlijk_persoon.naamgebruik, natuurlijk_persoon.geslachtsnaam])) AS assignee_person_display_name, bedrijf.uuid AS assignee_organization_uuid, get_display_name_for_company(hstore(ARRAY[%(param_5)s], ARRAY[bedrijf.handelsnaam])) AS assignee_organization_display_name, groups.name AS department_name, groups.uuid AS department_uuid, zaaktype_node.titel AS case_type_title, zaaktype.uuid AS case_type_uuid, CAST(coalesce(CAST(zaaktype_node.properties AS JSON) ->> %(param_6)s, %(coalesce_1)s) AS BOOLEAN) AS allow_external_task_assignment \n"
            "FROM checklist JOIN checklist_item ON checklist.id = checklist_item.checklist_id JOIN zaak ON zaak.id = checklist.case_id JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id JOIN zaaktype ON zaak.zaaktype_id = zaaktype.id LEFT OUTER JOIN subject ON checklist_item.assignee_id = subject.id LEFT OUTER JOIN natuurlijk_persoon ON checklist_item.assignee_person_id = natuurlijk_persoon.id LEFT OUTER JOIN bedrijf ON checklist_item.assignee_organization_id = bedrijf.id JOIN groups ON zaak.route_ou = groups.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND zaak.uuid IN (%(uuid_2_1)s) AND zaak.id IN (%(id_1_1)s) AND zaaktype.uuid IN (%(uuid_3_1)s) AND subject.uuid IN (%(uuid_4_1)s) AND groups.uuid IN (%(uuid_5_1)s) AND (checklist_item.label ILIKE %(label_1)s ESCAPE '~' OR checklist_item.label ILIKE %(label_2)s ESCAPE '~') AND checklist_item.label ILIKE %(label_3)s ESCAPE '~' AND checklist.case_milestone = %(case_milestone_1)s AND checklist_item.state = false ORDER BY zaak.id ASC, checklist.case_milestone, checklist_item.sequence, checklist_item.id \n"
            " LIMIT %(param_7)s OFFSET %(param_8)s"
        )
        assert query_compiled.params == {
            "aanvrager_type_1": "medewerker",
            "coalesce_1": "0",
            "param_1": "properties",
            "param_2": "voorletters",
            "param_3": "naamgebruik",
            "param_4": "geslachtsnaam",
            "param_5": "handelsnaam",
            "param_6": "allow_external_task_assignment",
            "param_7": 10,
            "param_8": 90,
            "permission_1": "read",
            "status_1": "deleted",
            "subject_uuid_1": self.user_uuid,
            "uuid_1": self.user_uuid,
            "case_milestone_1": 3,
            "id_1_1": 1234,
            "label_1": "%ti~%tle%",
            "label_2": "%words%",
            "label_3": "%keyword%",
            "uuid_2_1": case_uuid,
            "uuid_3_1": case_type_uuid,
            "uuid_4_1": assignee_uuid,
            "uuid_5_1": department_uuid,
        }


class Test_Task_Create(TaskTestBase):
    def test_create_task(self):
        case_uuid = uuid4()
        task_uuid = uuid4()

        mock_checklist = mock.Mock()
        mock_checklist.id = 100

        self.session.execute().fetchone.side_effect = [
            # When creating a checklist_item, a checklist_id is needed
            mock_checklist,
        ]
        self.session.reset_mock()

        mock_case = mock.Mock(uuid=case_uuid, milestone=3)

        # Case repository is tested elsewhere
        mock_case_repo = mock.Mock()
        mock_case_repo.find_case_by_uuid.side_effect = [mock_case]

        self.cmd.repository_factory.repositories["case"] = mock_case_repo
        self.cmd.create_task(
            case_uuid=str(case_uuid),
            task_uuid=str(task_uuid),
            title="A Task",
            phase=3,
        )

        # Assert that the current user has write access to the case
        mock_case_repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=case_uuid,
            user_info=self.cmd.user_info,
            permission="write",
        )

        get_checklist = self.session.execute.call_args_list[0][0][0]
        insert_task = self.session.execute.call_args_list[1][0][0]

        compiled_get_checklist = get_checklist.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_get_checklist) == (
            "SELECT checklist.id \n"
            "FROM checklist \n"
            "WHERE checklist.case_id = (SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s) AND checklist.case_milestone = %(case_milestone_1)s"
        )
        assert compiled_get_checklist.params == {
            "uuid_1": str(case_uuid),
            "case_milestone_1": 3,
        }

        compiled_insert_task = insert_task.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(compiled_insert_task)
            == "INSERT INTO checklist_item (uuid, checklist_id, label, state, sequence, user_defined, due_date, description, assignee_id) VALUES (%(uuid)s, %(checklist_id)s, %(label)s, %(state)s, %(sequence)s, %(user_defined)s, %(due_date)s, %(description)s, %(assignee_id)s) RETURNING checklist_item.id"
        )
        assert compiled_insert_task.params == {
            "assignee_id": None,
            "checklist_id": 100,
            "description": "",
            "due_date": None,
            "label": "A Task",
            "sequence": None,
            "state": False,
            "user_defined": True,
            "uuid": task_uuid,
        }

    def test_create_task_new_checklist(self):
        case_uuid = uuid4()
        task_uuid = uuid4()

        mock_checklist = None

        self.session.execute().fetchone.side_effect = [
            # When creating a checklist_item, a checklist_id is needed
            mock_checklist,
        ]
        self.session.execute().inserted_primary_key = [100]
        self.session.reset_mock()

        mock_case = mock.Mock(uuid=case_uuid, milestone=3)

        # Case repository is tested elsewhere
        mock_case_repo = mock.Mock()
        mock_case_repo.find_case_by_uuid.side_effect = [mock_case]

        self.cmd.repository_factory.repositories["case"] = mock_case_repo
        self.cmd.create_task(
            case_uuid=str(case_uuid),
            task_uuid=str(task_uuid),
            title="A Task",
            phase=3,
        )

        # Assert that the current user has write access to the case
        mock_case_repo.find_case_by_uuid.assert_called_once_with(
            case_uuid=case_uuid,
            user_info=self.cmd.user_info,
            permission="write",
        )

        get_checklist = self.session.execute.call_args_list[0][0][0]
        insert_tasklist = self.session.execute.call_args_list[1][0][0]
        insert_task = self.session.execute.call_args_list[2][0][0]

        compiled_get_checklist = get_checklist.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_get_checklist) == (
            "SELECT checklist.id \n"
            "FROM checklist \n"
            "WHERE checklist.case_id = (SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s) AND checklist.case_milestone = %(case_milestone_1)s"
        )
        assert compiled_get_checklist.params == {
            "uuid_1": str(case_uuid),
            "case_milestone_1": 3,
        }

        compiled_insert_tasklist = insert_tasklist.compile(
            dialect=postgresql.dialect()
        )
        assert str(compiled_insert_tasklist) == (
            "INSERT INTO checklist (case_id, case_milestone) VALUES ((SELECT zaak.id \n"
            "FROM zaak \n"
            "WHERE zaak.uuid = %(uuid_1)s), %(case_milestone)s) RETURNING checklist.id"
        )
        assert compiled_insert_tasklist.params == {
            "uuid_1": str(case_uuid),
            "case_milestone": 3,
        }

        compiled_insert_task = insert_task.compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(compiled_insert_task)
            == "INSERT INTO checklist_item (uuid, checklist_id, label, state, sequence, user_defined, due_date, description, assignee_id) VALUES (%(uuid)s, %(checklist_id)s, %(label)s, %(state)s, %(sequence)s, %(user_defined)s, %(due_date)s, %(description)s, %(assignee_id)s) RETURNING checklist_item.id"
        )
        assert compiled_insert_task.params == {
            "assignee_id": None,
            "checklist_id": 100,
            "description": "",
            "due_date": None,
            "label": "A Task",
            "sequence": None,
            "state": False,
            "user_defined": True,
            "uuid": task_uuid,
        }

    def test_create_task_case_not_allowed(self):
        case_uuid = uuid4()
        task_uuid = uuid4()

        # Case repository is tested elsewhere
        mock_case_repo = mock.Mock()
        mock_case_repo.find_case_by_uuid.side_effect = [
            minty.exceptions.NotFound
        ]

        self.cmd.repository_factory.repositories["case"] = mock_case_repo

        with pytest.raises(minty.exceptions.Forbidden):
            self.cmd.create_task(
                case_uuid=str(case_uuid),
                task_uuid=str(task_uuid),
                title="A Task",
                phase=3,
            )

    def test_create_task_case_invalid_phase(self):
        case_uuid = uuid4()
        task_uuid = uuid4()

        mock_checklist = mock.Mock()
        mock_checklist.id = 100

        self.session.execute().fetchone.side_effect = [
            # When creating a checklist_item, a checklist_id is needed
            mock_checklist,
        ]
        self.session.reset_mock()

        mock_case = mock.Mock(uuid=case_uuid, milestone=5)

        # Case repository is tested elsewhere
        mock_case_repo = mock.Mock()
        mock_case_repo.find_case_by_uuid.side_effect = [mock_case]

        self.cmd.repository_factory.repositories["case"] = mock_case_repo

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.create_task(
                case_uuid=str(case_uuid),
                task_uuid=str(task_uuid),
                title="A Task",
                phase=3,
            )

    def test_create_task_case_resolved_case(self):
        case_uuid = uuid4()
        task_uuid = uuid4()

        mock_checklist = mock.Mock()
        mock_checklist.id = 100

        self.session.execute().fetchone.side_effect = [
            # When creating a checklist_item, a checklist_id is needed
            mock_checklist,
        ]
        self.session.reset_mock()

        mock_case = mock.Mock(uuid=case_uuid, milestone=3, status="resolved")

        # Case repository is tested elsewhere
        mock_case_repo = mock.Mock()
        mock_case_repo.find_case_by_uuid.side_effect = [mock_case]

        self.cmd.repository_factory.repositories["case"] = mock_case_repo

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.create_task(
                case_uuid=str(case_uuid),
                task_uuid=str(task_uuid),
                title="A Task",
                phase=3,
            )

    def test_create_task_case_existing_uuid(self):
        case_uuid = uuid4()
        task_uuid = uuid4()

        mock_checklist = mock.Mock()
        mock_checklist.id = 100

        self.session.execute().fetchone.side_effect = [
            # When creating a checklist_item, a checklist_id is needed
            mock_checklist,
        ]
        self.session.execute.side_effect = [
            self.session.execute(),
            sql_exc.IntegrityError("foo", {}, ""),
        ]
        self.session.reset_mock()

        mock_case = mock.Mock(uuid=case_uuid, milestone=3, status="open")

        # Case repository is tested elsewhere
        mock_case_repo = mock.Mock()
        mock_case_repo.find_case_by_uuid.side_effect = [mock_case]

        self.cmd.repository_factory.repositories["case"] = mock_case_repo

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.create_task(
                case_uuid=str(case_uuid),
                task_uuid=str(task_uuid),
                title="A Task",
                phase=3,
            )


class Test_Task_Update(TaskTestBase):
    def test_update_task_assignee_employee(self, task_db_row_maker):
        task_uuid = uuid4()
        assignee_uuid = uuid4()

        mock_employee_row = mock.Mock()
        mock_employee_row.id = 123

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(uuid=task_uuid),
            mock_employee_row,
        ]
        self.session.execute().inserted_primary_key = [321]
        self.session.reset_mock()

        self.cmd.update_task(
            task_uuid=str(task_uuid),
            title="New Title",
            description="New Description",
            due_date="2022-12-06",
            assignee={"type": "employee", "id": assignee_uuid},
        )

        get_task_query = self.session.execute.call_args_list[0][0][0]
        insert_logging_query = self.session.execute.call_args_list[1][0][0]
        insert_message_query = self.session.execute.call_args_list[2][0][0]
        get_assignee_query = self.session.execute.call_args_list[3][0][0]
        update_task_query = self.session.execute.call_args_list[4][0][0]

        get_task_compiled = get_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_task_compiled) == RETRIEVE_TASK_QUERY

        get_assignee_compiled = get_assignee_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_assignee_compiled) == (
            "SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s"
        )
        assert get_assignee_compiled.params == {"uuid_1": str(assignee_uuid)}

        update_task_compiled = update_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(update_task_compiled) == (
            "UPDATE checklist_item SET label=%(label)s, due_date=%(due_date)s, description=%(description)s, assignee_id=%(assignee_id)s, assignee_person_id=%(assignee_person_id)s, assignee_organization_id=%(assignee_organization_id)s WHERE checklist_item.uuid = %(uuid_1)s"
        )
        assert update_task_compiled.params == {
            "label": "New Title",
            "due_date": "2022-12-06",
            "description": "New Description",
            "assignee_id": 123,
            "assignee_organization_id": None,
            "assignee_person_id": None,
            "uuid_1": task_uuid,
        }

        insert_logging_compiled = insert_logging_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_logging_compiled) == (
            "INSERT INTO logging (zaak_id, component, onderwerp, event_type, event_data, created_by_name_cache, restricted) VALUES (%(zaak_id)s, %(component)s, %(onderwerp)s, %(event_type)s, %(event_data)s, (SELECT CAST(subject.properties AS JSON) -> %(param_1)s AS anon_1 \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s), (SELECT CASE WHEN (zaak.confidentiality = %(confidentiality_1)s) THEN %(param_2)s ELSE %(param_3)s END AS anon_2 \n"
            "FROM zaak \n"
            "WHERE zaak.id = %(id_1)s)) RETURNING logging.id"
        )

        assert insert_logging_compiled.params == {
            "component": "zaak",
            "confidentiality_1": "confidential",
            "event_data": {"case_id": "123", "content": ""},
            "event_type": "case/task/new_assignee",
            "id_1": 123,
            "onderwerp": 'Taak "A Task" toegewezen.',
            "param_1": "displayname",
            "param_2": True,
            "param_3": False,
            "uuid_1": self.user_uuid,
            "zaak_id": 123,
        }

        insert_message_compiled = insert_message_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_message_compiled) == (
            "INSERT INTO message (message, subject_id, logging_id, is_read, is_archived) VALUES (%(message)s, (SELECT %(param_1)s || CAST(subject.id AS VARCHAR) AS anon_1 \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s), %(logging_id)s, %(is_read)s, %(is_archived)s) RETURNING message.id"
        )
        assert insert_message_compiled.params == {
            "is_archived": False,
            "is_read": False,
            "logging_id": 321,
            "message": 'Taak "A Task" toegewezen.',
            "param_1": "betrokkene-medewerker-",
            "uuid_1": assignee_uuid,
        }

    def test_update_task_assignee_employee_uuid(self, task_db_row_maker):
        task_uuid = uuid4()
        assignee_uuid = uuid4()

        mock_employee_row = mock.Mock()
        mock_employee_row.id = 123

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(uuid=task_uuid),
            mock_employee_row,
        ]
        self.session.execute().inserted_primary_key = [321]
        self.session.reset_mock()

        self.cmd.update_task(
            task_uuid=str(task_uuid),
            title="New Title",
            description="New Description",
            due_date="2022-12-06",
            assignee=assignee_uuid,
        )

        get_task_query = self.session.execute.call_args_list[0][0][0]
        insert_logging_query = self.session.execute.call_args_list[1][0][0]
        insert_message_query = self.session.execute.call_args_list[2][0][0]
        get_assignee_query = self.session.execute.call_args_list[3][0][0]
        update_task_query = self.session.execute.call_args_list[4][0][0]

        get_task_compiled = get_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_task_compiled) == RETRIEVE_TASK_QUERY

        get_assignee_compiled = get_assignee_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_assignee_compiled) == (
            "SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s"
        )
        assert get_assignee_compiled.params == {"uuid_1": str(assignee_uuid)}

        update_task_compiled = update_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(update_task_compiled) == (
            "UPDATE checklist_item SET label=%(label)s, due_date=%(due_date)s, description=%(description)s, assignee_id=%(assignee_id)s, assignee_person_id=%(assignee_person_id)s, assignee_organization_id=%(assignee_organization_id)s WHERE checklist_item.uuid = %(uuid_1)s"
        )
        assert update_task_compiled.params == {
            "label": "New Title",
            "due_date": "2022-12-06",
            "description": "New Description",
            "assignee_id": 123,
            "assignee_organization_id": None,
            "assignee_person_id": None,
            "uuid_1": task_uuid,
        }

        insert_logging_compiled = insert_logging_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_logging_compiled) == (
            "INSERT INTO logging (zaak_id, component, onderwerp, event_type, event_data, created_by_name_cache, restricted) VALUES (%(zaak_id)s, %(component)s, %(onderwerp)s, %(event_type)s, %(event_data)s, (SELECT CAST(subject.properties AS JSON) -> %(param_1)s AS anon_1 \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s), (SELECT CASE WHEN (zaak.confidentiality = %(confidentiality_1)s) THEN %(param_2)s ELSE %(param_3)s END AS anon_2 \n"
            "FROM zaak \n"
            "WHERE zaak.id = %(id_1)s)) RETURNING logging.id"
        )

        assert insert_logging_compiled.params == {
            "component": "zaak",
            "confidentiality_1": "confidential",
            "event_data": {"case_id": "123", "content": ""},
            "event_type": "case/task/new_assignee",
            "id_1": 123,
            "onderwerp": 'Taak "A Task" toegewezen.',
            "param_1": "displayname",
            "param_2": True,
            "param_3": False,
            "uuid_1": self.user_uuid,
            "zaak_id": 123,
        }

        insert_message_compiled = insert_message_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_message_compiled) == (
            "INSERT INTO message (message, subject_id, logging_id, is_read, is_archived) VALUES (%(message)s, (SELECT %(param_1)s || CAST(subject.id AS VARCHAR) AS anon_1 \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s), %(logging_id)s, %(is_read)s, %(is_archived)s) RETURNING message.id"
        )
        assert insert_message_compiled.params == {
            "is_archived": False,
            "is_read": False,
            "logging_id": 321,
            "message": 'Taak "A Task" toegewezen.',
            "param_1": "betrokkene-medewerker-",
            "uuid_1": assignee_uuid,
        }

    def test_update_task_no_assignee(self, task_db_row_maker):
        task_uuid = uuid4()

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(uuid=task_uuid),
        ]
        self.session.reset_mock()

        self.cmd.update_task(
            task_uuid=str(task_uuid),
            title="New Title",
            description="New Description",
            due_date="2022-12-06",
            assignee=None,
        )

        get_task_query = self.session.execute.call_args_list[0][0][0]
        update_task_query = self.session.execute.call_args_list[1][0][0]

        get_task_compiled = get_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_task_compiled) == RETRIEVE_TASK_QUERY

        update_task_compiled = update_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(update_task_compiled) == (
            "UPDATE checklist_item SET label=%(label)s, due_date=%(due_date)s, description=%(description)s, assignee_id=%(assignee_id)s, assignee_person_id=%(assignee_person_id)s, assignee_organization_id=%(assignee_organization_id)s WHERE checklist_item.uuid = %(uuid_1)s"
        )
        assert update_task_compiled.params == {
            "label": "New Title",
            "due_date": "2022-12-06",
            "description": "New Description",
            "assignee_id": None,
            "assignee_organization_id": None,
            "assignee_person_id": None,
            "uuid_1": task_uuid,
        }

    def test_update_task_same_assignee(self, task_db_row_maker):
        task_uuid = uuid4()
        assignee_uuid = uuid4()

        mock_employee_row = mock.Mock()
        mock_employee_row.id = 123

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(
                uuid=task_uuid,
                assignee_uuid=assignee_uuid,
                assignee_display_name="Foobar",
            ),
            mock_employee_row,
        ]
        self.session.reset_mock()

        self.cmd.update_task(
            task_uuid=str(task_uuid),
            title="New Title",
            description="New Description",
            due_date="2022-12-06",
            assignee=assignee_uuid,
        )

        get_task_query = self.session.execute.call_args_list[0][0][0]
        get_assignee_query = self.session.execute.call_args_list[1][0][0]
        update_task_query = self.session.execute.call_args_list[2][0][0]

        get_task_compiled = get_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_task_compiled) == RETRIEVE_TASK_QUERY

        get_assignee_compiled = get_assignee_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_assignee_compiled) == (
            "SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s"
        )
        assert get_assignee_compiled.params == {"uuid_1": str(assignee_uuid)}

        update_task_compiled = update_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(update_task_compiled) == (
            "UPDATE checklist_item SET label=%(label)s, due_date=%(due_date)s, description=%(description)s, assignee_id=%(assignee_id)s, assignee_person_id=%(assignee_person_id)s, assignee_organization_id=%(assignee_organization_id)s WHERE checklist_item.uuid = %(uuid_1)s"
        )
        assert update_task_compiled.params == {
            "label": "New Title",
            "due_date": "2022-12-06",
            "description": "New Description",
            "assignee_id": 123,
            "assignee_organization_id": None,
            "assignee_person_id": None,
            "uuid_1": task_uuid,
        }

    def test_update_task_not_editable(self, task_db_row_maker):
        task_uuid = uuid4()

        mock_employee_row = mock.Mock()
        mock_employee_row.id = 123

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(uuid=task_uuid, case_status="resolved"),
            mock_employee_row,
        ]
        self.session.reset_mock()

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.update_task(
                task_uuid=str(task_uuid),
                title="New Title",
                description="New Description",
                due_date="2022-12-06",
                assignee=None,
            )

    def test_update_task_assignee_person(self, task_db_row_maker):
        task_uuid = uuid4()
        assignee_uuid = uuid4()

        mock_employee_row = mock.Mock()
        mock_employee_row.id = 123

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(
                uuid=task_uuid, allow_external_task_assignment=True
            ),
            mock_employee_row,
        ]
        self.session.execute().inserted_primary_key = [321]
        self.session.reset_mock()

        self.cmd.update_task(
            task_uuid=str(task_uuid),
            title="New Title",
            description="New Description",
            due_date="2022-12-06",
            assignee={"type": "person", "id": assignee_uuid},
        )

        get_task_query = self.session.execute.call_args_list[0][0][0]
        get_assignee_query = self.session.execute.call_args_list[1][0][0]
        update_task_query = self.session.execute.call_args_list[2][0][0]

        get_task_compiled = get_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_task_compiled) == RETRIEVE_TASK_QUERY

        get_assignee_compiled = get_assignee_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_assignee_compiled) == (
            "SELECT natuurlijk_persoon.id \n"
            "FROM natuurlijk_persoon \n"
            "WHERE natuurlijk_persoon.uuid = %(uuid_1)s"
        )
        assert get_assignee_compiled.params == {"uuid_1": str(assignee_uuid)}

        update_task_compiled = update_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(update_task_compiled) == (
            "UPDATE checklist_item SET label=%(label)s, due_date=%(due_date)s, description=%(description)s, assignee_id=%(assignee_id)s, assignee_person_id=%(assignee_person_id)s, assignee_organization_id=%(assignee_organization_id)s WHERE checklist_item.uuid = %(uuid_1)s"
        )
        assert update_task_compiled.params == {
            "label": "New Title",
            "due_date": "2022-12-06",
            "description": "New Description",
            "assignee_id": None,
            "assignee_organization_id": None,
            "assignee_person_id": 123,
            "uuid_1": task_uuid,
        }

    def test_update_task_assignee_organization(self, task_db_row_maker):
        task_uuid = uuid4()
        assignee_uuid = uuid4()

        mock_employee_row = mock.Mock()
        mock_employee_row.id = 123

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(
                uuid=task_uuid, allow_external_task_assignment=True
            ),
            mock_employee_row,
        ]
        self.session.execute().inserted_primary_key = [321]
        self.session.reset_mock()

        self.cmd.update_task(
            task_uuid=str(task_uuid),
            title="New Title",
            description="New Description",
            due_date="2022-12-06",
            assignee={"type": "organization", "id": assignee_uuid},
        )

        get_task_query = self.session.execute.call_args_list[0][0][0]
        get_assignee_query = self.session.execute.call_args_list[1][0][0]
        update_task_query = self.session.execute.call_args_list[2][0][0]

        get_task_compiled = get_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_task_compiled) == RETRIEVE_TASK_QUERY

        get_assignee_compiled = get_assignee_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_assignee_compiled) == (
            "SELECT bedrijf.id \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.uuid = %(uuid_1)s"
        )
        assert get_assignee_compiled.params == {"uuid_1": str(assignee_uuid)}

        update_task_compiled = update_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(update_task_compiled) == (
            "UPDATE checklist_item SET label=%(label)s, due_date=%(due_date)s, description=%(description)s, assignee_id=%(assignee_id)s, assignee_person_id=%(assignee_person_id)s, assignee_organization_id=%(assignee_organization_id)s WHERE checklist_item.uuid = %(uuid_1)s"
        )
        assert update_task_compiled.params == {
            "label": "New Title",
            "due_date": "2022-12-06",
            "description": "New Description",
            "assignee_id": None,
            "assignee_organization_id": 123,
            "assignee_person_id": None,
            "uuid_1": task_uuid,
        }

    def test_update_task_assignee_notallowed(self, task_db_row_maker):
        task_uuid = uuid4()
        assignee_uuid = uuid4()

        mock_employee_row = mock.Mock()
        mock_employee_row.id = 123

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(
                uuid=task_uuid, allow_external_task_assignment=False
            ),
            None,
        ]
        self.session.execute().inserted_primary_key = [321]
        self.session.reset_mock()

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.update_task(
                task_uuid=str(task_uuid),
                title="New Title",
                description="New Description",
                due_date="2022-12-06",
                assignee={"type": "organization", "id": assignee_uuid},
            )

    def test_update_task_assignee_notfound(self, task_db_row_maker):
        task_uuid = uuid4()
        assignee_uuid = uuid4()

        mock_employee_row = mock.Mock()
        mock_employee_row.id = 123

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(
                uuid=task_uuid, allow_external_task_assignment=True
            ),
            None,
        ]
        self.session.execute().inserted_primary_key = [321]
        self.session.reset_mock()

        with pytest.raises(minty.exceptions.NotFound):
            self.cmd.update_task(
                task_uuid=str(task_uuid),
                title="New Title",
                description="New Description",
                due_date="2022-12-06",
                assignee={"type": "organization", "id": assignee_uuid},
            )


# - Complete task
class Test_Task_Complete(TaskTestBase):
    def test_complete_task(self, task_db_row_maker):
        task_uuid = uuid4()

        self.session.execute().inserted_primary_key = [100]
        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(uuid=task_uuid),
            None,
        ]
        self.session.reset_mock()

        self.cmd.set_completion_on_task(
            task_uuid=str(task_uuid), completed=True
        )

        assert len(self.session.execute.call_args_list) == 5
        get_task_query = self.session.execute.call_args_list[0][0][0]
        update_query = self.session.execute.call_args_list[1][0][0]
        insert_logging_query = self.session.execute.call_args_list[2][0][0]
        get_case_contact_query = self.session.execute.call_args_list[3][0][0]
        insert_message_query = self.session.execute.call_args_list[4][0][0]

        get_task_compiled = get_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_task_compiled) == RETRIEVE_TASK_QUERY

        update_compiled = update_query.compile(dialect=postgresql.dialect())

        assert (
            str(update_compiled)
            == "UPDATE checklist_item SET state=%(state)s WHERE checklist_item.uuid = %(uuid_1)s"
        )
        assert update_compiled.params == {"state": True, "uuid_1": task_uuid}

        insert_logging_compiled = insert_logging_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_logging_compiled) == (
            "INSERT INTO logging (zaak_id, component, onderwerp, event_type, event_data, created_by_name_cache, restricted) VALUES (%(zaak_id)s, %(component)s, %(onderwerp)s, %(event_type)s, %(event_data)s, (SELECT CAST(subject.properties AS JSON) -> %(param_1)s AS anon_1 \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s), (SELECT CASE WHEN (zaak.confidentiality = %(confidentiality_1)s) THEN %(param_2)s ELSE %(param_3)s END AS anon_2 \n"
            "FROM zaak \n"
            "WHERE zaak.id = %(id_1)s)) RETURNING logging.id"
        )
        assert insert_logging_compiled.params == {
            "component": "zaak",
            "confidentiality_1": "confidential",
            "event_data": {"case_id": "123", "content": ""},
            "event_type": "case/task/set_completion",
            "id_1": 123,
            "onderwerp": 'Taak "A Task" afgerond.',
            "param_1": "displayname",
            "param_2": True,
            "param_3": False,
            "uuid_1": self.user_uuid,
            "zaak_id": 123,
        }

        get_case_contact_compiled = get_case_contact_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_case_contact_compiled) == (
            "SELECT zaak_betrokkenen.betrokkene_type, zaak_betrokkenen.betrokkene_id, subject.uuid \n"
            "FROM zaak JOIN zaak_betrokkenen ON zaak.behandelaar = zaak_betrokkenen.id JOIN subject ON subject.id = zaak_betrokkenen.betrokkene_id AND zaak_betrokkenen.betrokkene_type = %(betrokkene_type_1)s \n"
            "WHERE zaak.id = %(id_1)s"
        )
        assert get_case_contact_compiled.params == {
            "id_1": 123,
            "betrokkene_type_1": "medewerker",
        }

        insert_message_compiled = insert_message_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_message_compiled) == (
            "INSERT INTO message (message, subject_id, logging_id, is_read, is_archived) VALUES (%(message)s, (SELECT %(param_1)s || CAST(subject.id AS VARCHAR) AS anon_1 \n"
            "FROM subject \n"
            "WHERE subject.uuid IS NULL), %(logging_id)s, %(is_read)s, %(is_archived)s) RETURNING message.id"
        )
        assert insert_message_compiled.params == {
            "is_archived": False,
            "is_read": False,
            "logging_id": 100,
            "message": 'Taak "A Task" afgerond.',
            "param_1": "betrokkene-medewerker-",
        }

    def test_complete_task_with_case_contact(self, task_db_row_maker):
        task_uuid = uuid4()
        contact_uuid = uuid4()

        mock_case_contact = mock.Mock()
        mock_case_contact.configure_mock(
            betrokkene_type="medewerker",
            betrokkene_id=123,
            uuid=contact_uuid,
        )

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(uuid=task_uuid),
            mock_case_contact,
        ]
        self.session.reset_mock()

        self.cmd.set_completion_on_task(
            task_uuid=str(task_uuid), completed=True
        )

        assert len(self.session.execute.call_args_list) == 5
        get_task_query = self.session.execute.call_args_list[0][0][0]
        update_query = self.session.execute.call_args_list[1][0][0]

        get_task_compiled = get_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_task_compiled) == RETRIEVE_TASK_QUERY

        update_compiled = update_query.compile(dialect=postgresql.dialect())

        assert (
            str(update_compiled)
            == "UPDATE checklist_item SET state=%(state)s WHERE checklist_item.uuid = %(uuid_1)s"
        )
        assert update_compiled.params == {"state": True, "uuid_1": task_uuid}

    def test_complete_task_assigned_to_self(self, task_db_row_maker):
        task_uuid = uuid4()

        mock_case_contact = mock.Mock()
        mock_case_contact.configure_mock(
            betrokkene_type="medewerker",
            betrokkene_id=123,
            uuid=self.user_uuid,
        )

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(uuid=task_uuid),
            mock_case_contact,
        ]
        self.session.reset_mock()

        self.cmd.set_completion_on_task(
            task_uuid=str(task_uuid), completed=True
        )

        assert len(self.session.execute.call_args_list) == 4
        get_task_query = self.session.execute.call_args_list[0][0][0]
        update_query = self.session.execute.call_args_list[1][0][0]

        get_task_compiled = get_task_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(get_task_compiled) == RETRIEVE_TASK_QUERY

        update_compiled = update_query.compile(dialect=postgresql.dialect())

        assert (
            str(update_compiled)
            == "UPDATE checklist_item SET state=%(state)s WHERE checklist_item.uuid = %(uuid_1)s"
        )
        assert update_compiled.params == {"state": True, "uuid_1": task_uuid}

    def test_complete_task_not_completeable(self, task_db_row_maker):
        task_uuid = uuid4()

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(uuid=task_uuid, case_status="resolved"),
            None,
        ]
        self.session.reset_mock()

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.set_completion_on_task(
                task_uuid=str(task_uuid), completed=True
            )


class Test_Task_Delete(TaskTestBase):
    def test_delete_task(self, task_db_row_maker):
        task_uuid = uuid4()

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(uuid=task_uuid)
        ]
        self.session.reset_mock()

        self.cmd.delete_task(task_uuid=str(task_uuid))

        assert len(self.session.execute.call_args_list) == 2

        retrieve_query = self.session.execute.call_args_list[0][0][0]
        retrieve_compiled = retrieve_query.compile(
            dialect=postgresql.dialect()
        )
        assert str(retrieve_compiled) == RETRIEVE_TASK_QUERY

        delete_query = self.session.execute.call_args_list[1][0][0]
        delete_compiled = delete_query.compile(dialect=postgresql.dialect())
        assert (
            str(delete_compiled)
            == "DELETE FROM checklist_item WHERE checklist_item.uuid = %(uuid_1)s"
        )
        assert delete_compiled.params == {"uuid_1": task_uuid}

    def test_delete_task_not_editable(self, task_db_row_maker):
        task_uuid = uuid4()

        self.session.execute().fetchone.side_effect = [
            task_db_row_maker(uuid=task_uuid, case_status="resolved")
        ]
        self.session.reset_mock()

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.delete_task(task_uuid=str(task_uuid))

    def test_delete_task_notfound(self):
        task_uuid = uuid4()

        self.session.execute().fetchone.side_effect = [None]

        with pytest.raises(minty.exceptions.Forbidden):
            self.cmd.delete_task(task_uuid=str(task_uuid))
