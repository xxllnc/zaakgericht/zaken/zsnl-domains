# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from datetime import datetime
from minty.cqrs import UserInfo
from minty.cqrs.test import TestBase
from minty.entity import EntityCollection
from sqlalchemy.dialects import postgresql as sqlalchemy_pg
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_AsUser_SearchInCaseManagement(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )
        self.qry.user_info = self.user_info

    def test_search_for_custom_object_type(self):
        mock_custom_object_types = [mock.Mock(), mock.Mock()]
        custom_object_type_uuids = [uuid4(), uuid4()]

        mock_custom_object_types[0].configure_mock(
            type="custom_object_type",
            uuid=custom_object_type_uuids[0],
            summary="test summary 1",
            description="",
        )
        mock_custom_object_types[1].configure_mock(
            type="custom_object_type",
            uuid=custom_object_type_uuids[1],
            summary="test summary 2",
            description="",
        )

        self.session.execute().fetchall.return_value = mock_custom_object_types

        search_results = self.qry.search(
            type="custom_object_type", keyword="object"
        )
        assert isinstance(search_results, EntityCollection)

        custom_object_types = list(search_results)
        assert len(custom_object_types) == 2

        assert custom_object_types[0].uuid == custom_object_type_uuids[0]
        assert (
            custom_object_types[0].entity_id
            == mock_custom_object_types[0].uuid
        )

        assert custom_object_types[1].uuid == custom_object_type_uuids[1]
        assert (
            custom_object_types[1].entity_id
            == mock_custom_object_types[1].uuid
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT custom_object_type.uuid AS uuid, custom_object_type_version.name AS summary, %(param_1)s AS description, %(param_2)s AS type, %(param_3)s AS parent_uuid \n"
            "FROM custom_object_type JOIN custom_object_type_version ON custom_object_type_version.id = custom_object_type.custom_object_type_version_id AND (custom_object_type_version.date_deleted IS NULL OR custom_object_type_version.date_deleted > %(date_deleted_1)s) \n"
            "WHERE custom_object_type_version.name ILIKE %(name_1)s OR custom_object_type_version.title ILIKE %(title_1)s ORDER BY custom_object_type_version.last_modified DESC)) AS search_query \n"
            " LIMIT %(param_4)s"
        )

    def test_for_search_custom_object(self):
        mock_custom_objects = [mock.Mock(), mock.Mock()]
        custom_object_uuids = [uuid4(), uuid4()]
        custom_object_type_uuids = [uuid4(), uuid4()]

        mock_custom_objects[0].configure_mock(
            type="custom_object",
            uuid=custom_object_uuids[0],
            summary="test object",
            description="test description",
            parent_uuid=custom_object_type_uuids[0],
        )
        mock_custom_objects[1].configure_mock(
            type="custom_object",
            uuid=custom_object_uuids[1],
            summary="test object 2",
            description="test description 2",
            parent_uuid=custom_object_type_uuids[0],
        )

        self.session.execute().fetchall.return_value = mock_custom_objects

        search_results = self.qry.search(
            type="custom_object", keyword="object"
        )
        assert isinstance(search_results, EntityCollection)

        custom_objects = list(search_results)
        assert len(custom_objects) == 2

        assert custom_objects[0].result_type == "custom_object"
        assert custom_objects[0].uuid == custom_object_uuids[0]
        assert custom_objects[0].entity_id == mock_custom_objects[0].uuid
        assert (
            custom_objects[0].description == mock_custom_objects[0].description
        )
        assert (
            custom_objects[0].entity_meta_summary
            == mock_custom_objects[0].summary
        )

        assert custom_objects[1].result_type == "custom_object"
        assert custom_objects[1].uuid == custom_object_uuids[1]
        assert custom_objects[1].entity_id == mock_custom_objects[1].uuid
        assert (
            custom_objects[1].description == mock_custom_objects[1].description
        )
        assert (
            custom_objects[1].entity_meta_summary
            == mock_custom_objects[1].summary
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT custom_object_1.uuid AS uuid, custom_object_version.title AS summary, coalesce(custom_object_version.subtitle, %(coalesce_1)s) AS description, %(param_1)s AS type, CAST(custom_object_type.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM custom_object AS custom_object_1 JOIN custom_object_version ON custom_object_version.id = custom_object_1.custom_object_version_id AND (custom_object_version.date_deleted IS NULL OR custom_object_version.date_deleted > %(date_deleted_1)s) JOIN custom_object_type_version ON custom_object_type_version.id = custom_object_version.custom_object_type_version_id JOIN custom_object_type ON custom_object_type.id = custom_object_type_version.custom_object_type_id \n"
            "WHERE (EXISTS (SELECT 1 \n"
            'FROM custom_object_version JOIN custom_object ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id JOIN custom_object_type_acl ON custom_object_type_acl.custom_object_type_id = custom_object_type.id AND custom_object_type_acl."authorization" IN (%(authorization_1_1)s, %(authorization_1_2)s, %(authorization_1_3)s) JOIN subject ON subject.uuid = %(uuid_1)s JOIN subject_position_matrix ON subject_position_matrix.role_id = custom_object_type_acl.role_id AND subject_position_matrix.group_id = custom_object_type_acl.group_id AND subject_position_matrix.subject_id = subject.id \n'
            "WHERE custom_object.id = custom_object_1.id)) AND (custom_object_version.title ILIKE %(title_1)s OR custom_object_version.subtitle ILIKE %(subtitle_1)s OR custom_object_version.external_reference ILIKE %(external_reference_1)s) AND custom_object_version.status = %(status_1)s ORDER BY custom_object_version.last_modified DESC)) AS search_query \n"
            " LIMIT %(param_2)s"
        )
        assert query.params["title_1"] == "%object%"

    def test_for_search_custom_object_filtered(self):
        mock_custom_objects = [mock.Mock(), mock.Mock()]
        custom_object_uuids = [uuid4(), uuid4()]
        custom_object_type_uuids = [uuid4(), uuid4()]

        mock_custom_objects[0].configure_mock(
            type="custom_object",
            uuid=custom_object_uuids[0],
            summary="test object",
            description="test description",
            parent_uuid=custom_object_type_uuids[0],
        )
        mock_custom_objects[1].configure_mock(
            type="custom_object",
            uuid=custom_object_uuids[1],
            summary="test object 2",
            description="test description 2",
            parent_uuid=custom_object_type_uuids[0],
        )

        self.session.execute().fetchall.return_value = mock_custom_objects

        search_results = self.qry.search(
            type="custom_object",
            keyword="object",
            filter_params={"relationships.custom_object_type.id": uuid4()},
        )
        assert isinstance(search_results, EntityCollection)

        custom_objects = list(search_results)
        assert len(custom_objects) == 2

        assert custom_objects[0].result_type == "custom_object"
        assert custom_objects[0].uuid == custom_object_uuids[0]
        assert custom_objects[0].entity_id == mock_custom_objects[0].uuid
        assert (
            custom_objects[0].description == mock_custom_objects[0].description
        )
        assert (
            custom_objects[0].entity_meta_summary
            == mock_custom_objects[0].summary
        )

        assert custom_objects[1].result_type == "custom_object"
        assert custom_objects[1].uuid == custom_object_uuids[1]
        assert custom_objects[1].entity_id == mock_custom_objects[1].uuid
        assert (
            custom_objects[1].description == mock_custom_objects[1].description
        )
        assert (
            custom_objects[1].entity_meta_summary
            == mock_custom_objects[1].summary
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT custom_object_1.uuid AS uuid, custom_object_version.title AS summary, coalesce(custom_object_version.subtitle, %(coalesce_1)s) AS description, %(param_1)s AS type, CAST(custom_object_type.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM custom_object AS custom_object_1 JOIN custom_object_version ON custom_object_version.id = custom_object_1.custom_object_version_id AND (custom_object_version.date_deleted IS NULL OR custom_object_version.date_deleted > %(date_deleted_1)s) JOIN custom_object_type_version ON custom_object_type_version.id = custom_object_version.custom_object_type_version_id JOIN custom_object_type ON custom_object_type.id = custom_object_type_version.custom_object_type_id \n"
            "WHERE (EXISTS (SELECT 1 \n"
            'FROM custom_object_version JOIN custom_object ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id JOIN custom_object_type_acl ON custom_object_type_acl.custom_object_type_id = custom_object_type.id AND custom_object_type_acl."authorization" IN (%(authorization_1_1)s, %(authorization_1_2)s, %(authorization_1_3)s) JOIN subject ON subject.uuid = %(uuid_1)s JOIN subject_position_matrix ON subject_position_matrix.role_id = custom_object_type_acl.role_id AND subject_position_matrix.group_id = custom_object_type_acl.group_id AND subject_position_matrix.subject_id = subject.id \n'
            "WHERE custom_object.id = custom_object_1.id)) AND (custom_object_version.title ILIKE %(title_1)s OR custom_object_version.subtitle ILIKE %(subtitle_1)s OR custom_object_version.external_reference ILIKE %(external_reference_1)s) AND custom_object_type.uuid = %(uuid_2)s AND custom_object_version.status = %(status_1)s ORDER BY custom_object_version.last_modified DESC)) AS search_query \n"
            " LIMIT %(param_2)s"
        )
        assert query.params["title_1"] == "%object%"

    def test_for_search_custom_object_filtered_2(self):
        mock_custom_objects = [mock.Mock(), mock.Mock()]
        custom_object_uuids = [uuid4(), uuid4()]
        custom_object_type_uuids = [uuid4(), uuid4()]

        mock_custom_objects[0].configure_mock(
            type="custom_object",
            uuid=custom_object_uuids[0],
            summary="test object",
            description="test description",
            parent_uuid=custom_object_type_uuids[0],
        )
        mock_custom_objects[1].configure_mock(
            type="custom_object",
            uuid=custom_object_uuids[1],
            summary="test object 2",
            description="test description 2",
            parent_uuid=custom_object_type_uuids[0],
        )

        self.session.execute().fetchall.return_value = mock_custom_objects

        search_results = self.qry.search(
            type="custom_object",
            keyword="object",
            filter_params={
                "relationships.custom_object_type.id": uuid4(),
                "relationships.custom_object.active": "true",
            },
        )
        assert isinstance(search_results, EntityCollection)

        custom_objects = list(search_results)
        assert len(custom_objects) == 2

        assert custom_objects[0].result_type == "custom_object"
        assert custom_objects[0].uuid == custom_object_uuids[0]
        assert custom_objects[0].entity_id == mock_custom_objects[0].uuid
        assert (
            custom_objects[0].description == mock_custom_objects[0].description
        )
        assert (
            custom_objects[0].entity_meta_summary
            == mock_custom_objects[0].summary
        )

        assert custom_objects[1].result_type == "custom_object"
        assert custom_objects[1].uuid == custom_object_uuids[1]
        assert custom_objects[1].entity_id == mock_custom_objects[1].uuid
        assert (
            custom_objects[1].description == mock_custom_objects[1].description
        )
        assert (
            custom_objects[1].entity_meta_summary
            == mock_custom_objects[1].summary
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT custom_object_1.uuid AS uuid, custom_object_version.title AS summary, coalesce(custom_object_version.subtitle, %(coalesce_1)s) AS description, %(param_1)s AS type, CAST(custom_object_type.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM custom_object AS custom_object_1 JOIN custom_object_version ON custom_object_version.id = custom_object_1.custom_object_version_id AND (custom_object_version.date_deleted IS NULL OR custom_object_version.date_deleted > %(date_deleted_1)s) JOIN custom_object_type_version ON custom_object_type_version.id = custom_object_version.custom_object_type_version_id JOIN custom_object_type ON custom_object_type.id = custom_object_type_version.custom_object_type_id \n"
            "WHERE (EXISTS (SELECT 1 \n"
            'FROM custom_object_version JOIN custom_object ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id JOIN custom_object_type_acl ON custom_object_type_acl.custom_object_type_id = custom_object_type.id AND custom_object_type_acl."authorization" IN (%(authorization_1_1)s, %(authorization_1_2)s, %(authorization_1_3)s) JOIN subject ON subject.uuid = %(uuid_1)s JOIN subject_position_matrix ON subject_position_matrix.role_id = custom_object_type_acl.role_id AND subject_position_matrix.group_id = custom_object_type_acl.group_id AND subject_position_matrix.subject_id = subject.id \n'
            "WHERE custom_object.id = custom_object_1.id)) AND (custom_object_version.title ILIKE %(title_1)s OR custom_object_version.subtitle ILIKE %(subtitle_1)s OR custom_object_version.external_reference ILIKE %(external_reference_1)s) AND custom_object_type.uuid = %(uuid_2)s AND custom_object_version.status = %(status_1)s ORDER BY custom_object_version.last_modified DESC)) AS search_query \n"
            " LIMIT %(param_2)s"
        )
        assert query.params["title_1"] == "%object%"

    def test_for_search_case_as_admin(self):
        mock_cases = [mock.Mock(), mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        case_type_uuids = [uuid4(), uuid4()]

        mock_cases[0].configure_mock(
            type="case",
            uuid=case_uuids[0],
            summary="Case summary",
            description="Case description",
            parent_uuid=case_type_uuids[0],
        )
        mock_cases[1].configure_mock(
            type="case",
            uuid=case_uuids[1],
            summary="Object 2 summary",
            description="Object 2 description",
            parent_uuid=case_type_uuids[1],
        )

        self.session.execute().fetchall.return_value = mock_cases
        self.qry.user_info.permissions["admin"] = False

        search_results = self.qry.search(type="case", keyword="case")
        assert isinstance(search_results, EntityCollection)

        custom_objects = list(search_results)
        assert len(custom_objects) == 2

        assert custom_objects[0].uuid == case_uuids[0]
        assert custom_objects[0].entity_id == mock_cases[0].uuid

        assert custom_objects[1].uuid == case_uuids[1]
        assert custom_objects[1].entity_id == mock_cases[1].uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT zaak.uuid AS uuid, CAST(zaak.id AS VARCHAR) || %(param_1)s || CASE WHEN (zaak.status = %(status_1)s) THEN %(param_2)s WHEN (zaak.status = %(status_2)s) THEN %(param_3)s WHEN (zaak.status = %(status_3)s) THEN %(param_4)s WHEN (zaak.status = %(status_4)s) THEN %(param_5)s ELSE %(param_6)s END || %(param_7)s || zaaktype_node.titel AS summary, concat(coalesce(CAST(subject.properties AS JSON) ->> %(param_8)s, %(coalesce_1)s), %(coalesce_2)s || coalesce(zaak.onderwerp)) AS description, %(param_9)s AS type, CAST(zaaktype.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id LEFT OUTER JOIN subject ON zaak.behandelaar_gm_id = subject.id JOIN zaaktype ON zaak.zaaktype_id = zaaktype.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_5)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND (zaak.onderwerp ILIKE %(onderwerp_1)s OR zaaktype_node.titel ILIKE %(titel_1)s OR CAST(subject.properties AS JSON) ->> %(param_10)s ILIKE %(param_11)s) ORDER BY zaak.id DESC)) AS search_query \n"
            " LIMIT %(param_12)s"
        )
        assert query.params["status_5"] == "deleted"
        assert (
            query.params["titel_1"]
            == query.params["onderwerp_1"]
            == query.params["param_11"]
            == "%case%"
        )
        assert query.params["param_10"] == "displayname"

    def test_search_for_case_with_casenumber_as_admin(self):
        mock_cases = [mock.Mock(), mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        case_type_uuids = [uuid4(), uuid4()]

        mock_cases[0].configure_mock(
            type="case",
            uuid=case_uuids[0],
            summary="Case summary",
            description="Case description",
            parent_uuid=case_type_uuids[0],
        )
        mock_cases[1].configure_mock(
            type="case",
            uuid=case_uuids[1],
            summary="Object 2 summary",
            description="Object 2 description",
            parent_uuid=case_type_uuids[1],
        )

        self.session.execute().fetchall.return_value = mock_cases
        self.qry.user_info.permissions["admin"] = False

        search_results = self.qry.search(type="case", keyword="1337")
        assert isinstance(search_results, EntityCollection)

        custom_objects = list(search_results)
        assert len(custom_objects) == 2

        assert custom_objects[0].uuid == case_uuids[0]
        assert custom_objects[0].entity_id == mock_cases[0].uuid

        assert custom_objects[1].uuid == case_uuids[1]
        assert custom_objects[1].entity_id == mock_cases[1].uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT zaak.uuid AS uuid, CAST(zaak.id AS VARCHAR) || %(param_1)s || CASE WHEN (zaak.status = %(status_1)s) THEN %(param_2)s WHEN (zaak.status = %(status_2)s) THEN %(param_3)s WHEN (zaak.status = %(status_3)s) THEN %(param_4)s WHEN (zaak.status = %(status_4)s) THEN %(param_5)s ELSE %(param_6)s END || %(param_7)s || zaaktype_node.titel AS summary, concat(coalesce(CAST(subject.properties AS JSON) ->> %(param_8)s, %(coalesce_1)s), %(coalesce_2)s || coalesce(zaak.onderwerp)) AS description, %(param_9)s AS type, CAST(zaaktype.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id LEFT OUTER JOIN subject ON zaak.behandelaar_gm_id = subject.id JOIN zaaktype ON zaak.zaaktype_id = zaaktype.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_5)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND (zaak.id = %(id_1)s OR zaak.onderwerp ILIKE %(onderwerp_1)s OR zaaktype_node.titel ILIKE %(titel_1)s OR CAST(subject.properties AS JSON) ->> %(param_10)s ILIKE %(param_11)s) ORDER BY zaak.id DESC)) AS search_query \n"
            " LIMIT %(param_12)s"
        )
        assert query.params["id_1"] == 1337
        assert query.params["status_5"] == "deleted"
        assert (
            query.params["titel_1"]
            == query.params["onderwerp_1"]
            == query.params["param_11"]
            == "%1337%"
        )
        assert query.params["param_10"] == "displayname"

    def test_search_for_case_as_regular_user(self):
        mock_cases = [mock.Mock(), mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        case_type_uuids = [uuid4(), uuid4()]

        mock_cases[0].configure_mock(
            type="case",
            uuid=case_uuids[0],
            summary="Case summary",
            description="Case description",
            parent_uuid=case_type_uuids[0],
        )
        mock_cases[1].configure_mock(
            type="case",
            uuid=case_uuids[1],
            summary="Object 2 summary",
            description="Object 2 description",
            parent_uuid=case_type_uuids[1],
        )

        self.session.execute().fetchall.return_value = mock_cases
        self.qry.user_info.permissions["admin"] = False

        search_results = self.qry.search(type="case", keyword="1337")
        assert isinstance(search_results, EntityCollection)

        custom_objects = list(search_results)
        assert len(custom_objects) == 2

        assert custom_objects[0].uuid == case_uuids[0]
        assert custom_objects[0].entity_id == mock_cases[0].uuid

        assert custom_objects[1].uuid == case_uuids[1]
        assert custom_objects[1].entity_id == mock_cases[1].uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT zaak.uuid AS uuid, CAST(zaak.id AS VARCHAR) || %(param_1)s || CASE WHEN (zaak.status = %(status_1)s) THEN %(param_2)s WHEN (zaak.status = %(status_2)s) THEN %(param_3)s WHEN (zaak.status = %(status_3)s) THEN %(param_4)s WHEN (zaak.status = %(status_4)s) THEN %(param_5)s ELSE %(param_6)s END || %(param_7)s || zaaktype_node.titel AS summary, concat(coalesce(CAST(subject.properties AS JSON) ->> %(param_8)s, %(coalesce_1)s), %(coalesce_2)s || coalesce(zaak.onderwerp)) AS description, %(param_9)s AS type, CAST(zaaktype.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id LEFT OUTER JOIN subject ON zaak.behandelaar_gm_id = subject.id JOIN zaaktype ON zaak.zaaktype_id = zaaktype.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_5)s AND (zaak.behandelaar_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.coordinator_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) OR zaak.aanvrager_gm_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.uuid = %(uuid_1)s) AND zaak.aanvrager_type = %(aanvrager_type_1)s OR (EXISTS (SELECT 1 \n"
            "FROM case_acl \n"
            "WHERE case_acl.case_id = zaak.id AND case_acl.casetype_id = zaak.zaaktype_id AND case_acl.permission = %(permission_1)s AND case_acl.subject_uuid = %(subject_uuid_1)s))) AND (zaak.id = %(id_1)s OR zaak.onderwerp ILIKE %(onderwerp_1)s OR zaaktype_node.titel ILIKE %(titel_1)s OR CAST(subject.properties AS JSON) ->> %(param_10)s ILIKE %(param_11)s) ORDER BY zaak.id DESC)) AS search_query \n"
            " LIMIT %(param_12)s"
        )
        assert query.params["id_1"] == 1337
        assert query.params["status_5"] == "deleted"
        assert (
            query.params["titel_1"]
            == query.params["onderwerp_1"]
            == query.params["param_11"]
            == "%1337%"
        )
        assert query.params["permission_1"] == "search"
        assert query.params["param_10"] == "displayname"

    def test_search_for_cases_with_filter_casetype(self):
        mock_cases = [mock.Mock(), mock.Mock()]
        case_uuids = [uuid4(), uuid4()]
        case_type_uuid = uuid4()

        mock_cases[0].configure_mock(
            type="case",
            uuid=case_uuids[0],
            summary="1234(open)-Test casetype",
            description="Admin | case extra_info",
            parent_uuid=case_type_uuid,
        )
        mock_cases[1].configure_mock(
            type="case",
            uuid=case_uuids[1],
            summary="1234(stalled)-Test casetype",
            description="Admin | case extra_info",
            parent_uuid=case_type_uuid,
        )

        self.session.execute().fetchall.return_value = mock_cases
        self.qry.user_info.permissions["admin"] = True

        search_results = self.qry.search(
            type="case",
            keyword="1337",
            filter_params={"relationships.case_type.id": case_type_uuid},
        )
        assert isinstance(search_results, EntityCollection)

        cases = list(search_results)
        assert len(cases) == 2

        assert cases[0].uuid == case_uuids[0]
        assert cases[0].entity_id == mock_cases[0].uuid

        assert cases[1].uuid == case_uuids[1]
        assert cases[1].entity_id == mock_cases[1].uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT zaak.uuid AS uuid, CAST(zaak.id AS VARCHAR) || %(param_1)s || CASE WHEN (zaak.status = %(status_1)s) THEN %(param_2)s WHEN (zaak.status = %(status_2)s) THEN %(param_3)s WHEN (zaak.status = %(status_3)s) THEN %(param_4)s WHEN (zaak.status = %(status_4)s) THEN %(param_5)s ELSE %(param_6)s END || %(param_7)s || zaaktype_node.titel AS summary, concat(coalesce(CAST(subject.properties AS JSON) ->> %(param_8)s, %(coalesce_1)s), %(coalesce_2)s || coalesce(zaak.onderwerp)) AS description, %(param_9)s AS type, CAST(zaaktype.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id LEFT OUTER JOIN subject ON zaak.behandelaar_gm_id = subject.id JOIN zaaktype ON zaak.zaaktype_id = zaaktype.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_5)s AND (zaak.id = %(id_1)s OR zaak.onderwerp ILIKE %(onderwerp_1)s OR zaaktype_node.titel ILIKE %(titel_1)s OR CAST(subject.properties AS JSON) ->> %(param_10)s ILIKE %(param_11)s) AND zaaktype.uuid = %(uuid_1)s ORDER BY zaak.id DESC)) AS search_query \n"
            " LIMIT %(param_12)s"
        )
        assert query.params["id_1"] == 1337
        assert query.params["status_5"] == "deleted"
        assert (
            query.params["titel_1"]
            == query.params["onderwerp_1"]
            == query.params["param_11"]
            == "%1337%"
        )
        assert query.params["param_10"] == "displayname"

    def test_search_for_person(self):
        mock_persons = [mock.Mock(), mock.Mock()]
        person_uuids = [uuid4(), uuid4()]

        mock_persons[0].configure_mock(
            type="person",
            uuid=person_uuids[0],
            summary="Person Summary 1",
            description="Person Description 1",
        )
        mock_persons[1].configure_mock(
            type="person",
            uuid=person_uuids[1],
            summary="Person Summary 2",
            description="Person Description 2",
        )

        self.session.execute().fetchall.return_value = mock_persons

        search_results = self.qry.search(type="person", keyword="person")
        assert isinstance(search_results, EntityCollection)

        person_results = list(search_results)
        assert len(person_results) == 2

        assert person_results[0].uuid == person_uuids[0]
        assert person_results[0].entity_id == mock_persons[0].uuid
        assert person_results[0].description == mock_persons[0].description
        assert person_results[0].entity_meta_summary == mock_persons[0].summary

        assert person_results[1].uuid == person_uuids[1]
        assert person_results[1].entity_id == mock_persons[1].uuid
        assert person_results[1].description == mock_persons[1].description
        assert person_results[1].entity_meta_summary == mock_persons[1].summary

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT natuurlijk_persoon.uuid AS uuid, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname, %(to_char_1)s || to_char((CAST(natuurlijk_persoon.geboortedatum AS TIMESTAMP WITHOUT TIME ZONE) AT TIME ZONE %(param_1)s) AT TIME ZONE %(param_2)s, %(to_char_2)s) || %(param_3)s) AS summary, coalesce(concat(adres.straatnaam || %(straatnaam_1)s || CAST(adres.huisnummer AS VARCHAR), adres.huisletter, %(huisnummertoevoeging_1)s || adres.huisnummertoevoeging, %(concat_1)s, adres.postcode || %(postcode_1)s, adres.woonplaats), %(coalesce_1)s) AS description, %(param_4)s AS type, %(param_5)s AS parent_uuid \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN adres ON natuurlijk_persoon.adres_id = adres.id \n"
            "WHERE natuurlijk_persoon.search_term ILIKE %(search_term_1)s AND natuurlijk_persoon.active IS true AND natuurlijk_persoon.deleted_on IS NULL ORDER BY natuurlijk_persoon.id DESC)) AS search_query \n"
            " LIMIT %(param_6)s"
        )
        assert query.params["search_term_1"] == "%person%"

    def test_search_for_organization(self):
        mock_organization = [mock.Mock(), mock.Mock()]
        organization_uuids = [uuid4(), uuid4()]

        mock_organization[0].configure_mock(
            type="organization",
            uuid=organization_uuids[0],
            summary="Organization Summary 1",
            description="Organization Description 1",
        )
        mock_organization[1].configure_mock(
            type="organization",
            uuid=organization_uuids[1],
            summary="Organization Summary 2",
            description="Organization Description 2",
        )

        self.session.execute().fetchall.return_value = mock_organization

        search_results = self.qry.search(
            type="organization", keyword="organization"
        )
        assert isinstance(search_results, EntityCollection)

        person_results = list(search_results)
        assert len(person_results) == 2

        assert person_results[0].uuid == organization_uuids[0]
        assert person_results[0].entity_id == mock_organization[0].uuid
        assert (
            person_results[0].description == mock_organization[0].description
        )
        assert (
            person_results[0].entity_meta_summary
            == mock_organization[0].summary
        )

        assert person_results[1].uuid == organization_uuids[1]
        assert person_results[1].entity_id == mock_organization[1].uuid
        assert (
            person_results[1].description == mock_organization[1].description
        )
        assert (
            person_results[1].entity_meta_summary
            == mock_organization[1].summary
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT bedrijf.uuid AS uuid, bedrijf.handelsnaam AS summary, CASE WHEN (bedrijf.vestiging_straatnaam IS NOT NULL) THEN concat(bedrijf.vestiging_straatnaam, %(param_1)s || CAST(bedrijf.vestiging_huisnummer AS VARCHAR), bedrijf.vestiging_huisletter, %(nullif_1)s || nullif(bedrijf.vestiging_huisnummertoevoeging, %(nullif_2)s), %(concat_1)s, %(vestiging_postcode_1)s || bedrijf.vestiging_postcode, %(vestiging_woonplaats_1)s || bedrijf.vestiging_woonplaats) WHEN (bedrijf.vestiging_adres_buitenland1 IS NOT NULL) THEN concat(bedrijf.vestiging_adres_buitenland1, %(vestiging_adres_buitenland2_1)s || bedrijf.vestiging_adres_buitenland2, %(vestiging_adres_buitenland3_1)s || bedrijf.vestiging_adres_buitenland3) WHEN (bedrijf.correspondentie_straatnaam IS NOT NULL) THEN concat(bedrijf.correspondentie_straatnaam, %(param_2)s || CAST(bedrijf.correspondentie_huisnummer AS VARCHAR), bedrijf.correspondentie_huisletter, %(nullif_3)s || nullif(bedrijf.correspondentie_huisnummertoevoeging, %(nullif_4)s), %(concat_2)s, %(correspondentie_postcode_1)s || bedrijf.correspondentie_postcode, %(correspondentie_woonplaats_1)s || bedrijf.correspondentie_woonplaats) WHEN (bedrijf.correspondentie_adres_buitenland1 IS NOT NULL) THEN concat(bedrijf.correspondentie_adres_buitenland1, %(correspondentie_adres_buitenland2_1)s || bedrijf.correspondentie_adres_buitenland2, %(correspondentie_adres_buitenland3_1)s || bedrijf.correspondentie_adres_buitenland3) ELSE %(param_3)s END AS description, %(param_4)s AS type, %(param_5)s AS parent_uuid \n"
            "FROM bedrijf \n"
            "WHERE bedrijf.search_term ILIKE %(search_term_1)s AND bedrijf.deleted_on IS NULL ORDER BY bedrijf.id DESC)) AS search_query \n"
            " LIMIT %(param_6)s"
        )
        assert query.params["search_term_1"] == "%organization%"

    def test_search_for_employee(self):
        mock_empoyees = [mock.Mock(), mock.Mock()]
        employee_uuids = [uuid4(), uuid4()]

        mock_empoyees[0].configure_mock(
            type="employee",
            uuid=employee_uuids[0],
            summary="Employee Summary 1",
            description="Employee Description 1",
        )
        mock_empoyees[1].configure_mock(
            type="employee",
            uuid=employee_uuids[1],
            summary="Employee Summary 2",
            description="Employee Description 2",
        )

        self.session.execute().fetchall.return_value = mock_empoyees

        search_results = self.qry.search(type="employee", keyword="employee")
        assert isinstance(search_results, EntityCollection)

        person_results = list(search_results)
        assert len(person_results) == 2

        assert person_results[0].uuid == employee_uuids[0]
        assert person_results[0].entity_id == mock_empoyees[0].uuid
        assert person_results[0].description == mock_empoyees[0].description
        assert (
            person_results[0].entity_meta_summary == mock_empoyees[0].summary
        )

        assert person_results[1].uuid == employee_uuids[1]
        assert person_results[1].entity_id == mock_empoyees[1].uuid
        assert person_results[1].description == mock_empoyees[1].description
        assert (
            person_results[1].entity_meta_summary == mock_empoyees[1].summary
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT subject.uuid AS uuid, CAST(subject.properties AS JSON) ->> %(param_1)s AS summary, groups.name AS description, %(param_2)s AS type, %(param_3)s AS parent_uuid \n"
            "FROM subject JOIN groups ON groups.id = subject.group_ids[%(group_ids_1)s] \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND (CAST(subject.properties AS JSON) ->> %(param_4)s ILIKE %(param_5)s OR CAST(subject.properties AS JSON) ->> %(param_6)s ILIKE %(param_7)s) AND (array_length(subject.role_ids, %(array_length_1)s) IS NOT NULL OR array_length(subject.role_ids, %(array_length_2)s) > %(array_length_3)s) ORDER BY subject.last_modified DESC, subject.id DESC)) AS search_query \n"
            " LIMIT %(param_8)s"
        )
        assert query.params["subject_type_1"] == "employee"
        assert query.params["param_4"] == "displayname"
        assert query.params["param_5"] == "%employee%"
        assert query.params["param_6"] == "mail"
        assert query.params["param_7"] == "%employee%"

    def test_for_search_object_v1_as_admin(self):
        mock_objects = [mock.Mock(), mock.Mock()]
        object_uuids = [uuid4(), uuid4()]

        mock_objects[0].configure_mock(
            type="object_v1",
            uuid=object_uuids[0],
            summary="Object summary",
            description="Object description",
            parent_uuid=None,
        )
        mock_objects[1].configure_mock(
            type="object_v1",
            uuid=object_uuids[1],
            summary="Object 2 summary",
            description="Object 2 description",
            parent_uuid=None,
        )

        self.session.execute().fetchall.return_value = mock_objects

        search_results = self.qry.search(type="object_v1", keyword="object")
        assert isinstance(search_results, EntityCollection)

        custom_objects = list(search_results)
        assert len(custom_objects) == 2

        assert custom_objects[0].uuid == object_uuids[0]
        assert custom_objects[0].entity_id == mock_objects[0].uuid

        assert custom_objects[1].uuid == object_uuids[1]
        assert custom_objects[1].entity_id == mock_objects[1].uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT object_data_1.uuid AS uuid, coalesce(CAST(object_data_1.properties AS JSON) ->> %(param_1)s, %(coalesce_1)s) AS summary, (((CAST(object_data_2.properties AS JSON) -> %(param_2)s) -> %(param_3)s)) ->> %(param_4)s AS description, %(param_5)s AS type, %(param_6)s AS parent_uuid \n"
            "FROM object_data AS object_data_1 JOIN object_data AS object_data_2 ON object_data_1.class_uuid = object_data_2.uuid AND object_data_2.object_class = %(object_class_1)s \n"
            "WHERE object_data_1.text_vector @@ to_tsquery(%(text_vector_1)s) AND %(param_7)s ORDER BY object_data_1.date_modified DESC)) AS search_query \n"
            " LIMIT %(param_8)s"
        )
        assert query.params["text_vector_1"] == "object:*"
        assert query.params["param_7"] is True

    def test_search_for_object_v1_as_normal_user(self):
        mock_objects = [mock.Mock(), mock.Mock()]
        object_uuids = [uuid4(), uuid4()]

        mock_objects[0].configure_mock(
            type="object_v1",
            uuid=object_uuids[0],
            summary="Object summary",
            description="Object description",
            parent_uuid=None,
        )
        mock_objects[1].configure_mock(
            type="object_v1",
            uuid=object_uuids[1],
            summary="Object 2 summary",
            description="Object 2 description",
            parent_uuid=None,
        )

        self.session.execute().fetchall.return_value = mock_objects

        self.qry.user_info.permissions["admin"] = False

        search_results = self.qry.search(type="object_v1", keyword="object")
        assert isinstance(search_results, EntityCollection)

        custom_objects = list(search_results)
        assert len(custom_objects) == 2

        assert custom_objects[0].uuid == object_uuids[0]
        assert custom_objects[0].entity_id == mock_objects[0].uuid

        assert custom_objects[1].uuid == object_uuids[1]
        assert custom_objects[1].entity_id == mock_objects[1].uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT object_data_1.uuid AS uuid, coalesce(CAST(object_data_1.properties AS JSON) ->> %(param_1)s, %(coalesce_1)s) AS summary, (((CAST(object_data_2.properties AS JSON) -> %(param_2)s) -> %(param_3)s)) ->> %(param_4)s AS description, %(param_5)s AS type, %(param_6)s AS parent_uuid \n"
            "FROM object_data AS object_data_1 JOIN object_data AS object_data_2 ON object_data_1.class_uuid = object_data_2.uuid AND object_data_2.object_class = %(object_class_1)s \n"
            "WHERE object_data_1.text_vector @@ to_tsquery(%(text_vector_1)s) AND (EXISTS (SELECT 1 \n"
            "FROM object_acl_entry JOIN subject_position_matrix ON object_acl_entry.entity_id = subject_position_matrix.position AND object_acl_entry.entity_type = %(entity_type_1)s AND object_acl_entry.scope = %(scope_1)s AND object_acl_entry.capability IN (%(capability_1_1)s, %(capability_1_2)s) AND object_acl_entry.object_uuid = object_data_2.uuid JOIN subject ON subject.id = subject_position_matrix.subject_id \n"
            "WHERE subject.uuid = %(uuid_1)s UNION ALL SELECT 1 \n"
            "FROM object_acl_entry JOIN subject_position_matrix ON object_acl_entry.entity_id = subject_position_matrix.position AND object_acl_entry.entity_type = %(entity_type_2)s AND object_acl_entry.scope = %(scope_2)s AND object_acl_entry.capability IN (%(capability_2_1)s, %(capability_2_2)s) AND object_acl_entry.object_uuid = object_data_1.uuid JOIN subject ON subject.id = subject_position_matrix.subject_id \n"
            "WHERE subject.uuid = %(uuid_2)s UNION ALL SELECT 1 \n"
            "FROM object_acl_entry JOIN subject ON object_acl_entry.entity_id = subject.username AND object_acl_entry.entity_type = %(entity_type_3)s AND object_acl_entry.scope = %(scope_3)s AND object_acl_entry.capability IN (%(capability_3_1)s, %(capability_3_2)s) AND object_acl_entry.object_uuid = object_data_1.uuid \n"
            "WHERE subject.uuid = %(uuid_3)s)) ORDER BY object_data_1.date_modified DESC)) AS search_query \n"
            " LIMIT %(param_7)s"
        )
        assert query.params["uuid_1"] == self.user_info.user_uuid
        assert query.params["uuid_2"] == self.user_info.user_uuid
        assert query.params["text_vector_1"] == "object:*"

    def test_for_search_document_as_admin(self):
        mock_objects = [mock.Mock(), mock.Mock()]
        object_uuids = [uuid4(), uuid4()]

        mock_objects[0].configure_mock(
            type="document",
            uuid=object_uuids[0],
            summary="Documentsummary",
            description="Document description",
            parent_uuid=str(uuid4()),
        )
        mock_objects[1].configure_mock(
            type="document",
            uuid=object_uuids[1],
            summary="Document 2 summary",
            description="Document 2 description",
            parent_uuid=str(uuid4()),
        )

        self.session.execute().fetchall.return_value = mock_objects

        search_results = self.qry.search(
            type="document", keyword="document-123"
        )
        assert isinstance(search_results, EntityCollection)

        custom_objects = list(search_results)
        assert len(custom_objects) == 2

        assert custom_objects[0].uuid == object_uuids[0]
        assert custom_objects[0].entity_id == mock_objects[0].uuid

        assert custom_objects[1].uuid == object_uuids[1]
        assert custom_objects[1].entity_id == mock_objects[1].uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT file.uuid AS uuid, file.name || file.extension AS summary, concat(CAST(file.case_id AS VARCHAR), %(description_1)s || file_metadata.description) AS description, %(param_1)s AS type, CAST(zaak.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM file LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id JOIN zaak ON zaak.id = file.case_id \n"
            "WHERE file.date_deleted IS NULL AND file.destroyed IS false AND file.active_version IS true AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (file.name || file.extension ILIKE %(param_2)s OR file_metadata.description ILIKE %(description_2)s OR file.search_index @@ to_tsquery(%(search_index_1)s)) ORDER BY file.id DESC)) AS search_query \n"
            " LIMIT %(param_3)s"
        )
        assert query.params["search_index_1"] == "123:* & document:*"
        assert query.params["param_2"] == "%document-123%"
        assert query.params["description_2"] == "%document-123%"

    def test_for_search_document_as_admin_by_caseid(self):
        mock_objects = [mock.Mock(), mock.Mock()]
        object_uuids = [uuid4(), uuid4()]

        mock_objects[0].configure_mock(
            type="document",
            uuid=object_uuids[0],
            summary="Documentsummary",
            description="Document description",
            parent_uuid=str(uuid4()),
        )
        mock_objects[1].configure_mock(
            type="document",
            uuid=object_uuids[1],
            summary="Document 2 summary",
            description="Document 2 description",
            parent_uuid=str(uuid4()),
        )

        self.session.execute().fetchall.return_value = mock_objects

        search_results = self.qry.search(type="document", keyword="42")
        assert isinstance(search_results, EntityCollection)

        custom_objects = list(search_results)
        assert len(custom_objects) == 2

        assert custom_objects[0].uuid == object_uuids[0]
        assert custom_objects[0].entity_id == mock_objects[0].uuid

        assert custom_objects[1].uuid == object_uuids[1]
        assert custom_objects[1].entity_id == mock_objects[1].uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT file.uuid AS uuid, file.name || file.extension AS summary, concat(CAST(file.case_id AS VARCHAR), %(description_1)s || file_metadata.description) AS description, %(param_1)s AS type, CAST(zaak.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM file LEFT OUTER JOIN file_metadata ON file.metadata_id = file_metadata.id JOIN zaak ON zaak.id = file.case_id \n"
            "WHERE file.date_deleted IS NULL AND file.destroyed IS false AND file.active_version IS true AND zaak.deleted IS NULL AND zaak.status != %(status_1)s AND (file.case_id = %(case_id_1)s OR file.name || file.extension ILIKE %(param_2)s OR file_metadata.description ILIKE %(description_2)s OR file.search_index @@ to_tsquery(%(search_index_1)s)) ORDER BY file.id DESC)) AS search_query \n"
            " LIMIT %(param_3)s"
        )
        assert query.params["case_id_1"] == 42
        assert query.params["search_index_1"] == "42:*"
        assert query.params["param_2"] == "%42%"
        assert query.params["description_2"] == "%42%"

    def test_search_for_multiple_types_with_per_type_limit(self):
        mock_search_results = [mock.Mock(), mock.Mock()]

        search_uuids = [uuid4(), uuid4()]
        mock_search_results[0].configure_mock(
            type="person",
            uuid=search_uuids[0],
            summary="test summary 1",
            description="description 1",
        )
        mock_search_results[1].configure_mock(
            type="case",
            uuid=search_uuids[1],
            summary="test summary 2",
            description="description 2",
            parent_uuid=uuid4(),
        )

        self.session.execute().fetchall.return_value = mock_search_results

        search_results = self.qry.search(
            type="person,case", keyword="foobar", max_results_per_type=3
        )
        assert isinstance(search_results, EntityCollection)

        search_result_list = list(search_results)
        assert len(search_result_list) == 2

        assert search_result_list[0].uuid == search_uuids[0]
        assert search_result_list[0].entity_id == mock_search_results[0].uuid

        assert search_result_list[1].uuid == search_uuids[1]
        assert search_result_list[1].entity_id == mock_search_results[1].uuid

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(dialect=sqlalchemy_pg.dialect())

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT zaak.uuid AS uuid, CAST(zaak.id AS VARCHAR) || %(param_1)s || CASE WHEN (zaak.status = %(status_1)s) THEN %(param_2)s WHEN (zaak.status = %(status_2)s) THEN %(param_3)s WHEN (zaak.status = %(status_3)s) THEN %(param_4)s WHEN (zaak.status = %(status_4)s) THEN %(param_5)s ELSE %(param_6)s END || %(param_7)s || zaaktype_node.titel AS summary, concat(coalesce(CAST(subject.properties AS JSON) ->> %(param_8)s, %(coalesce_1)s), %(coalesce_2)s || coalesce(zaak.onderwerp)) AS description, %(param_9)s AS type, CAST(zaaktype.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id LEFT OUTER JOIN subject ON zaak.behandelaar_gm_id = subject.id JOIN zaaktype ON zaak.zaaktype_id = zaaktype.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_5)s AND (zaak.onderwerp ILIKE %(onderwerp_1)s OR zaaktype_node.titel ILIKE %(titel_1)s OR CAST(subject.properties AS JSON) ->> %(param_10)s ILIKE %(param_11)s) ORDER BY zaak.id DESC \n"
            " LIMIT %(param_12)s) UNION ALL (SELECT natuurlijk_persoon.uuid AS uuid, concat(natuurlijk_persoon.adellijke_titel || %(adellijke_titel_1)s, natuurlijk_persoon.voorletters || %(voorletters_1)s, natuurlijk_persoon.surname, %(to_char_1)s || to_char((CAST(natuurlijk_persoon.geboortedatum AS TIMESTAMP WITHOUT TIME ZONE) AT TIME ZONE %(param_13)s) AT TIME ZONE %(param_14)s, %(to_char_2)s) || %(param_15)s) AS summary, coalesce(concat(adres.straatnaam || %(straatnaam_1)s || CAST(adres.huisnummer AS VARCHAR), adres.huisletter, %(huisnummertoevoeging_1)s || adres.huisnummertoevoeging, %(concat_1)s, adres.postcode || %(postcode_1)s, adres.woonplaats), %(coalesce_3)s) AS description, %(param_16)s AS type, %(param_17)s AS parent_uuid \n"
            "FROM natuurlijk_persoon LEFT OUTER JOIN adres ON natuurlijk_persoon.adres_id = adres.id \n"
            "WHERE natuurlijk_persoon.search_term ILIKE %(search_term_1)s AND natuurlijk_persoon.active IS true AND natuurlijk_persoon.deleted_on IS NULL ORDER BY natuurlijk_persoon.id DESC \n"
            " LIMIT %(param_18)s)) AS search_query \n"
            " LIMIT %(param_19)s"
        )

        assert query.params == {
            "adellijke_titel_1": " ",
            "coalesce_1": "Geen behandelaar",
            "coalesce_2": " | ",
            "coalesce_3": "Adres onbekend",
            "concat_1": ", ",
            "huisnummertoevoeging_1": "-",
            "onderwerp_1": "%foobar%",
            "param_1": " (",
            "param_10": "displayname",
            "param_11": "%foobar%",
            "param_12": 3,
            "param_13": "UTC",
            "param_14": "Europe/Amsterdam",
            "param_15": ")",
            "param_16": "person",
            "param_17": None,
            "param_18": 3,
            "param_19": 100,
            "param_2": "Nieuw",
            "param_3": "In behandeling",
            "param_4": "Opgeschort",
            "param_5": "Afgehandeld",
            "param_6": "Onbekend",
            "param_7": ") - ",
            "param_8": "displayname",
            "param_9": "case",
            "postcode_1": " ",
            "search_term_1": "%foobar%",
            "status_1": "new",
            "status_2": "open",
            "status_3": "stalled",
            "status_4": "resolved",
            "status_5": "deleted",
            "straatnaam_1": " ",
            "titel_1": "%foobar%",
            "to_char_1": " (",
            "to_char_2": "DD-MM-YYYY",
            "voorletters_1": " ",
        }

    def test_search_for_casetype(self):
        mock_case_types = [mock.Mock(), mock.Mock()]
        case_type_uuids = [uuid4(), uuid4()]

        mock_case_types[0].configure_mock(
            type="case_type",
            uuid=case_type_uuids[0],
            summary="Case type 1",
            description="",
        )
        mock_case_types[1].configure_mock(
            type="case_type",
            uuid=case_type_uuids[1],
            summary="Case type 2",
            description="",
        )

        self.session.execute().fetchall.return_value = mock_case_types

        search_results = self.qry.search(
            type="case_type",
            keyword="Case type",
            filter_params={
                "relationships.case_type.requestor_type": "employee",
            },
        )
        assert isinstance(search_results, EntityCollection)

        case_type_results = list(search_results)
        assert len(case_type_results) == 2

        assert case_type_results[0].uuid == case_type_uuids[0]
        assert case_type_results[0].entity_id == mock_case_types[0].uuid
        assert (
            case_type_results[0].description == mock_case_types[0].description
        )
        assert (
            case_type_results[0].entity_meta_summary
            == mock_case_types[0].summary
        )

        assert case_type_results[1].uuid == case_type_uuids[1]
        assert case_type_results[1].entity_id == mock_case_types[1].uuid
        assert (
            case_type_results[1].description == mock_case_types[1].description
        )
        assert (
            case_type_results[1].entity_meta_summary
            == mock_case_types[1].summary
        )

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT zaaktype.uuid AS uuid, zaaktype_node.titel AS summary, %(param_1)s AS description, %(param_2)s AS type, %(param_3)s AS parent_uuid \n"
            "FROM zaaktype_node JOIN zaaktype ON zaaktype.zaaktype_node_id = zaaktype_node.id AND zaaktype.active IS true \n"
            "WHERE zaaktype.deleted IS NULL AND zaaktype.active IS true AND (zaaktype_node.titel ILIKE %(titel_1)s OR zaaktype_node.zaaktype_omschrijving ILIKE %(zaaktype_omschrijving_1)s) AND (EXISTS (SELECT %(param_4)s AS anon_1 \n"
            "FROM zaaktype_betrokkenen \n"
            "WHERE zaaktype_betrokkenen.zaaktype_node_id = zaaktype_node.id AND zaaktype_betrokkenen.betrokkene_type IN (%(betrokkene_type_1_1)s))) ORDER BY zaaktype_node.last_modified DESC)) AS search_query \n"
            " LIMIT %(param_5)s"
        )

        assert query.params["param_2"] == "case_type"
        assert query.params["titel_1"] == "%Case type%"

    def test_search_for_multiple_types(self):
        mock_results = [mock.Mock(), mock.Mock()]
        mock_uuids = [uuid4(), uuid4()]

        mock_results[0].configure_mock(
            type="case_type",
            uuid=mock_uuids[0],
            summary="Case type",
            description="",
            parent_uuid=None,
        )
        mock_results[1].configure_mock(
            type="case",
            uuid=mock_uuids[1],
            summary="Case",
            description="",
            parent_uuid=uuid4(),
        )

        self.session.execute().fetchall.return_value = mock_results

        results = self.qry.search(
            type="case_type,case",
            keyword="Case type",
            filter_params={
                "relationships.case_type.requestor_type": "person",
            },
        )
        assert isinstance(results, EntityCollection)

        search_results = list(results)
        assert len(search_results) == 2

        assert search_results[0].uuid == mock_uuids[0]
        assert search_results[0].entity_id == mock_results[0].uuid
        assert search_results[0].description == mock_results[0].description
        assert search_results[0].entity_meta_summary == mock_results[0].summary

        assert search_results[1].uuid == mock_uuids[1]
        assert search_results[1].entity_id == mock_results[1].uuid
        assert search_results[1].description == mock_results[1].description
        assert search_results[1].entity_meta_summary == mock_results[1].summary

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )

        assert str(query) == (
            "SELECT search_query.uuid, search_query.summary, search_query.description, search_query.type, search_query.parent_uuid \n"
            "FROM ((SELECT zaak.uuid AS uuid, CAST(zaak.id AS VARCHAR) || %(param_1)s || CASE WHEN (zaak.status = %(status_1)s) THEN %(param_2)s WHEN (zaak.status = %(status_2)s) THEN %(param_3)s WHEN (zaak.status = %(status_3)s) THEN %(param_4)s WHEN (zaak.status = %(status_4)s) THEN %(param_5)s ELSE %(param_6)s END || %(param_7)s || zaaktype_node.titel AS summary, concat(coalesce(CAST(subject.properties AS JSON) ->> %(param_8)s, %(coalesce_1)s), %(coalesce_2)s || coalesce(zaak.onderwerp)) AS description, %(param_9)s AS type, CAST(zaaktype.uuid AS VARCHAR) AS parent_uuid \n"
            "FROM zaak JOIN zaaktype_node ON zaak.zaaktype_node_id = zaaktype_node.id LEFT OUTER JOIN subject ON zaak.behandelaar_gm_id = subject.id JOIN zaaktype ON zaak.zaaktype_id = zaaktype.id \n"
            "WHERE zaak.deleted IS NULL AND zaak.status != %(status_5)s AND (zaak.onderwerp ILIKE %(onderwerp_1)s OR zaaktype_node.titel ILIKE %(titel_1)s OR CAST(subject.properties AS JSON) ->> %(param_10)s ILIKE %(param_11)s) ORDER BY zaak.id DESC) UNION ALL (SELECT zaaktype.uuid AS uuid, zaaktype_node.titel AS summary, %(param_12)s AS description, %(param_13)s AS type, %(param_14)s AS parent_uuid \n"
            "FROM zaaktype_node JOIN zaaktype ON zaaktype.zaaktype_node_id = zaaktype_node.id AND zaaktype.active IS true \n"
            "WHERE zaaktype.deleted IS NULL AND zaaktype.active IS true AND (zaaktype_node.titel ILIKE %(titel_2)s OR zaaktype_node.zaaktype_omschrijving ILIKE %(zaaktype_omschrijving_1)s) AND (EXISTS (SELECT %(param_15)s AS anon_1 \n"
            "FROM zaaktype_betrokkenen \n"
            "WHERE zaaktype_betrokkenen.zaaktype_node_id = zaaktype_node.id AND zaaktype_betrokkenen.betrokkene_type IN (%(betrokkene_type_1_1)s, %(betrokkene_type_1_2)s))) ORDER BY zaaktype_node.last_modified DESC)) AS search_query \n"
            " LIMIT %(param_16)s"
        )

        assert query.params["param_9"] == "case"
        assert query.params["param_11"] == "%Case type%"


class Test_AsUser_SearchCustomObjects(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

        self.qry.user_info = UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_search_custom_objects_based_on_type(self):
        mock_custom_object_list = [mock.Mock(), mock.Mock()]

        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2020, month=10, day=1, hour=13, minute=44, second=27
        )
        custom_object_type_uuid = uuid4()
        uuid1 = uuid4()
        uuid2 = uuid4()
        page = "2"
        page_size = "10"

        mock_custom_object_list[0].configure_mock(
            entity_id=uuid1,
            uuid=uuid1,
            object_type_name="custom-object-2",
            title="custom-object-2",
            subtitle="custom-object-2",
            status="active",
            archive_status="archived",
            external_reference=None,
            version=2,
            date_created=created1,
            last_modified=created2,
            date_deleted=None,
            version_independent_uuid=uuid4(),
            is_active_version=True,
            object_type_uuid=custom_object_type_uuid,
            custom_fields={
                "geo1": {
                    "type": "geojson",
                    "value": {
                        "type": "FeatureCollection",
                        "features": [
                            {
                                "type": "Feature",
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": [4.704882, 52.286563],
                                },
                                "properties": {},
                            }
                        ],
                    },
                },
                "subject": {"type": "text", "value": "test43"},
                "basic_date": {"type": "date", "value": "2021-10-09"},
            },
        )
        mock_custom_object_list[1].configure_mock(
            entity_id=uuid2,
            uuid=uuid2,
            object_type_name="custom-object-2",
            title="custom-object-2",
            subtitle="custom-object-2",
            status="active",
            archive_status="to preserve",
            external_reference=None,
            version=2,
            date_created=created1,
            last_modified=created2,
            date_deleted=None,
            version_independent_uuid=uuid4(),
            is_active_version=True,
            object_type_uuid=custom_object_type_uuid,
            custom_fields={
                "geo1": {
                    "type": "geojson",
                    "value": {
                        "type": "FeatureCollection",
                        "features": [
                            {
                                "type": "Feature",
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": [4.704882, 52.286563],
                                },
                                "properties": {},
                            }
                        ],
                    },
                },
                "subject": {"type": "text", "value": "test43"},
                "basic_date": {"type": "date", "value": "2021-10-09"},
            },
        )

        self.session.execute().fetchall.return_value = mock_custom_object_list

        mock_custom_object_event_list = self.qry.search_custom_objects(
            page=page,
            page_size=page_size,
            filters={
                "relationship.custom_object_type.id": str(
                    custom_object_type_uuid
                ),
            },
        )

        mock_custom_objects = list(mock_custom_object_event_list)
        assert len(mock_custom_objects) == 2

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        query = call_list[1][0][0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert (
            str(query)
            == "SELECT otv.uuid AS object_type_uuid, otv.name AS object_type_name, co.uuid AS version_independent_uuid, cov.uuid, cov.title, cov.subtitle, cov.external_reference, cov.status, cov.version, cov.date_created, cov.last_modified, cov.date_deleted, covc.archive_status, covc.archive_ground, covc.archive_retention, covc.custom_fields \n"
            "FROM custom_object AS co JOIN custom_object_version AS cov ON cov.id = co.custom_object_version_id AND (cov.date_deleted IS NULL OR cov.date_deleted > %(date_deleted_1)s) JOIN custom_object_version_content AS covc ON cov.custom_object_version_content_id = covc.id JOIN custom_object_type_version AS otv ON otv.id = cov.custom_object_type_version_id JOIN custom_object_type AS ot ON ot.id = otv.custom_object_type_id \n"
            "WHERE (EXISTS (SELECT 1 \n"
            'FROM custom_object_version JOIN custom_object ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id JOIN custom_object_type_acl ON custom_object_type_acl.custom_object_type_id = custom_object_type.id AND custom_object_type_acl."authorization" IN (%(authorization_1_1)s, %(authorization_1_2)s, %(authorization_1_3)s) JOIN subject ON subject.uuid = %(uuid_1)s JOIN subject_position_matrix ON subject_position_matrix.role_id = custom_object_type_acl.role_id AND subject_position_matrix.group_id = custom_object_type_acl.group_id AND subject_position_matrix.subject_id = subject.id \n'
            "WHERE custom_object.id = co.id)) AND ot.uuid = %(uuid_2)s ORDER BY cov.title ASC, cov.last_modified DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )

        assert query.params["uuid_2"] == custom_object_type_uuid

    def test_search_custom_objects_filters(self):
        mock_custom_object_list = [mock.Mock()]

        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )
        custom_object_type_uuid = uuid4()
        uuid1 = uuid4()
        page = "2"
        page_size = "10"

        mock_custom_object_list[0].configure_mock(
            entity_id=uuid1,
            uuid=uuid1,
            object_type_name="custom-object-2",
            title="custom-object-2",
            subtitle="custom-object-2",
            status="active",
            archive_status="archived",
            external_reference=None,
            version=2,
            date_created=created1,
            last_modified=created1,
            date_deleted=None,
            version_independent_uuid=uuid4(),
            is_active_version=True,
            object_type_uuid=custom_object_type_uuid,
            custom_fields={
                "geo1": {
                    "type": "geojson",
                    "value": {
                        "type": "FeatureCollection",
                        "features": [
                            {
                                "type": "Feature",
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": [4.704882, 52.286563],
                                },
                                "properties": {},
                            }
                        ],
                    },
                },
                "subject": {"type": "text", "value": "test43"},
                "basic_date": {"type": "date", "value": "2021-10-09"},
            },
        )

        self.session.execute().fetchall.return_value = mock_custom_object_list

        mock_custom_object_event_list = self.qry.search_custom_objects(
            page=page,
            page_size=page_size,
            filters={
                "relationship.custom_object_type.id": str(
                    custom_object_type_uuid
                ),
                "keyword": "foo",
                "attributes.last_modified": "gt 2021-11-05T00:00:00Z,le 2022-11-05T00:00:00Z",
                "attributes.archive_status": "to destroy,to preserve",
                "attributes.status": "active,inactive",
            },
        )

        mock_custom_objects = list(mock_custom_object_event_list)
        assert len(mock_custom_objects) == 1

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        query = call_list[1][0][0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert (
            str(query)
            == "SELECT otv.uuid AS object_type_uuid, otv.name AS object_type_name, co.uuid AS version_independent_uuid, cov.uuid, cov.title, cov.subtitle, cov.external_reference, cov.status, cov.version, cov.date_created, cov.last_modified, cov.date_deleted, covc.archive_status, covc.archive_ground, covc.archive_retention, covc.custom_fields \n"
            "FROM custom_object AS co JOIN custom_object_version AS cov ON cov.id = co.custom_object_version_id AND (cov.date_deleted IS NULL OR cov.date_deleted > %(date_deleted_1)s) JOIN custom_object_version_content AS covc ON cov.custom_object_version_content_id = covc.id JOIN custom_object_type_version AS otv ON otv.id = cov.custom_object_type_version_id JOIN custom_object_type AS ot ON ot.id = otv.custom_object_type_id \n"
            "WHERE (EXISTS (SELECT 1 \n"
            'FROM custom_object_version JOIN custom_object ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id JOIN custom_object_type_acl ON custom_object_type_acl.custom_object_type_id = custom_object_type.id AND custom_object_type_acl."authorization" IN (%(authorization_1_1)s, %(authorization_1_2)s, %(authorization_1_3)s) JOIN subject ON subject.uuid = %(uuid_1)s JOIN subject_position_matrix ON subject_position_matrix.role_id = custom_object_type_acl.role_id AND subject_position_matrix.group_id = custom_object_type_acl.group_id AND subject_position_matrix.subject_id = subject.id \n'
            "WHERE custom_object.id = co.id)) AND ot.uuid = %(uuid_2)s AND cov.status IN (%(status_1_1)s, %(status_1_2)s) AND covc.archive_status IN (%(archive_status_1_1)s, %(archive_status_1_2)s) AND cov.last_modified > %(last_modified_1)s AND cov.last_modified <= %(last_modified_2)s AND (cov.title ILIKE %(title_1)s ESCAPE '~' OR cov.subtitle ILIKE %(subtitle_1)s ESCAPE '~' OR cov.external_reference ILIKE %(external_reference_1)s ESCAPE '~') ORDER BY cov.title ASC, cov.last_modified DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )
        assert query.params["uuid_2"] == custom_object_type_uuid

    def test_search_custom_objects_sort(self):
        mock_custom_object_list = [mock.Mock()]

        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )
        custom_object_type_uuid = uuid4()
        uuid1 = uuid4()
        page = "2"
        page_size = "10"

        mock_custom_object_list[0].configure_mock(
            entity_id=uuid1,
            uuid=uuid1,
            object_type_name="custom-object-2",
            title="custom-object-2",
            subtitle="custom-object-2",
            status="active",
            archive_status="archived",
            external_reference=None,
            version=2,
            date_created=created1,
            last_modified=created1,
            date_deleted=None,
            version_independent_uuid=uuid4(),
            is_active_version=True,
            object_type_uuid=custom_object_type_uuid,
            custom_fields={
                "geo1": {
                    "type": "geojson",
                    "value": {
                        "type": "FeatureCollection",
                        "features": [
                            {
                                "type": "Feature",
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": [4.704882, 52.286563],
                                },
                                "properties": {},
                            }
                        ],
                    },
                },
                "subject": {"type": "text", "value": "test43"},
                "basic_date": {"type": "date", "value": "2021-10-09"},
            },
        )

        self.session.execute().fetchall.return_value = mock_custom_object_list

        mock_custom_object_event_list = self.qry.search_custom_objects(
            page=page,
            page_size=page_size,
            filters={
                "relationship.custom_object_type.id": str(
                    custom_object_type_uuid
                ),
            },
            sort="-external_reference",
        )

        mock_custom_objects = list(mock_custom_object_event_list)
        assert len(mock_custom_objects) == 1

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        query = call_list[1][0][0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert (
            str(query)
            == "SELECT otv.uuid AS object_type_uuid, otv.name AS object_type_name, co.uuid AS version_independent_uuid, cov.uuid, cov.title, cov.subtitle, cov.external_reference, cov.status, cov.version, cov.date_created, cov.last_modified, cov.date_deleted, covc.archive_status, covc.archive_ground, covc.archive_retention, covc.custom_fields \n"
            "FROM custom_object AS co JOIN custom_object_version AS cov ON cov.id = co.custom_object_version_id AND (cov.date_deleted IS NULL OR cov.date_deleted > %(date_deleted_1)s) JOIN custom_object_version_content AS covc ON cov.custom_object_version_content_id = covc.id JOIN custom_object_type_version AS otv ON otv.id = cov.custom_object_type_version_id JOIN custom_object_type AS ot ON ot.id = otv.custom_object_type_id \n"
            "WHERE (EXISTS (SELECT 1 \n"
            'FROM custom_object_version JOIN custom_object ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id JOIN custom_object_type_acl ON custom_object_type_acl.custom_object_type_id = custom_object_type.id AND custom_object_type_acl."authorization" IN (%(authorization_1_1)s, %(authorization_1_2)s, %(authorization_1_3)s) JOIN subject ON subject.uuid = %(uuid_1)s JOIN subject_position_matrix ON subject_position_matrix.role_id = custom_object_type_acl.role_id AND subject_position_matrix.group_id = custom_object_type_acl.group_id AND subject_position_matrix.subject_id = subject.id \n'
            "WHERE custom_object.id = co.id)) AND ot.uuid = %(uuid_2)s ORDER BY cov.external_reference DESC, cov.last_modified DESC \n"
            " LIMIT %(param_1)s OFFSET %(param_2)s"
        )
        assert query.params["uuid_2"] == custom_object_type_uuid

    def test_search_custom_objects_total_count_based_on_type(self):
        mock_custom_object_list = [mock.Mock(), mock.Mock()]

        created1 = datetime(
            year=2020, month=10, day=2, hour=10, minute=12, second=16
        )
        created2 = datetime(
            year=2020, month=10, day=1, hour=13, minute=44, second=27
        )
        custom_object_type_uuid = uuid4()
        uuid1 = uuid4()
        uuid2 = uuid4()

        mock_custom_object_list[0].configure_mock(
            entity_id=uuid1,
            uuid=uuid1,
            object_type_name="custom-object-2",
            title="custom-object-2",
            subtitle="custom-object-2",
            status="active",
            archive_status="archived",
            external_reference=None,
            version=2,
            date_created=created1,
            last_modified=created2,
            date_deleted=None,
            version_independent_uuid=uuid4(),
            is_active_version=True,
            object_type_uuid=custom_object_type_uuid,
            custom_fields={
                "geo1": {
                    "type": "geojson",
                    "value": {
                        "type": "FeatureCollection",
                        "features": [
                            {
                                "type": "Feature",
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": [4.704882, 52.286563],
                                },
                                "properties": {},
                            }
                        ],
                    },
                },
                "subject": {"type": "text", "value": "test43"},
                "basic_date": {"type": "date", "value": "2021-10-09"},
            },
        )
        mock_custom_object_list[1].configure_mock(
            entity_id=uuid2,
            uuid=uuid2,
            object_type_name="custom-object-2",
            title="custom-object-2",
            subtitle="custom-object-2",
            status="active",
            archive_status="to preserve",
            external_reference=None,
            version=2,
            date_created=created1,
            last_modified=created2,
            date_deleted=None,
            version_independent_uuid=uuid4(),
            is_active_version=True,
            object_type_uuid=custom_object_type_uuid,
            custom_fields={
                "geo1": {
                    "type": "geojson",
                    "value": {
                        "type": "FeatureCollection",
                        "features": [
                            {
                                "type": "Feature",
                                "geometry": {
                                    "type": "Point",
                                    "coordinates": [4.704882, 52.286563],
                                },
                                "properties": {},
                            }
                        ],
                    },
                },
                "subject": {"type": "text", "value": "test43"},
                "basic_date": {"type": "date", "value": "2021-10-09"},
            },
        )

        self.session.execute().fetchall.return_value = mock_custom_object_list

        self.qry.search_custom_object_total_results(
            filters={
                "relationship.custom_object_type.id": str(
                    custom_object_type_uuid
                ),
            },
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        query = call_list[1][0][0].compile(
            dialect=sqlalchemy_pg.dialect(),
            compile_kwargs={"render_postcompile": True},
        )
        assert (
            str(query) == "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM custom_object AS co JOIN custom_object_version AS cov ON cov.id = co.custom_object_version_id AND (cov.date_deleted IS NULL OR cov.date_deleted > %(date_deleted_1)s) JOIN custom_object_version_content AS covc ON cov.custom_object_version_content_id = covc.id JOIN custom_object_type_version AS otv ON otv.id = cov.custom_object_type_version_id JOIN custom_object_type AS ot ON ot.id = otv.custom_object_type_id \n"
            "WHERE (EXISTS (SELECT 1 \n"
            'FROM custom_object_version JOIN custom_object ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id JOIN custom_object_type_acl ON custom_object_type_acl.custom_object_type_id = custom_object_type.id AND custom_object_type_acl."authorization" IN (%(authorization_1_1)s, %(authorization_1_2)s, %(authorization_1_3)s) JOIN subject ON subject.uuid = %(uuid_1)s JOIN subject_position_matrix ON subject_position_matrix.role_id = custom_object_type_acl.role_id AND subject_position_matrix.group_id = custom_object_type_acl.group_id AND subject_position_matrix.subject_id = subject.id \n'
            "WHERE custom_object.id = co.id)) AND ot.uuid = %(uuid_2)s"
        )

        assert query.params["uuid_2"] == custom_object_type_uuid
