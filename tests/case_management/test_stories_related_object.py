# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

from minty.cqrs.test import TestBase
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management


class Test_AsUser_GetRelatedObjectsForCustomObject(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)

    def test_get_related_objects_for_custom_object(self):
        mock_objects = [mock.Mock(), mock.Mock()]
        object_uuids = [uuid4(), uuid4()]
        object_type_uuids = [uuid4(), uuid4()]

        mock_objects[0].configure_mock(
            uuid=object_uuids[0],
            title="Object1",
            object_type_uuid=object_type_uuids[0],
            object_type_name="ObjectType1",
        )
        mock_objects[1].configure_mock(
            uuid=object_uuids[1],
            title="Object2",
            object_type_uuid=object_type_uuids[1],
            object_type_name="ObjectType2",
        )

        self.session.execute().fetchall.return_value = mock_objects

        objects = self.qry.get_related_objects_for_custom_object(
            object_uuid=str(uuid4())
        )
        assert isinstance(objects, list)
        assert len(objects) == 2

        assert objects[0].uuid == object_uuids[0]
        assert objects[0].entity_id == mock_objects[0].uuid
        assert objects[0].object_type.entity_id == object_type_uuids[0]

        assert objects[1].uuid == object_uuids[1]
        assert objects[1].entity_id == mock_objects[1].uuid
        assert objects[1].object_type.entity_id == object_type_uuids[1]

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert str(query) == (
            "SELECT custom_object.uuid, custom_object_version.title, custom_object_type.uuid AS object_type_uuid, custom_object_type_version.name AS object_type_name \n"
            "FROM custom_object_relationship, custom_object AS custom_object_1, custom_object "
            "JOIN custom_object_version ON custom_object.custom_object_version_id = custom_object_version.id "
            "JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id "
            "JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id \n"
            "WHERE custom_object_relationship.relationship_type = :relationship_type_1 "
            "AND custom_object_relationship.related_uuid = custom_object.uuid "
            "AND custom_object_1.uuid = :uuid_1 AND custom_object_relationship.custom_object_id = custom_object_1.id "
            "UNION SELECT custom_object.uuid, custom_object_version.title, custom_object_type.uuid AS object_type_uuid, custom_object_type_version.name AS object_type_name \n"
            "FROM custom_object_relationship, custom_object JOIN custom_object_version ON custom_object.custom_object_version_id = custom_object_version.id "
            "JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id "
            "JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id \n"
            "WHERE custom_object_relationship.relationship_type = :relationship_type_2 AND custom_object_relationship.related_uuid = :related_uuid_1 "
            "AND custom_object.id = custom_object_relationship.custom_object_id"
        )

    def test_get_related_objects_for_subject(self):
        mock_objects = [mock.Mock(), mock.Mock()]
        object_uuids = [uuid4(), uuid4()]
        object_type_uuids = [uuid4(), uuid4()]

        mock_objects[0].configure_mock(
            uuid=object_uuids[0],
            title="Object1",
            object_type_uuid=object_type_uuids[0],
            object_type_name="ObjectType1",
        )
        mock_objects[1].configure_mock(
            uuid=object_uuids[1],
            title="Object2",
            object_type_uuid=object_type_uuids[1],
            object_type_name="ObjectType2",
        )

        self.session.execute().fetchall.return_value = mock_objects

        objects = self.qry.get_related_custom_objects_for_subject(
            subject_uuid=str(uuid4())
        )
        assert isinstance(objects, list)
        assert len(objects) == 2

        assert objects[0].uuid == object_uuids[0]
        assert objects[0].entity_id == mock_objects[0].uuid
        assert objects[0].object_type.entity_id == object_type_uuids[0]

        assert objects[1].uuid == object_uuids[1]
        assert objects[1].entity_id == mock_objects[1].uuid
        assert objects[1].object_type.entity_id == object_type_uuids[1]

        (args, kwargs) = self.session.execute.call_args
        query = args[0].compile()

        assert str(query) == (
            "SELECT custom_object.uuid, custom_object_version.title, custom_object_type.uuid AS object_type_uuid, custom_object_type_version.name AS object_type_name \n"
            "FROM custom_object JOIN custom_object_version ON custom_object.custom_object_version_id = custom_object_version.id JOIN custom_object_type_version ON custom_object_version.custom_object_type_version_id = custom_object_type_version.id JOIN custom_object_type ON custom_object_type_version.custom_object_type_id = custom_object_type.id \n"
            "WHERE custom_object_version.id IN (SELECT custom_object_relationship.custom_object_version_id \n"
            "FROM custom_object_relationship \n"
            "WHERE custom_object_relationship.relationship_type = :relationship_type_1 AND custom_object_relationship.related_uuid = :related_uuid_1)"
        )
