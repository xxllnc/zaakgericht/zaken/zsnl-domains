# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import minty.cqrs
import minty.entity
import minty.exceptions
import pytest
import random
from minty.cqrs.test import TestBase
from sqlalchemy import exc as sqlalchemy_exc
from sqlalchemy.dialects import postgresql
from unittest import mock
from uuid import uuid4
from zsnl_domains import case_management
from zsnl_domains.case_management.entities import saved_search as entity


class TestGetSavedSearch(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_get_saved_search(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        custom_object_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "keyword",
                        "parameters": {"label": None, "value": "test"},
                    },
                    {
                        "type": "custom_field",
                        "parameters": {
                            "operator": "equals",
                            "magic_string": "ztc_aanvraag",
                            "type": "text",
                            "value": "foobar",
                        },
                    },
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "label": "ObjectTypeNameHere",
                            "value": custom_object_uuid,
                        },
                    },
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "write",
                },
            ],
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute.reset_mock()

        saved_search = self.qry.get_saved_search(saved_search_uuid)
        assert saved_search.schema()["definitions"][
            "SimpleFilterObject_List_Annotated_Union_zsnl_domains.case_management.entities.saved_search.AbsoluteDate__zsnl_domains.case_management.entities.saved_search.RangeDate__zsnl_domains.case_management.entities.saved_search.RelativeDate___FieldInfo_default_PydanticUndefined__discriminator__type___extra_______"
        ] == {
            "title": "SimpleFilterObject[List[Annotated[Union[zsnl_domains.case_management.entities.saved_search.AbsoluteDate, zsnl_domains.case_management.entities.saved_search.RangeDate, zsnl_domains.case_management.entities.saved_search.RelativeDate], FieldInfo(default=PydanticUndefined, discriminator='type', extra={})]]]",
            "type": "object",
            "properties": {
                "label": {
                    "title": "Human-readable preview value of the filter value",
                    "type": "string",
                },
                "value": {
                    "title": "Value to filter for",
                    "type": "array",
                    "items": {
                        "discriminator": {
                            "propertyName": "type",
                            "mapping": {
                                "absolute": "#/definitions/AbsoluteDate",
                                "range": "#/definitions/RangeDate",
                                "relative": "#/definitions/RelativeDate",
                            },
                        },
                        "oneOf": [
                            {"$ref": "#/definitions/AbsoluteDate"},
                            {"$ref": "#/definitions/RangeDate"},
                            {"$ref": "#/definitions/RelativeDate"},
                        ],
                    },
                },
            },
            "required": ["value"],
        }

        assert saved_search == entity.SavedSearch.parse_obj(
            {
                "entity_id": saved_search_uuid,
                "uuid": saved_search_uuid,
                "name": "Test Name",
                "kind": "custom_object",
                "owner": {
                    "entity_id": owner_uuid,
                    "uuid": owner_uuid,
                    "entity_meta_summary": "Owner Name",
                },
                "permissions": [
                    {
                        "group_id": group_uuid,
                        "role_id": role_uuid,
                        "permission": [
                            entity.SavedSearchPermission.read,
                            entity.SavedSearchPermission.write,
                        ],
                    },
                ],
                "filters": {
                    "kind_type": "custom_object",
                    "filters": [
                        {
                            "type": "keyword",
                            "parameters": {"label": None, "value": "test"},
                        },
                        {
                            "type": "custom_field",
                            "parameters": {
                                "operator": "equals",
                                "magic_string": "ztc_aanvraag",
                                "type": "text",
                                "value": "foobar",
                            },
                        },
                        {
                            "type": "relationship.custom_object_type",
                            "parameters": {
                                "label": "ObjectTypeNameHere",
                                "value": custom_object_uuid,
                            },
                        },
                    ],
                },
                "columns": [
                    {
                        "source": ["attributes", "a"],
                        "label": "a",
                        "type": "string",
                    },
                    {
                        "source": ["attributes", "b"],
                        "label": "b",
                        "type": "datetime",
                    },
                ],
                "sort_column": "a",
                "sort_order": "asc",
                "entity_meta_authorizations": {
                    entity.AuthorizationLevel.read,
                    entity.AuthorizationLevel.readwrite,
                    entity.AuthorizationLevel.admin,
                },
                "_event_service": None,
            }
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 1

        saved_search_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(saved_search_query) == (
            "SELECT saved_search.uuid, saved_search.name, saved_search.kind, json_build_object(%(json_build_object_1)s, subject.uuid, %(json_build_object_2)s, CAST(subject.properties AS JSON) ->> %(param_1)s) AS owner, saved_search.filters, saved_search.columns, saved_search.sort_column, saved_search.sort_order, coalesce(anon_1.permission_rows, CAST(ARRAY[] AS JSON[])) AS permission_rows, array((SELECT CASE WHEN (saved_search_permission.permission = %(permission_1)s) THEN %(param_2)s WHEN (saved_search_permission.permission = %(permission_2)s) THEN %(param_3)s END AS anon_2 \n"
            "FROM saved_search_permission JOIN subject_position_matrix ON saved_search_permission.group_id = subject_position_matrix.group_id AND saved_search_permission.role_id = subject_position_matrix.role_id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.permission)) AS authorizations, CASE WHEN (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s)) THEN %(param_4)s ELSE %(param_5)s END AS user_is_owner \n"
            "FROM saved_search JOIN subject ON saved_search.owner_id = subject.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_3)s, groups.uuid, %(json_build_object_4)s, roles.uuid, %(json_build_object_5)s, saved_search_permission.permission)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id \n"
            "WHERE (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_3 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s)))) AND saved_search.uuid = %(uuid_2)s"
        )
        assert saved_search_query.params == {
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
            "uuid_2": saved_search_uuid,
            "json_build_object_1": "uuid",
            "json_build_object_2": "summary",
            "json_build_object_3": "group_id",
            "json_build_object_4": "role_id",
            "json_build_object_5": "permission",
            "param_1": "displayname",
            "param_2": "read",
            "param_3": "readwrite",
            "param_4": True,
            "param_5": False,
            "permission_1": "read",
            "permission_2": "write",
            "position_1": "|read",
        }

    def test_get_saved_search_notfound(self):
        self.session.execute().fetchone.return_value = None
        self.session.execute.reset_mock()

        with pytest.raises(minty.exceptions.NotFound):
            self.qry.get_saved_search(uuid4())


class TestListSavedSearch(TestBase):
    def setup_method(self):
        self.load_query_instance(case_management)
        self.qry.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_list_saved_search(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        object_type_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "label": "ObjectTypeNameHere",
                            "value": object_type_uuid,
                        },
                    },
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
            ],
            authorizations=["read", "readwrite"],
            user_is_owner=False,
        )

        self.session.execute().fetchall.return_value = [mock_db_row]
        self.session.execute().scalar.return_value = 31337
        self.session.execute.reset_mock()

        result = self.qry.list_saved_search(page=5, page_size=10)

        assert isinstance(result, minty.entity.EntityCollection)
        assert result.total_results == 31337
        assert result.entities == [
            entity.SavedSearch.parse_obj(
                {
                    "entity_id": saved_search_uuid,
                    "uuid": saved_search_uuid,
                    "name": "Test Name",
                    "kind": "custom_object",
                    "owner": {
                        "entity_id": owner_uuid,
                        "uuid": owner_uuid,
                        "entity_meta_summary": "Owner Name",
                    },
                    "permissions": [
                        {
                            "group_id": group_uuid,
                            "role_id": role_uuid,
                            "permission": ["read"],
                        },
                    ],
                    "filters": {
                        "kind_type": "custom_object",
                        "filters": [
                            {
                                "type": "relationship.custom_object_type",
                                "parameters": {
                                    "label": "ObjectTypeNameHere",
                                    "value": object_type_uuid,
                                },
                            },
                        ],
                    },
                    "columns": [
                        {
                            "source": ["attributes", "a"],
                            "label": "a",
                            "type": "string",
                        },
                        {
                            "source": ["attributes", "b"],
                            "label": "b",
                            "type": "datetime",
                        },
                    ],
                    "sort_column": "a",
                    "sort_order": "asc",
                    "entity_meta_authorizations": {
                        entity.AuthorizationLevel.read,
                        entity.AuthorizationLevel.readwrite,
                    },
                    "_event_service": None,
                }
            )
        ]

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        saved_search_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(saved_search_query) == (
            "SELECT saved_search.uuid, saved_search.name, saved_search.kind, json_build_object(%(json_build_object_1)s, subject.uuid, %(json_build_object_2)s, CAST(subject.properties AS JSON) ->> %(param_1)s) AS owner, saved_search.filters, saved_search.columns, saved_search.sort_column, saved_search.sort_order, coalesce(anon_1.permission_rows, CAST(ARRAY[] AS JSON[])) AS permission_rows, array((SELECT CASE WHEN (saved_search_permission.permission = %(permission_1)s) THEN %(param_2)s WHEN (saved_search_permission.permission = %(permission_2)s) THEN %(param_3)s END AS anon_2 \n"
            "FROM saved_search_permission JOIN subject_position_matrix ON saved_search_permission.group_id = subject_position_matrix.group_id AND saved_search_permission.role_id = subject_position_matrix.role_id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.permission)) AS authorizations, CASE WHEN (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s)) THEN %(param_4)s ELSE %(param_5)s END AS user_is_owner \n"
            "FROM saved_search JOIN subject ON saved_search.owner_id = subject.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_3)s, groups.uuid, %(json_build_object_4)s, roles.uuid, %(json_build_object_5)s, saved_search_permission.permission)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id \n"
            "WHERE saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_3 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s))) ORDER BY saved_search.name \n"
            " LIMIT %(param_6)s OFFSET %(param_7)s"
        )
        assert saved_search_query.params == {
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
            "json_build_object_1": "uuid",
            "json_build_object_2": "summary",
            "json_build_object_3": "group_id",
            "json_build_object_4": "role_id",
            "json_build_object_5": "permission",
            "param_1": "displayname",
            "param_2": "read",
            "param_3": "readwrite",
            "param_4": True,
            "param_5": False,
            "param_6": 10,  # Page size
            "param_7": 40,  # 5th page
            "permission_1": "read",
            "permission_2": "write",
            "position_1": "|read",
        }

        saved_search_query = call_list[1][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(saved_search_query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM saved_search JOIN subject ON saved_search.owner_id = subject.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_1)s, groups.uuid, %(json_build_object_2)s, roles.uuid, %(json_build_object_3)s, saved_search_permission.permission)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id \n"
            "WHERE saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_2 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s)))"
        )
        assert saved_search_query.params == {
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
            "json_build_object_1": "group_id",
            "json_build_object_2": "role_id",
            "json_build_object_3": "permission",
            "param_1": 1,
            "position_1": "|read",
        }

    def test_list_saved_search_with_kind_filter(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        custom_object_type_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "label": "ObjectTypeNameHere",
                            "value": custom_object_type_uuid,
                        },
                    },
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "write",
                },
            ],
        )

        self.session.execute().fetchall.return_value = [mock_db_row]
        self.session.execute().scalar.return_value = 31337
        self.session.execute.reset_mock()

        result = self.qry.list_saved_search(
            filter={"kind": "custom_object"}, page=5, page_size=10
        )

        assert isinstance(result, minty.entity.EntityCollection)
        assert result.total_results == 31337
        assert result.entities == [
            entity.SavedSearch.parse_obj(
                {
                    "entity_id": saved_search_uuid,
                    "uuid": saved_search_uuid,
                    "name": "Test Name",
                    "kind": "custom_object",
                    "owner": {
                        "entity_id": owner_uuid,
                        "uuid": owner_uuid,
                        "entity_meta_summary": "Owner Name",
                    },
                    "permissions": [
                        {
                            "group_id": group_uuid,
                            "role_id": role_uuid,
                            "permission": ["read", "write"],
                        },
                    ],
                    "filters": {
                        "kind_type": "custom_object",
                        "filters": [
                            {
                                "type": "relationship.custom_object_type",
                                "parameters": {
                                    "label": "ObjectTypeNameHere",
                                    "value": custom_object_type_uuid,
                                },
                            },
                        ],
                    },
                    "columns": [
                        {
                            "source": ["attributes", "a"],
                            "label": "a",
                            "type": "string",
                        },
                        {
                            "source": ["attributes", "b"],
                            "label": "b",
                            "type": "datetime",
                        },
                    ],
                    "sort_column": "a",
                    "sort_order": "asc",
                    "entity_meta_authorizations": {
                        entity.AuthorizationLevel.read,
                        entity.AuthorizationLevel.readwrite,
                        entity.AuthorizationLevel.admin,
                    },
                    "_event_service": None,
                }
            )
        ]

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        saved_search_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(saved_search_query) == (
            "SELECT saved_search.uuid, saved_search.name, saved_search.kind, json_build_object(%(json_build_object_1)s, subject.uuid, %(json_build_object_2)s, CAST(subject.properties AS JSON) ->> %(param_1)s) AS owner, saved_search.filters, saved_search.columns, saved_search.sort_column, saved_search.sort_order, coalesce(anon_1.permission_rows, CAST(ARRAY[] AS JSON[])) AS permission_rows, array((SELECT CASE WHEN (saved_search_permission.permission = %(permission_1)s) THEN %(param_2)s WHEN (saved_search_permission.permission = %(permission_2)s) THEN %(param_3)s END AS anon_2 \n"
            "FROM saved_search_permission JOIN subject_position_matrix ON saved_search_permission.group_id = subject_position_matrix.group_id AND saved_search_permission.role_id = subject_position_matrix.role_id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.permission)) AS authorizations, CASE WHEN (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s)) THEN %(param_4)s ELSE %(param_5)s END AS user_is_owner \n"
            "FROM saved_search JOIN subject ON saved_search.owner_id = subject.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_3)s, groups.uuid, %(json_build_object_4)s, roles.uuid, %(json_build_object_5)s, saved_search_permission.permission)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id \n"
            "WHERE (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_3 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s)))) AND saved_search.kind = %(kind_1)s ORDER BY saved_search.name \n"
            " LIMIT %(param_6)s OFFSET %(param_7)s"
        )
        assert saved_search_query.params == {
            "json_build_object_1": "uuid",
            "json_build_object_2": "summary",
            "json_build_object_3": "group_id",
            "json_build_object_4": "role_id",
            "json_build_object_5": "permission",
            "kind_1": entity.SavedSearchKind.custom_object,
            "param_1": "displayname",
            "param_2": "read",
            "param_3": "readwrite",
            "param_4": True,
            "param_5": False,
            "param_6": 10,  # Page size
            "param_7": 40,  # 5th page
            "permission_1": "read",
            "permission_2": "write",
            "position_1": "|read",
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
        }

        saved_search_query = call_list[1][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(saved_search_query) == (
            "SELECT count(%(param_1)s) AS count_1 \n"
            "FROM saved_search JOIN subject ON saved_search.owner_id = subject.id LEFT OUTER JOIN LATERAL (SELECT saved_search_permission.saved_search_id AS saved_search_id, array_agg(json_build_object(%(json_build_object_1)s, groups.uuid, %(json_build_object_2)s, roles.uuid, %(json_build_object_3)s, saved_search_permission.permission)) AS permission_rows \n"
            "FROM saved_search_permission JOIN groups ON saved_search_permission.group_id = groups.id JOIN roles ON saved_search_permission.role_id = roles.id \n"
            "WHERE saved_search_permission.saved_search_id = saved_search.id GROUP BY saved_search_permission.saved_search_id) AS anon_1 ON saved_search.id = anon_1.saved_search_id \n"
            "WHERE (saved_search.owner_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s) OR saved_search.permissions && array((SELECT subject_position_matrix.position || %(position_1)s AS anon_2 \n"
            "FROM subject_position_matrix \n"
            "WHERE subject_position_matrix.subject_id = (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s)))) AND saved_search.kind = %(kind_1)s"
        )
        assert saved_search_query.params == {
            "json_build_object_1": "group_id",
            "json_build_object_2": "role_id",
            "json_build_object_3": "permission",
            "kind_1": "custom_object",
            "param_1": 1,
            "position_1": "|read",
            "subject_type_1": "employee",
            "uuid_1": self.qry.user_info.user_uuid,
        }


class TestCreateSavedSearch(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_create_saved_search(self):
        saved_search_db_id = random.randint(0, 2**32)
        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        self.session.execute().fetchone.return_value = fetchone_rv_mock
        self.session.reset_mock()

        saved_search_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        object_type_uuid = uuid4()

        self.cmd.create_saved_search(
            uuid=saved_search_uuid,
            name="Test Search",
            kind="custom_object",
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "label": "ObjectTypeNameHere",
                            "value": object_type_uuid,
                        },
                    },
                ],
            },
            permissions=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": ["read"],
                }
            ],
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="random",
            sort_order="asc",
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        # Query the owner's full name
        owner_name_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(owner_name_query)
            == "SELECT CAST(subject.properties AS JSON) ->> %(param_1)s AS summary \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s"
        )
        assert owner_name_query.params == {
            "param_1": "displayname",
            "subject_type_1": "employee",
            "uuid_1": self.cmd.user_info.user_uuid,
        }

        # Insert the saved search
        insert_saved_search = call_list[1][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_saved_search) == (
            "INSERT INTO saved_search (uuid, name, kind, owner_id, filters, columns, sort_column, sort_order) VALUES (%(uuid)s, %(name)s, %(kind)s, (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s), %(filters)s, %(columns)s, %(sort_column)s, %(sort_order)s) RETURNING saved_search.id"
        )

        assert insert_saved_search.params == {
            "uuid": str(saved_search_uuid),
            "name": "Test Search",
            "kind": "custom_object",
            "subject_type_1": "employee",
            "uuid_1": str(self.cmd.user_info.user_uuid),
            "filters": {
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "label": "ObjectTypeNameHere",
                            "value": str(object_type_uuid),
                        },
                    },
                ],
            },
            "columns": [
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            "sort_column": "random",
            "sort_order": "asc",
        }

        # Insert permissions
        insert_permissions = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(insert_permissions) == (
            "INSERT INTO saved_search_permission (saved_search_id, group_id, role_id, permission) VALUES (%(saved_search_id_m0)s, (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_1)s), (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_2)s), %(permission_m0)s)"
        )
        assert insert_permissions.params == {
            "saved_search_id_m0": saved_search_db_id,
            "uuid_1": str(group_uuid),
            "uuid_2": str(role_uuid),
            "permission_m0": "read",
        }

        # Call procedure to update permissions
        procedure_call = call_list[3][0][0]

        assert (
            procedure_call.text
            == "CALL saved_search_permission_sync(:saved_search_id)"
        )

    def test_create_saved_search_with_multiple_filters(self):
        saved_search_db_id = random.randint(0, 2**32)
        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        self.session.execute().fetchone.return_value = fetchone_rv_mock
        self.session.reset_mock()

        saved_search_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()
        object_type_uuid = uuid4()

        self.cmd.create_saved_search(
            uuid=saved_search_uuid,
            name="Test Search",
            kind="custom_object",
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "value": object_type_uuid,
                            "label": "ObjectTypeNameHere",
                        },
                    },
                    {
                        "type": "attributes.last_modified",
                        "parameters": {
                            "label": "attributes.last_modified",
                            "value": [
                                {
                                    "type": "absolute",
                                    "value": "2022-01-01T00:00:00Z",
                                    "operator": "lt",
                                },
                                {
                                    "type": "relative",
                                    "value": "+P0Y0M0DT3H0M0S",
                                    "operator": "lt",
                                },
                                {
                                    "type": "range",
                                    "start_value": "2022-08-04T11:00:00.000Z",
                                    "end_value": "2022-08-04T11:00:00.000Z",
                                    "time_set_by_user": True,
                                },
                            ],
                        },
                    },
                ],
            },
            permissions=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": ["read"],
                }
            ],
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="random",
            sort_order="asc",
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 4

        # Query the owner's full name
        owner_name_query = call_list[0][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert (
            str(owner_name_query)
            == "SELECT CAST(subject.properties AS JSON) ->> %(param_1)s AS summary \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s"
        )
        assert owner_name_query.params == {
            "param_1": "displayname",
            "subject_type_1": "employee",
            "uuid_1": self.cmd.user_info.user_uuid,
        }

        # Insert the saved search
        insert_saved_search = call_list[1][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(insert_saved_search) == (
            "INSERT INTO saved_search (uuid, name, kind, owner_id, filters, columns, sort_column, sort_order) VALUES (%(uuid)s, %(name)s, %(kind)s, (SELECT subject.id \n"
            "FROM subject \n"
            "WHERE subject.subject_type = %(subject_type_1)s AND subject.uuid = %(uuid_1)s), %(filters)s, %(columns)s, %(sort_column)s, %(sort_order)s) RETURNING saved_search.id"
        )

        assert insert_saved_search.params == {
            "uuid": str(saved_search_uuid),
            "name": "Test Search",
            "kind": "custom_object",
            "subject_type_1": "employee",
            "uuid_1": str(self.cmd.user_info.user_uuid),
            "filters": {
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "value": str(object_type_uuid),
                            "label": "ObjectTypeNameHere",
                        },
                    },
                    {
                        "type": "attributes.last_modified",
                        "parameters": {
                            "label": "attributes.last_modified",
                            "value": [
                                {
                                    "type": "absolute",
                                    "value": "2022-01-01T00:00:00+00:00",
                                    "operator": "lt",
                                },
                                {
                                    "type": "relative",
                                    "value": "+P0Y0M0DT3H0M0S",
                                    "operator": "lt",
                                },
                                {
                                    "type": "range",
                                    "start_value": "2022-08-04T11:00:00+00:00",
                                    "end_value": "2022-08-04T11:00:00+00:00",
                                    "time_set_by_user": True,
                                },
                            ],
                        },
                    },
                ],
            },
            "columns": [
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            "sort_column": "random",
            "sort_order": "asc",
        }

        # Insert permissions
        insert_permissions = call_list[2][0][0].compile(
            dialect=postgresql.dialect()
        )

        assert str(insert_permissions) == (
            "INSERT INTO saved_search_permission (saved_search_id, group_id, role_id, permission) VALUES (%(saved_search_id_m0)s, (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_1)s), (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_2)s), %(permission_m0)s)"
        )
        assert insert_permissions.params == {
            "saved_search_id_m0": saved_search_db_id,
            "uuid_1": str(group_uuid),
            "uuid_2": str(role_uuid),
            "permission_m0": "read",
        }

        # Call procedure to update permissions
        procedure_call = call_list[3][0][0]

        assert (
            procedure_call.text
            == "CALL saved_search_permission_sync(:saved_search_id)"
        )

    def test_create_saved_search_dupe(self):
        saved_search_db_id = random.randint(0, 2**32)
        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        self.session.execute().fetchone.side_effect = [
            fetchone_rv_mock,
            sqlalchemy_exc.IntegrityError("X", {}, "X"),
        ]
        self.session.reset_mock()

        saved_search_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.create_saved_search(
                uuid=saved_search_uuid,
                name="Test Search",
                kind="case",
                filters={
                    "kind_type": "case",
                    "filters": [
                        {
                            "type": "attributes.confidentiality",
                            "parameters": {
                                "value": "public",
                                "label": "ConfidentialityValue",
                            },
                        },
                    ],
                },
                permissions=[
                    {
                        "group_id": group_uuid,
                        "role_id": role_uuid,
                        "permission": ["read"],
                    }
                ],
                columns=[
                    {
                        "source": ["attributes", "a"],
                        "label": "a",
                        "type": "string",
                    },
                    {
                        "source": ["attributes", "b"],
                        "label": "b",
                        "type": "datetime",
                    },
                ],
                sort_column="random",
                sort_order="asc",
            )

    def test_create_saved_search_bad_permission(self):
        saved_search_db_id = random.randint(0, 2**32)
        fetchone_rv_mock = mock.Mock(summary="Employee Name Here")
        fetchone_rv_mock.id = saved_search_db_id

        self.session.execute().fetchone.return_value = fetchone_rv_mock
        self.session.reset_mock()

        saved_search_uuid = uuid4()
        group_uuid = uuid4()
        role_uuid = uuid4()

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.create_saved_search(
                uuid=saved_search_uuid,
                name="Test Search",
                kind="case",
                filters={
                    "kind_type": "case",
                    "filters": [
                        {
                            "type": "attributes.confidentiality",
                            "parameters": {
                                "value": "public",
                                "label": "ConfidentialityValue",
                            },
                        },
                    ],
                },
                permissions=[
                    {
                        "group_id": group_uuid,
                        "role_id": role_uuid,
                        "permission": ["write"],
                    }
                ],
                columns=[
                    {
                        "source": ["attributes", "a"],
                        "label": "a",
                        "type": "string",
                    },
                    {
                        "source": ["attributes", "b"],
                        "label": "b",
                        "type": "datetime",
                    },
                ],
                sort_column="random",
                sort_order="asc",
            )


class TestDeleteSavedSearch(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_delete_saved_search(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "case",
                "filters": [
                    {
                        "type": "attributes.confidentiality",
                        "parameters": {
                            "value": "public",
                            "label": "ConfidentialityValue",
                        },
                    },
                    {
                        "type": "relationship.requestor.id",
                        "parameters": [
                            {
                                "label": "relationshipid",
                                "value": str(uuid4()),
                                "type": "person",
                            }
                        ],
                    },
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                    "magic_string": "b_magic_string",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[],
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute.reset_mock()

        self.cmd.delete_saved_search(uuid=saved_search_uuid)

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        # Query the owner's full name
        delete_query = call_list[1][0][0].compile(dialect=postgresql.dialect())
        assert (
            str(delete_query)
            == "DELETE FROM saved_search WHERE saved_search.uuid = %(uuid_1)s"
        )
        assert delete_query.params == {"uuid_1": str(saved_search_uuid)}


class TestUpdateSavedSearch(TestBase):
    def setup_method(self):
        self.load_command_instance(case_management)
        self.cmd.user_info = minty.cqrs.UserInfo(
            user_uuid=uuid4(), permissions={"admin": True}
        )

    def test_update_saved_search_only_content(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()
        cutsom_object_type_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "value": cutsom_object_type_uuid,
                            "label": "ObjectTypeNameHere",
                        },
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[],
            user_is_owner=False,
            authorizations=["read", "readwrite"],
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute.reset_mock()

        self.cmd.update_saved_search(
            uuid=saved_search_uuid,
            name="New Test Name",
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "value": cutsom_object_type_uuid,
                            "label": "ObjectTypeNameHere",
                        },
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "c"],
                    "label": "c",
                    "type": "string",
                },
            ],
            sort_column="c",
            sort_order="desc",
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 2

        # Query the owner's full name
        update_query = call_list[1][0][0].compile(dialect=postgresql.dialect())
        assert (
            str(update_query)
            == "UPDATE saved_search SET name=%(name)s, filters=%(filters)s, columns=%(columns)s, sort_column=%(sort_column)s, sort_order=%(sort_order)s WHERE saved_search.uuid = %(uuid_1)s"
        )
        assert update_query.params == {
            "columns": [
                {"label": "c", "source": ["attributes", "c"], "type": "string"}
            ],
            "filters": {
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "value": str(cutsom_object_type_uuid),
                            "label": "ObjectTypeNameHere",
                        },
                    }
                ],
            },
            "name": "New Test Name",
            "sort_column": "c",
            "sort_order": "desc",
            "uuid_1": str(saved_search_uuid),
        }

        with pytest.raises(minty.exceptions.Forbidden):
            # Cannot update permissions if user does not have admin rights
            self.cmd.update_saved_search(
                uuid=saved_search_uuid,
                name="New Test Name",
                filters={
                    "kind_type": "custom_object",
                    "filters": [
                        {
                            "type": "relationship.custom_object_type",
                            "parameters": {
                                "value": str(cutsom_object_type_uuid),
                                "label": "ObjectTypeNameHere",
                            },
                        }
                    ],
                },
                columns=[
                    {
                        "source": ["attributes", "c"],
                        "label": "c",
                        "type": "string",
                    },
                ],
                sort_column="c",
                sort_order="desc",
                permissions=[],
            )

    def test_update_saved_search_content_and_permission(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()

        group_uuid = uuid4()
        role_uuid = uuid4()

        group2_uuid = uuid4()
        role2_uuid = uuid4()
        custom_object_type_uuid = uuid4()
        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "value": custom_object_type_uuid,
                            "label": "ObjectTypeNameHere",
                        },
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
                {
                    "group_id": group2_uuid,
                    "role_id": role2_uuid,
                    "permission": "read",
                },
            ],
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute.reset_mock()

        self.cmd.update_saved_search(
            uuid=saved_search_uuid,
            name="New Test Name",
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "value": custom_object_type_uuid,
                            "label": "ObjectTypeNameHere",
                        },
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "c"],
                    "label": "c",
                    "type": "string",
                },
            ],
            sort_column="c",
            sort_order="desc",
            permissions=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": ["read", "write"],
                },
            ],
        )

        call_list = self.session.execute.call_args_list
        assert len(call_list) == 6

        # Query the owner's full name
        update_query = call_list[1][0][0].compile(dialect=postgresql.dialect())
        assert (
            str(update_query)
            == "UPDATE saved_search SET name=%(name)s, filters=%(filters)s, columns=%(columns)s, sort_column=%(sort_column)s, sort_order=%(sort_order)s WHERE saved_search.uuid = %(uuid_1)s"
        )
        assert update_query.params == {
            "columns": [
                {"label": "c", "source": ["attributes", "c"], "type": "string"}
            ],
            "filters": {
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "value": str(custom_object_type_uuid),
                            "label": "ObjectTypeNameHere",
                        },
                    }
                ],
            },
            "name": "New Test Name",
            "sort_column": "c",
            "sort_order": "desc",
            "uuid_1": str(saved_search_uuid),
        }

        # Insert new permission
        insert_query = call_list[2][0][0].compile(dialect=postgresql.dialect())
        assert str(insert_query) == (
            "INSERT INTO saved_search_permission (saved_search_id, group_id, role_id, permission) VALUES ((SELECT saved_search.id \n"
            "FROM saved_search \n"
            "WHERE saved_search.uuid = %(uuid_1)s), (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_2)s), (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_3)s), %(permission_m0)s)"
        )
        assert insert_query.params == {
            "permission_m0": "write",
            "uuid_1": str(saved_search_uuid),
            "uuid_2": str(group_uuid),
            "uuid_3": str(role_uuid),
        }

        # Remove old permission
        delete_query = call_list[3][0][0].compile(dialect=postgresql.dialect())
        assert str(delete_query) == (
            "DELETE FROM saved_search_permission WHERE saved_search_permission.saved_search_id = (SELECT saved_search.id \n"
            "FROM saved_search \n"
            "WHERE saved_search.uuid = %(uuid_1)s) AND saved_search_permission.group_id = (SELECT groups.id \n"
            "FROM groups \n"
            "WHERE groups.uuid = %(uuid_2)s) AND saved_search_permission.role_id = (SELECT roles.id \n"
            "FROM roles \n"
            "WHERE roles.uuid = %(uuid_3)s) AND saved_search_permission.permission = %(permission_1)s"
        )
        assert delete_query.params == {
            "permission_1": "read",
            "uuid_1": str(saved_search_uuid),
            "uuid_2": str(group2_uuid),
            "uuid_3": str(role2_uuid),
        }

        # Call procedure to update permissions
        id_lookup_select = call_list[4][0][0].compile(
            dialect=postgresql.dialect()
        )
        assert str(id_lookup_select) == (
            "SELECT saved_search.id \n"
            "FROM saved_search \n"
            "WHERE saved_search.uuid = %(uuid_1)s"
        )
        assert id_lookup_select.params == {"uuid_1": str(saved_search_uuid)}

        procedure_call = call_list[5][0][0]
        assert (
            procedure_call.text
            == "CALL saved_search_permission_sync(:saved_search_id)"
        )

    def test_update_saved_search_readonly_forbidden(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()

        group_uuid = uuid4()
        role_uuid = uuid4()
        custom_object_type_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "value": custom_object_type_uuid,
                            "label": "ObjectTypeNameHere",
                        },
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
            ],
            user_is_owner=False,
            authorizations=["read"],
        )

        self.session.execute().fetchone.return_value = mock_db_row
        self.session.execute.reset_mock()

        with pytest.raises(minty.exceptions.Forbidden):
            self.cmd.update_saved_search(
                uuid=saved_search_uuid,
                name="New Test Name",
                filters={
                    "kind_type": "custom_object",
                    "filters": [
                        {
                            "type": "relationship.custom_object_type",
                            "parameters": {
                                "value": custom_object_type_uuid,
                                "label": "ObjectTypeNameHere",
                            },
                        }
                    ],
                },
                columns=[
                    {
                        "source": ["attributes", "c"],
                        "label": "c",
                        "type": "string",
                    },
                ],
                sort_column="c",
                sort_order="desc",
            )

    def test_update_saved_search_dupe_name(self):
        saved_search_uuid = uuid4()
        owner_uuid = uuid4()

        group_uuid = uuid4()
        role_uuid = uuid4()
        custom_object_type_uuid = uuid4()

        mock_db_row = mock.Mock()
        mock_db_row.configure_mock(
            name="Test Name",
            uuid=saved_search_uuid,
            kind="custom_object",
            owner={"uuid": owner_uuid, "summary": "Owner Name"},
            filters={
                "kind_type": "custom_object",
                "filters": [
                    {
                        "type": "relationship.custom_object_type",
                        "parameters": {
                            "value": custom_object_type_uuid,
                            "label": "ObjectTypeNameHere",
                        },
                    }
                ],
            },
            columns=[
                {
                    "source": ["attributes", "a"],
                    "label": "a",
                    "type": "string",
                },
                {
                    "source": ["attributes", "b"],
                    "label": "b",
                    "type": "datetime",
                },
            ],
            sort_column="a",
            sort_order="asc",
            permission_rows=[
                {
                    "group_id": group_uuid,
                    "role_id": role_uuid,
                    "permission": "read",
                },
            ],
            user_is_owner=False,
            authorizations=["read", "readwrite", "admin"],
        )

        mock_execute = mock.Mock()
        mock_execute.fetchone.return_value = mock_db_row
        self.session.execute.side_effect = [
            mock_execute,
            sqlalchemy_exc.IntegrityError("X", {}, "X"),
        ]
        self.session.execute.reset_mock()

        with pytest.raises(minty.exceptions.Conflict):
            self.cmd.update_saved_search(
                uuid=saved_search_uuid,
                name="New Test Name",
                filters={
                    "kind_type": "custom_object",
                    "filters": [
                        {
                            "type": "relationship.custom_object_type",
                            "parameters": {
                                "value": custom_object_type_uuid,
                                "label": "ObjectTypeNameHere",
                            },
                        }
                    ],
                },
                columns=[
                    {
                        "source": ["attributes", "c"],
                        "label": "c",
                        "type": "string",
                    },
                ],
                sort_column="c",
                sort_order="desc",
            )
