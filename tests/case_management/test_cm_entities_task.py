# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import copy
from datetime import date
from unittest import mock
from uuid import uuid4
from zsnl_domains.case_management import entities


class TestTaskEntity:
    def setup_method(self):
        self.task = entities.Task(
            uuid=uuid4(),
            title="some_title",
            description="some descript",
            due_date="2019-10-21",
            completed=False,
            user_defined=True,
            assignee={
                "type": "employee",
                "uuid": uuid4(),
                "display_name": "Ad Min",
            },
            case={"uuid": uuid4(), "status": "open", "milestone": 2},
            case_type=None,
            phase=2,
            department={
                "type": "department",
                "uuid": uuid4(),
                "display_name": "Backoffice",
            },
        )
        self.task.event_service = mock.MagicMock()

    def test_create(self):
        case = entities.Case(
            id=1,
            number=1,
            uuid=uuid4(),
            department=entities.Department(
                uuid=uuid4(), name="Dept", description="Dept"
            ),
            role=None,
            destruction_date=date(2019, 7, 8),
            archival_state=False,
            status="open",
            created_date=date(2019, 12, 11),
            registration_date=None,
            target_completion_date=None,
            completion=None,
            completion_date=None,
            stalled_until_date=None,
            milestone=2,
            suspension_reason=None,
            stalled_since_date=None,
            last_modified_date=None,
            coordinator=None,
            assignee=None,
        )
        task = entities.Task(
            uuid=uuid4(),
            title=None,
            description=None,
            due_date=None,
            completed=None,
            user_defined=None,
            assignee=None,
            case=None,
            case_type=None,
            phase=None,
            department=None,
        )
        event_service_mock = mock.MagicMock()
        task.event_service = event_service_mock
        task.create(title="new_title", case=case, phase=2)
        task.event_service.log_event.assert_called_with(
            entity_type=task.__class__.__name__,
            entity_id=task.entity_id,
            event_name="TaskCreated",
            changes=[
                {
                    "key": "case",
                    "new_value": {
                        "milestone": 2,
                        "status": "open",
                        "uuid": str(case.uuid),
                        "id": case.id,
                    },
                    "old_value": None,
                },
                {"key": "title", "new_value": "new_title", "old_value": None},
                {"key": "phase", "new_value": 2, "old_value": None},
                {"key": "completed", "new_value": False, "old_value": None},
                {"key": "user_defined", "new_value": True, "old_value": None},
                {"key": "description", "new_value": "", "old_value": None},
            ],
            entity_data={
                "case": {
                    "milestone": 2,
                    "status": "open",
                    "uuid": str(task.case["uuid"]),
                    "id": case.id,
                },
                "title": "new_title",
            },
        )

    def test_is_editable(self):
        task = copy.deepcopy(self.task)
        task.case["status"] = "open"
        task.case["milestone"] = 1
        task.phase = 1
        assert task.can_set_completion is True
        task.case["milestone"] = 2
        assert task.can_set_completion is False
        task.case["status"] = "resolved"
        assert task.can_set_completion is False

    def test_can_set_completion(self):
        task = copy.deepcopy(self.task)
        task.case["status"] = "open"
        task.case["milestone"] = 1
        task.phase = 1
        task.user_defined = True
        task.completed = False
        assert task.is_editable is True

        task.case["milestone"] = 2
        assert task.is_editable is False

        task.case["milestone"] = 1
        task.case["status"] = "resolved"
        assert task.is_editable is False

        task.case["status"] = "open"
        # Completed-ness is not part of the "editable" calculation
        task.completed = True
        assert task.is_editable is True

        task.completed = False
        task.user_defined = False
        assert task.is_editable is True

    def test_delete(self):
        self.task.delete()
        self.task.event_service.log_event.assert_called_with(
            entity_type=self.task.__class__.__name__,
            entity_id=self.task.entity_id,
            event_name="TaskDeleted",
            changes=[],
            entity_data={
                "case": {
                    "milestone": 2,
                    "status": "open",
                    "uuid": str(self.task.case["uuid"]),
                },
                "title": "some_title",
            },
        )

    def test_update(self):
        uuid = self.task.assignee["uuid"]

        self.task.update(
            title="new_title2",
            description="new_description",
            due_date="2019-01-02",
            assignee=None,
        )
        self.task.event_service.log_event.assert_called_with(
            entity_type=self.task.__class__.__name__,
            entity_id=self.task.entity_id,
            event_name="TaskUpdated",
            changes=[
                {
                    "key": "title",
                    "new_value": "new_title2",
                    "old_value": "some_title",
                },
                {
                    "key": "description",
                    "new_value": "new_description",
                    "old_value": "some descript",
                },
                {
                    "key": "due_date",
                    "new_value": "2019-01-02",
                    "old_value": "2019-10-21",
                },
                {
                    "key": "assignee",
                    "new_value": None,
                    "old_value": {
                        "display_name": "Ad Min",
                        "type": "employee",
                        "uuid": str(uuid),
                    },
                },
            ],
            entity_data={
                "case": {
                    "milestone": 2,
                    "status": "open",
                    "uuid": str(self.task.case["uuid"]),
                },
                "title": "new_title2",
            },
        )

    def test_set_completion(self):
        self.task.set_completion(completed=True)
        self.task.event_service.log_event.assert_called_with(
            entity_type=self.task.__class__.__name__,
            entity_id=self.task.entity_id,
            event_name="TaskCompletionSet",
            changes=[
                {"key": "completed", "new_value": True, "old_value": False}
            ],
            entity_data={
                "case": {
                    "milestone": 2,
                    "status": "open",
                    "uuid": str(self.task.case["uuid"]),
                }
            },
        )
