# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from collections import namedtuple
from datetime import datetime
from minty.cqrs.events import Event, EventService
from minty.exceptions import Conflict, NotFound
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog.entities.object_type import ObjectType
from zsnl_domains.admin.catalog.repositories.object_type import (
    ObjectTypeRepository,
)


class TestObjectTypeRepository:
    def setup_method(self):
        self.mock_infra = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

        self.object_type_repo = ObjectTypeRepository(
            infrastructure_factory=self.mock_infra,
            context="devtest",
            event_service=event_service,
        )

    def test_get_session(self):
        assert (
            self.object_type_repo.session
            is self.mock_infra.get_infrastructure()
        )

    def test_get_object_type_by_uuid(self):
        uuid = str(uuid4())
        mock_ses = mock.MagicMock()
        self.object_type_repo.session = mock_ses
        mock_ses.query().filter().one.return_value = namedtuple(
            "query_result", "object_uuid name"
        )(object_uuid=uuid, name="test obj type")

        res = self.object_type_repo.get_object_type_by_uuid(uuid=uuid)

        assert res.uuid == uuid
        assert res.name == "test obj type"

        with pytest.raises(NotFound) as excinfo:
            mock_ses.query().filter().one.side_effect = NoResultFound()
            self.object_type_repo.get_object_type_by_uuid(uuid=uuid)

        assert excinfo.value.args == (
            f"Object type with uuid: '{uuid}' not found.",
            "object_type/not_found",
        )

    @mock.patch.object(ObjectTypeRepository, "get_object_type_by_uuid")
    def test_delete_object_type(self, mock_get_object_type):
        uuid = str(uuid4())
        reason = "no more used"
        mock_get_object_type.return_value = ObjectType(
            uuid=uuid, name="test obj type"
        )

        res = self.object_type_repo.delete_object_type(
            uuid=uuid, reason=reason
        )

        assert res.name == "test obj type"
        assert res.uuid == uuid
        assert res.commit_message == reason
        assert isinstance(res.deleted, datetime)

    def test__delete_object_type(self):
        uuid = uuid4()
        event_service = mock.MagicMock()
        event = Event(
            uuid=uuid4(),
            created_date="2019-04-19",
            correlation_id="req-12345",
            domain="admin",
            context="localhost",
            user_uuid=uuid4(),
            entity_type="ObjectType",
            entity_id=uuid,
            event_name="ObjectTypeDeleted",
            changes=[
                {
                    "key": "deleted",
                    "old_value": None,
                    "new_value": "2019-02-23",
                },
                {
                    "key": "commit_message",
                    "old_value": None,
                    "new_value": "no more used",
                },
            ],
            entity_data={},
        )
        event_service.event_list = [event]
        self.object_type_repo.event_service = event_service

        self.object_type_repo.save()

        with pytest.raises(Conflict) as excinfo:
            mock_ses = mock.MagicMock()
            self.object_type_repo.session = mock_ses
            mock_ses.execute.side_effect = IntegrityError(
                statement=None, params={}, orig=Exception
            )

            self.object_type_repo.save()

        assert excinfo.value.args == (
            "Object type is used in case type",
            "object_type/used_in_case_types",
        )
