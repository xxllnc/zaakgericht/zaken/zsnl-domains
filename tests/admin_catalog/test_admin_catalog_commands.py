# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from minty.exceptions import ValidationError
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog.commands import Commands


class TestCommandClass:
    def setup_method(self):
        self.mock_repo_factory = mock.MagicMock()
        self.command = Commands(
            repository_factory=self.mock_repo_factory,
            context=None,
            user_uuid=None,
            event_service=None,
        )

    def test_set_case_type_status(self):

        cmd = self.command
        uuid = str(uuid4())
        cmd.change_case_type_online_status(
            case_type_uuid=uuid, active=False, reason="reason"
        )

        self.mock_repo_factory.get_repository.assert_called_once_with(
            name="case_type", context=None, event_service=None
        )
        case_type_repo = self.mock_repo_factory.get_repository("case_type")
        case_type_repo.get_case_type.assert_called_once_with(uuid=uuid)
        case_type_repo.get_case_type().change_online_status.assert_called_once_with(
            active=False, reason="reason"
        )

    def test_move_folder_entries_root(self):

        cmd = self.command

        dest_uuid = None
        fold_entries = [
            {"type": "case_type", "id": str(uuid4())},
            {"type": "case_type", "id": str(uuid4())},
        ]
        return_ent = mock.MagicMock()
        entities = [return_ent]
        folder_entires_repo = self.mock_repo_factory.get_repository(
            "folder_entries"
        )
        folder_entires_repo.get_folder_entries.return_value = entities

        cmd.move_folder_entries(
            destination_folder_id=dest_uuid, folder_entries=fold_entries
        )

        folder_entires_repo.get_folder_entries.assert_called_once_with(
            folder_entries=fold_entries
        )
        return_ent.move.assert_called_once()
        folder_entires_repo.save_list.assert_called_once()

    def test_edit_attribute(self):

        cmd = self.command
        uuid = str(uuid4())
        cmd.edit_attribute(attribute_uuid=uuid, fields={"name": "new_name"})

        self.mock_repo_factory.get_repository.assert_called_once_with(
            name="attribute", context=None, event_service=None
        )
        attribute_repo = self.mock_repo_factory.get_repository("attribute")
        attribute_repo.get_attribute_details_by_uuid.assert_called_once_with(
            uuid=uuid
        )
        attribute_repo.save.assert_called_once()

    def test_edit_attribute_invalid_type(self):

        cmd = self.command
        uuid = str(uuid4())

        with pytest.raises(ValidationError):
            cmd.edit_attribute(attribute_uuid=uuid, fields={"name": 123})

    def test_create_attribute(self):

        cmd = self.command
        uuid = str(uuid4())
        cmd.create_attribute(
            attribute_uuid=uuid,
            fields={
                "name": "new_name",
                "attribute_type": "checkbox",
                "magic_string": "magic_string",
                "category_uuid": str(uuid4()),
            },
        )

        self.mock_repo_factory.get_repository.assert_called_once_with(
            name="attribute", context=None, event_service=None
        )
        attribute_repo = self.mock_repo_factory.get_repository("attribute")
        attribute_repo.create_new_attribute.assert_called_once()
        attribute_repo.save.assert_called_once()

    def test_create_attribute_appointment_v2(self):

        cmd = self.command
        uuid = uuid4()

        cmd.create_attribute(
            attribute_uuid=str(uuid),
            fields={
                "name": "new_name",
                "attribute_type": "appointment_v2",
                "magic_string": "magic_string",
                "category_uuid": str(uuid4()),
                "appointment_interface_uuid": str(uuid4()),
            },
        )

        self.mock_repo_factory.get_repository.assert_called_once_with(
            name="attribute", context=None, event_service=None
        )
        attribute_repo = self.mock_repo_factory.get_repository("attribute")
        attribute_repo.create_new_attribute.assert_called_once()
        attribute_repo.save.assert_called_once()

    def test_create_attribute_invalid_magic_string(self):

        cmd = self.command
        uuid = str(uuid4())
        with pytest.raises(ValidationError):
            cmd.create_attribute(
                attribute_uuid=uuid,
                fields={
                    "name": "name",
                    "attribute_type": "checkbox",
                    "magic_string": 123,
                    "category_uuid": str(uuid4()),
                },
            )

    def test_create_document_template(self):
        cmd = self.command
        entity_uuid = str(uuid4())
        fields = fields = {
            "commit_message": "commit message",
            "name": "document",
            "interface_uuid": "2c424b09-fd77-4bbc-b158-715a6701862e",
            "integration_reference": "reference",
            "help": "sender_address@sender.nl",
            "category_uuid": "6ea6d7f8-932d-11e9-b6e4-bf755288c2e1",
        }

        cmd.create_document_template(uuid=entity_uuid, fields=fields)

        document_template_repo = self.mock_repo_factory.get_repository(
            "document_template"
        )
        document_template_repo.create_new_document_template.assert_called_once_with(
            uuid=entity_uuid, fields=fields
        )
        document_template_repo.save.assert_called_once()

    def test_edit_document_template(self):
        cmd = self.command
        entity_uuid = str(uuid4())
        fields = fields = {
            "commit_message": "commit message",
            "name": "document",
            "interface_uuid": "2c424b09-fd77-4bbc-b158-715a6701862e",
            "integration_reference": "reference",
            "help": "sender_address@sender.nl",
            "category_uuid": "6ea6d7f8-932d-11e9-b6e4-bf755288c2e1",
        }

        cmd.edit_document_template(uuid=entity_uuid, fields=fields)

        document_template_repo = self.mock_repo_factory.get_repository(
            "document_template"
        )
        document_template_repo.get_document_template_details_by_uuid.assert_called_once_with(
            uuid=entity_uuid
        )
        document_template_repo.save.assert_called_once()

    def test_delete_attribute(self):
        cmd = self.command
        attribute_uuid = str(uuid4())

        cmd.delete_attribute(uuid=attribute_uuid, reason="No more used")

        attribute_repo = self.mock_repo_factory.get_repository("attribute")
        attribute_repo.delete_attribute.assert_called_once_with(
            uuid=attribute_uuid, reason="No more used"
        )
        attribute_repo.save.assert_called_once()

    def test_delete_case_type(self):
        cmd = self.command
        case_type_uuid = str(uuid4())

        cmd.delete_case_type(uuid=case_type_uuid, reason="No more used")

        case_type_repo = self.mock_repo_factory.get_repository("case_type")
        case_type_repo.delete_case_type.assert_called_once_with(
            uuid=case_type_uuid, reason="No more used"
        )
        case_type_repo.save.assert_called_once()

    def test_delete_object_type(self):
        cmd = self.command
        object_type_uuid = str(uuid4())

        cmd.delete_object_type(uuid=object_type_uuid, reason="No more used")

        self.mock_repo_factory.get_repository(
            "object_type"
        ).delete_object_type.assert_called_once_with(
            uuid=object_type_uuid, reason="No more used"
        )
        self.mock_repo_factory.get_repository(
            "object_type"
        ).save.assert_called_once()

    def test_delete_document_template(self):
        cmd = self.command
        document_template_uuid = str(uuid4())

        cmd.delete_document_template(
            uuid=document_template_uuid, reason="No more used"
        )

        self.mock_repo_factory.get_repository(
            "document_template"
        ).delete_document_template.assert_called_once_with(
            uuid=document_template_uuid, reason="No more used"
        )
        self.mock_repo_factory.get_repository(
            "document_template"
        ).save.assert_called_once()
