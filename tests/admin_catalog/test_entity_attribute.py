# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
from datetime import datetime
from minty.exceptions import Conflict
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog.entities.attribute import Attribute


class TestAttributeEntity:
    def test_edit(self):
        attribute = Attribute(
            id=2,
            uuid=uuid4(),
            name="name",
            public_name="public name",
            attribute_type="option",
            magic_string="magic string",
            sensitive_field=False,
            help=None,
            value_default=None,
            type_multiple=False,
            category_uuid=uuid4(),
            category_name="category name",
            document_origin=None,
            document_category=None,
            document_trust_level=None,
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            commit_message=None,
        )
        attribute.event_service = mock.MagicMock()
        attribute.edit(
            {
                "id": 3,
                "name": "new_name",
                "attribute_type": "checkbox",
                "sensitive_field": True,
                "attribute_values": [
                    {"value": "a", "active": True, "sort_order": 0}
                ],
                "commit_message": "Attribute edited",
            }
        )

        assert attribute.id == 2
        assert attribute.name == "new_name"
        assert attribute.attribute_type == "option"
        assert attribute.sensitive_field is True
        assert attribute.attribute_values == [
            {"value": "a", "active": True, "sort_order": 0}
        ]
        assert attribute.commit_message == "Attribute edited"

    def test_create(self):
        attribute = Attribute(
            id=None,
            uuid=uuid4(),
            name=None,
            public_name=None,
            attribute_type=None,
            magic_string=None,
            sensitive_field=False,
            help=None,
            value_default=None,
            type_multiple=False,
            category_uuid=None,
            category_name=None,
            document_origin=None,
            document_category=None,
            document_trust_level=None,
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            commit_message=None,
        )
        attribute.event_service = mock.MagicMock()
        category_uuid = uuid4()
        attribute.create(
            fields={
                "id": 1,
                "name": "name",
                "public_name": "public name",
                "attribute_type": "option",
                "magic_string": "doc_varibale",
                "sensitive_field": True,
                "category_uuid": category_uuid,
                "attribute_values": [
                    {"value": "a", "active": True, "sort_order": 0}
                ],
                "commit_message": "Attribute created",
            }
        )

        assert attribute.id is None
        assert attribute.name == "name"
        assert attribute.public_name == "public name"
        assert attribute.sensitive_field is True
        assert attribute.category_uuid == category_uuid
        assert attribute.attribute_values == [
            {"value": "a", "active": True, "sort_order": 0}
        ]
        assert attribute.commit_message == "Attribute created"

    def test_create_of_type_adddress_v2(self):
        attribute = Attribute(
            id=None,
            uuid=uuid4(),
            name=None,
            public_name=None,
            attribute_type=None,
            magic_string=None,
            sensitive_field=False,
            help=None,
            value_default=None,
            type_multiple=False,
            category_uuid=None,
            category_name=None,
            document_origin=None,
            document_category=None,
            document_trust_level=None,
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            commit_message=None,
        )
        attribute.event_service = mock.MagicMock()
        category_uuid = uuid4()
        attribute.create(
            fields={
                "id": 1,
                "name": "name",
                "public_name": "public name",
                "attribute_type": "address_v2",
                "magic_string": "address_v2",
                "sensitive_field": True,
                "category_uuid": category_uuid,
                "attribute_values": [],
                "commit_message": "Attribute created",
            }
        )

        assert attribute.id is None
        assert attribute.name == "name"
        assert attribute.public_name == "public name"
        assert attribute.sensitive_field is True
        assert attribute.category_uuid == category_uuid
        assert attribute.attribute_values == []
        assert attribute.commit_message == "Attribute created"

    def test_create_missing_field(self):
        attribute = Attribute(
            id=None,
            uuid=uuid4(),
            name=None,
            public_name=None,
            attribute_type=None,
            magic_string=None,
            sensitive_field=False,
            help=None,
            value_default=None,
            type_multiple=False,
            category_uuid=None,
            category_name=None,
            document_origin=None,
            document_category=None,
            document_trust_level=None,
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            commit_message=None,
        )
        attribute.event_service = mock.MagicMock()
        category_uuid = uuid4()
        with pytest.raises(Conflict) as excinfo:
            attribute.create(
                fields={
                    "attribute_type": "text",
                    "magic_string": "doc_varibale",
                    "category_uuid": category_uuid,
                }
            )
        assert excinfo.value.args == (
            "'name' is a required field",
            "attribute/missing_required_field",
        )

    def test_create_missing_invalid_attribute_type(self):
        attribute = Attribute(
            id=None,
            uuid=uuid4(),
            name=None,
            public_name=None,
            attribute_type=None,
            magic_string=None,
            sensitive_field=False,
            help=None,
            value_default=None,
            type_multiple=False,
            category_uuid=None,
            category_name=None,
            document_origin=None,
            document_category=None,
            document_trust_level=None,
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            commit_message=None,
        )
        attribute.event_service = mock.MagicMock()
        category_uuid = uuid4()
        with pytest.raises(Conflict) as excinfo:
            attribute.create(
                fields={
                    "name": "name",
                    "attribute_type": "custom_type",
                    "magic_string": "doc_varibale",
                    "category_uuid": category_uuid,
                }
            )
        assert excinfo.value.args == (
            "Invalid attribute_type 'custom_type'",
            "attribute/invalid_attribute_type",
        )

    def test_edit_file(self):
        attribute = Attribute(
            id=2,
            uuid=uuid4(),
            name="name",
            public_name="public name",
            attribute_type="file",
            magic_string="magic string",
            sensitive_field=False,
            help=None,
            value_default=None,
            type_multiple=False,
            category_uuid=uuid4(),
            category_name="category name",
            document_origin="old_origin",
            document_category="old_category",
            document_trust_level="old_trust_level",
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            commit_message=None,
        )
        attribute.event_service = mock.MagicMock()
        attribute.edit(
            {
                "id": 3,
                "name": "new_name",
                "attribute_type": "checkbox",
                "document_origin": "origin",
                "document_trust_level": "trust_level",
                "document_category": "document_category",
                "attribute_values": [
                    {"value": "a", "active": True, "sort_order": 0}
                ],
            }
        )

        assert attribute.id == 2
        assert attribute.name == "new_name"
        assert attribute.attribute_type == "file"
        assert attribute.document_origin == "origin"
        assert attribute.document_category == "document_category"
        assert attribute.document_trust_level == "trust_level"
        assert attribute.attribute_values == []

    def test_create_file(self):
        attribute = Attribute(
            id=None,
            uuid=uuid4(),
            name=None,
            public_name=None,
            attribute_type=None,
            magic_string=None,
            sensitive_field=False,
            help=None,
            value_default=None,
            type_multiple=False,
            category_uuid=None,
            category_name=None,
            document_origin=None,
            document_category=None,
            document_trust_level=None,
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            commit_message=None,
        )
        attribute.event_service = mock.MagicMock()
        attribute.create(
            {
                "id": 3,
                "name": "new_name",
                "attribute_type": "file",
                "magic_string": "magic_string",
                "document_origin": "origin",
                "document_trust_level": "trust_level",
                "document_category": "document_category",
                "attribute_values": [
                    {"value": "a", "active": True, "sort_order": 0}
                ],
            }
        )

        assert attribute.id is None
        assert attribute.name == "new_name"
        assert attribute.attribute_type == "file"
        assert attribute.document_origin == "origin"
        assert attribute.document_category == "document_category"
        assert attribute.document_trust_level == "trust_level"
        assert attribute.attribute_values == []

    def test_edit_appointment(self):
        attribute = Attribute(
            id=2,
            uuid=uuid4(),
            name="name",
            public_name="public name",
            attribute_type="appointment",
            magic_string="magic string",
            sensitive_field=False,
            help=None,
            value_default=None,
            type_multiple=False,
            category_uuid=uuid4(),
            category_name="category name",
            document_origin=None,
            document_category=None,
            document_trust_level=None,
            attribute_values=[],
            appointment_location_id=1,
            appointment_product_id=1,
            appointment_interface_uuid="b31fb2ab-5262-49f2-a53a-eaf2d29a1f4f",
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            commit_message=None,
        )
        attribute.event_service = mock.MagicMock()
        appointment_interface_uuid = str(uuid4())
        attribute.edit(
            {
                "id": 3,
                "name": "new_name",
                "attribute_type": "checkbox",
                "appointment_location_id": 2,
                "appointment_product_id": 2,
                "appointment_interface_uuid": appointment_interface_uuid,
                "attribute_values": [],
            }
        )

        assert attribute.id == 2
        assert attribute.name == "new_name"
        assert attribute.attribute_type == "appointment"
        assert attribute.appointment_location_id == 2
        assert attribute.appointment_location_id == 2
        assert (
            attribute.appointment_interface_uuid == appointment_interface_uuid
        )
        assert attribute.attribute_values == []

    def test_create_appointment(self):
        attribute = Attribute(
            id=None,
            uuid=uuid4(),
            name=None,
            public_name=None,
            attribute_type=None,
            magic_string=None,
            sensitive_field=False,
            help=None,
            value_default=None,
            type_multiple=False,
            category_uuid=None,
            category_name=None,
            document_origin=None,
            document_category=None,
            document_trust_level=None,
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            commit_message=None,
        )
        attribute.event_service = mock.MagicMock()
        appointment_interface_uuid = str(uuid4())
        attribute.create(
            {
                "id": 3,
                "name": "new_name",
                "magic_string": "magic_string",
                "attribute_type": "appointment",
                "appointment_location_id": 1,
                "appointment_product_id": 1,
                "appointment_interface_uuid": appointment_interface_uuid,
                "attribute_values": [],
            }
        )

        assert attribute.id is None
        assert attribute.name == "new_name"
        assert attribute.magic_string == "magic_string"
        assert attribute.attribute_type == "appointment"
        assert attribute.appointment_location_id == 1
        assert attribute.appointment_location_id == 1
        assert (
            attribute.appointment_interface_uuid == appointment_interface_uuid
        )
        assert attribute.attribute_values == []

    def test_delete(self):
        attribute = Attribute(
            id=2,
            uuid=uuid4(),
            name="name",
            public_name="public name",
            attribute_type="file",
            magic_string="magic string",
            sensitive_field=False,
            help=None,
            value_default=None,
            type_multiple=False,
            category_uuid=uuid4(),
            category_name="category name",
            document_origin="old_origin",
            document_category="old_category",
            document_trust_level="old_trust_level",
            attribute_values=[],
            appointment_location_id=None,
            appointment_product_id=None,
            appointment_interface_uuid=None,
            relationship_type=None,
            relationship_name=None,
            relationship_uuid=None,
            commit_message=None,
        )
        attribute.event_service = mock.MagicMock()
        attribute.delete(reason="No more used")

        assert attribute.commit_message == "No more used"
        assert isinstance(attribute.deleted, datetime)
