# SPDX-FileCopyrightText: Mintlab B.V.
#
# SPDX-License-Identifier: EUPL-1.2

import pytest
import sqlalchemy
from minty.cqrs.events import Event, EventService
from minty.exceptions import NotFound
from unittest import mock
from uuid import uuid4
from zsnl_domains.admin.catalog.repositories.case_type_version import (
    CaseTypeVersionRepository,
    map_modified_components,
)
from zsnl_domains.admin.catalog.repositories.database_queries import (
    case_type_version,
    case_type_versions_history,
)


class TestCaseTypeVersionRepository:
    def setup_method(self):
        self.mock_infra = mock.MagicMock()
        event_service = EventService(
            correlation_id="correlation_id",
            domain="domain",
            context="context",
            user_uuid=uuid4(),
        )

        self.case_type_version_repo = CaseTypeVersionRepository(
            infrastructure_factory=self.mock_infra,
            context="devtest",
            event_service=event_service,
        )

    def test_get_session(self):
        assert (
            self.case_type_version_repo.session
            is self.mock_infra.get_infrastructure()
        )

    def test_get_case_type_versions_history_by_uuid_no_result(self):
        mock_ses = mock.MagicMock()
        self.case_type_version_repo.session = mock_ses

        uuid = str(uuid4())
        result = (
            self.case_type_version_repo.get_case_type_versions_history_by_uuid(
                uuid=uuid
            )
        )

        assert result == []

    def test_get_case_type_version_by_uuid_no_result(self):
        mock_ses = mock.MagicMock()
        self.case_type_version_repo.session = mock_ses

        mock_ses.execute().fetchone.side_effect = [None]
        uuid = str(uuid4())
        with pytest.raises(NotFound) as excinfo:
            self.case_type_version_repo.get_case_type_version_by_uuid(
                uuid=uuid
            )
        assert excinfo.value.args == (
            f"Case type version with uuid {uuid} not found",
            "case_type_version/not_found",
        )

    def test_get_case_type_versions_history_by_uuid(self):
        uuid = str(uuid4())
        mock_ses = mock.MagicMock()
        self.case_type_version_repo.session = mock_ses

        mock_ses.query().filter().filter().one.return_value = mock.MagicMock()
        self.case_type_version_repo.get_case_type_versions_history_by_uuid(
            uuid=uuid
        )

    def test_case_type_versions_history(self):
        res = case_type_versions_history(str(uuid4()))

        assert isinstance(res, sqlalchemy.sql.expression.SelectBase)
        assert res.subquery().columns.keys() == [
            "id",
            "uuid",
            "name",
            "created",
            "last_modified",
            "case_type_uuid",
            "version",
            "active",
            "username",
            "display_name",
            "reason",
            "event_data",
        ]

    def test_case_type_version(self):
        res = case_type_version(str(uuid4()))

        assert isinstance(res, sqlalchemy.sql.expression.SelectBase)
        assert res.subquery().columns.keys() == [
            "id",
            "uuid",
            "name",
            "created",
            "last_modified",
            "version",
            "active",
            "case_type_uuid",
        ]

    def test_save(self):
        uuid = str(uuid4())
        self.case_type_version_repo.cache[uuid] = mock.MagicMock()
        event_service = mock.MagicMock()
        event = Event(
            uuid=uuid4(),
            created_date="2019-04-19",
            correlation_id="req-12345",
            domain="admin",
            context="localhost",
            user_uuid=uuid4(),
            entity_type="CaseTypeVersionEntity",
            entity_id=uuid,
            event_name="CaseTypeActiveVersionChanged",
            changes=[
                {"key": "active", "old_value": False, "new_value": True},
                {"key": "name", "old_value": False, "new_value": "new name"},
                {
                    "key": "reason",
                    "old_value": False,
                    "new_value": "new reason",
                },
            ],
            entity_data={},
        )
        event_service.event_list = [event]
        self.case_type_version_repo.event_service = event_service
        with pytest.raises(NotImplementedError):
            self.case_type_version_repo.save()

        # assert self.case_type_version_repo.cache[uuid].active is True
        # assert self.case_type_version_repo.cache[uuid].name == "new name"
        # assert self.case_type_version_repo.cache[uuid].reason == "new reason"

    def test_get_case_type_versions_history_by_uuid_data(self):
        mock_ses = mock.MagicMock()
        self.case_type_version_repo.session = mock_ses
        mock_entity = mock.MagicMock()
        mock_ses.query().filter().filter().one.return_value = mock_entity

        uuid = str(uuid4())

        mock_sqla_case_type_version = mock.MagicMock()
        mock_sqla_case_type_version.uuid = uuid
        mock_sqla_case_type_version.id = 5
        mock_sqla_case_type_version.name = "test"
        mock_sqla_case_type_version.username = "test"
        mock_sqla_case_type_version.display_name = "test"
        mock_sqla_case_type_version.active = True
        mock_sqla_case_type_version.version = 5
        mock_sqla_case_type_version.created = 5
        mock_sqla_case_type_version.last_modified = 5
        mock_sqla_case_type_version.event_data = (
            '{"case_type":1,"commit_message":" via moederzaaktypetest. '
            'Onderdelen overgenomen: Relaties, Acties, Rechten.","components":null,"title":"Test type"}'
        )

        mock_ses.execute().fetchall.return_value = [
            mock_sqla_case_type_version
        ]

        result = (
            self.case_type_version_repo.get_case_type_versions_history_by_uuid(
                uuid=uuid
            )
        )

        # assert result[0].case_type_uuid == uuid
        assert result[0].name == "test"
        assert result[0].active is True
        assert result[0].version == 5

    def test_get_case_type_version_by_uuid(self):
        mock_ses = mock.MagicMock()
        self.case_type_version_repo.session = mock_ses
        mock_entity = mock.MagicMock()
        mock_ses.execute().fetchone.return_value = mock_entity

        uuid = str(uuid4())
        case_type_uuid = str(uuid4())
        mock_sqla_case_type_version = mock.MagicMock()
        mock_sqla_case_type_version.uuid = uuid
        mock_sqla_case_type_version.case_type_uuid = case_type_uuid
        mock_sqla_case_type_version.id = 5
        mock_sqla_case_type_version.name = "test"
        mock_sqla_case_type_version.active = True
        mock_sqla_case_type_version.version = 5
        mock_sqla_case_type_version.created = 5
        mock_sqla_case_type_version.last_modified = 5

        mock_ses.execute().fetchone.return_value = mock_sqla_case_type_version

        result = self.case_type_version_repo.get_case_type_version_by_uuid(
            uuid=uuid
        )

        assert result.uuid == uuid
        assert result.id == 5
        assert result.case_type_uuid == case_type_uuid
        assert result.name == "test"
        assert result.active is True
        assert result.version == 5

    def test_map_modified_components(self):
        active_components = [
            "basisattributen",
            "zaakacties",
            "kenmerken",
            "tekstblokken",
            "betrokkenen",
            "sjablonen",
            "autorisatie",
            "kinderen",
        ]
        result = map_modified_components(active_components)
        assert result["basic_properties"] is True
        assert result["case_actions"] is True
        assert result["phases_and_assignment"] is False
        assert result["custom_fields"] is True
        assert result["text_fields"] is True
        assert result["rules"] is False
        assert result["related_subjects"] is True
        assert result["templates"] is True
        assert result["checklists"] is False
        assert result["authorisations"] is True
        assert result["children"] is True
